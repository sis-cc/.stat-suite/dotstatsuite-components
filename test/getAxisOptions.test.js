import { expect } from 'chai';
import * as R from 'ramda';
import { getAxisOptions } from '../src/rules/src/chart/getAxisOptions';

describe('getAxisOptions tests', () => {
  it('blank test', () => {
    expect(getAxisOptions(null, null, null)).to.deep.equal({
      x: {
        linear: { pivot: { color: 'white' } },
        ordinal: { gap: .3, padding: .3 },
        thickness: 0
      },
      y: {
        linear: { pivot: { color: 'white' } },
        ordinal: { gap: .3, padding: .3 },
        thickness: 0
      }
    });
  });
  it('default bar options', () => {
    expect(
      R.pipe(
        R.dissocPath(['x', 'format', 'proc']),
        R.dissocPath(['y', 'format', 'proc']),
      )(getAxisOptions(null, 'BarChart', null))).to.deep.equal({
      x: {
        format: {},
        linear: { pivot: { color: 'white' } },
        ordinal: { gap: .3, padding: .3 },
        thickness: 0,
        tick: { thickness: 0, size: 0 }
      },
      y: {
        font: { baseline: 'ideographic' },
        format: {},
        linear: { pivot: { color: 'white', value: 0 } },
        ordinal: { gap: .3, padding: .3 },
        padding: 10,
        thickness: 0,
        tick: { thickness: 0 }
      }
    });
  });
  it('default bar procs options', () => {
    const options = getAxisOptions(null, 'BarChart', null);
    const xProc = R.path(['x', 'format', 'proc'], options);
    const yProc = R.path(['y', 'format', 'proc'], options);
    expect({ x: xProc('ordinal'), y: yProc(321.573) }).to.deep.equal({ x: 'ordinal', y: '321.57' });
  });
  it('default row options', () => {
    expect(
      R.pipe(
        R.dissocPath(['x', 'format', 'proc']),
        R.dissocPath(['y', 'format', 'proc']),
      )(getAxisOptions(null, 'RowChart', null))).to.deep.equal({
      x: {
        format: {},
        linear: { pivot: { color: 'white', value: 0 } },
        ordinal: { gap: .3, padding: .3 },
        orient: 'top',
        padding: 10,
        thickness: 0,
        tick: { thickness: 0 }
      },
      y: {
        format: {},
        linear: { pivot: { color: 'white' } },
        ordinal: { gap: .3, padding: .3, minDisplaySize: 300 },
        thickness: 0,
        tick: { thickness: 0, size: 0 }
      }
    });
  });
  it('default row procs options', () => {
    const options = getAxisOptions(null, 'RowChart', null);
    const xProc = R.path(['x', 'format', 'proc'], options);
    const yProc = R.path(['y', 'format', 'proc'], options);
    expect({ y: yProc('ordinal'), x: xProc(321.573) }).to.deep.equal({ y: 'ordinal', x: '321.57' });
  });
  it('default scatter options', () => {
    expect(
      R.pipe(
        R.dissocPath(['x', 'format', 'proc']),
        R.dissocPath(['y', 'format', 'proc']),
      )(getAxisOptions(null, 'ScatterChart', null))).to.deep.equal({
      x: {
        format: {},
        linear: { pivot: { color: 'white' } },
        ordinal: { gap: .3, padding: .3 },
        thickness: 0,
        tick: { thickness: 0, size: 0 }
      },
      y: {
        font: { baseline: 'ideographic' },
        format: {},
        linear: { pivot: { color: 'white' } },
        ordinal: { gap: .3, padding: .3 },
        padding: 10,
        thickness: 0,
        tick: { thickness: 0 }
      }
    });
  });
  it('default scatter procs options', () => {
    const options = getAxisOptions(null, 'ScatterChart', null);
    const xProc = R.path(['x', 'format', 'proc'], options);
    const yProc = R.path(['y', 'format', 'proc'], options);
    expect({ x: xProc(29.686), y: yProc(321.573) }).to.deep.equal({ x: '29.69', y: '321.57' });
  });
  it('default annual timeline options', () => {
    expect(
      R.pipe(
        R.dissocPath(['x', 'format', 'proc']),
        R.dissocPath(['y', 'format', 'proc']),
      )(getAxisOptions({ frequency: 'A' }, 'TimelineChart', null, null))).to.deep.equal({
      x: {
        format: { isTime: true, pattern: null },
        linear: {
          frequency: 'year',
          step: 1,
          pivot: { color: 'white' }
        },
        ordinal: { gap: .3, padding: .3 },
        thickness: 0,
        tick: { size: -6, minorSize: -3, thickness: 2, minorThickness: 2, color: 'white', step: 1 }
      },
      y: {
        font: { baseline: 'ideographic' },
        format: {},
        linear: { pivot: { color: 'white' } },
        ordinal: { gap: .3, padding: .3 },
        padding: 10,
        thickness: 0,
        tick: { thickness: 0 }
      }
    });
  });
  it('default quarterly timeline options', () => {
    expect(
      R.pipe(
        R.dissocPath(['x', 'format', 'proc']),
        R.dissocPath(['y', 'format', 'proc']),
      )(getAxisOptions({ frequency: 'Q' }, 'TimelineChart', null, null))).to.deep.equal({
      x: {
        format: { isTime: true, pattern: null },
        linear: {
          frequency: 'month',
          step: 3,
          pivot: { color: 'white' }
        },
        ordinal: { gap: .3, padding: .3 },
        thickness: 0,
        tick: { size: -6, minorSize: -3, thickness: 2, minorThickness: 2, color: 'white', step: 3 }
      },
      y: {
        font: { baseline: 'ideographic' },
        format: {},
        linear: { pivot: { color: 'white' } },
        ordinal: { gap: .3, padding: .3 },
        padding: 10,
        thickness: 0,
        tick: { thickness: 0 }
      }
    });
  });
  it('default monthly timeline options', () => {
    expect(
      R.pipe(
        R.dissocPath(['x', 'format', 'proc']),
        R.dissocPath(['y', 'format', 'proc']),
      )(getAxisOptions({ frequency: 'M' }, 'TimelineChart', null, null))).to.deep.equal({
      x: {
        format: { isTime: true, pattern: null },
        linear: {
          frequency: 'month',
          step: 1,
          pivot: { color: 'white' }
        },
        ordinal: { gap: .3, padding: .3 },
        thickness: 0,
        tick: { size: -6, minorSize: -3, thickness: 2, minorThickness: 2, color: 'white', step: 1 }
      },
      y: {
        font: { baseline: 'ideographic' },
        format: {},
        linear: { pivot: { color: 'white' } },
        ordinal: { gap: .3, padding: .3 },
        padding: 10,
        thickness: 0,
        tick: { thickness: 0 }
      }
    });
  });
  it('default quarterly timeline procs options', () => {
    const options = getAxisOptions({ frequency: 'Q' }, 'TimelineChart', null, null);
    const xProc = R.path(['x', 'format', 'proc'], options);
    const yProc = R.path(['y', 'format', 'proc'], options);
    expect({ x: xProc(new Date(2021, 5, 1)), y: yProc(321.573) }).to.deep.equal({ x: '2021-Q2', y: '321.57' });
  });
  it('default horizontal symbol options', () => {
    expect(
      R.pipe(
        R.dissocPath(['x', 'format', 'proc']),
        R.dissocPath(['y', 'format', 'proc']),
      )(getAxisOptions(null, 'HorizontalSymbolChart', null))).to.deep.equal({
      x: {
        format: {},
        linear: { pivot: { color: 'white' } },
        ordinal: { gap: .3, padding: .3 },
        orient: 'top',
        padding: 10,
        thickness: 0,
        tick: { thickness: 0 }
      },
      y: {
        format: {},
        linear: { pivot: { color: 'white' } },
        ordinal: { gap: .3, padding: .3, minDisplaySize: 300 },
        thickness: 0,
        tick: { thickness: 0, size: 5 }
      }
    });
  });
  it('default horizontal symbol procs options', () => {
    const options = getAxisOptions(null, 'HorizontalSymbolChart', null);
    const xProc = R.path(['x', 'format', 'proc'], options);
    const yProc = R.path(['y', 'format', 'proc'], options);
    expect({ y: yProc('ordinal'), x: xProc(321.573) }).to.deep.equal({ y: 'ordinal', x: '321.57' });
  });
  it('default vertical symbol options', () => {
    expect(
      R.pipe(
        R.dissocPath(['x', 'format', 'proc']),
        R.dissocPath(['y', 'format', 'proc']),
      )(getAxisOptions(null, 'VerticalSymbolChart', null))).to.deep.equal({
      x: {
        format: {},
        linear: { pivot: { color: 'white' } },
        ordinal: { gap: .3, padding: .3 },
        thickness: 0,
        tick: { thickness: 0, size: 0 }
      },
      y: {
        font: { baseline: 'ideographic' },
        format: {},
        linear: { pivot: { color: 'white' } },
        ordinal: { gap: .3, padding: .3 },
        padding: 10,
        thickness: 0,
        tick: { thickness: 0 }
      }
    });
  });
  it('default vertical symbol procs options', () => {
    const options = getAxisOptions(null, 'VerticalSymbolChart', null);
    const xProc = R.path(['x', 'format', 'proc'], options);
    const yProc = R.path(['y', 'format', 'proc'], options);
    expect({ x: xProc('ordinal'), y: yProc(321.573) }).to.deep.equal({ x: 'ordinal', y: '321.57' });
  });
  it('default stacked bar options', () => {
    expect(
      R.pipe(
        R.dissocPath(['x', 'format', 'proc']),
        R.dissocPath(['y', 'format', 'proc']),
      )(getAxisOptions(null, 'StackedBarChart', null))).to.deep.equal({
      x: {
        format: {},
        linear: { pivot: { color: 'white' } },
        ordinal: { gap: .3, padding: .3 },
        thickness: 0,
        tick: { thickness: 0, size: 0 }
      },
      y: {
        font: { baseline: 'ideographic' },
        format: {},
        linear: { pivot: { color: 'white', value: 0 } },
        ordinal: { gap: .3, padding: .3 },
        padding: 10,
        thickness: 0,
        tick: { thickness: 0 }
      }
    });
  });
  it('default stacked procs options', () => {
    const options = getAxisOptions(null, 'StackedBarChart', null);
    const xProc = R.path(['x', 'format', 'proc'], options);
    const yProc = R.path(['y', 'format', 'proc'], options);
    expect({ x: xProc('ordinal'), y: yProc(321.573) }).to.deep.equal({ x: 'ordinal', y: '321.57' });
  });
  it('customization test', () => {
    expect(getAxisOptions(null, null, {
      axis: {
        x: { ordinal: { prop: 'test' }, thickness: 5 },
        y: { linear: { pivot: { value: 10 } } }
      }
    })).to.deep.equal({
      x: {
        linear: { pivot: { color: 'white' } },
        ordinal: { gap: .3, padding: .3, prop: 'test' },
        thickness: 5
      },
      y: {
        linear: { pivot: { color: 'white', value: 10 } },
        ordinal: { gap: .3, padding: .3 },
        thickness: 0
      }
    });
  });
});
