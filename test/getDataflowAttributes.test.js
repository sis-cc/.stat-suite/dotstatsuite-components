import { expect } from 'chai';
import { getDataflowAttributes } from '../src/rules2/src/';

describe('getDataflowAttributes tests', () => {
  it('complete case', () => {
    const attributes = [
      { id: 'HEADER1', header: true, relationship: [], display: false, values: [{ id: 'v' }] },
      { id: 'HEADER2', header: true, relationship: [], values: [{ id: 'v', display: false }] },
      { id: 'HEADER3', header: true, relationship: [], values: [{ id: '_Z' }] },
      { id: 'HEADER4', header: true, relationship: [], values: [] },
      { id: 'HEADER5', header: true, relationship: [], values: [{ id: 'v' }] },
      { id: 'HEADER6', header: true, relationship: [], values: [{ id: 'v' }] },
      { id: 'HEADER7', header: true, relationship: ['DIM1'], values: [{ id: 'v' }] },
      { id: 'SERIE', series: true, relationship: [], values: [{ id: 'v' }] },
    ];

    const combinations = [{ id: 'COMB', concepts: ['DIM1', 'DIM2', 'HEADER5'] }];

    expect(getDataflowAttributes(attributes, combinations)).to.deep.equal({
      HEADER6: { id: 'HEADER6', value: { id: 'v' } }
    });
  });
});
