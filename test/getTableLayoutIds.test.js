import { expect } from 'chai';
import { injectCombinationsInLayout, getTableLayoutIds } from '../src/rules2/src/table/getTableLayoutIds';

describe('injectCombinationsInLayout', () => {
  it('no combinations', () => {
    const layoutIds = {
      header: ['D1'],
      sections: ['D2'],
      rows: ['D3']
    };
    const combinations = [];
    expect(injectCombinationsInLayout(combinations, layoutIds)).to.deep.equal({
      header: ['D1'],
      sections: ['D2'],
      rows: ['D3']
    });
  });
  it('partial start', () => {
    const layoutIds = {
      header: ['D1', 'C1', 'D2'],
      sections: ['C5', 'D5', 'D3'],
      rows: ['D4']
    };

    const combinations = [
      { id: 'C1', concepts: ['D0', 'A2'], relationship: ['D0'] }, // already in header
      { id: 'C2', concepts: ['D1', 'D2', 'A1'], relationship: ['D1', 'D2', 'D0'] }, // concepts are in header and relate to one of C1 concepts
      { id: 'C3', concepts: ['D1', 'D2', 'D3'], relationship: ['D1', 'D2', 'D3'] }, // concepts both in header & sections -> rejected
      { id: 'C4', concepts: ['D1', 'D2', 'D4'], relationship: ['D1', 'D2', 'D4'] }, // concepts both in header & rows -> rejected
      { id: 'C5', concepts: ['D6', 'D7'], relationship: ['D6', 'D7', 'D5'] }, // already in sections
      { id: 'C6', concepts: ['D3', 'D5'], relationship: ['D3', 'D5', 'D7'] }, // concepts in sections and relate to one C5 concept
      { id: 'C7', concepts: ['D3', 'D8'], relationship: ['D3', 'D8'] }, // partial concepts in sections, none in rows
      { id: 'C8', concepts: ['A1', 'A2', 'D9'], relationship: ['D0', 'D9'] }, // partial concepts in header nothing elsewhere
      { id: 'C9', concepts: ['A3', 'D10', 'D11', 'D12'], relationship: ['D10', 'D11', 'D12'] }, // all new concepts
      { id: 'C10', concepts: ['D10', 'D8'], relationship: ['D10', 'D8'] }, // relates to one concept of C7 and to one of C9
    ];

    expect(injectCombinationsInLayout(combinations, layoutIds)).to.deep.equal({
      header: ['C2', 'C1', 'C8'],
      sections: ['C5', 'C6', 'C7'],
      rows: ['D4', 'C9', 'C10']
    });
  });
});

describe('getTableLayoutIds tests', () => {
  it('simple case blank start', () => {
    const defaultLayoutIds = {
      header: [],
      sections: [],
      rows: []
    };
    const layoutIds = {
      header: [],
      sections: [],
      rows: []
    };
    const combinations = [];
    const attributes = [];
    const dimensions = [
      { id: 'REF_AREA' },
      { id: 'MEASURE' },
      { id: 'REF_SECTOR' },
      { id: 'TIME_PERIOD' },
    ];

    expect(getTableLayoutIds(defaultLayoutIds, layoutIds, dimensions, attributes, combinations)).to.deep.equal({
      header: ['TIME_PERIOD'],
      sections: ['MEASURE', 'REF_SECTOR'],
      rows: ['REF_AREA']
    });
  });
  it('blank start with combinations', () => {
    const defaultLayoutIds = {
      header: [],
      sections: [],
      rows: []
    };
    const layoutIds = {
      header: [],
      sections: ['COMB1'],
      rows: []
    };
    const combinations = [
      {
        id: 'COMB1',
        concepts: ['REF_SECTOR', 'MEASURE'],
      },
      {
        id: 'COMB2',
        concepts: ['UNIT_MEASURE', 'CURRENCY'],
      },
      {
        id: 'COMB3',
        concepts: ['ATTR1', 'ATTR2'],
      },
      {
        id: 'COMB4',
        concepts: ['ATTR3', 'ATTR4']
      }
    ];
    const dimensions = [
      { id: 'REF_AREA' },
      { id: 'REF_SECTOR' },
      { id: 'TRANSACTION' },
      { id: 'MEASURE' },
      { id: 'UNIT_MEASURE' },
      { id: 'CURRENCY' }
    ];
    const attributes = [
      { id: 'ATTR1', relationship: ['MEASURE'], series: true },
      { id: 'ATTR2', relationship: ['MEASURE'], series: true },
      { id: 'ATTR3', relationship: [], series: true },
      { id: 'ATTR4', relationship: [], series: true },
    ];

    expect(getTableLayoutIds(defaultLayoutIds, layoutIds, dimensions, attributes, combinations)).to.deep.equal({
      header: ['TRANSACTION'],
      sections: ['COMB1', 'COMB3'],
      rows: ['COMB2', 'REF_AREA']
    });
  });
  it('nothing in rows 2', () => {
    const combinations = [];
    const attributes = [];
    const dimensions = [
      { id: 'D1', header: true },
      { id: 'D2' },
      { id: 'D3', header: true }
    ];
    const layoutIds = {
      sections: ['D1'],
      header: ['D2'],
      rows: ['D3']
    };
    const defaultLayoutIds = {
      header: [],
      sections: [],
      rows: []
    };

    expect(getTableLayoutIds(defaultLayoutIds, layoutIds, dimensions, attributes, combinations)).to.deep.equal({
      header: [],
      sections: [],
      rows: ['D2']
    });
  });
  it('nothing in rows with combinations', () => {
    const combinations = [
      {
        id: 'COMB1',
        concepts: ['D1', 'D2']
      },
      {
        id: 'COMB2',
        concepts: ['A1', 'A2']
      },
    ];
    const attributes = [
      { id: 'A1', series: true, relationship: ['D1', 'D2'] },
      { id: 'A2', series: true, relationship: ['D1', 'D2'] },
    ];
    const dimensions = [
      { id: 'D1' },
      { id: 'D2' },
      { id: 'D3' }
    ];
    const layoutIds = {
      sections: ['D1', 'D2'],
      header: ['D3'],
      rows: []
    };
    const defaultLayoutIds = {
      header: [],
      sections: [],
      rows: []
    };

    expect(getTableLayoutIds(defaultLayoutIds, layoutIds, dimensions, attributes, combinations)).to.deep.equal({
      header: ['D3'],
      sections: [],
      rows: ['COMB1', 'COMB2']
    });
  });
  it('nothing in rows with combinations 2', () => {
    const combinations = [
      {
        id: 'COMB1',
        concepts: ['D1', 'D2']
      },
      {
        id: 'COMB2',
        concepts: ['A1', 'A2']
      },
    ];
    const attributes = [
      { id: 'A1', series: true, relationship: ['D1', 'D2'] },
      { id: 'A2', series: true, relationship: ['D1', 'D2'] },
    ];
    const dimensions = [
      { id: 'D1' },
      { id: 'D2' },
      { id: 'D3' },
      { id: 'D4' }
    ];
    const defaultLayoutIds = {
      header: [],
      sections: [],
      rows: []
    };
    const layoutIds1 = {
      sections: ['COMB1', 'D4', 'COMB2'],
      header: ['D3'],
      rows: []
    };
    const layoutIds2 = {
      sections: ['D4', 'COMB1', 'COMB2'],
      header: ['D3'],
      rows: []
    };
    const layoutIds3 = {
      sections: ['COMB2', 'COMB1', 'D4'],
      header: ['D3'],
      rows: []
    };
    const layoutIds4 = {
      sections: ['COMB1', 'COMB2'],
      header: ['D3'],
      rows: []
    };
    expect(getTableLayoutIds(defaultLayoutIds, layoutIds1, dimensions, attributes, combinations)).to.deep.equal({
      header: ['D3'],
      sections: ['D4'],
      rows: ['COMB1', 'COMB2']
    });
    expect(getTableLayoutIds(defaultLayoutIds, layoutIds2, dimensions, attributes, combinations)).to.deep.equal({
      header: ['D3'],
      sections: ['COMB1', 'COMB2'],
      rows: ['D4']
    });
    expect(getTableLayoutIds(defaultLayoutIds, layoutIds3, dimensions, attributes, combinations)).to.deep.equal({
      header: ['D3'],
      sections: ['D4'],
      rows: ['COMB2', 'COMB1']
    });
    expect(getTableLayoutIds(defaultLayoutIds, layoutIds4, dimensions, attributes, combinations)).to.deep.equal({
      header: ['D3'],
      sections: ['COMB1', 'COMB2'],
      rows: ['D4']
    });
  });
  it('nothing in rows with combination without dimensions', () => {
    const combinations = [{
      id: 'COMB',
      concepts: ['A1', 'A2']
    }];
    const attributes = [
      { id: 'A1', series: true, relationship: ['D1'] },
      { id: 'A2', series: true, relationship: ['D1'] },
    ]
    const dimensions = [
      { id: 'D1' },
      { id: 'D2' },
    ];
    const defaultLayoutIds = {
      header: [],
      sections: [],
      rows: []
    };
    const layoutIds = {
      header: ['D2'],
      sections: ['COMB', 'D1'],
      rows: ['D3']
    };
    expect(getTableLayoutIds(defaultLayoutIds, layoutIds, dimensions, attributes, combinations)).to.deep.equal({
      header: ['D2'],
      sections: [],
      rows: ['COMB', 'D1']
    });
  });
  it('combinations and default layout definition', () => {
    const defaultLayoutIds = {
      header: ['MEASURE', 'REF_AREA'],
      sections: ['COMB2', 'COMB3'],
      rows: []
    };
    const combinations = [
      {
        id: 'COMB1',
        concepts: ['REF_SECTOR', 'MEASURE'],
      },
      {
        id: 'COMB2',
        concepts: ['UNIT_MEASURE', 'CURRENCY'],
      },
      {
        id: 'COMB3',
        concepts: ['ATTR1', 'ATTR2'],
      },
      {
        id: 'COMB4',
        concepts: ['ATTR3', 'ATTR4']
      }
    ];
    const dimensions = [
      { id: 'REF_AREA' },
      { id: 'REF_SECTOR' },
      { id: 'TRANSACTION' },
      { id: 'MEASURE' },
      { id: 'UNIT_MEASURE' },
      { id: 'CURRENCY' }
    ];
    const attributes = [
      { id: 'ATTR1', relationship: ['MEASURE'], series: true },
      { id: 'ATTR2', relationship: ['MEASURE'], series: true },
      { id: 'ATTR3', relationship: [], series: true },
      { id: 'ATTR4', relationship: [], series: true },
    ];

    const layoutIds1 = {
      header: [],
      sections: [],
      rows: []
    };

    const layoutIds2 = {
      header: ['TRANSACTION'],
      sections: ['REF_AREA'],
      rows: []
    };

    const layoutIds3 = {
      header: [],
      sections: ['MEASURE'],
      rows: []
    }

    expect(getTableLayoutIds(defaultLayoutIds, layoutIds1, dimensions, attributes, combinations)).to.deep.equal({
      header: ['COMB1', 'REF_AREA', 'COMB3',],
      sections: ['COMB2'],
      rows: ['TRANSACTION']
    });

    expect(getTableLayoutIds(defaultLayoutIds, layoutIds2, dimensions, attributes, combinations)).to.deep.equal({
      header: ['TRANSACTION', 'COMB1', 'COMB3'],
      sections: ['COMB2'],
      rows: ['REF_AREA']
    });

    expect(getTableLayoutIds(defaultLayoutIds, layoutIds3, dimensions, attributes, combinations)).to.deep.equal({
      header: ['REF_AREA'],
      sections: ['COMB1', 'COMB2', 'COMB3'],
      rows: ['TRANSACTION']
    });
  });
});
