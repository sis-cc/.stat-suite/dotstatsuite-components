import { expect } from 'chai';
import { getTableLabelAccessor } from '../src/rules2/src/table/getTableLabelAccessor';

describe('getTableLabelAccessor tests', () => {
  const values = [
    { id: 'V0', name: 'Value 0', display: false },
    { id: 'V1', name: 'Value 1', missingParents: [{ id: 'P0', name: 'Parent 0' }, { id: 'P1', name: 'Parent 1', display: false }] },
    { id: '_T', name: 'Value 2' },
  ];
  it('combination label display', () => {
    expect(getTableLabelAccessor('label')(values)).to.deep.equal('Parent 0 > Value 1');
  });
  it('combination code display', () => {
    expect(getTableLabelAccessor('code')(values)).to.deep.equal('V0, V1, _T');
  });
  it('combination both display', () => {
    expect(getTableLabelAccessor('both')(values)).to.deep.equal('(V0, V1, _T) Parent 0 > Value 1');
  });
  it('single value witrh missingParents', () => {
    expect(getTableLabelAccessor('label')(values[1])).to.deep.equal('Parent 0 > Value 1');
  });
  it('not displayed single value', () => {
    expect(getTableLabelAccessor('both')(values[0])).to.deep.equal('(V0) Value 0');
  });
  it('display _T label if all hidden', () => {
    const _values = [
        { id: 'v0', name: 'value 0', display: false },
        { id: 'v1', name: 'value 1', display: false },
        { id: '_T', name: 'Total' },
        { id: 'v3', name: 'value 3', display: false },
    ];
    expect(getTableLabelAccessor('label')(_values)).to.deep.equal('Total');
    expect(getTableLabelAccessor('both')(_values)).to.deep.equal('(v0, v1, _T, v3) Total');
  });
  it('display label first value if all hidden and no _T', () => {
    const _values = [
        { id: 'v0', name: 'value 0', display: false },
        { id: 'v1', name: 'value 1', display: false },
        { id: 'v2', name: 'value 2', display: false },
        { id: 'v3', name: 'value 3', display: false },
    ];
    expect(getTableLabelAccessor('label')(_values)).to.deep.equal('value 0');
    expect(getTableLabelAccessor('both')(_values)).to.deep.equal('(v0, v1, v2, v3) value 0');
  });
  it('empty case', () => {
    expect(getTableLabelAccessor('label')([])).to.deep.equal('');
    expect(getTableLabelAccessor('code')([])).to.deep.equal('');
    expect(getTableLabelAccessor('both')([])).to.deep.equal('');
  });
});
