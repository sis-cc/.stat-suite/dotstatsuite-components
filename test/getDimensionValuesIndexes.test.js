import { expect } from 'chai';
import { getDimensionValuesIndexes } from '../src/rules2/src/getDimensionValuesIndexes';

describe('getDimensionValuesIndexes test', () => {
  it('basic test', () => {
    const values = [
      { id: 'OECD', parent: undefined, parents: [], __index: 2 },
      { id: 'GER', parent: 'OECD', parents: ['OECD'], __index: 1 },
      { id: 'FRA', parent: 'OECD', parents: ['OECD'], __index: 0 },
      { id: 'USA', parent: 'OECD', parents: ['OECD'], __index: 3 },
      { id: 'A', parent: undefined, parents: [], __index: 4 },
      { id: 'USA', parent: 'A', parents: ['A'], __index: 3 },
      { id: 'MEX', parent: 'A', parents: ['A'], __index: 7 },
      { id: 'EA', parent: undefined, parents: [], __index: 5 },
      { id: 'FRA', parent: 'EA', parents: ['EA'], __index: 0 },
      { id: 'GER', parent: 'EA', parents: ['EA'], __index: 1 },
      { id: 'CHI', parent: undefined, parents: [], __index: 6 }
    ];

    const indexes = {
      '0': [2, 8],
      '1': [1, 9],
      '2': [0],
      '3': [3, 5],
      '4': [4],
      '5': [7],
      '6': [10],
      '7': [6]
    };

    expect(getDimensionValuesIndexes(values)).to.deep.equal(indexes);
  });
});
