import { expect } from 'chai';
import { enhanceObservations } from '../src/rules2/src/enhanceObservations';
import numeral from 'numeral';

numeral.register('locale', 'eObs', { delimiters: { thousands: ",", decimal: "."  } });
describe('enhanceObservations tests', () => {
  it('blank test', () => {
    expect(enhanceObservations([], {}, [], {})).to.deep.equal({});
  });
  it('formatValue test', () => {
    numeral.locale('eObs');
    const attributes = [
      { id: 'A1', values: [] },
      { id: 'A2', values: [{ id: '-1' }, { id: '0' }, { id: '1' }, { id: '3' }, { id: 'Z' }] },
      { id: 'A3', values: [{ id: '0' }, { id: '2' }, { id: '3' }, { id: '5' }, { id: 'A' }] }
    ];

    const customAttributes = { prefscale: 'A2', decimals: 'A3' };

    const observations = {
      a: { value: 2, attrValuesIndexes: [null, null, null], dimValuesIndexes: [] },
      c: { value: 10, attrValuesIndexes: [null, 4, null], dimValuesIndexes: [] },
      d: { value: 5, attrValuesIndexes: [null, 1, null], dimValuesIndexes: [] },
      e: { value: 8.3, attrValuesIndexes: [null, 0, null], dimValuesIndexes: [] },
      f: { value: 25.62, attrValuesIndexes: [null, 3, null], dimValuesIndexes: [] },
      g: { value: 3.7, attrValuesIndexes: [null, null, 4], dimValuesIndexes: [] },
      h: { value: 21.1234567, attrValuesIndexes: [null, null, 2], dimValuesIndexes: [] },
      i: { value: 20.1, attrValuesIndexes: [null, null, 3], dimValuesIndexes: [] },
      j: { value: 19.9, attrValuesIndexes: [null, null, 0], dimValuesIndexes: [] },
      k: { value: 16.35, attrValuesIndexes: [null, 2, 1], dimValuesIndexes: [] },
      l: { value: 155062.56, attrValuesIndexes: [null, null, 2], dimValuesIndexes: [] },
      m: { value: 155062.56, attrValuesIndexes: [null, null, 0], dimValuesIndexes: [] },
      n: { value: null, attrValuesIndexes: [null, null, 1], dimValuesIndexes: [] },
    };

    expect(enhanceObservations([], observations, attributes, { customAttributes })).to.deep.equal({
      a: { attributes: {}, value: 2, formattedValue: '2', attrValuesIndexes: [null, null, null], dimValuesIndexes: [], indexedDimValIds: {} },
      c: { attributes: {}, value: 10, formattedValue: '10', attrValuesIndexes: [null, 4, null], dimValuesIndexes: [], indexedDimValIds: {} },
      d: { attributes: {}, value: 5, formattedValue: '5', attrValuesIndexes: [null, 1, null], dimValuesIndexes: [], indexedDimValIds: {} },
      e: { attributes: {}, value: 8.3, formattedValue: '83', attrValuesIndexes: [null, 0, null], dimValuesIndexes: [], indexedDimValIds: {} },
      f: { attributes: {}, value: 25.62, formattedValue: '0.02562', attrValuesIndexes: [null, 3, null], dimValuesIndexes: [], indexedDimValIds: {} },
      g: { attributes: {}, value: 3.7, formattedValue: '3.7', attrValuesIndexes: [null, null, 4], dimValuesIndexes: [], indexedDimValIds: {} },
      h: { attributes: {}, value: 21.1234567, formattedValue: '21.123', attrValuesIndexes: [null, null, 2], dimValuesIndexes: [], indexedDimValIds: {} },
      i: { attributes: {}, value: 20.1, formattedValue: '20.10000', attrValuesIndexes: [null, null, 3], dimValuesIndexes: [], indexedDimValIds: {} },
      j: { attributes: {}, value: 19.9, formattedValue: '20', attrValuesIndexes: [null, null, 0], dimValuesIndexes: [], indexedDimValIds: {} },
      k: { attributes: {}, value: 16.35, formattedValue: '1.64', attrValuesIndexes: [null, 2, 1], dimValuesIndexes: [], indexedDimValIds: {} },
      l: { attributes: {}, value: 155062.56, formattedValue: '155,062.560', attrValuesIndexes: [null, null, 2], dimValuesIndexes: [], indexedDimValIds: {} },
      m: { attributes: {}, value: 155062.56, formattedValue: '155,063', attrValuesIndexes: [null, null, 0], dimValuesIndexes: [], indexedDimValIds: {} },
      n: { attributes: {}, value: null, formattedValue: '..', attrValuesIndexes: [null, null, 1], dimValuesIndexes: [], indexedDimValIds: {} },
    });
  });
  it('attributes', () => {
    const attributes = [
      {
        id: 'a0',
        values: [{ id: 'v0' }, { id: 'v1' }, { id: 'v2' }],
        relationship: ['d0', 'd1'],
        series: true
      },
      {
        id: 'a1',
        values: [{ id: 'v0' }],
      },
      {
        id: 'a2',
        values: [{ id: 'v0' }, { id: 'v1' }],
        relationship: []
      },
      {
        id: 'a3',
        values: [{ id: 'v0' }, { id: 'v1' }],
        relationship: ['d2', 'd3'],
        series: true
      },
      {
        id: 'a4',
        values: [{ id: 'v0' }, { id: 'v1' }],
        relationship: ['d4'],
        series: true
      },
    ];

    const dimensions = [
      {
        id: 'd0',
        __index: 0,
        values: [
          { id: 'v0', __index: 0, __indexPosition: 0 },
          { id: 'v1', __index: 1, __indexPosition: 1 },
          { id: 'v2', __index: 2, __indexPosition: 2 },
          { id: 'v3', __index: 3, __indexPosition: 3, display: false }
        ]
      },
      {
        id: 'd1',
        __index: 1,
        values: [
          { id: 'v0', __index: 0, __indexPosition: 0 }
        ]
      },
      {
        id: 'd2',
        __index: 2,
        values: [
          { id: 'v0', __index: 0, __indexPosition: 0 }
        ]
      },
      {
        id: 'd3',
        __index: 3,
        values: [
          { id: 'v0', __index: 0, __indexPosition: 0 },
          { id: 'v1', __index: 1, __indexPosition: 1 }
        ]
      },
      {
        id: 'd4',
        __index: 4,
        values: [
          { id: 'v0', __index: 0, __indexPosition: 0 }
        ]
      }
    ];


    const observations = {
      a: {
        attrValuesIndexes: [0, 0, 0, 0, 0],
        dimValuesIndexes: [0, 0, 0, 0, 0],
        value: 22,
      },
      b: {
        attrValuesIndexes: [1, 0, 1, 1, 0],
        dimValuesIndexes: [0, 0, 0, 1, 0],
        value: 17,
      },
      c: {
        attrValuesIndexes: [2, 0, null, null, 0],
        dimValuesIndexes: [1, 0, 0, 0, 0],
        value: 55,
      },
      d: {
        attrValuesIndexes: [2, 0, null, null, null],
        dimValuesIndexes: [2, 0, 0, 0, 0],
        value: 35,
      },
      e: {
        attrValuesIndexes: [2, null, null, null, null],
        dimValuesIndexes: [3, 0, 0, 0, 0],
        value: 35,
      }
    };


    expect(enhanceObservations(dimensions, observations, attributes, {})).to.deep.equal({
      a: {
        attrValuesIndexes: [0, 0, 0, 0, 0],
        dimValuesIndexes: [0, 0, 0, 0, 0],
        value: 22,
        formattedValue: '22',
        attributes: {
          a0: { id: 'a0', relationship: ['d0', 'd1'], value: { id: 'v0', display: true }, serieKey: 'd0=v0:d1=v0', coordinates: { d0: 'v0', d1: 'v0' }, isObs: false },
          a3: { id: 'a3', relationship: ['d2', 'd3'], value: { id: 'v0', display: true }, serieKey: 'd2=v0:d3=v0', coordinates: { d2: 'v0', d3: 'v0' }, isObs: false },
          a4: { id: 'a4', relationship: ['d4'], value: { id: 'v0', display: true }, serieKey: 'd4=v0', coordinates: { d4: 'v0' }, isObs: false },
        },
        indexedDimValIds: { d0: 'v0', d1: 'v0', d2: 'v0', d3: 'v0', d4: 'v0' }
      },
      b: {
        attrValuesIndexes: [1, 0, 1, 1, 0],
        dimValuesIndexes: [0, 0, 0, 1, 0],
        value: 17,
        formattedValue: '17',
        attributes: {
          a0: { id: 'a0', relationship: ['d0', 'd1'], value: { id: 'v1', display: true }, serieKey: 'd0=v0:d1=v0', coordinates: { d0: 'v0', d1: 'v0' }, isObs: false },
          a3: { id: 'a3', relationship: ['d2', 'd3'], value: { id: 'v1', display: true }, serieKey: 'd2=v0:d3=v1', coordinates: { d2: 'v0', d3: 'v1' }, isObs: false },
          a4: { id: 'a4', relationship: ['d4'], value: { id: 'v0', display: true }, serieKey: 'd4=v0', coordinates: { d4: 'v0' }, isObs: false },
        },
        indexedDimValIds: { d0: 'v0', d1: 'v0', d2: 'v0', d3: 'v1', d4: 'v0' }
      },
      c: {
        attrValuesIndexes: [2, 0, null, null, 0],
        dimValuesIndexes: [1, 0, 0, 0, 0],
        value: 55,
        formattedValue: '55',
        attributes: {
          a0: { id: 'a0', relationship: ['d0', 'd1'], value: { id: 'v2', display: true }, serieKey: 'd0=v1:d1=v0', coordinates: { d0: 'v1', d1: 'v0' }, isObs: false },
          a3: { id: 'a3', relationship: ['d2', 'd3'], value: undefined, serieKey: 'd2=v0:d3=v0', coordinates: { d2: 'v0', d3: 'v0' }, isObs: false },
          a4: { id: 'a4', relationship: ['d4'], value: { id: 'v0', display: true }, serieKey: 'd4=v0', coordinates: { d4: 'v0' }, isObs: false },
        },
        indexedDimValIds: { d0: 'v1', d1: 'v0', d2: 'v0', d3: 'v0', d4: 'v0' }
      },
      d: {
        attrValuesIndexes: [2, 0, null, null, null],
        dimValuesIndexes: [2, 0, 0, 0, 0],
        value: 35,
        formattedValue: '35',
        attributes: {
          a0: { id: 'a0', relationship: ['d0', 'd1'], value: { id: 'v2', display: true }, serieKey: 'd0=v2:d1=v0', coordinates: { d0: 'v2', d1: 'v0' }, isObs: false },
          a3: { id: 'a3', relationship: ['d2', 'd3'], value: undefined, serieKey: 'd2=v0:d3=v0', coordinates: { d2: 'v0', d3: 'v0' }, isObs: false },
          a4: { id: 'a4', relationship: ['d4'], value: undefined, serieKey: 'd4=v0', coordinates: { d4: 'v0' }, isObs: false },
        },
        indexedDimValIds: { d0: 'v2', d1: 'v0', d2: 'v0', d3: 'v0', d4: 'v0' }
      },
      e: {
        attrValuesIndexes: [2, null, null, null, null],
        dimValuesIndexes: [3, 0, 0, 0, 0],
        value: 35,
        formattedValue: '35',
        attributes: {
          a0: { id: 'a0', relationship: ['d0', 'd1'], value: { id: 'v2', display: true }, serieKey: 'd0=v3:d1=v0', coordinates: { d0: 'v3', d1: 'v0' }, isObs: false },
          a3: { id: 'a3', relationship: ['d2', 'd3'], value: undefined, serieKey: 'd2=v0:d3=v0', coordinates: { d2: 'v0', d3: 'v0' }, isObs: false },
          a4: { id: 'a4', relationship: ['d4'], value: undefined, serieKey: 'd4=v0', coordinates: { d4: 'v0' }, isObs: false },
        },
        indexedDimValIds: { d0: 'v3', d1: 'v0', d2: 'v0', d3: 'v0', d4: 'v0' }
      }
    });
  });
  it('not displayed attributes and combinations', () => {
    const attributes = [
      { id: 'A1', display: false, series: true, values: [{ id: 'A1v' }], relationship: [] },
      { id: 'A2', series: true, values: [{ id: 'A2v', display: false }], relationship: [] },
      { id: 'A3', series: true, values: [{ id: '_T', display: true }], relationship: [] },
      { id: 'A4', display: false, series: true, values: [{ id: 'A4v' }], relationship: [], combined: true },
      { id: 'A5', series: true, values: [{ id: 'A5v', display: false }], relationship: [], combined: true },
      { id: 'A6', series: true, values: [{ id: '_T', display: true }], relationship: [], combined: true },
    ];

    const dimensions = [];

    const observations = {
      obs: {
        attrValuesIndexes: [0, 0, 0, 0, 0, 0],
        dimValuesIndexes: [],
        value: 22,
      }
    };

    expect(enhanceObservations(dimensions, observations, attributes, {})).to.deep.equal({
      obs: {
        attrValuesIndexes: [0, 0, 0, 0, 0, 0],
        dimValuesIndexes: [],
        value: 22,
        formattedValue: '22',
        attributes: {
          A1: { id: 'A1', display: false, value: null, relationship: [], serieKey: null, coordinates: {}, isObs: true },
          A2: { id: 'A2', value: null, relationship: [], serieKey: null, coordinates: {}, isObs: true },
          A3: { id: 'A3', value: null, relationship: [], serieKey: null, coordinates: {}, isObs: true },
          A4: { id: 'A4', display: false, value: null, relationship: [], serieKey: null, coordinates: {}, isObs: true, combined: true },
          A5: { id: 'A5', value: null, relationship: [], serieKey: null, coordinates: {}, isObs: true, combined: true },
          A6: { id: 'A6', value: null, relationship: [], serieKey: null, coordinates: {}, isObs: true, combined: true },
        },
        indexedDimValIds: {}
      }
    })
  });
});

