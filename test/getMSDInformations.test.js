import { expect } from 'chai';
import { getMSDInformations } from '../src/rules2/src/getMSDInformations';
import msd from './mocks/MSD_TEST.json';

describe('getMSDInformations tests', () => {
  it('complete test', () => {
    const expected = {
      attributes: {
        STRING_TYPE: { format: 'String', parent: '#ROOT', id: 'STRING_TYPE', name: 'STRING_TYPE en' },
        STRING_MULTILANG_TYPE: { format: 'String', parent: '#ROOT', id: 'STRING_MULTILANG_TYPE', name: undefined },
        ALPHANUMERIC_TYPE: { format: 'AlphaNumeric', parent: '#ROOT', id: 'ALPHANUMERIC_TYPE', name: undefined },
        ALPHANUMERIC_MULTILANG_TYPE: { format: 'AlphaNumeric', parent: '#ROOT', id: 'ALPHANUMERIC_MULTILANG_TYPE', name: undefined },
        BOOLEAN_TYPE: { format: 'Boolean', parent: '#ROOT', id: 'BOOLEAN_TYPE', name: undefined },
        XHTML_TYPE: { format: 'XHTML', parent: '#ROOT', id: 'XHTML_TYPE', name: undefined },
        XHTML_MULTILANG_TYPE: { format: 'XHTML', parent: '#ROOT', id: 'XHTML_MULTILANG_TYPE', name: undefined },
        INTEGER_TYPE: { format: 'Integer', parent: '#ROOT', id: 'INTEGER_TYPE', name: undefined },
        INTEGER_MULTILANG_TYPE: { format: 'Integer', parent: '#ROOT', id: 'INTEGER_MULTILANG_TYPE', name: undefined },
        DECIMAL_TYPE: { format: 'Decimal', parent: '#ROOT', id: 'DECIMAL_TYPE', name: undefined },
        DECIMAL_MULTILANG_TYPE: { format: 'Decimal', parent: '#ROOT', id: 'DECIMAL_MULTILANG_TYPE', name: undefined },
        DATETIME_TYPE: { format: 'DateTime', parent: '#ROOT', id: 'DATETIME_TYPE', name: undefined },
        TIME_TYPE: { format: 'Time', parent: '#ROOT', id: 'TIME_TYPE', name: undefined },
        GREGORIANDAY_TYPE: { format: 'GregorianDay', parent: '#ROOT', id: 'GREGORIANDAY_TYPE', name: undefined },
        GREGORIAN_YEAR_TYPE: { format: 'GregorianYear', parent: '#ROOT', id: 'GREGORIAN_YEAR_TYPE', name: undefined },
        GREGORIAN_YEARMONTH_TYPE: { format: 'GregorianYearMonth', parent: '#ROOT', id: 'GREGORIAN_YEARMONTH_TYPE', name: undefined },
        CONTACT: { format: 'String', parent: '#ROOT', id: 'CONTACT', name: undefined },
        'CONTACT.CONTACT_NAME': { format: 'String', parent: 'CONTACT', id: 'CONTACT_NAME', name: undefined },
        'CONTACT.CONTACT_EMAIL': { format: 'String', parent: 'CONTACT', id: 'CONTACT_EMAIL', name: undefined },
        'CONTACT.CONTACT_PHONE': { format: 'Numeric', parent: 'CONTACT', id: 'CONTACT_PHONE', name: undefined },
        'CONTACT.CONTACT_ORGANISATION': { format: 'String', parent: 'CONTACT', id: 'CONTACT_ORGANISATION', name: undefined }
      }
    };

    expect(getMSDInformations(msd)).to.deep.equal(expected);
  });
});
