import { expect } from 'chai';
import { getSeriesCombinations } from '../src/rules2/src/';

describe('getSeriesCombinations tests', () => {
  it('basic test', () => {
    const oneValueDimensions = [
      { id: 'D1', values: [{ id: 'v', display: false }] },
      { id: 'D2', values: [{ id: 'v' }] }
    ];
    const combinations = [
      { id: 'COMB1', header: true, concepts: ['D1', 'D2'] },
      { id: 'COMB2', series: true, concepts: ['D1', 'D2', 'A1', 'A2'] },
      { id: 'COMB3', series: true, concepts: ['D3', 'D4', 'A3'] },
    ];

    expect(getSeriesCombinations(combinations, oneValueDimensions)).to.deep.equal([
      {
        id: 'COMB2',
        series: true,
        concepts: ['D1', 'D2', 'A1', 'A2'],
        fixedDimValues: {
          D1: { id: 'v', display: false },
          D2: { id: 'v' }
        }
      },
      { id: 'COMB3', series: true, concepts: ['D3', 'D4', 'A3'], fixedDimValues: {} },
    ])
  });
});
