import { expect } from 'chai';
import { getDataflowTooltipAttributesIds } from '../src/rules2/src';

const def = {
  flags: ['f0', 'f1'],
  notes: ['foot0', 'foot1']
};

describe('getDataflowTooltipAttributesIds tests', () => {
  it('blank case', () => {
    expect(getDataflowTooltipAttributesIds({ data: undefined }, undefined))
      .to.deep.equal({ flags: [], notes: [] });
  });
  it('no override', () => {
    const data = {
      dataSets: [{
        annotations: [0, 1]
      }],
      structure: {
        annotations: [
          { id: 'annot0', title: 'test' },
          { id: 'annot1', title: 'test' },
          { id: 'annot2', title: 'test' },
        ]
      }
    };

    expect(getDataflowTooltipAttributesIds({ data }, def)).to.deep.equal(def);
  });
  it('flags override', () => {
    const data = {
      dataSets: [{
        annotations: [0, 1]
      }],
      structure: {
        annotations: [
          { id: 'annot0', title: 'test' },
          { id: 'annot1', type: 'LAYOUT_FLAG', title: 'f2,f3' },
          { id: 'annot2', title: 'test' },
        ]
      }
    };

    expect(getDataflowTooltipAttributesIds({ data }, def)).to.deep.equal({ flags: ['f2', 'f3'], notes: ['foot0', 'foot1'] });
  });
  it('footnotes override', () => {
    const data = {
      dataSets: [{
        annotations: [0, 1]
      }],
      structure: {
        annotations: [
          { id: 'annot0', title: 'test' },
          { id: 'annot1', type: 'LAYOUT_NOTE', title: 'foot2,foot3' },
          { id: 'annot2', title: 'test' },
        ]
      }
    };

    expect(getDataflowTooltipAttributesIds({ data }, def)).to.deep.equal({ flags: ['f0', 'f1'], notes: ['foot2', 'foot3'] });
  });
  it('flags and footnotes override', () => {
    const data = {
      dataSets: [{
        annotations: [0, 1]
      }],
      structure: {
        annotations: [
          { id: 'annot0', type: 'LAYOUT_FLAG', title: 'f2,f3' },
          { id: 'annot1', type: 'LAYOUT_NOTE', title: 'foot2,foot3' },
          { id: 'annot2', title: 'test' },
        ]
      }
    };

    expect(getDataflowTooltipAttributesIds({ data }, def)).to.deep.equal({ flags: ['f2', 'f3'], notes: ['foot2', 'foot3'] });
  });
  it('annotation without content erase defaults', () => {
    const data = {
      dataSets: [{
        annotations: [0, 1]
      }],
      structure: {
        annotations: [
          { id: 'annot0', type: 'LAYOUT_FLAG', title: 'f2,f3' },
          { id: 'annot1', type: 'LAYOUT_NOTE' },
          { id: 'annot2', title: 'test' },
        ]
      }
    };

    expect(getDataflowTooltipAttributesIds({ data }, def)).to.deep.equal({ flags: ['f2', 'f3'], notes: [] });
  });
  it('custom flags overrides default notes', () => {
    const defaultIds = {
      flags: [],
      notes: ['ATTR0', 'ATTR1']
    };
    const data = {
      dataSets: [{
        annotations: [0]
      }],
      structure: {
        annotations: [
          { id: 'annot0', type: 'LAYOUT_FLAG', title: 'ATTR0,ATTR1' },
        ]
      }
    };
    expect(getDataflowTooltipAttributesIds({ data }, defaultIds)).to.deep.equal({ flags: ['ATTR0', 'ATTR1'], notes: [] });
  });
  it('custom notes overrides default flags', () => {
    const defaultIds  = {
      flags: ['ATTR0', 'ATTR1'],
      notes: ['ATTR2', 'ATTR3']
    };
    const data = {
      dataSets: [{
        annotations: [0]
      }],
      structure: {
        annotations: [
          { id: 'annot0', type: 'LAYOUT_NOTE', title: 'ATTR1,ATTR2' },
        ]
      }
    };
    expect(getDataflowTooltipAttributesIds({ data }, defaultIds)).to.deep.equal({ flags: ['ATTR0'], notes: ['ATTR1', 'ATTR2'] });
  });
  it('custom flags overrides custom notes', () => {
    const defaultIds  = {
      flags: [],
      notes: []
    };
    const data = {
      dataSets: [{
        annotations: [0, 1]
      }],
      structure: {
        annotations: [
          { id: 'annot0', type: 'LAYOUT_FLAG', title: 'ATTR0,ATTR1' },
          { id: 'annot1', type: 'LAYOUT_NOTE', title: 'ATTR1,ATTR2' },
        ]
      }
    };
    expect(getDataflowTooltipAttributesIds({ data }, defaultIds)).to.deep.equal({ flags: ['ATTR0', 'ATTR1'], notes: ['ATTR2'] });
  });
});
