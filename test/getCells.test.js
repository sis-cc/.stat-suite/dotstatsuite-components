import { expect } from 'chai';
import { getCells } from '../src/rules2/src/';

const customAttributes = {
  flags: ['FL1', 'FL2'],
  notes: ['FT1', 'FT2', 'FT3']
};

describe('getCells tests', () => {
  it('flags', () => {
    expect(
      getCells(customAttributes, ['FL1', 'FL2', 'FT1', 'FT2', 'FT3'], [], {}, [])({
        a: {
          key: 'a',
          value: 33,
          formattedValue: '33',
          attributes: {
            FL1: { id: 'FL1', name: 'Flag 1', Value: null },
            FL2: { id: 'FL2', name: 'Flag 2', value: { id: 'FL2.1', name: 'Flag 2 Value 1' } },
            FT1: { id: 'FT1', name: 'Footnote 1', value: { id: 'FT1.1', name: 'Footnote 1 Value 1' } },
            FT3: { id: 'FT3', name: 'Footnote 3', value: { id: 'FT3.3', name: 'Footnote 3 Value 3' } },
          },
          indexedDimValIds: {}
        }
    })).to.deep.equal({
      a: {
        key: 'a',
        value: '33',
        intValue: 33,
        flags: [
          { code: 'FL2.1', id: 'FL2', name: 'Flag 2', value: { id: 'FL2.1', name: 'Flag 2 Value 1' } },
          { id: 'FT1', name: 'Footnote 1', value: { id: 'FT1.1', name: 'Footnote 1 Value 1' } },
          { id: 'FT3', name: 'Footnote 3', value: { id: 'FT3.3', name: 'Footnote 3 Value 3' } }
        ],
        indexedDimValIds: {},
        sideProps: null
      }
    })
  });
  it('combinations', () => {
    const combinations = {
      cells: [{ id: 'CELLS_COMB', concepts: ['D1', 'D2', 'A1', 'A4'], fixedDimValues: { D1: { id: 'D1V' } } }],
      layout: [{ id: 'LAYOUT_COMB', concepts: ['D3', 'A2', 'A3'] }]
    };

    const attributesSeries = {
      'D3=v0': { serieKey: 'D3=v0', A2: { value: 'A2V' } }
    };

    const cellsAttributesIds = ['FL1', 'FT1', 'A1'];

    const observations = {
      'obs': {
        formattedValue: '33',
        value: 33,
        attributes: {
          FL1: { id: 'FL1', value: { id: 'A' } },
          FT1: { id: 'FT1', value: { id: 'V' } },
          A1: { id: 'A1', value: { id: 'A1V' } },
          A2: { id: 'A2', value: { id: 'A2V' }, serieKey: 'D3=v0' },
          A3: { id: 'A3', value: { id: 'A3V' }, serieKey: 'D3=v0' },
        }
      }
    };


    expect(getCells(customAttributes, cellsAttributesIds, combinations, attributesSeries, [])(observations)).to.deep.equal({
      'obs': {
        value: '33',
        intValue: 33,
        sideProps: null,
        flags: [
          { code: 'A', id: 'FL1', value: { id: 'A' } }, // regular flag
          { id: 'FT1', value: { id: 'V' } }, // regular foot note
          { id: 'A3', value: { id: 'A3V' } }, // rejected layout combination value displayed as a footnote
          { id: 'CELLS_COMB', values: [{ id: 'D1V' }, { id: 'A1V' }] } // cell level combination with fixed dim value
        ]
      }
    });
  });
  it('duplication between notes and combinations definitions', () => {
    const _customAttributes = {
      flags: [],
      notes: ['A1', 'A2']
    };

    const combinations = {
      cells: [],
      layout: [{ id: 'LAYOUT_COMB', concepts: ['D1', 'A1'] }]
    };

    const attributesSeries = { 'D1=V0': {} };

    const cellsAttributesIds = ['A2'];

    const observations = {
      'obs': {
        value: 'val',
        formattedValue: 'val',
        attributes: {
          A1: { id: 'A1', value: { id: 'A1V' }, serieKey: 'D1=V0' },
          A2: { id: 'A2', value: { id: 'A2V' } },
        }
      }
    };
    expect(getCells(_customAttributes, cellsAttributesIds, combinations, attributesSeries, [])(observations)).to.deep.equal({
      'obs': {
        value: 'val',
        intValue: null,
        sideProps: null,
        flags: [
          { id: 'A1', value: { id: 'A1V' } },
          { id: 'A2', value: { id: 'A2V' } }
        ]
      }
    });
  });
  it('metadata and OBS_ATTRIBUTES', () => {
    const _customAttributes = {
      flags: [],
      notes: []
    };

    const combinations = {
      cells: [],
      layout: []
    };

    const observations = {
      'a': {
        value: 'val',
        formattedValue: 'val',
        attributes: {},
        indexedDimValIds: { d0: 'v0', d1: 'v0', OBS_ATTRIBUTES: 'OBS_VALUE' }
      },
      'b': {
        value: 'attr val',
        formattedValue: 'attr val',
        attributes: {},
        indexedDimValIds: { d0: 'v0', d1: 'v0', OBS_ATTRIBUTES: 'A' }
      },
      'c': {
        value: 'val',
        formattedValue: 'val',
        attributes: { ADV_ATTR: { id: 'ADV_ATTR', value: { id: 'v' } } },
        indexedDimValIds: { d0: 'v1', d1: 'v0', OBS_ATTRIBUTES: 'OBS_VALUE' }
      },
    };
    const metadataCoordinates = [{ d0: 'v0', d1: 'v0' }];
    expect(getCells(_customAttributes, {}, combinations, {}, metadataCoordinates)(observations)).to.deep.equal({
      'a': {
        value: 'val',
        intValue: null,
        indexedDimValIds: { d0: 'v0', d1: 'v0', OBS_ATTRIBUTES: 'OBS_VALUE' },
        sideProps: { advancedAttributes: {}, hasMetadata: true, coordinates: { d0: 'v0', d1: 'v0', OBS_ATTRIBUTES: 'OBS_VALUE' } },
        flags: []
      },
      'b': {
        value: 'attr val',
        intValue: null,
        indexedDimValIds: { d0: 'v0', d1: 'v0', OBS_ATTRIBUTES: 'A' },
        sideProps: null,
        flags: []
      },
      'c': {
        value: 'val',
        intValue: null,
        indexedDimValIds: { d0: 'v1', d1: 'v0', OBS_ATTRIBUTES: 'OBS_VALUE' },
        flags: [],
        sideProps: { 
          advancedAttributes: { ADV_ATTR: { id: 'ADV_ATTR', value: { id: 'v' } } },
          hasMetadata: false,
          coordinates: { d0: 'v1', d1: 'v0', OBS_ATTRIBUTES: 'OBS_VALUE' }
        },
      },
    });
  });
});
