import { expect } from 'chai';
import { getCellsMetadataCoordinates } from '../src/rules2/src/';

describe('getCellsMetadataCoordinates tests', () => {
  it('basic tests', () => {
    const oneValueDimensions = [
      { id: 'HEADER1' },
      { id: 'HEADER2' },
    ];

    const layoutIds = {
      header: ['COLUMN1', 'COLUMN2'],
      sections: ['SECTION1', 'SECTION2'],
      rows: ['ROW1', 'ROW2'],
    };

    const coordinates = [
      {},
      { HEADER1: 'v' },
      { HEADER1: 'v', HEADER2: 'v' },
      { COLUMN1: 'v' },
      { COLUMN1: 'v', COLUMN2: 'v' },
      { HEADER1: 'v', COLUMN1: 'v', COLUMN2: 'v' },
      { SECTION1: 'v' },
      { SECTION1: 'v', SECTION2: 'v' },
      { HEADER1: 'v', SECTION1: 'v', SECTION2: 'v' },
      { ROW1: 'v' },
      { ROW1: 'v', ROW2: 'v' },
      { HEADER1: 'v', ROW1: 'v', ROW2: 'v' },
      { HEADER1: 'v', SECTION1: 'v', SECTION2: 'v', ROW1: 'v', ROW2: 'v' },
      { ROW1: 'v', HEADER2: 'v', COLUMN2: 'v' },
      { SECTION1: 'v', ROW2: 'v', COLUMN1: 'v' }
    ];

    expect(getCellsMetadataCoordinates(coordinates, oneValueDimensions, layoutIds)).to.deep.equal([
      { ROW1: 'v', HEADER2: 'v', COLUMN2: 'v' },
      { SECTION1: 'v', ROW2: 'v', COLUMN1: 'v' }
    ]);
  });
});
