import { expect } from 'chai';
import { getNotDisplayedIds } from '../src/rules2/src/getNotDisplayedIds';

describe('getNotDisplayedIds tests', () => {
  it('not provided case', () => {
    const annotations = [
      { type: 'test0' },
      { type: 'test1' },
      { type: 'NOT_DISPLAYED', title: '' },
      { type: 'test2' },
    ];
    expect(getNotDisplayedIds(annotations)).to.deep.equal({ hiddenValues: {}, hiddenCombinations: {} });
  });
  it('complete case', () => {
    const annotations = [
      { type: 'test0' },
      { type: 'test1' },
      { type: 'NOT_DISPLAYED', title: 'd0,d1=,d2=v0,d3=v0+,d4=v0+v1,d5=(v0+v1)+v2,d6=(v0+v1),d7=(v0+v1,d8=v0+v1),d9=()' },
      { type: 'test2' },
    ];
    const expected = {
      hiddenValues: {
        d0: 'd0',
        d1: 'd1',
        'd2.v0': 'd2.v0',
        'd3.v0': 'd3.v0',
        'd4.v0': 'd4.v0',
        'd4.v1': 'd4.v1',
        'd5.v0': 'd5.v0',
        'd5.v1': 'd5.v1',
        'd5.v2': 'd5.v2',
        'd6.v0': 'd6.v0',
        'd6.v1': 'd6.v1',
        'd7.v0': 'd7.v0',
        'd7.v1': 'd7.v1',
        'd8.v0': 'd8.v0',
        'd8.v1': 'd8.v1',
        d9: 'd9',
      },
      hiddenCombinations: {
        d5: ['v0', 'v1'],
        d6: ['v0', 'v1'],
      }
    };
    expect(getNotDisplayedIds(annotations)).to.deep.equal(expected);
  });
})
