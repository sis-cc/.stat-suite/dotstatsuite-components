import { expect } from 'chai';
import { parseMetadataSeries } from '../src/rules2/src/parseMetadataSeries';
import metadataMock from './mocks/OECD_SNA_TABLE1_1.0_-_AUS_V_metadata.json';

const dimensions = [
  {
    id: "LOCATION",
    values: [{ id: "AUS", __index: 0 }]
  },
  {
    id: "TRANSACT",
  },
  {
    id: "MEASURE", values: [{ id: "V", __index: 0 }]
  },
  {
    id: "TIME_PERIOD"
  }
];

const attributes = { 
  DIRECT_SOURCE: { id: 'DIRECT_SOURCE', prop: 'test', parent: '#ROOT', name: 'DIRECT_SOURCE'  },
  KEY_STATISTICAL_CONCEPT: { id: 'KEY_STATISTICAL_CONCEPT', parent: '#ROOT', name: 'KEY_STATISTICAL_CONCEPT'  },
  UNIT_OF_MEASURE_USED: { id: 'UNIT_OF_MEASURE_USED', parent: '#ROOT', name: 'UNIT_OF_MEASURE_USED'  },
  POWER_CODE: { id: 'POWER_CODE', parent: '#ROOT', name: 'POWER_CODE'  },
  POREFERENCE_PERIODWER_CODE: { id: 'POREFERENCE_PERIODWER_CODE', parent: '#ROOT', name: 'POREFERENCE_PERIODWER_CODE'  },
  RANDOM: { prop: 'invalid', name: 'RANDOM'  }
}

describe('parseMetadataSeries test', () => {
  it('complete case', () => {
    expect(parseMetadataSeries(metadataMock, { display: 'both', locale: 'en', dimensions, attributes })).to.deep.equal({
      '0:::': [
        {
          id: 'DIRECT_SOURCE',
          label: 'DIRECT_SOURCE: Direct source',
          value: "Reply to the National Accounts Questionnaire from the Australian Bureau of Statistics, Canberra. The official estimates are published in 'Australian system of national accounts', ABS, catalogue 5204.0.",
          handlerProps: { prop: 'test', parent: '#ROOT', name: 'DIRECT_SOURCE' },
        },
        {
          id: 'KEY_STATISTICAL_CONCEPT',
          label: 'KEY_STATISTICAL_CONCEPT: Key statistical concept',
          value: "As from December 2009, national accounts estimates are compiled according to the 2008 SNA ('System of National Accounts 2008', Commission of the European Communities-Eurostat, International Monetary Fund, Organisation for Economic Co-operation and Development, United Nations and World Bank, 2009).<br><br>Detailed information about the implementation of the 2008 SNA in Australian national accounts could be found in ABS website: <br><br><a href=\"http://www.abs.gov.au/ausstats/abs@.nsf/mf/5310.0.55.002\">ABS website</a><br><br>All the data refer to fiscal years beginning on the 1st July of the year indicated.<br><br>The financial intermediation services indirectly measured (FISIM) are allocated to industries and institutional sectors. <br><br>Original chain constant price estimates for main aggregates are referenced to fiscal year 2019-2020.",
          handlerProps: { parent: '#ROOT', name: 'KEY_STATISTICAL_CONCEPT' },
        }
      ],
      '0::0:': [
        {
          id: 'UNIT_OF_MEASURE_USED',
          label: 'UNIT_OF_MEASURE_USED: Unit of measure used',
          value: 'Australian Dollar',
          handlerProps: {  parent: '#ROOT', name: 'UNIT_OF_MEASURE_USED' },
        },
        {
          id: 'POWER_CODE',
          label: 'POWER_CODE: Power code',
          value: 6,
          handlerProps: { parent: '#ROOT', name: 'POWER_CODE' },
        },
        {
          id: 'POREFERENCE_PERIODWER_CODE',
          label: 'POREFERENCE_PERIODWER_CODE: Reference period',
          value: '2019',
          handlerProps: { parent: '#ROOT', name: 'POREFERENCE_PERIODWER_CODE' },
        }
      ]
    });
  });
  it('hierarchy test', () => {
    const attributes = {
      ROOT1: { id: 'ROOT1', parent: '#ROOT', name: 'ROOT1'  },
      ROOT2: { id: 'ROOT2', parent: '#ROOT', name: 'ROOT2'  },
      PARENT1: { id: 'PARENT1', parent: '#ROOT', name: 'PARENT1'  },
      'PARENT1.CHILD1': { id: 'CHILD1', parent: 'PARENT1', name: 'CHILD1'  },
      'PARENT1.CHILD2': { id: 'CHILD2', parent: 'PARENT1', name: 'CHILD2'  },
      PARENT2: { id: 'PARENT1', parent: '#ROOT', name: 'PARENT2'  },
      'PARENT2.CHILD3': { id: 'CHILD3', parent: 'PARENT2', name: 'CHILD3'  },
      'PARENT2.CHILD4': { id: 'CHILD4', parent: 'PARENT2', name: 'CHILD4'  },
    };
    const metadata = {
      data: {
        dataSets: [{
          dimensionGroupAttributes: {
            '~:~:~': [0, 0, null, 0, 0, 0, 0]
          }
        }],
        structures: [{
          attributes: {
            dimensionGroup: [
              { id: 'ROOT1', name: 'Root 1', values: [{ value: 'v' }] },
              { id: 'ROOT2', name: 'Root 2', values: [{ value: 'v' }] },
              { id: 'PARENT1', name: 'Parent 1', isPresentational: true, values: [] },
              { id: 'PARENT1.CHILD1', name: 'Child 1',  values: [{ value: 'v' }] },
              { id: 'PARENT1.CHILD2', name: 'Child 2',  values: [{ value: 'v' }] },
              { id: 'PARENT2.CHILD3', name: 'Child 3',  values: [{ value: 'v' }] },
              { id: 'PARENT2.CHILD4', name: 'Child 4',  values: [{ value: 'v' }] },
            ]
          }
        }]
      }
    };

    expect(parseMetadataSeries(metadata, { display: 'name', locale: 'en', dimensions: [], attributes })).to.deep.equal({
      '::': [
        { id: 'ROOT1', label: 'Root 1', value: 'v', handlerProps: { parent: '#ROOT', name: 'ROOT1' } },
        { id: 'ROOT2', label: 'Root 2', value: 'v', handlerProps: { parent: '#ROOT', name: 'ROOT2' } },
        {
          id: 'PARENT1',
          label: 'Parent 1',
          handlerProps: { parent: '#ROOT', name: 'PARENT1' },
          children: [
            { id: 'PARENT1.CHILD1', label: 'Child 1', value: 'v', handlerProps: { parent: 'PARENT1', name: 'CHILD1' } },
            { id: 'PARENT1.CHILD2', label: 'Child 2', value: 'v', handlerProps: { parent: 'PARENT1', name: 'CHILD2' } },
          ]
        },
        {
          id: 'PARENT2',
          label: 'PARENT2',
          handlerProps: { parent: '#ROOT', name: 'PARENT2' },
          children: [
            { id: 'PARENT2.CHILD3', label: 'Child 3', value: 'v', handlerProps: { parent: 'PARENT2', name: 'CHILD3' } },
            { id: 'PARENT2.CHILD4', label: 'Child 4', value: 'v', handlerProps: { parent: 'PARENT2', name: 'CHILD4' } },
          ]
        }
      ]
    });
  });
});
