import { expect } from 'chai';
import { getGridOptions } from '../src/rules/src/chart/getGridOptions';

describe('getGridOptions tests', () => {
  it('blank test', () => {
    expect(getGridOptions(null, null)).to.deep.equal({
      x: { baselines: [0], color: 'white' },
      y: { baselines: [0], color: 'white' }
    });
  });
  it('default bar test', () => {
    expect(getGridOptions('BarChart', {})).to.deep.equal({
      x: { baselines: [0], color: 'white', thickness: 0 },
      y: { baselines: [0], color: 'white' }
    });
  });
  it('default row test', () => {
    expect(getGridOptions('RowChart', {})).to.deep.equal({
      x: { baselines: [0], color: 'white' },
      y: { baselines: [0], color: 'white', thickness: 0 }
    });
  });
  it('default scatter test', () => {
    expect(getGridOptions('ScatterChart', {})).to.deep.equal({
      x: { baselines: [0], color: 'white' },
      y: { baselines: [0], color: 'white' }
    });
  });
  it('default timeline test', () => {
    expect(getGridOptions('TimelineChart', {})).to.deep.equal({
      x: { baselines: [0], color: 'white', thickness: 0 },
      y: { baselines: [0], color: 'white' }
    });
  });
  it('default horizontal symbol test', () => {
    expect(getGridOptions('HorizontalSymbolChart', {})).to.deep.equal({
      x: { baselines: [0], color: 'white' },
      y: { baselines: [0], color: 'white', thickness: 0 }
    });
  });
  it('default vertical symbol test', () => {
    expect(getGridOptions('VerticalSymbolChart', {})).to.deep.equal({
      x: { baselines: [0], color: 'white', thickness: 0 },
      y: { baselines: [0], color: 'white' }
    });
  });
  it('default stacked test', () => {
    expect(getGridOptions('StackedBarChart', {})).to.deep.equal({
      x: { baselines: [0], color: 'white', thickness: 0 },
      y: { baselines: [0], color: 'white' }
    });
  });
  it('customization test', () => {
    expect(getGridOptions('BarChart', {
      grid: {
        customOptions: { deep: 'test' },
        x: { color: 'red', thickness: 2 }
      }
    })).to.deep.equal({
      customOptions: { deep: 'test' },
      x: { baselines: [0], color: 'red', thickness: 2 },
      y: { baselines: [0], color: 'white' }
    });
  });
});
