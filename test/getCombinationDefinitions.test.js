import { expect } from 'chai';
import { getCombinationDefinitions } from '../src/rules2/src';

describe('getCombinationDefinitions tests', () => {
  it('no annotations', () => {
    const annotations = [
      { type: 'random', title: 'test' },
      { type: 'NOT_DISPLAYED', title: 'FREQ=A'  }
    ];

    expect(getCombinationDefinitions(annotations, 'en')).to.deep.equal([]);
  });
  it('basic case', () => {
    const annotations = [
      { type: 'random', title: 'test' },
      { type: 'NOT_DISPLAYED', title: 'FREQ=A'  },
      {
        type: 'COMBINED_CONCEPTS',
        texts: { en: 'COMB_1:First Combination;COMB_2:Second Combination' },
        title: 'COMB_1:D1,D2,A1;COMB_2:D3,D4,A2,A3'
      }
    ];

    expect(getCombinationDefinitions(annotations, 'en')).to.deep.equal([
      { id: 'COMB_1', name: 'First Combination', concepts: ['D1', 'D2', 'A1'] },
      { id: 'COMB_2', name: 'Second Combination', concepts: ['D3', 'D4', 'A2', 'A3'] },
    ]);
  });
  it('bad formatted test 1', () => {
    const annotations = [
      { type: 'random', title: 'test' },
      { type: 'NOT_DISPLAYED', title: 'FREQ=A'  },
      {
        type: 'COMBINED_CONCEPTS',
        texts: { en: 'COMB_1:First Combination;COMB_2:Second Combination' },
        title: 'COMB_1:D1,D2,A1;D3,D4,A2,A3'
      }
    ];

    expect(getCombinationDefinitions(annotations, 'en')).to.deep.equal([
      { id: 'COMB_1', name: 'First Combination', concepts: ['D1', 'D2', 'A1'] },
    ]);
  });
  it('bad formatted test 2', () => {
    const annotations = [
      { type: 'random', title: 'test' },
      { type: 'NOT_DISPLAYED', title: 'FREQ=A'  },
      {
        type: 'COMBINED_CONCEPTS',
        texts: { en: 'COMB_1:First Combination;COMB_2:Second Combination' },
        title: 'COMB_1:D1,D2,A1;COMB_2:'
      }
    ];

    expect(getCombinationDefinitions(annotations, 'en')).to.deep.equal([
      { id: 'COMB_1', name: 'First Combination', concepts: ['D1', 'D2', 'A1'] },
    ]);
  });
  it('bad formatted test 3', () => {
    const annotations = [
      { type: 'random', title: 'test' },
      { type: 'NOT_DISPLAYED', title: 'FREQ=A'  },
      {
        type: 'COMBINED_CONCEPTS',
        texts: { en: 'First Combination;Second Combination' },
        title: 'D1,D2,A1;D3,D4,A2,A3'
      }
    ];

    expect(getCombinationDefinitions(annotations, 'en')).to.deep.equal([]);
  });
  it('missing localised text', () => {
    const annotations = [
      { type: 'random', title: 'test' },
      { type: 'NOT_DISPLAYED', title: 'FREQ=A'  },
      {
        type: 'COMBINED_CONCEPTS',
        texts: { en: 'COMB_1:First Combination;COMB_2:Second Combination' },
        title: 'COMB_1:D1,D2,A1;COMB_2:D3,D4,A2,A3'
      }
    ];

    expect(getCombinationDefinitions(annotations, 'fr')).to.deep.equal([
      { id: 'COMB_1', name: '[COMB_1]', concepts: ['D1', 'D2', 'A1'] },
      { id: 'COMB_2', name: '[COMB_2]', concepts: ['D3', 'D4', 'A2', 'A3'] },
    ]);
  });
  it('mixed order texts', () => {
    const annotations = [
      { type: 'random', title: 'test' },
      { type: 'NOT_DISPLAYED', title: 'FREQ=A'  },
      {
        type: 'COMBINED_CONCEPTS',
        texts: { en: 'COMB_2:Second Combination;COMB_1:First Combination' },
        title: 'COMB_1:D1,D2,A1;COMB_2:D3,D4,A2,A3'
      }
    ];

    expect(getCombinationDefinitions(annotations, 'en')).to.deep.equal([
      { id: 'COMB_1', name: 'First Combination', concepts: ['D1', 'D2', 'A1'] },
      { id: 'COMB_2', name: 'Second Combination', concepts: ['D3', 'D4', 'A2', 'A3'] },
    ]);
  });
});
