import { expect } from 'chai';
import { getHeaderTitle } from '../src/rules2/src/';

describe('getHeaderTitle tests', () => {
  it('basic case', () => {

    const dataflow = { id: 'DF', name: 'dataflow' };

    const attributes = {
      FLAG: { id: 'FLAG', name: 'flag', value: { id: 'v', name: 'flag value' } },
      NOTE: { id: 'NOTE', name: 'note', value: { id: 'v2', name: 'note value' } },
      OTHER: { id: 'OTHER', name: 'other', value: { id: 'v3', name: 'other value' } },
    }

    const customAttributes = { flags: ['FLAG'], notes: ['NOTE'] };

    expect(getHeaderTitle(dataflow, attributes, 'label', customAttributes)).to.deep.equal({
      label: 'dataflow',
      flags: [
        { code: 'v', header: 'flag:', label: 'flag value' },
        { code: undefined, header: 'note:', label: 'note value' },
      ]
    });
  });
});
