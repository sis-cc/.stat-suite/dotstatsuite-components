import { expect } from 'chai';
import { getAttributesSeries } from '../src/rules2/src/getAttributesSeries';

describe('getAttributesSeries tests', () => {
  it('complete case', () => {
    const observations = {
      a: {
        attributes: {
          A: { id: 'A', value: { id: 'A0' }, serieKey: 'd0=v0' },
          B: { id: 'B', value: { id: 'B0' }, serieKey: 'd1=v0' },
          C: { id: 'C', value: null, serieKey: 'd2=v0' },
          D: { id: 'D', value: { id: 'D0' }, serieKey: 'd2=v0', isObs: true }
        }
      },
      b: {
        attributes: {
          A: { id: 'A', value: { id: 'A0' }, serieKey: 'd0=v0' },
          B: { id: 'B', value: { id: 'B1' }, serieKey: 'd1=v0' },
          D: { id: 'D', value: { id: 'D0' }, serieKey: 'd2=v0', isObs: true }
        }
      },
      c: {
        attributes: {
          A: { id: 'A', value: { id: 'A0' }, serieKey: 'd0=v0' },
          B: { id: 'B', value: { id: 'B1' }, serieKey: 'd1=v1' },
          D: { id: 'D', value: { id: 'D0' }, serieKey: 'd2=v0', isObs: true }
        }
      },
      d: {
        attributes: {
          A: { id: 'A', value: { id: 'A0' }, serieKey: 'd0=v0' },
          B: { id: 'B', value: { id: 'B1' }, serieKey: 'd1=v1' },
          D: { id: 'D', value: { id: 'D0' }, serieKey: 'd2=v0', isObs: true }
        }
      },
      e: {
        attributes: {
          A: { id: 'A', value: { id: 'A0' }, serieKey: 'd0=v0' },
          B: { id: 'B', value: { id: 'B3' }, serieKey: 'd1=v2' },
          D: { id: 'D', value: { id: 'D0' }, serieKey: 'd2=v0', isObs: true }
        }
      },
      f: {
        attributes: {
          A: { id: 'A', value: { id: 'A0' }, serieKey: 'd0=v0' },
          B: { id: 'B', value: { id: 'B3' }, serieKey: 'd1=v2' },
          C: { id: 'C', value: { id: 'C0' }, serieKey: 'd2=v0' },
          D: { id: 'D', value: { id: 'D0' }, serieKey: 'd2=v0', isObs: true }
        }
      }
    };

    expect(getAttributesSeries(observations)).to.deep.equal({
      'd0=v0': {
        A: { id: 'A', value: { id: 'A0' }, serieKey: 'd0=v0' },
      },
      'd1=v0': {},
      'd1=v1': {
        B: { id: 'B', value: { id: 'B1' }, serieKey: 'd1=v1' },
      },
      'd1=v2': {
        B: { id: 'B', value: { id: 'B3' }, serieKey: 'd1=v2' },
      }
    })
  });
});
