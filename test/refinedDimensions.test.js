import { expect } from 'chai';
import { refineDimensions } from '../src/rules2/src';

describe('refine dimensions tests', () => {
  it('blank case', () => {
    expect(refineDimensions([], '')).to.deep.equal([]);
  });
  it('complete case', () => {
    const dimensions = [
      { id: 'd0', values: [] },
      { id: 'd1', values: [{ id: 'v0' }] },
      { id: 'd2', values: [{ id: 'v0' }, { id: 'v1' }, { id: 'v2' }] },
      { id: 'd3', values: [{ id: 'v0' }, { id: 'v1' }, { id: 'v2' }] },
      { id: 'd4', values: [{ id: 'v0' }, { id: 'v1' }, { id: 'v2' }] },
      { id: 'd5', values: [{ id: 'v0' }] },
      { id: 'd6', values: [{ id: 'v0' }, { id: 'v1' }] },
      { id: 'd7', values: [{ id: 'v0' }] },
      { id: 'd8', values: [{ id: 'v0' }, { id: 'v1' }] }
    ];

    const dataquery = '.v0+v1.v0.v0+v1...';

    expect(refineDimensions(dimensions, dataquery)).to.deep.equal([
      { id: 'd0', values: [], header: false },
      { id: 'd1', values: [{ id: 'v0', isSelected: true }], header: true },
      { id: 'd2', values: [{ id: 'v0', isSelected: true }, { id: 'v1', empty: true }, { id: 'v2', empty: true }], header: true },
      { id: 'd3', values: [{ id: 'v0', isSelected: true }, { id: 'v1', isSelected: true }, { id: 'v2', empty: true }], header: false },
      { id: 'd4', values: [{ id: 'v0', isSelected: true }, { id: 'v1', isSelected: true }, { id: 'v2', isSelected: true }], header: false },
      { id: 'd5', values: [{ id: 'v0', isSelected: true }], header: true },
      { id: 'd6', values: [{ id: 'v0', isSelected: true }, { id: 'v1', isSelected: true }], header: false },
      { id: 'd7', values: [{ id: 'v0', isSelected: true }], header: true },
      { id: 'd8', values: [{ id: 'v0', isSelected: true }, { id: 'v1', isSelected: true }], header: false }
    ]);
  })
});
