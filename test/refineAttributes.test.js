import { expect } from 'chai';
import { refineAttributes } from '../src/rules2/src';

describe('refineAttributes tests', () => {
  it('basic test', () => {
    const attributes = [
      { id: 'A1', relationship: [] },
      { id: 'A2', relationship: ['D1', 'D2'] },
      { id: 'A3', series: true, relationship: [] },
      { id: 'A4', series: true, relationship: ['D3', 'D4'] },
      { id: 'A5', header: true, relationship: [] },
      { id: 'A6', series: true, relationship: ['D3', 'D4'] },
    ];

    const combinations = [
      { id: 'COMB1', concepts: ['A1', 'A3', 'A4'], relationship: ['D5', 'D6'] },
      { id: 'COMB2', concepts: ['A2'], relationship: [] },
    ];

    expect(refineAttributes(attributes, combinations)).to.deep.equal([
      { id: 'A1', series: true, relationship: ['D5', 'D6'], combined: true },
      { id: 'A2', series: true, relationship: [], combined: true },
      { id: 'A3', series: true, relationship: ['D5', 'D6'], combined: true },
      { id: 'A4', series: true, relationship: ['D5', 'D6'], combined: true },
      { id: 'A5', header: true, relationship: [] },
      { id: 'A6', series: true, relationship: ['D3', 'D4'] },
    ]);
  });
});
