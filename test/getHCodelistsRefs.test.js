import { expect } from 'chai';
import { getHCodelistsRefs } from '../src/rules2/src';

describe('getHCodelistsRefs tests', () => {
  it('complete case', () => {
    const annotations = [
      { type: 'test' },
      { type: 'HIER_CONTEXT', text: 'C1:A:HCL1(1.0).HIER1' },
      { type: 'HIER_CONTEXT', title: 'C2:A:HCL2(1.0).HIER1' },
      { type: 'HIER_CONTEXT', text: 'C1:A:HCL1(1.0).HIER2' },
      { type: 'HIER_CONTEXT', text: 'C3:A:HCL3(1.0).HIER,C4:A:HCL4(1.0).HIER' }
    ];

    const expected = {
      C1: { agencyId: 'A', codelistId: 'C1', code: 'HCL1', hierarchy: 'HIER2', version: '1.0' },
      C2: { agencyId: 'A', codelistId: 'C2', code: 'HCL2', hierarchy: 'HIER1', version: '1.0' },
      C3: { agencyId: 'A', codelistId: 'C3', code: 'HCL3', hierarchy: 'HIER', version: '1.0' },
      C4: { agencyId: 'A', codelistId: 'C4', code: 'HCL4', hierarchy: 'HIER', version: '1.0' },
    }
  
    expect(getHCodelistsRefs(annotations)).to.deep.equal(expected);
  });
});
