import { expect } from 'chai';
import { getReportingYearStart } from '../src/rules/src/preparators/getReportingYearStart';

describe('getReportingYearStart tests', () => {
  it('no data', () => {
    expect(getReportingYearStart()).to.deep.equal({ month: '01', day: '01' });
  });
  it('unexpected data in REPYEARSTART attr: "0"', () => {
    const data = {
      data: {
        structure: {
          attributes: {
            observation: [
              { "id": "REPYEARSTART",
                "values": [{ "value": "0" }]
              }
            ]
          }
        }
      }
    };
    expect(getReportingYearStart(data)).to.deep.equal({ month: '01', day: '01' });
  });
  it('REPORTING_YEAR_START_DAY', () => {
    const dataReportingYearStartDay = {
      data: {
        structure: {
          attributes: {
            observation: [
              {
                "id": "REPORTING_YEAR_START_DAY",
                "values": [{ "value": "--07-01" }]
              }
            ]
          }
        }
      }
    };
    
    expect(getReportingYearStart(dataReportingYearStartDay)).to.deep.equal({ month: '07', day: '01' });
  });
  it('REPYEARSTART', () => {
    const dataReportingYearStart = {
      data: {
        structure: {
          attributes: {
            observation: [
              {
                "id": "REPYEARSTART",
                "values": [{ "value": "--08-09" }]
              }
            ]
          }
        }
      }
    };
    expect(getReportingYearStart(dataReportingYearStart)).to.deep.equal({ month: '08', day: '09' });
  });
});
