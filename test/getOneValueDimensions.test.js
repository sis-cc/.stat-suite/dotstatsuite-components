import { expect } from 'chai';
import { getOneValueDimensions } from '../src/rules2/src/';

describe('getOneValueDimensions tests', () => {
  it('basic case', () => {
    const attributes = [
      { id: 'a0', header: true, values: [{ id: 'v' }] },
      { id: 'a1', header: true, values: [{ id: 'v' }], relationship: ['d0'] },
      { id: 'a2', header: true, values: [{ id: 'v' }], relationship: ['d0', 'd1'] },
      { id: 'a3', display: false, header: true, values: [{ id: 'v' }], relationship: ['d0'] },
      { id: 'a4', header: true, values: [{ id: 'v', display: false }], relationship: ['d0'] },
      { id: 'a5', header: true, values: [{ id: '_Z' }], relationship: ['d0'] },
    ];

    const dimensions = [
      { id: 'd0', header: true, values: [{ id: 'v' }] },
      { id: 'd1', header: true, values: [{ id: 'v' }] },
      { id: 'd2', header: true, display: false, values: [{ id: 'v' }] },
      { id: 'd3', header: true, values: [{ id: 'v', display: false }] },
      { id: 'd4', header: true, values: [{ id: '_T' }] },
      { id: 'd5', header: true, values: [{ id: 'v0' }, { id: 'v1' }] },
      { id: 'd6', header: false, values: [{ id: 'v0' }] },
      { id: 'd7', header: false, values: [{ id: 'v0' }, { id: 'v1' }] },
    ];

    expect(getOneValueDimensions(dimensions, attributes)).to.deep.equal([
      { id: 'd0', header: true, values: [{ id: 'v' }], attrValues: { a1: { id: 'a1', header: true, values: [{ id: 'v' }], relationship: ['d0'], value: { id: 'v' } } } },
      { id: 'd1', header: true, values: [{ id: 'v' }], attrValues: {} },
      { id: 'd2', header: true, values: [{ id: 'v' }], display: false, attrValues: {} },
      { id: 'd3', header: true, values: [{ id: 'v', display: false }], attrValues: {} },
      { id: 'd4', header: true, values: [{ id: '_T' }], attrValues: {} },
      { id: 'd5', header: true, values: [{ id: 'v0' }, { id: 'v1' }], attrValues: {} },
    ])
  });
});
