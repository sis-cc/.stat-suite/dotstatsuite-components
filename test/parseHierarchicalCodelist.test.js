import { expect } from 'chai';
import { parseHierarchicalCodelist } from '../src/rules2/src';
import hierarchicalCodelist from './mocks/h-codelist.json';

describe('parseHierarchicalCodelist tests', () => {
  it('simple case', () => {
    const expected = {
      '#ROOT': ['W', 'A', 'A2', 'OECD', 'EA00'],
      W: ['A', 'E', 'F', 'O', 'S', 'W_X'],
      A: ['A2', 'A3', 'A4', 'A5', 'A7', 'A8', 'A8_M', 'A9', 'A_X', 'A_O'],
      A2: ['BMU', 'CAN', 'GRL', 'SPM', 'USA'],
      OECD: [
        'AUS',
        'AUT',
        'BEL',
        'CAN',
        'CHL',
        'COL',
        'CRI',
        'CZE',
        'DNK',
        'EST',
        'FIN',
        'FRA',
        'DEU',
        'GRC',
        'HUN',
        'ISL',
        'IRL',
        'ISR',
        'ITA',
        'JPN',
        'KOR',
        'LVA',
        'LTU',
        'LUX',
        'MEX',
        'NLD',
        'NZL',
        'NOR',
        'POL',
        'PRT',
        'SVK',
        'SVN',
        'ESP',
        'SWE',
        'CHE',
        'TUR',
        'GBR',
        'USA',
      ],
      EA00: [
        'BEL',
        'DEU',
        'IRL',
        'ESP',
        'FRA',
        'ITA',
        'LUX',
        'NLD',
        'AUT',
        'PRT',
        'FIN',
        'GRC',
        'SVN',
        'CYP',
        'MLT',
        'SVK',
        'EST',
        'LVA',
        'LTU',
      ],
    };

    expect(parseHierarchicalCodelist(hierarchicalCodelist, 'CONT_EN')).to.deep.equal(expected);
  });
  it('deep hierarchies case', () => {
    const hCodelist = {
      data: {
        hierarchicalCodelists: [{
          hierarchies: [{
            id: 'HIER',
            hierarchicalCodes: [
              {
                codeID: 'OECD',
                hierarchicalCodes: [
                  {
                    codeID: 'GER',
                    hierarchicalCodes: [
                      { codeID: 'BAV' }
                    ]
                  },
                  {
                    codeID: 'FRA',
                    hierarchicalCodes: [
                      { codeID: 'BZH' },
                      { codeID: 'IDF' }
                    ]
                  }
                ]
              },
              {
                codeID: 'EA',
                hierarchicalCodes: [
                  {
                    codeID: 'FRA',
                    hierarchicalCodes: [
                      { codeID: 'IDF' },
                      { codeID: 'ALS' },
                      { codeID: 'BZH' },
                    ]
                  },
                  {
                    codeID: 'GER',
                    hierarchicalCodes: [
                      { codeID: 'BAV' },
                      { codeID: 'RHN' }
                    ]
                  }
                ]
              }
            ]
          }]
        }]
      }
    };

    const expected = {
      '#ROOT': ['OECD', 'EA'],
      'OECD': ['GER', 'FRA'],
      'OECD.GER': ['BAV'],
      'OECD.FRA': ['BZH', 'IDF'],
      'EA': ['FRA', 'GER'],
      'EA.FRA': ['IDF', 'ALS', 'BZH'],
      'EA.GER': ['BAV', 'RHN']
    };

    expect(parseHierarchicalCodelist(hCodelist, 'HIER')).to.deep.equal(expected);
  });
});