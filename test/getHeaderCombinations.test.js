import { expect } from 'chai';
import { getHeaderCombinations } from '../src/rules2/src/';

describe('getHeaderCombinations tests', () => {
  it('complete test', () => {
    const combinations = [
      { id: 'COMB1', name: 'comb 1', header: true, concepts: ['DIM1', 'DIM2', 'DIM4', 'ATTR1'] },
      { id: 'COMB2', name: 'comb 2', series: true, concepts: ['DIM3', 'ATTR2'] },
      { id: 'COMB3', name: 'comb 3', header: true, display: false, concepts: ['DIM3', 'ATTR2'] }
    ];

    const dimensions = [
      { id: 'DIM1', name: 'dim 1', values: [{ id: 'v', name: 'dim 1 value' }] },
      { id: 'DIM3', name: 'dim 3', values: [{ id: 'v', name: 'dim 3 value' }] },
      { id: 'DIM4', name: 'dim 4', values: [{ id: '_Z', name: 'Not applicable' }] },
    ];

    const attributes = [
      { id: 'ATTR1', name: 'attr 1', values: [{ id: 'v', name: 'attr 1 value', display: false }] },
      { id: 'ATTR2', name: 'attr 2', values: [{ id: 'v', name: 'attr 2 value' }] },
    ];

    expect(getHeaderCombinations(combinations, dimensions, attributes, 'label')).to.deep.equal([
      { header: 'comb 1:', label: 'dim 1 value' }
    ]);
  });
  it('not display usecases', () => {
    const dimensions = [
      { id: 'd0', values: [{ id: 'd0v', name: 'd0 value', display: false }] },
      { id: 'd1', values: [{ id: '_T', name: 'Total' }] },
      { id: 'd2', values: [{ id: 'd2v', name: 'd2 value', display: false }] },
    ];

    const attributes = [
      { id: 'a0', values: [{ id: 'a0v', name: 'a0 value', display: false }] },
      { id: 'a1', values: [{ id: 'a1v', name: 'a1 value', display: false }] },
      { id: 'a2', values: [{ id: '_T', name: 'Total' }] },
    ];

    const combinations = [
      { id: 'comb0', name: 'combination 0', header: true, concepts: ['a0', 'a1', 'd0'] },
      { id: 'comb1', name: 'combination 1', header: true, concepts: ['d0', 'd2', 'a3'] },
      { id: 'comb2', name: 'combination 2', header: true, concepts: ['d0', 'd1', 'd2'] },
      { id: 'comb3', name: 'combination 3', header: true, concepts: ['a0', 'a1', 'a2'] },
    ];
    expect(getHeaderCombinations(combinations, dimensions, attributes, 'label')).to.deep.equal([
      { header: 'combination 0:', label: 'd0 value' },
      { header: 'combination 1:', label: 'd0 value' },
      { header: 'combination 2:', label: 'Total' },
    ]);
    expect(getHeaderCombinations(combinations, dimensions, attributes, 'code')).to.deep.equal([
      { header: 'comb0:', label: 'd0v' },
      { header: 'comb1:', label: 'd0v, d2v' },
      { header: 'comb2:', label: 'd0v, _T, d2v' },
    ]);
    expect(getHeaderCombinations(combinations, dimensions, attributes, 'both')).to.deep.equal([
      { header: '(comb0) combination 0:', label: '(d0v) d0 value' },
      { header: '(comb1) combination 1:', label: '(d0v, d2v) d0 value' },
      { header: '(comb2) combination 2:', label: '(d0v, _T, d2v) Total' },
    ]);
  })
});
