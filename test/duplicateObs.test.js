import { expect } from 'chai';
import { duplicateObs } from '../src/rules2/src/duplicateObservations';

describe('duplicate obs tests', () => {
  it('simple test', () => {
    const dims = [{
      id: 'REF_AREA',
      __index: 2,
      values: [
        { id: 'W' },
        { id: 'OECD' },
        { id: 'FRA' },
        { id: 'GER' },
        { id: 'EA' },
        { id: 'GER' },
        { id: 'FRA' },
      ]
    }];

    const obs = {
      '0:0:0': {
        indexedDimValIds: { FREQ: 'A', VAR: '_T', REF_AREA: 'FRA' },
        dimValuesIndexes: [0, 0, 0],
      },
      '0:0:1': {
        indexedDimValIds: { FREQ: 'A', VAR: '_T', REF_AREA: 'EA' },
        dimValuesIndexes: [0, 0, 1],
      },
      '0:0:2': {
        indexedDimValIds: { FREQ: 'A', VAR: '_T', REF_AREA: 'OECD' },
        dimValuesIndexes: [0, 0, 2],
      },
      '0:0:3': {
        indexedDimValIds: { FREQ: 'A', VAR: '_T', REF_AREA: 'GER' },
        dimValuesIndexes: [0, 0, 3],
      },
      '0:0:4': {
        indexedDimValIds: { FREQ: 'A', VAR: '_T', REF_AREA: 'W' },
        dimValuesIndexes: [0, 0, 4],
      },
    };

    const expected = {
      '0:0:0': {
        indexedDimValIds: { FREQ: 'A', VAR: '_T', REF_AREA: 'W' },
        orderedDimIndexes: [0, 0, 0],
        dimValuesIndexes: [0, 0, 4],
      },
      '0:0:1': {
        indexedDimValIds: { FREQ: 'A', VAR: '_T', REF_AREA: 'OECD' },
        orderedDimIndexes: [0, 0, 1],
        dimValuesIndexes: [0, 0, 2],
      },
      '0:0:2': {
        indexedDimValIds: { FREQ: 'A', VAR: '_T', REF_AREA: 'FRA' },
        orderedDimIndexes: [0, 0, 2],
        dimValuesIndexes: [0, 0, 0],
      },
      '0:0:3': {
        indexedDimValIds: { FREQ: 'A', VAR: '_T', REF_AREA: 'GER' },
        orderedDimIndexes: [0, 0, 3],
        dimValuesIndexes: [0, 0, 3],
      },
      '0:0:4': {
        indexedDimValIds: { FREQ: 'A', VAR: '_T', REF_AREA: 'EA' },
        orderedDimIndexes: [0, 0, 4],
        dimValuesIndexes: [0, 0, 1],
      },
      '0:0:5': {
        indexedDimValIds: { FREQ: 'A', VAR: '_T', REF_AREA: 'GER' },
        orderedDimIndexes: [0, 0, 5],
        dimValuesIndexes: [0, 0, 3],
      },
      '0:0:6': {
        indexedDimValIds: { FREQ: 'A', VAR: '_T', REF_AREA: 'FRA' },
        orderedDimIndexes: [0, 0, 6],
        dimValuesIndexes: [0, 0, 0],
      }
    };

    expect(duplicateObs(dims, obs)).to.deep.equal(expected);
  });
  it('handle uniq observation case', () => {
    const dims = [{
      id: 'REF_AREA',
      __index: 2,
      values: [
        { id: 'W' },
        { id: 'OECD' },
        { id: 'FRA' },
        { id: 'GER' },
        { id: 'EA' },
        { id: 'GER' },
        { id: 'FRA' },
      ]
    }];
    const obs = {
      '0:0:0': {
        indexedDimValIds: { FREQ: 'A', VAR: '_T', REF_AREA: 'FRA' },
        dimValuesIndexes: [0, 0, 0],
      }
    };
    const expected = {
      '0:0:2': {
        indexedDimValIds: { FREQ: 'A', VAR: '_T', REF_AREA: 'FRA' },
        orderedDimIndexes: [0, 0, 2],
        dimValuesIndexes: [0, 0, 0],
      },
      '0:0:6': {
        indexedDimValIds: { FREQ: 'A', VAR: '_T', REF_AREA: 'FRA' },
        orderedDimIndexes: [0, 0, 6],
        dimValuesIndexes: [0, 0, 0],
      }
    };

    expect(duplicateObs(dims, obs)).to.deep.equal(expected);
  });
});
