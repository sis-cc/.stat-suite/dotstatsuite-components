import { applyHierarchicalCodesToDim } from '../src/rules2/src/applyHierarchicalCodesToDim';
import { expect } from 'chai';
import * as R from 'ramda';

const hCodes = [
  { codeID: 'PARENT_A', hierarchicalCodes: [
    { codeID: 'ITEM', hierarchicalCodes: [
      { codeID: 'CHILD_A', hierarchicalCodes: [] },
      { codeID: 'CHILD_B', hierarchicalCodes: [] },
    ] },
    { codeID: 'BROTHER_A', hierarchicalCodes: [] },
  ] },
  { codeID: 'PARENT_B', hierarchicalCodes: [
    { codeID: 'ITEM', hierarchicalCodes: [
      { codeID: 'CHILD_A', hierarchicalCodes: [] },
      { codeID: 'CHILD_C', hierarchicalCodes: [] },
    ] },
    { codeID: 'BROTHER_B', hierarchicalCodes: [] },
  ] },
];

const dim = {
  id: 'DIM',
  values: [
    { id: 'PARENT_A', isSelected: true },
    { id: 'PARENT_B', isSelected: true },
    { id: 'ITEM', isSelected: true },
    { id: 'BROTHER_A', isSelected: true },
    { id: 'BROTHER_B', isSelected: true },
    { id: 'CHILD_A', isSelected: true },
    { id: 'CHILD_B', isSelected: true },
    { id: 'CHILD_C', isSelected: true },
  ]
};

const deselectedDim = (indexes) => R.over(
  R.lensProp('values'),
  R.addIndex(R.map)((v, i) => R.includes(i, indexes) ? { ...v, isSelected: false } : v)
)(dim);

describe('refineHierarchicalCodelist tests', () => {
  it('complete test', () => {
    const expected = {
      id: 'DIM',
      values: [
        { id: 'PARENT_A', parents: [], parent: undefined, isSelected: true, __indexPosition: 0 },
        { id: 'ITEM', parents: [0], parent: 'PARENT_A', isSelected: true, __indexPosition: 1 },
        { id: 'CHILD_A', parents: [0, 1], parent: 'PARENT_A.ITEM', isSelected: true, __indexPosition: 2 },
        { id: 'CHILD_B', parents: [0, 1], parent: 'PARENT_A.ITEM', isSelected: true, __indexPosition: 3 },
        { id: 'BROTHER_A', parents: [0], parent: 'PARENT_A', isSelected: true, __indexPosition: 4 },
        { id: 'PARENT_B', parents: [], parent: undefined, isSelected: true, __indexPosition: 5 },
        { id: 'ITEM', parents: [5], parent: 'PARENT_B', isSelected: true, __indexPosition: 6 },
        { id: 'CHILD_A', parents: [5, 6], parent: 'PARENT_B.ITEM', isSelected: true, __indexPosition: 7 },
        { id: 'CHILD_C', parents: [5, 6], parent: 'PARENT_B.ITEM', isSelected: true, __indexPosition: 8 },
        { id: 'BROTHER_B', parents: [5], parent: 'PARENT_B', isSelected: true, __indexPosition: 9 },
      ]
    };

    expect(applyHierarchicalCodesToDim(hCodes, dim)).to.deep.equal(expected);
  });
  it('partial test 1', () => {
    const partialDim = deselectedDim([0]);

    const expected = {
      id: 'DIM',
      values: [
        { id: 'ITEM', parents: [], parent: undefined, isSelected: true, __indexPosition: 0 },
        { id: 'CHILD_A', parents: [0], parent: 'ITEM', isSelected: true, __indexPosition: 1 },
        { id: 'CHILD_B', parents: [0], parent: 'ITEM', isSelected: true, __indexPosition: 2 },
        { id: 'BROTHER_A', parents: [], parent: undefined, isSelected: true, __indexPosition: 3 },
        { id: 'PARENT_B', parents: [], parent: undefined, isSelected: true, __indexPosition: 4 },
        { id: 'ITEM', parents: [4], parent: 'PARENT_B', isSelected: true, __indexPosition: 5 },
        { id: 'CHILD_A', parents: [4, 5], parent: 'PARENT_B.ITEM', isSelected: true, __indexPosition: 6 },
        { id: 'CHILD_C', parents: [4, 5], parent: 'PARENT_B.ITEM', isSelected: true, __indexPosition: 7 },
        { id: 'BROTHER_B', parents: [4], parent: 'PARENT_B', isSelected: true, __indexPosition: 8 },
      ]
    };

    expect(applyHierarchicalCodesToDim(hCodes, partialDim)).to.deep.equal(expected);
  });
  it('partial test 2', () => {
    const partialDim = deselectedDim([1, 4, 7]);

    const expected = {
      id: 'DIM',
      values: [
        { id: 'PARENT_A', parents: [], parent: undefined, isSelected: true, __indexPosition: 0 },
        { id: 'ITEM', parents: [0], parent: 'PARENT_A', isSelected: true, __indexPosition: 1 },
        { id: 'CHILD_A', parents: [0, 1], parent: 'PARENT_A.ITEM', isSelected: true, __indexPosition: 2 },
        { id: 'CHILD_B', parents: [0, 1], parent: 'PARENT_A.ITEM', isSelected: true, __indexPosition: 3 },
        { id: 'BROTHER_A', parents: [0], parent: 'PARENT_A', isSelected: true, __indexPosition: 4 },
      ]
    };

    expect(applyHierarchicalCodesToDim(hCodes, partialDim)).to.deep.equal(expected);
  });
  it('partial test 3', () => {
    const partialDim = deselectedDim([0, 6]);

    const expected = {
      id: 'DIM',
      values: [
        { id: 'BROTHER_A', parents: [], parent: undefined, isSelected: true, __indexPosition: 0 },
        { id: 'PARENT_B', parents: [], parent: undefined, isSelected: true, __indexPosition: 1 },
        { id: 'ITEM', parents: [1], parent: 'PARENT_B', isSelected: true, __indexPosition: 2 },
        { id: 'CHILD_A', parents: [1, 2], parent: 'PARENT_B.ITEM', isSelected: true, __indexPosition: 3 },
        { id: 'CHILD_C', parents: [1, 2], parent: 'PARENT_B.ITEM', isSelected: true, __indexPosition: 4 },
        { id: 'BROTHER_B', parents: [1], parent: 'PARENT_B', isSelected: true, __indexPosition: 5 },
      ]
    };

    expect(applyHierarchicalCodesToDim(hCodes, partialDim)).to.deep.equal(expected);
  });
  it('partial test 4', () => {
    const partialDim = deselectedDim([6]);

    const expected = {
      id: 'DIM',
      values: [
        { id: 'PARENT_A', parents: [], parent: undefined, isSelected: true, __indexPosition: 0 },
        { id: 'ITEM', parents: [0], parent: 'PARENT_A', isSelected: true, __indexPosition: 1 },
        { id: 'CHILD_A', parents: [0, 1], parent: 'PARENT_A.ITEM', isSelected: true, __indexPosition: 2 },
        { id: 'BROTHER_A', parents: [0], parent: 'PARENT_A', isSelected: true, __indexPosition: 3 },
        { id: 'PARENT_B', parents: [], parent: undefined, isSelected: true, __indexPosition: 4 },
        { id: 'ITEM', parents: [4], parent: 'PARENT_B', isSelected: true, __indexPosition: 5 },
        { id: 'CHILD_A', parents: [4, 5], parent: 'PARENT_B.ITEM', isSelected: true, __indexPosition: 6 },
        {  id: 'CHILD_C', parents: [4, 5], parent: 'PARENT_B.ITEM', isSelected: true, __indexPosition: 7 },
        { id: 'BROTHER_B', parents: [4], parent: 'PARENT_B', isSelected: true, __indexPosition: 8 },
      ]
    };

    expect(applyHierarchicalCodesToDim(hCodes, partialDim)).to.deep.equal(expected);
  });
  it('partial test 5', () => {
    const partialDim = deselectedDim([0, 1, 2, 3, 4]);

    const expected = {
      id: 'DIM',
      values: [
        { id: 'CHILD_A', parents: [], parent: undefined, isSelected: true, __indexPosition: 0 },
        { id: 'CHILD_B', parents: [], parent: undefined, isSelected: true, __indexPosition: 1 },
        { id: 'CHILD_C', parents: [], parent: undefined, isSelected: true, __indexPosition: 2 },
      ]
    };
    expect(applyHierarchicalCodesToDim(hCodes, partialDim)).to.deep.equal(expected);
  });
  it('partial test 6', () => {
    const partialDim = deselectedDim([0, 1, 3, 4, 6]);

    const expected = {
      id: 'DIM',
      values: [
        { id: 'ITEM', parents: [], parent: undefined, isSelected: true, __indexPosition: 0 },
        { id: 'CHILD_A', parents: [0], parent: 'ITEM', isSelected: true, __indexPosition: 1 },
        { id: 'CHILD_C', parents: [0], parent: 'ITEM', isSelected: true, __indexPosition: 2 },
      ]
    };
    expect(applyHierarchicalCodesToDim(hCodes, partialDim)).to.deep.equal(expected);
  });
});