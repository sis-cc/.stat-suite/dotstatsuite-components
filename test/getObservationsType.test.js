import { expect } from 'chai';
import { getObservationsType } from '../src/rules/src/properties/getObservationsType';
import {
  OBS_TYPE_NUMBER,
  OBS_TYPE_NUMERICAL_STRING,
  OBS_TYPE_STRING
} from '../src/rules/src/constants';

describe('getObservationsType tests', () => {
  it('blank test', () => {
    expect(getObservationsType({ dataSets: [{ observations: {} }] })).to.equal(undefined);
  });
  it('all obs are null test', () => {
    expect(getObservationsType({ dataSets: [{
      observations: {
        "a": [null, 0, 0, 0],
        "b": [null, 0, 0, 0], 
        "c": [null, 0, 0, 0], 
        "d": [null, 0, 0, 0], 
      }
    }] })).to.equal(undefined);
  });
  it('number case', () => {
    expect(getObservationsType({ dataSets: [{
      observations: {
        "a": [null, 0, 0, 0],
        "b": [456, 0, 0, 0], 
        "c": ['toto', 0, 0, 0], 
        "d": ['456', 0, 0, 0], 
      }
    }] })).to.equal(OBS_TYPE_NUMBER);
  });
  it('numerical string test', () => {
    expect(getObservationsType({ dataSets: [{
      observations: {
        "a": [null, 0, 0, 0],
        "b": ['456', 0, 0, 0], 
        "c": [456, 0, 0, 0], 
        "d": ['toto', 0, 0, 0], 
      }
    }] })).to.equal(OBS_TYPE_NUMERICAL_STRING);
  });
  it('string test', () => {
    expect(getObservationsType({ dataSets: [{
      observations: {
        "a": [null, 0, 0, 0],
        "b": ['toto', 0, 0, 0], 
        "c": [456, 0, 0, 0], 
        "d": ['456', 0, 0, 0], 
      }
    }] })).to.equal(OBS_TYPE_STRING);
  });

});

