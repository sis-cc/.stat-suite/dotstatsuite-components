import { expect } from 'chai';
import { parseValueHierarchy } from '../src/rules2/src/table/parseValueHierarchy';

const indexedValues = {
  OECD: { id: 'OECD' },
  EU: { id: 'EU' },
  FRA: { id: 'FRA' },
  GER: { id: 'GER' },
  IDF: { id: 'IDF' },
};

describe('parseValueHierarchy tests', () => {
  it('no hierarchy in value', () => {
    const value = { id: 'EU', parents: [] };
    const previousValue = { id: 'IDF', parents: ['OECD', 'FRA'] };
    expect(parseValueHierarchy(value, previousValue, indexedValues)).to.deep.equal(value);
  });
  it('previous is self', () => {
    const value = { id: 'GER', parents: ['OECD'] };
    const previousValue = { id: 'GER', parentsIds: ['OECD'], missingParents: [], parents: [{ id: 'OECD' }] };
    expect(parseValueHierarchy(value, previousValue, indexedValues)).to.deep.equal({
      id: 'GER',
      parents: [{ id: 'OECD' }],
      parentsIds: ['OECD'],
      missingParents: []
    });
  });
  it('previous is self with missing parent', () => {
    const value = { id: 'GER', parents: ['OECD'] };
    const previousValue = { id: 'GER', parentsIds: ['OECD'], missingParents: [{ id: 'OECD' }], parents: [] };
    expect(parseValueHierarchy(value, previousValue, indexedValues)).to.deep.equal({
      id: 'GER',
      parents: [],
      parentsIds: ['OECD'],
      missingParents: [{ id: 'OECD' }]
    });
  });
  it('previous is parent', () => {
    const value = { id: 'IDF', parents: ['OECD', 'FRA'] };
    const previousValue = { id: 'FRA', parents: [{ id: 'OECD' }], parentsIds: ['OECD'], missingParents: [] };
    expect(parseValueHierarchy(value, previousValue, indexedValues)).to.deep.equal({
      id: 'IDF',
      parents: [{ id: 'OECD' }, { id: 'FRA' }],
      parentsIds: ['OECD', 'FRA'],
      missingParents: []
    });
  });
  it('previous is parent with missing parent', () => {
    const value = { id: 'IDF', parents: ['OECD', 'FRA'] };
    const previousValue = { id: 'FRA', parents: [], parentsIds: ['OECD'], missingParents: [{ id: 'OECD' }] };
    expect(parseValueHierarchy(value, previousValue, indexedValues)).to.deep.equal({
      id: 'IDF',
      parents: [{ id: 'FRA' }],
      parentsIds: ['OECD', 'FRA'],
      missingParents: []
    });
  });
  it('missing parents', () => {
    const value = { id: 'IDF', parents: ['OECD', 'FRA'] };
    const previousValue = null;
    expect(parseValueHierarchy(value, previousValue, indexedValues)).to.deep.equal({
      id: 'IDF',
      parents: [],
      parentsIds: ['OECD', 'FRA'],
      missingParents: [{ id: 'OECD' }, { id: 'FRA' }]
    });
  });
  it('previous is bro', () => {
    const value = { id: 'FRA', parents: ['OECD'] };
    const previousValue = { id: 'FRA', parentsIds: ['OECD'], missingParents: [], parents: [{ id: 'OECD' }] };
    expect(parseValueHierarchy(value, previousValue, indexedValues)).to.deep.equal({
      id: 'FRA',
      parents: [{ id: 'OECD' }],
      parentsIds: ['OECD'],
      missingParents: []
    });
  });
  it('previous is bro with missing parent', () => {
    const value = { id: 'FRA', parents: ['OECD'] };
    const previousValue = { id: 'GER', parentsIds: ['OECD'], missingParents: [{ id: 'OECD' }], parents: [] };
    expect(parseValueHierarchy(value, previousValue, indexedValues)).to.deep.equal({
      id: 'FRA',
      parents: [],
      parentsIds: ['OECD'],
      missingParents: [{ id: 'OECD' }]
    });
  });
  it('previous is parent but in different hierarchy', () => {
    const value = { id: 'IDF', parents: ['OECD', 'FRA'] };
    const previousValue = { id: 'FRA', parentsIds: ['EU'], missingParents: [], parents: [{ id: 'EU' }] };
    expect(parseValueHierarchy(value, previousValue, indexedValues)).to.deep.equal({
      id: 'IDF',
      parents: [],
      parentsIds: ['OECD', 'FRA'],
      missingParents: [{ id: 'OECD' }, { id: 'FRA' }]
    });
  });
  it('previous is bro but in different hierarchy', () => {
    const value = { id: 'IDF', parents: ['OECD', 'FRA'] };
    const previousValue = { id: 'BRE', parentsIds: ['EU', 'FRA'], missingParents: [], parents: [{ id: 'EU' }, { id: 'FRA' }] };
    expect(parseValueHierarchy(value, previousValue, indexedValues)).to.deep.equal({
      id: 'IDF',
      parents: [],
      parentsIds: ['OECD', 'FRA'],
      missingParents: [{ id: 'OECD' }, { id: 'FRA' }]
    });
  });
  it('previous is nephew but there is missing ids on top', () => {
    const value = { id: 'GER', parents: ['OECD'] };
    const previousValue = { id: 'IDF', parentsIds: ['OECD', 'FRA'], missingParents: [], parents: [{ id: 'OECD' }, { id: 'FRA' }] };
    expect(parseValueHierarchy(value, previousValue, indexedValues)).to.deep.equal({
      id: 'GER',
      parents: [{ id: 'OECD' }],
      parentsIds: ['OECD'],
      missingParents: []
    });
  });
  it('previous is nephew but there is missing ids on top', () => {
    const value = { id: 'GER', parents: ['OECD'] };
    const previousValue = { id: 'IDF', parentsIds: ['OECD', 'FRA'], missingParents: [], parents: [{ id: 'FRA' }] };
    expect(parseValueHierarchy(value, previousValue, indexedValues)).to.deep.equal({
      id: 'GER',
      parents: [],
      parentsIds: ['OECD'],
      missingParents: [{ id: 'OECD' }]
    });
  });
  it('previous is nephew with missing parent and missing ids on top', () => {
    const value = { id: 'GER', parents: ['OECD'] };
    const previousValue = { id: 'IDF', parentsIds: ['OECD', 'FRA'], missingParents: [{ id: 'OECD' }, { id: 'FRA' }], parents: [] };
    expect(parseValueHierarchy(value, previousValue, indexedValues)).to.deep.equal({
      id: 'GER',
      parents: [],
      parentsIds: ['OECD'],
      missingParents: [{ id: 'OECD' }]
    });
  });
});
