import { expect } from 'chai';
import { getCombinationDimensionsData } from '../src/rules2/src/table/getCombinationDimensionsData';

describe('getCombinationDimensionsData', () => {
  it('basic test', () => {
    const combination = {
      id: 'COMB',
      dimensions: [
        {
          id: 'D0',
          values: [
            { id: 'v0' },
            { id: 'v1' },
            { id: 'v2' },
          ]
        },
        {
          id: 'D1',
          display: false,
          values: [
            { id: 'v0' },
            { id: 'v1', hasAdvancedAttributes: true },
            { id: 'v2' },
          ]
        },
        {
          id: 'D2',
          values: [
            { id: 'v0' },
            { id: 'v1' },
            { id: 'v2' },
          ]
        }
      ]
    };

    const indexes = [0, 1, -2];

    const previous = { D0: { id: 'v0', display: true }, D1: { id: 'v0', display: false }, D2: { id: 'v2', display: true } };

    expect(getCombinationDimensionsData(indexes, combination, previous, true)).to.deep.equal({
      dimValues: {
        D0: { id: 'v0', display: true },
        D1: { id: 'v1', hasAdvancedAttributes: true, display: false },
        D2: { id: 'v2', display: true }
      },
      sameSerie: false,
      coordinates: { D0: 'v0', D1: 'v1', D2: 'v2' },
      ids: ['D0=v0', 'D1=v1', 'D2=v2'],
      hasAdvancedAttributes: true
    });
  });
  it('same value but under different hierarchy', () => {
    const combination = {
      id: 'COMB',
      dimensions: [
        {
          id: 'D0',
          values: [
            { id: 'v0' },
            { id: 'v1' },
            { id: 'v2' },
          ]
        },
        {
          id: 'D1',
          values: [
            { id: 'EU' },
            { id: 'FRA', parents: ['EU'] },
            { id: 'GER', parents: ['EU'] },
            { id: 'OECD' },
            { id: 'FRA', parents: ['OECD'] },
            { id: 'GER', parents: ['OECD'] },
          ],
          indexedValues: {
            EU: { id: 'EU' },
            FRA: { id: 'FRA' },
            GER: { id: 'GER' },
            OECD: { id: 'OECD' },
          }
        },
      ]
    };

    const indexes = [0, 4];

    const previous = { D0: { id: 'v0', display: true }, D1: { id: 'FRA', display: true, parents: [{ id: 'EU' }], parentsIds: ['EU'], missingParents: [] } };
    expect(getCombinationDimensionsData(indexes, combination, previous, true)).to.deep.equal({
      dimValues: {
        D0: { id: 'v0', display: true },
        D1: { id: 'FRA', display: true, parents: [], missingParents: [{ id: 'OECD' }], parentsIds: ['OECD'] },
      },
      sameSerie: false,
      coordinates: { D0: 'v0', D1: 'FRA' },
      ids: ['D0=v0', 'D1=FRA'],
      hasAdvancedAttributes: undefined
    });
  });
});
