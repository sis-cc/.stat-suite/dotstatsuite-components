import { expect } from 'chai';
import { refineTimePeriod } from '../src/rules2/src/refineTimePeriod';

describe('refineTimePeriod tests', () => {
  it('simple year', () => {
    expect(refineTimePeriod({ id: '2015', start: '2015-01-01T00:00:00', end: '2016-12-31T23:59:59' }, { locale: 'en' })).to.deep.equal({
      id: '2015',
      start: '2015-01-01T00:00:00.000Z',
      end: '2016-12-31T23:59:59.000Z'
    });
  });
  it('2 years range', () => {
    expect(refineTimePeriod({ id: '2015/P2Y' }, { locale: 'en' })).to.deep.equal({
      id: '2015/P2Y',
      name: '2015 - 2016',
      start: '2015-01-01T00:00:00.000Z',
      end: '2016-12-31T23:59:59.000Z'
    });
  });
  it('2 years range on March, en', () => {
    expect(refineTimePeriod({ id: '2015-03/P2Y' }, { locale: 'en' })).to.deep.equal({
      id: '2015-03/P2Y',
      name: '2015-Mar - 2017-Feb',
      start: '2015-03-01T00:00:00.000Z',
      end: '2017-02-28T23:59:59.000Z'
    });
  });
  it('2 years range on March, fr', () => {
    expect(refineTimePeriod({ id: '2015-03/P2Y' }, { locale: 'fr', monthlyFormat: 'MMM YYYY' })).to.deep.equal({
      id: '2015-03/P2Y',
      name: 'mars 2015 - févr. 2017',
      start: '2015-03-01T00:00:00.000Z',
      end: '2017-02-28T23:59:59.000Z'
    });
  });
  it('4 years range on Jun 17, en', () => {
    expect(refineTimePeriod({ id: '2015-06-17/P4Y' }, { locale: 'en' })).to.deep.equal({
      id: '2015-06-17/P4Y',
      name: '2015-06-17 - 2019-06-16',
      start: '2015-06-17T00:00:00.000Z',
      end: '2019-06-16T23:59:59.000Z'
    });
  });
  it('4 years range on Jun 17 15h, en', () => {
    expect(refineTimePeriod({ id: '2015-06-17T15:00:00/P4Y' }, { locale: 'en' })).to.deep.equal({
      id: '2015-06-17T15:00:00/P4Y',
      name: '2015-06-17 15:00 - 2019-06-17 14:59',
      start: '2015-06-17T15:00:00.000Z',
      end: '2019-06-17T14:59:59.000Z'
    });
  });
  it('4 years range on Jun 17 15h26, en', () => {
    expect(refineTimePeriod({ id: '2015-06-17T15:26:00/P4Y' }, { locale: 'en' })).to.deep.equal({
      id: '2015-06-17T15:26:00/P4Y',
      name: '2015-06-17 15:26 - 2019-06-17 15:25',
      start: '2015-06-17T15:26:00.000Z',
      end: '2019-06-17T15:25:59.000Z'
    });
  });
  it('4 years range on Jun 17 15h26m46, en', () => {
    expect(refineTimePeriod({ id: '2015-06-17T15:26:46/P4Y' }, { locale: 'en' })).to.deep.equal({
      id: '2015-06-17T15:26:46/P4Y',
      name: '2015-06-17 15:26:46 - 2019-06-17 15:26:45',
      start: '2015-06-17T15:26:46.000Z',
      end: '2019-06-17T15:26:45.000Z'
    });
  });
  it('3 months range, en', () => {
    expect(refineTimePeriod({ id: '2015/P3M' }, { locale: 'en' })).to.deep.equal({
      id: '2015/P3M',
      name: '2015-Jan - 2015-Mar',
      start: '2015-01-01T00:00:00.000Z',
      end: '2015-03-31T23:59:59.000Z'
    });
  });
  it('3 months range, fr', () => {
    expect(refineTimePeriod({ id: '2015/P3M' }, { locale: 'fr', monthlyFormat: 'MMM YYYY' })).to.deep.equal({
      id: '2015/P3M',
      name: 'janv. - mars 2015',
      start: '2015-01-01T00:00:00.000Z',
      end: '2015-03-31T23:59:59.000Z'
    });
  });
  it('3 months range, fr, format = "YY Mo"', () => {
    expect(refineTimePeriod({ id: '2015/P3M' }, { locale: 'fr', monthlyFormat: 'YY Mo' })).to.deep.equal({
      id: '2015/P3M',
      name: '15 1 - 3o',
      start: '2015-01-01T00:00:00.000Z',
      end: '2015-03-31T23:59:59.000Z'
    });
  });
  it('3 months range, fr, format = "MMMM"', () => {
    expect(refineTimePeriod({ id: '2015/P3M' }, { locale: 'fr', monthlyFormat: 'MMMM' })).to.deep.equal({
      id: '2015/P3M',
      name: 'janvier - mars',
      start: '2015-01-01T00:00:00.000Z',
      end: '2015-03-31T23:59:59.000Z'
    });
  });
  it('19 months range, en', () => {
    expect(refineTimePeriod({ id: '2015/P19M' }, { locale: 'en' })).to.deep.equal({
      id: '2015/P19M',
      name: '2015-Jan - 2016-Jul',
      start: '2015-01-01T00:00:00.000Z',
      end: '2016-07-31T23:59:59.000Z'
    });
  });
  it('19 months range, fr', () => {
    expect(refineTimePeriod({ id: '2015/P19M' }, { locale: 'fr', monthlyFormat: 'MMM YYYY' })).to.deep.equal({
      id: '2015/P19M',
      name: 'janv. 2015 - juill. 2016',
      start: '2015-01-01T00:00:00.000Z',
      end: '2016-07-31T23:59:59.000Z'
    });
  });
  it('4 months range from April, en', () => {
    expect(refineTimePeriod({ id: '2015-04/P4M' }, { locale: 'en' })).to.deep.equal({
      id: '2015-04/P4M',
      name: '2015-Apr - 2015-Jul',
      start: '2015-04-01T00:00:00.000Z',
      end: '2015-07-31T23:59:59.000Z'
    });
  });
  it('4 months range from April, fr', () => {
    expect(refineTimePeriod({ id: '2015-04/P4M' }, { locale: 'fr', monthlyFormat: 'MMM YYYY' })).to.deep.equal({
      id: '2015-04/P4M',
      name: 'avr. - juill. 2015',
      start: '2015-04-01T00:00:00.000Z',
      end: '2015-07-31T23:59:59.000Z'
    });
  });
  it('2 months range from December, en', () => {
    expect(refineTimePeriod({ id: '2015-12/P2M' }, { locale: 'en' })).to.deep.equal({
      id: '2015-12/P2M',
      name: '2015-Dec - 2016-Jan',
      start: '2015-12-01T00:00:00.000Z',
      end: '2016-01-31T23:59:59.000Z'
    });
  });
  it('2 months range from December, fr', () => {
    expect(refineTimePeriod({ id: '2015-12/P2M' }, { locale: 'fr', monthlyFormat: 'MMM YYYY' })).to.deep.equal({
      id: '2015-12/P2M',
      name: 'déc. 2015 - janv. 2016',
      start: '2015-12-01T00:00:00.000Z',
      end: '2016-01-31T23:59:59.000Z'
    });
  });
  it('5 months range from Feb 2', () => {
    expect(refineTimePeriod({ id: '2015-02-02/P5M' }, { locale: 'en' })).to.deep.equal({
      id: '2015-02-02/P5M',
      name: '2015-02-02 - 2015-07-01',
      start: '2015-02-02T00:00:00.000Z',
      end: '2015-07-01T23:59:59.000Z'
    });
  });
  it('17 months range from Feb 2', () => {
    expect(refineTimePeriod({ id: '2015-02-02/P17M' }, { locale: 'en' })).to.deep.equal({
      id: '2015-02-02/P17M',
      name: '2015-02-02 - 2016-07-01',
      start: '2015-02-02T00:00:00.000Z',
      end: '2016-07-01T23:59:59.000Z'
    });
  });
  it('5 months range from Feb 2 18h', () => {
    expect(refineTimePeriod({ id: '2015-02-02T18:00:00/P5M' }, { locale: 'en' })).to.deep.equal({
      id: '2015-02-02T18:00:00/P5M',
      name: '2015-02-02 18:00 - 2015-07-02 17:59',
      start: '2015-02-02T18:00:00.000Z',
      end: '2015-07-02T17:59:59.000Z'
    });
  });
  it('17 months range from Feb 2', () => {
    expect(refineTimePeriod({ id: '2015-02-02T18:00:00/P17M' }, { locale: 'en' })).to.deep.equal({
      id: '2015-02-02T18:00:00/P17M',
      name: '2015-02-02 18:00 - 2016-07-02 17:59',
      start: '2015-02-02T18:00:00.000Z',
      end: '2016-07-02T17:59:59.000Z'
    });
  });
  it('5 months range from Feb 2 18h25', () => {
    expect(refineTimePeriod({ id: '2015-02-02T18:25:00/P5M' }, { locale: 'en' })).to.deep.equal({
      id: '2015-02-02T18:25:00/P5M',
      name: '2015-02-02 18:25 - 2015-07-02 18:24',
      start: '2015-02-02T18:25:00.000Z',
      end: '2015-07-02T18:24:59.000Z'
    });
  });
  it('5 months range from Feb 2 18h25m23', () => {
    expect(refineTimePeriod({ id: '2015-02-02T18:25:23/P5M' }, { locale: 'en' })).to.deep.equal({
      id: '2015-02-02T18:25:23/P5M',
      name: '2015-02-02 18:25:23 - 2015-07-02 18:25:22',
      start: '2015-02-02T18:25:23.000Z',
      end: '2015-07-02T18:25:22.000Z'
    });
  });
  it('15 days range from 2015', () => {
    expect(refineTimePeriod({ id: '2015/P15D' }, { locale: 'en' })).to.deep.equal({
      id: '2015/P15D',
      name: '2015-01-01 - 2015-01-15',
      start: '2015-01-01T00:00:00.000Z',
      end: '2015-01-15T23:59:59.000Z'
    });
  });
  it('366 days range from 2015', () => {
    expect(refineTimePeriod({ id: '2015/P366D' }, { locale: 'en' })).to.deep.equal({
      id: '2015/P366D',
      name: '2015-01-01 - 2016-01-01',
      start: '2015-01-01T00:00:00.000Z',
      end: '2016-01-01T23:59:59.000Z'
    });
  });
  it('31 days range from 2015 May', () => {
    expect(refineTimePeriod({ id: '2015-05/P31D' }, { locale: 'en' })).to.deep.equal({
      id: '2015-05/P31D',
      name: '2015-05-01 - 2015-05-31',
      start: '2015-05-01T00:00:00.000Z',
      end: '2015-05-31T23:59:59.000Z'
    });
  });
  it('33 days range from 2015 Dec', () => {
    expect(refineTimePeriod({ id: '2015-12/P33D' }, { locale: 'en' })).to.deep.equal({
      id: '2015-12/P33D',
      name: '2015-12-01 - 2016-01-02',
      start: '2015-12-01T00:00:00.000Z',
      end: '2016-01-02T23:59:59.000Z'
    });
  });
  it('31 days range from 2015 May 23', () => {
    expect(refineTimePeriod({ id: '2015-05-23/P31D' }, { locale: 'en' })).to.deep.equal({
      id: '2015-05-23/P31D',
      name: '2015-05-23 - 2015-06-22',
      start: '2015-05-23T00:00:00.000Z',
      end: '2015-06-22T23:59:59.000Z'
    });
  });
  it('33 days range from 2015 Dec 19', () => {
    expect(refineTimePeriod({ id: '2015-12-19/P33D' }, { locale: 'en' })).to.deep.equal({
      id: '2015-12-19/P33D',
      name: '2015-12-19 - 2016-01-20',
      start: '2015-12-19T00:00:00.000Z',
      end: '2016-01-20T23:59:59.000Z'
    });
  });
  it('31 days range from 2015 May 23 05h', () => {
    expect(refineTimePeriod({ id: '2015-05-23T05:00:00/P31D' }, { locale: 'en' })).to.deep.equal({
      id: '2015-05-23T05:00:00/P31D',
      name: '2015-05-23 05:00 - 2015-06-23 04:59',
      start: '2015-05-23T05:00:00.000Z',
      end: '2015-06-23T04:59:59.000Z'
    });
  });
  it('33 days range from 2015 Dec 19 12h', () => {
    expect(refineTimePeriod({ id: '2015-12-19T12:00:00/P33D' }, { locale: 'en' })).to.deep.equal({
      id: '2015-12-19T12:00:00/P33D',
      name: '2015-12-19 12:00 - 2016-01-21 11:59',
      start: '2015-12-19T12:00:00.000Z',
      end: '2016-01-21T11:59:59.000Z'
    });
  });
  it('31 days range from 2015 May 23 05h32', () => {
    expect(refineTimePeriod({ id: '2015-05-23T05:32:00/P31D' }, { locale: 'en' })).to.deep.equal({
      id: '2015-05-23T05:32:00/P31D',
      name: '2015-05-23 05:32 - 2015-06-23 05:31',
      start: '2015-05-23T05:32:00.000Z',
      end: '2015-06-23T05:31:59.000Z'
    });
  });
  it('33 days range from 2015 Dec 19 12h18', () => {
    expect(refineTimePeriod({ id: '2015-12-19T12:18:00/P33D' }, { locale: 'en' })).to.deep.equal({
      id: '2015-12-19T12:18:00/P33D',
      name: '2015-12-19 12:18 - 2016-01-21 12:17',
      start: '2015-12-19T12:18:00.000Z',
      end: '2016-01-21T12:17:59.000Z'
    });
  });
  it('31 days range from 2015 May 23 05h32m15', () => {
    expect(refineTimePeriod({ id: '2015-05-23T05:32:15/P31D' }, { locale: 'en' })).to.deep.equal({
      id: '2015-05-23T05:32:15/P31D',
      name: '2015-05-23 05:32:15 - 2015-06-23 05:32:14',
      start: '2015-05-23T05:32:15.000Z',
      end: '2015-06-23T05:32:14.000Z'
    });
  });
  it('33 days range from 2015 Dec 19 12h18m01', () => {
    expect(refineTimePeriod({ id: '2015-12-19T12:18:01/P33D' }, { locale: 'en' })).to.deep.equal({
      id: '2015-12-19T12:18:01/P33D',
      name: '2015-12-19 12:18:01 - 2016-01-21 12:18:00',
      start: '2015-12-19T12:18:01.000Z',
      end: '2016-01-21T12:18:00.000Z'
    });
  });
  it('4 hours range from 2016', () => {
    expect(refineTimePeriod({ id: '2016/PT4H' }, { locale: 'en' })).to.deep.equal({
      id: '2016/PT4H',
      name: '2016-01-01 00:00 - 03:59',
      start: '2016-01-01T00:00:00.000Z',
      end: '2016-01-01T03:59:59.000Z'
    });
  });
  it('32 hours range from 2016', () => {
    expect(refineTimePeriod({ id: '2016/PT32H' }, { locale: 'en' })).to.deep.equal({
      id: '2016/PT32H',
      name: '2016-01-01 00:00 - 2016-01-02 07:59',
      start: '2016-01-01T00:00:00.000Z',
      end: '2016-01-02T07:59:59.000Z'
    });
  });
  it('4 hours range from 2016 Jun', () => {
    expect(refineTimePeriod({ id: '2016-06/PT4H' }, { locale: 'en' })).to.deep.equal({
      id: '2016-06/PT4H',
      name: '2016-06-01 00:00 - 03:59',
      start: '2016-06-01T00:00:00.000Z',
      end: '2016-06-01T03:59:59.000Z'
    });
  });
  it('32 hours range from 2016 Jun', () => {
    expect(refineTimePeriod({ id: '2016-06/PT32H' }, { locale: 'en' })).to.deep.equal({
      id: '2016-06/PT32H',
      name: '2016-06-01 00:00 - 2016-06-02 07:59',
      start: '2016-06-01T00:00:00.000Z',
      end: '2016-06-02T07:59:59.000Z'
    });
  });
  it('4 hours range from 2016 Jun 28', () => {
    expect(refineTimePeriod({ id: '2016-06-28/PT4H' }, { locale: 'en' })).to.deep.equal({
      id: '2016-06-28/PT4H',
      name: '2016-06-28 00:00 - 03:59',
      start: '2016-06-28T00:00:00.000Z',
      end: '2016-06-28T03:59:59.000Z'
    });
  });
  it('32 hours range from 2016 Jun 28', () => {
    expect(refineTimePeriod({ id: '2016-06-28/PT32H' }, { locale: 'en' })).to.deep.equal({
      id: '2016-06-28/PT32H',
      name: '2016-06-28 00:00 - 2016-06-29 07:59',
      start: '2016-06-28T00:00:00.000Z',
      end: '2016-06-29T07:59:59.000Z'
    });
  });
  it('4 hours range from 2016 Jun 28 17h', () => {
    expect(refineTimePeriod({ id: '2016-06-28T17:00:00/PT4H' }, { locale: 'en' })).to.deep.equal({
      id: '2016-06-28T17:00:00/PT4H',
      name: '2016-06-28 17:00 - 20:59',
      start: '2016-06-28T17:00:00.000Z',
      end: '2016-06-28T20:59:59.000Z'
    });
  });
  it('32 hours range from 2016 Jun 28 17h', () => {
    expect(refineTimePeriod({ id: '2016-06-28T17:00:00/PT32H' }, { locale: 'en' })).to.deep.equal({
      id: '2016-06-28T17:00:00/PT32H',
      name: '2016-06-28 17:00 - 2016-06-30 00:59',
      start: '2016-06-28T17:00:00.000Z',
      end: '2016-06-30T00:59:59.000Z'
    });
  });
  it('4 hours range from 2016 Jun 28 17h38', () => {
    expect(refineTimePeriod({ id: '2016-06-28T17:38:00/PT4H' }, { locale: 'en' })).to.deep.equal({
      id: '2016-06-28T17:38:00/PT4H',
      name: '2016-06-28 17:38 - 21:37',
      start: '2016-06-28T17:38:00.000Z',
      end: '2016-06-28T21:37:59.000Z'
    });
  });
  it('32 hours range from 2016 Jun 28 17h38', () => {
    expect(refineTimePeriod({ id: '2016-06-28T17:38:00/PT32H' }, { locale: 'en' })).to.deep.equal({
      id: '2016-06-28T17:38:00/PT32H',
      name: '2016-06-28 17:38 - 2016-06-30 01:37',
      start: '2016-06-28T17:38:00.000Z',
      end: '2016-06-30T01:37:59.000Z'
    });
  });
  it('4 hours range from 2016 Jun 28 17h38m19', () => {
    expect(refineTimePeriod({ id: '2016-06-28T17:38:19/PT4H' }, { locale: 'en' })).to.deep.equal({
      id: '2016-06-28T17:38:19/PT4H',
      name: '2016-06-28 17:38:19 - 21:38:18',
      start: '2016-06-28T17:38:19.000Z',
      end: '2016-06-28T21:38:18.000Z'
    });
  });
  it('32 hours range from 2016 Jun 28 17h38m19', () => {
    expect(refineTimePeriod({ id: '2016-06-28T17:38:19/PT32H' }, { locale: 'en' })).to.deep.equal({
      id: '2016-06-28T17:38:19/PT32H',
      name: '2016-06-28 17:38:19 - 2016-06-30 01:38:18',
      start: '2016-06-28T17:38:19.000Z',
      end: '2016-06-30T01:38:18.000Z'
    });
  });
  it('17 minutes range from 2017', () => {
    expect(refineTimePeriod({ id: '2017/PT17M' }, { locale: 'en' })).to.deep.equal({
      id: '2017/PT17M',
      name: '2017-01-01 00:00 - 00:16',
      start: '2017-01-01T00:00:00.000Z',
      end: '2017-01-01T00:16:59.000Z'
    });
  });
  it('1446 minutes range from 2017', () => {
    expect(refineTimePeriod({ id: '2017/PT1446M' }, { locale: 'en' })).to.deep.equal({
      id: '2017/PT1446M',
      name: '2017-01-01 00:00 - 2017-01-02 00:05',
      start: '2017-01-01T00:00:00.000Z',
      end: '2017-01-02T00:05:59.000Z'
    });
  });
  it('17 minutes range from 2017 Aug', () => {
    expect(refineTimePeriod({ id: '2017-08/PT17M' }, { locale: 'en' })).to.deep.equal({
      id: '2017-08/PT17M',
      name: '2017-08-01 00:00 - 00:16',
      start: '2017-08-01T00:00:00.000Z',
      end: '2017-08-01T00:16:59.000Z'
    });
  });
  it('1446 minutes range from 2017 Aug', () => {
    expect(refineTimePeriod({ id: '2017-08/PT1446M' }, { locale: 'en' })).to.deep.equal({
      id: '2017-08/PT1446M',
      name: '2017-08-01 00:00 - 2017-08-02 00:05',
      start: '2017-08-01T00:00:00.000Z',
      end: '2017-08-02T00:05:59.000Z'
    });
  });
  it('17 minutes range from 2017 Aug 31', () => {
    expect(refineTimePeriod({ id: '2017-08-31/PT17M' }, { locale: 'en' })).to.deep.equal({
      id: '2017-08-31/PT17M',
      name: '2017-08-31 00:00 - 00:16',
      start: '2017-08-31T00:00:00.000Z',
      end: '2017-08-31T00:16:59.000Z'
    });
  });
  it('1446 minutes range from 2017 Aug 31', () => {
    expect(refineTimePeriod({ id: '2017-08-31/PT1446M' }, { locale: 'en' })).to.deep.equal({
      id: '2017-08-31/PT1446M',
      name: '2017-08-31 00:00 - 2017-09-01 00:05',
      start: '2017-08-31T00:00:00.000Z',
      end: '2017-09-01T00:05:59.000Z'
    });
  });
  it('17 minutes range from 2017 Aug 31 20h', () => {
    expect(refineTimePeriod({ id: '2017-08-31T20:00:00/PT17M' }, { locale: 'en' })).to.deep.equal({
      id: '2017-08-31T20:00:00/PT17M',
      name: '2017-08-31 20:00 - 20:16',
      start: '2017-08-31T20:00:00.000Z',
      end: '2017-08-31T20:16:59.000Z'
    });
  });
  it('1446 minutes range from 2017 Aug 31 20h', () => {
    expect(refineTimePeriod({ id: '2017-08-31T20:00:00/PT1446M' }, { locale: 'en' })).to.deep.equal({
      id: '2017-08-31T20:00:00/PT1446M',
      name: '2017-08-31 20:00 - 2017-09-01 20:05',
      start: '2017-08-31T20:00:00.000Z',
      end: '2017-09-01T20:05:59.000Z'
    });
  });
  it('17 minutes range from 2017 Aug 31 20h10', () => {
    expect(refineTimePeriod({ id: '2017-08-31T20:10:00/PT17M' }, { locale: 'en' })).to.deep.equal({
      id: '2017-08-31T20:10:00/PT17M',
      name: '2017-08-31 20:10 - 20:26',
      start: '2017-08-31T20:10:00.000Z',
      end: '2017-08-31T20:26:59.000Z'
    });
  });
  it('1446 minutes range from 2017 Aug 31 20h10', () => {
    expect(refineTimePeriod({ id: '2017-08-31T20:10:00/PT1446M' }, { locale: 'en' })).to.deep.equal({
      id: '2017-08-31T20:10:00/PT1446M',
      name: '2017-08-31 20:10 - 2017-09-01 20:15',
      start: '2017-08-31T20:10:00.000Z',
      end: '2017-09-01T20:15:59.000Z'
    });
  });
  it('17 minutes range from 2017 Aug 31 20h10m32', () => {
    expect(refineTimePeriod({ id: '2017-08-31T20:10:32/PT17M' }, { locale: 'en' })).to.deep.equal({
      id: '2017-08-31T20:10:32/PT17M',
      name: '2017-08-31 20:10:32 - 20:27:31',
      start: '2017-08-31T20:10:32.000Z',
      end: '2017-08-31T20:27:31.000Z'
    });
  });
  it('1446 minutes range from 2017 Aug 31 20h10m32', () => {
    expect(refineTimePeriod({ id: '2017-08-31T20:10:32/PT1446M' }, { locale: 'en' })).to.deep.equal({
      id: '2017-08-31T20:10:32/PT1446M',
      name: '2017-08-31 20:10:32 - 2017-09-01 20:16:31',
      start: '2017-08-31T20:10:32.000Z',
      end: '2017-09-01T20:16:31.000Z'
    });
  });
  it('25 seconds range from 2016', () => {
    expect(refineTimePeriod({ id: '2016/PT25S' }, { locale: 'en' })).to.deep.equal({
      id: '2016/PT25S',
      name: '2016-01-01 00:00:00 - 00:00:24',
      start: '2016-01-01T00:00:00.000Z',
      end: '2016-01-01T00:00:24.000Z'
    });
  });
  it('86.405 seconds range from 2016', () => {
    expect(refineTimePeriod({ id: '2016/PT86405S' }, { locale: 'en' })).to.deep.equal({
      id: '2016/PT86405S',
      name: '2016-01-01 00:00:00 - 2016-01-02 00:00:04',
      start: '2016-01-01T00:00:00.000Z',
      end: '2016-01-02T00:00:04.000Z'
    });
  });
  it('25 seconds range from 2016 Sep', () => {
    expect(refineTimePeriod({ id: '2016-09/PT25S' }, { locale: 'en' })).to.deep.equal({
      id: '2016-09/PT25S',
      name: '2016-09-01 00:00:00 - 00:00:24',
      start: '2016-09-01T00:00:00.000Z',
      end: '2016-09-01T00:00:24.000Z'
    });
  });
  it('86.405 seconds range from 2016 Sep', () => {
    expect(refineTimePeriod({ id: '2016-09/PT86405S' }, { locale: 'en' })).to.deep.equal({
      id: '2016-09/PT86405S',
      name: '2016-09-01 00:00:00 - 2016-09-02 00:00:04',
      start: '2016-09-01T00:00:00.000Z',
      end: '2016-09-02T00:00:04.000Z'
    });
  });
  it('25 seconds range from 2016 Sep 26', () => {
    expect(refineTimePeriod({ id: '2016-09-26/PT25S' }, { locale: 'en' })).to.deep.equal({
      id: '2016-09-26/PT25S',
      name: '2016-09-26 00:00:00 - 00:00:24',
      start: '2016-09-26T00:00:00.000Z',
      end: '2016-09-26T00:00:24.000Z'
    });
  });
  it('86.405 seconds range from 2016 Sep 26', () => {
    expect(refineTimePeriod({ id: '2016-09-26/PT86405S' }, { locale: 'en' })).to.deep.equal({
      id: '2016-09-26/PT86405S',
      name: '2016-09-26 00:00:00 - 2016-09-27 00:00:04',
      start: '2016-09-26T00:00:00.000Z',
      end: '2016-09-27T00:00:04.000Z'
    });
  });
  it('25 seconds range from 2016 Sep 26 14h', () => {
    expect(refineTimePeriod({ id: '2016-09-26T14:00:00/PT25S' }, { locale: 'en' })).to.deep.equal({
      id: '2016-09-26T14:00:00/PT25S',
      name: '2016-09-26 14:00:00 - 14:00:24',
      start: '2016-09-26T14:00:00.000Z',
      end: '2016-09-26T14:00:24.000Z'
    });
  });
  it('86.405 seconds range from 2016 Sep 26 14h', () => {
    expect(refineTimePeriod({ id: '2016-09-26T14:00:00/PT86405S' }, { locale: 'en' })).to.deep.equal({
      id: '2016-09-26T14:00:00/PT86405S',
      name: '2016-09-26 14:00:00 - 2016-09-27 14:00:04',
      start: '2016-09-26T14:00:00.000Z',
      end: '2016-09-27T14:00:04.000Z'
    });
  });
  it('25 seconds range from 2016 Sep 26 14h32', () => {
    expect(refineTimePeriod({ id: '2016-09-26T14:32:00/PT25S' }, { locale: 'en' })).to.deep.equal({
      id: '2016-09-26T14:32:00/PT25S',
      name: '2016-09-26 14:32:00 - 14:32:24',
      start: '2016-09-26T14:32:00.000Z',
      end: '2016-09-26T14:32:24.000Z'
    });
  });
  it('86.405 seconds range from 2016 Sep 26 14h32', () => {
    expect(refineTimePeriod({ id: '2016-09-26T14:32:00/PT86405S' }, { locale: 'en' })).to.deep.equal({
      id: '2016-09-26T14:32:00/PT86405S',
      name: '2016-09-26 14:32:00 - 2016-09-27 14:32:04',
      start: '2016-09-26T14:32:00.000Z',
      end: '2016-09-27T14:32:04.000Z'
    });
  });
  it('25 seconds range from 2016 Sep 26 14h32m10', () => {
    expect(refineTimePeriod({ id: '2016-09-26T14:32:10/PT25S' }, { locale: 'en' })).to.deep.equal({
      id: '2016-09-26T14:32:10/PT25S',
      name: '2016-09-26 14:32:10 - 14:32:34',
      start: '2016-09-26T14:32:10.000Z',
      end: '2016-09-26T14:32:34.000Z'
    });
  });
  it('86.405 seconds range from 2016 Sep 26 14h32m10', () => {
    expect(refineTimePeriod({ id: '2016-09-26T14:32:10/PT86405S' }, { locale: 'en' })).to.deep.equal({
      id: '2016-09-26T14:32:10/PT86405S',
      name: '2016-09-26 14:32:10 - 2016-09-27 14:32:14',
      start: '2016-09-26T14:32:10.000Z',
      end: '2016-09-27T14:32:14.000Z'
    });
  });
});
