import { expect } from 'chai';
import { refineMetadataCoordinates } from "../src/rules2/src/refineMetadataCoordinates";

describe('refineMetadataCoordinates', () => {
  it('test', () => {
    const coordinates = [
      {},
      {
          "MEASURE": "EMP",
          "ACTIVITY": "_T",
          "UNIT_MEASURE": "PS",
          "PRICE_BASE": "_Z",
          "ADJUSTMENT": "Y",
          "TRANSFORMATION": "N"
      },
      {
          "MEASURE": "POP",
          "ACTIVITY": "_Z",
          "UNIT_MEASURE": "PS",
          "PRICE_BASE": "_Z",
          "ADJUSTMENT": "Y",
          "TRANSFORMATION": "N"
      },
      {
          "MEASURE": "SAL",
          "ACTIVITY": "_T",
          "UNIT_MEASURE": "PS",
          "PRICE_BASE": "_Z",
          "ADJUSTMENT": "Y",
          "TRANSFORMATION": "N"
      },
      {
          "MEASURE": "SELF",
          "ACTIVITY": "_T",
          "UNIT_MEASURE": "PS",
          "PRICE_BASE": "_Z",
          "ADJUSTMENT": "Y",
          "TRANSFORMATION": "N"
      }
    ];
    const layoutIds = {
      header: ['TIME_PERIOD'],
      sections: ['ACTIVITY', 'ADJUSTMENT'],
      rows: ['MEASURE', 'UNIT_MEASURE', 'PRICE_BASE', 'TRANSFORMATION']
    };
    expect(refineMetadataCoordinates(coordinates, layoutIds, [])).to.deep.equal({
      cells: [],
      header: [],
      sections: [],
      rows: [
        {
          "MEASURE": "EMP",
          "ACTIVITY": "_T",
          "UNIT_MEASURE": "PS",
          "PRICE_BASE": "_Z",
          "ADJUSTMENT": "Y",
          "TRANSFORMATION": "N"
        },
        {
          "MEASURE": "POP",
          "ACTIVITY": "_Z",
          "UNIT_MEASURE": "PS",
          "PRICE_BASE": "_Z",
          "ADJUSTMENT": "Y",
          "TRANSFORMATION": "N"
        },
        {
          "MEASURE": "SAL",
          "ACTIVITY": "_T",
          "UNIT_MEASURE": "PS",
          "PRICE_BASE": "_Z",
          "ADJUSTMENT": "Y",
          "TRANSFORMATION": "N"
        },
        {
          "MEASURE": "SELF",
          "ACTIVITY": "_T",
          "UNIT_MEASURE": "PS",
          "PRICE_BASE": "_Z",
          "ADJUSTMENT": "Y",
          "TRANSFORMATION": "N"
        }
      ]
    });
  });
});