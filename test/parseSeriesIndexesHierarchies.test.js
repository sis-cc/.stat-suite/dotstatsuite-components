import { expect } from 'chai';
import { parseSeriesIndexesHierarchies } from '../src/rules2/src/table/parseSeriesIndexesHierarchies';

describe('parseSeriesIndexesHierarchies tests', () => {
	it('simple uniq flat', () => {
		const dimensions = [
			{
				id: 'D0',
				values: [
					{ id: 'v0' },
					{ id: 'v1' },
					{ id: 'v2' },
					{ id: 'v3' },
					{ id: 'v4' },
				]
			},
		];

		const seriesIndexes = [
			[2],
			[3],
			[4]
		];

		expect(parseSeriesIndexesHierarchies(seriesIndexes, dimensions)).to.deep.equal([
			{ indexes: [2], parentsIndexes: [[]], missingIndexes: [[]] },
			{ indexes: [3], parentsIndexes: [[]], missingIndexes: [[]] },
			{ indexes: [4], parentsIndexes: [[]], missingIndexes: [[]] },
		])
	});
	it('hierarchical in second dimension, no replicate missing parents', () => {
		const dimensions = [
			{
				id: 'D0',
				values: [
					{ id: 'v0' },
					{ id: 'v1' },
				]
			},
			{
				id: 'D1',
				values: [
					{ id: 'v0' },
					{ id: 'v1', parents: [0] },
					{ id: 'v2', parents: [0, 1] },
					{ id: 'v3', parents: [0, 1] },
					{ id: 'v4', parents: [0, 1] },
				]
			},
		];

		const seriesIndexes = [
			[0, 0],
			[0, 1],
			[0, 2],
			[0, 3],
			[0, 4],
			[1, 2],
			[1, 3],
			[1, 4],
		];

		expect(parseSeriesIndexesHierarchies(seriesIndexes, dimensions)).to.deep.equal([
			{ indexes: [0, 0], parentsIndexes: [[], []], missingIndexes: [[], []] },
			{ indexes: [0, 1], parentsIndexes: [[], [0]], missingIndexes: [[], []] },
			{ indexes: [0, 2], parentsIndexes: [[], [0, 1]], missingIndexes: [[], []] },
			{ indexes: [0, 3], parentsIndexes: [[], [0, 1]], missingIndexes: [[], []] },
			{ indexes: [0, 4], parentsIndexes: [[], [0, 1]], missingIndexes: [[], []] },
			{ indexes: [1, 2], parentsIndexes: [[], []], missingIndexes: [[], [0, 1]] },
			{ indexes: [1, 3], parentsIndexes: [[], [0, 1]], missingIndexes: [[], []] },
			{ indexes: [1, 4], parentsIndexes: [[], [0, 1]], missingIndexes: [[], []] },
		])
	});
	it('hierarchical in second dimension, replicate missing parents', () => {
		const dimensions = [
			{
				id: 'D0',
				values: [
					{ id: 'v0' },
					{ id: 'v1' },
				]
			},
			{
				id: 'D1',
				values: [
					{ id: 'v0' },
					{ id: 'v1', parents: [0] },
					{ id: 'v2', parents: [0, 1] },
					{ id: 'v3', parents: [0, 1] },
					{ id: 'v4', parents: [0, 1] },
				]
			},
		];

		const seriesIndexes = [
			[0, 0],
			[0, 1],
			[0, 2],
			[0, 3],
			[0, 4],
			[1, 2],
			[1, 3],
			[1, 4],
		];

		expect(parseSeriesIndexesHierarchies(seriesIndexes, dimensions, true)).to.deep.equal([
			{ indexes: [0, 0], parentsIndexes: [[], []], missingIndexes: [[], []] },
			{ indexes: [0, 1], parentsIndexes: [[], [0]], missingIndexes: [[], []] },
			{ indexes: [0, 2], parentsIndexes: [[], [0, 1]], missingIndexes: [[], []] },
			{ indexes: [0, 3], parentsIndexes: [[], [0, 1]], missingIndexes: [[], []] },
			{ indexes: [0, 4], parentsIndexes: [[], [0, 1]], missingIndexes: [[], []] },
			{ indexes: [1, 2], parentsIndexes: [[], []], missingIndexes: [[], [0, 1]] },
			{ indexes: [1, 3], parentsIndexes: [[], []], missingIndexes: [[], [0, 1]] },
			{ indexes: [1, 4], parentsIndexes: [[], []], missingIndexes: [[], [0, 1]] },
		])
	});
	it('two hierarchical dimensions, no replicate missing parents', () => {
		const dimensions = [
			{
				id: 'D0',
				values: [
					{ id: 'v0' },
					{ id: 'v1', parents: [0] },
					{ id: 'v2', parents: [0, 1] },
					{ id: 'v3', parents: [0, 1] },
				]
			},
			{
				id: 'D1',
				values: [
					{ id: 'v0' },
					{ id: 'v1', parents: [0] },
					{ id: 'v2', parents: [0, 1] },
					{ id: 'v3', parents: [0, 1] },
				]
			},
		];

		const seriesIndexes = [
			[0, 0],
			[0, 1],
			[0, 2],
			[0, 3],
			[2, 2],
			[2, 3],
			[3, 2],
			[3, 3],
		];

		expect(parseSeriesIndexesHierarchies(seriesIndexes, dimensions)).to.deep.equal([
			{ indexes: [0, 0], parentsIndexes: [[], []], missingIndexes: [[], []] },
			{ indexes: [0, 1], parentsIndexes: [[], [0]], missingIndexes: [[], []] },
			{ indexes: [0, 2], parentsIndexes: [[], [0, 1]], missingIndexes: [[], []] },
			{ indexes: [0, 3], parentsIndexes: [[], [0, 1]], missingIndexes: [[], []] },
			{ indexes: [2, 2], parentsIndexes: [[0], []], missingIndexes: [[1], [0, 1]] },
			{ indexes: [2, 3], parentsIndexes: [[0, 1], [0, 1]], missingIndexes: [[], []] },
			{ indexes: [3, 2], parentsIndexes: [[0, 1], []], missingIndexes: [[], [0, 1]] },
			{ indexes: [3, 3], parentsIndexes: [[0, 1], [0, 1]], missingIndexes: [[], []] },
		])
	});
	it('two hierarchical dimensions, replicate missing parents', () => {
		const dimensions = [
			{
				id: 'D0',
				values: [
					{ id: 'v0' },
					{ id: 'v1', parents: [0] },
					{ id: 'v2', parents: [0, 1] },
					{ id: 'v3', parents: [0, 1] },
				]
			},
			{
				id: 'D1',
				values: [
					{ id: 'v0' },
					{ id: 'v1', parents: [0] },
					{ id: 'v2', parents: [0, 1] },
					{ id: 'v3', parents: [0, 1] },
				]
			},
		];

		const seriesIndexes = [
			[0, 0],
			[0, 1],
			[0, 2],
			[0, 3],
			[2, 2],
			[2, 3],
			[3, 2],
			[3, 3],
		];

		expect(parseSeriesIndexesHierarchies(seriesIndexes, dimensions, true)).to.deep.equal([
			{ indexes: [0, 0], parentsIndexes: [[], []], missingIndexes: [[], []] },
			{ indexes: [0, 1], parentsIndexes: [[], [0]], missingIndexes: [[], []] },
			{ indexes: [0, 2], parentsIndexes: [[], [0, 1]], missingIndexes: [[], []] },
			{ indexes: [0, 3], parentsIndexes: [[], [0, 1]], missingIndexes: [[], []] },
			{ indexes: [2, 2], parentsIndexes: [[0], []], missingIndexes: [[1], [0, 1]] },
			{ indexes: [2, 3], parentsIndexes: [[0], []], missingIndexes: [[1], [0, 1]] },
			{ indexes: [3, 2], parentsIndexes: [[0], []], missingIndexes: [[1], [0, 1]] },
			{ indexes: [3, 3], parentsIndexes: [[0], []], missingIndexes: [[1], [0, 1]] },
		])
	});
	it('two hierarchical dimensions in combination, no replicate missing parents', () => {
		const dimensions = [
			{
				id: 'D0',
					values: [
						{ id: 'v0' },
						{ id: 'v1', parents: [0] },
						{ id: 'v2', parents: [0, 1] },
					]
			},
			{
				id: 'COMB',
				dimensions: [{
					id: 'D1',
					values: [
						{ id: 'v0' },
						{ id: 'v1', parents: [0] },
						{ id: 'v2', parents: [0, 1] },
						{ id: 'v3', parents: [0, 1] },
					]
				},{
					id: 'D2',
					values: [
						{ id: 'v0' },
						{ id: 'v1', parents: [0] },
						{ id: 'v2', parents: [0, 1] },
						{ id: 'v3', parents: [0, 1] },
					]
				}]
			}
		];

		const seriesIndexes = [
			[0, [0, 0]],
			[0, [0, 1]],
			[0, [0, 2]],
			[0, [0, 3]],
			[0, [2, 2]],
			[0, [2, 3]],
			[0, [3, 2]],
			[0, [3, 3]],
			[2, [0, 0]],
			[2, [0, 1]],
			[2, [0, 2]],
			[2, [0, 3]],
			[2, [2, 2]],
			[2, [2, 3]],
			[2, [3, 2]],
			[2, [3, 3]],
		];

		expect(parseSeriesIndexesHierarchies(seriesIndexes, dimensions)).to.deep.equal([
			{ indexes: [0, [0, 0]], parentsIndexes: [[], [[], []]], missingIndexes: [[], [[], []]] },
			{ indexes: [0, [0, 1]], parentsIndexes: [[], [[], [0]]], missingIndexes: [[], [[], []]] },
			{ indexes: [0, [0, 2]], parentsIndexes: [[], [[], [0, 1]]], missingIndexes: [[], [[], []]] },
			{ indexes: [0, [0, 3]], parentsIndexes: [[], [[], [0, 1]]], missingIndexes: [[], [[], []]] },
			{ indexes: [0, [2, 2]], parentsIndexes: [[], [[0], []]], missingIndexes: [[], [[1], [0, 1]]] },
			{ indexes: [0, [2, 3]], parentsIndexes: [[], [[0, 1], [0, 1]]], missingIndexes: [[], [[], []]] },
			{ indexes: [0, [3, 2]], parentsIndexes: [[], [[0, 1], []]], missingIndexes: [[], [[], [0, 1]]] },
			{ indexes: [0, [3, 3]], parentsIndexes: [[], [[0, 1], [0, 1]]], missingIndexes: [[], [[], []]] },
			{ indexes: [2, [0, 0]], parentsIndexes: [[0], [[], []]], missingIndexes: [[1], [[], []]] },
			{ indexes: [2, [0, 1]], parentsIndexes: [[0, 1], [[], [0]]], missingIndexes: [[], [[], []]] },
			{ indexes: [2, [0, 2]], parentsIndexes: [[0, 1], [[], [0, 1]]], missingIndexes: [[], [[], []]] },
			{ indexes: [2, [0, 3]], parentsIndexes: [[0, 1], [[], [0, 1]]], missingIndexes: [[], [[], []]] },
			{ indexes: [2, [2, 2]], parentsIndexes: [[0, 1], [[0], []]], missingIndexes: [[], [[1], [0, 1]]] },
			{ indexes: [2, [2, 3]], parentsIndexes: [[0, 1], [[0, 1], [0, 1]]], missingIndexes: [[], [[], []]] },
			{ indexes: [2, [3, 2]], parentsIndexes: [[0, 1], [[0, 1], []]], missingIndexes: [[], [[], [0, 1]]] },
			{ indexes: [2, [3, 3]], parentsIndexes: [[0, 1], [[0, 1], [0, 1]]], missingIndexes: [[], [[], []]] },
		])
	});
	it('two hierarchical dimensions in combination, replicate missing parents', () => {
		const dimensions = [
			{
				id: 'D0',
					values: [
						{ id: 'v0' },
						{ id: 'v1', parents: [0] },
						{ id: 'v2', parents: [0, 1] },
					]
			},
			{
				id: 'COMB',
				dimensions: [{
					id: 'D1',
					values: [
						{ id: 'v0' },
						{ id: 'v1', parents: [0] },
						{ id: 'v2', parents: [0, 1] },
						{ id: 'v3', parents: [0, 1] },
					]
				},{
					id: 'D2',
					values: [
						{ id: 'v0' },
						{ id: 'v1', parents: [0] },
						{ id: 'v2', parents: [0, 1] },
						{ id: 'v3', parents: [0, 1] },
					]
				}]
			}
		];

		const seriesIndexes = [
			[0, [0, 0]],
			[0, [0, 1]],
			[0, [0, 2]],
			[0, [0, 3]],
			[0, [2, 2]],
			[0, [2, 3]],
			[0, [3, 2]],
			[0, [3, 3]],
			[2, [0, 0]],
			[2, [0, 1]],
			[2, [0, 2]],
			[2, [0, 3]],
			[2, [2, 2]],
			[2, [2, 3]],
			[2, [3, 2]],
			[2, [3, 3]],
		];

		expect(parseSeriesIndexesHierarchies(seriesIndexes, dimensions, true)).to.deep.equal([
			{ indexes: [0, [0, 0]], parentsIndexes: [[], [[], []]], missingIndexes: [[], [[], []]] },
			{ indexes: [0, [0, 1]], parentsIndexes: [[], [[], [0]]], missingIndexes: [[], [[], []]] },
			{ indexes: [0, [0, 2]], parentsIndexes: [[], [[], [0, 1]]], missingIndexes: [[], [[], []]] },
			{ indexes: [0, [0, 3]], parentsIndexes: [[], [[], [0, 1]]], missingIndexes: [[], [[], []]] },
			{ indexes: [0, [2, 2]], parentsIndexes: [[], [[0], []]], missingIndexes: [[], [[1], [0, 1]]] },
			{ indexes: [0, [2, 3]], parentsIndexes: [[], [[0], []]], missingIndexes: [[], [[1], [0, 1]]] },
			{ indexes: [0, [3, 2]], parentsIndexes: [[], [[0], []]], missingIndexes: [[], [[1], [0, 1]]] },
			{ indexes: [0, [3, 3]], parentsIndexes: [[], [[0], []]], missingIndexes: [[], [[1], [0, 1]]] },
			{ indexes: [2, [0, 0]], parentsIndexes: [[0], [[], []]], missingIndexes: [[1], [[], []]] },
			{ indexes: [2, [0, 1]], parentsIndexes: [[0], [[], [0]]], missingIndexes: [[1], [[], []]] },
			{ indexes: [2, [0, 2]], parentsIndexes: [[0], [[], [0, 1]]], missingIndexes: [[1], [[], []]] },
			{ indexes: [2, [0, 3]], parentsIndexes: [[0], [[], [0, 1]]], missingIndexes: [[1], [[], []]] },
			{ indexes: [2, [2, 2]], parentsIndexes: [[0], [[0], []]], missingIndexes: [[1], [[1], [0, 1]]] },
			{ indexes: [2, [2, 3]], parentsIndexes: [[0], [[0], []]], missingIndexes: [[1], [[1], [0, 1]]] },
			{ indexes: [2, [3, 2]], parentsIndexes: [[0], [[0], []]], missingIndexes: [[1], [[1], [0, 1]]] },
			{ indexes: [2, [3, 3]], parentsIndexes: [[0], [[0], []]], missingIndexes: [[1], [[1], [0, 1]]] },
		])
	});
});
