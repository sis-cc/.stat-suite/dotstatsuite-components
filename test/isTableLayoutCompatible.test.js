import { expect } from 'chai';
import { isSharedLayoutCompatible } from '../src/rules/src/layout';

const sdmxData = {
  structure: {
    dimensions: {
      observation: [
        { id: 'd0', values: [{ id: 'v1' }, { id: 'v2' }] },
        { id: 'd1', values: [{ id: 'v1' }, { id: 'v2' }, { id: 'v3' }] },
        { id: 'd2', values: [{ id: 'v1' }, { id: 'v2' }] },
        { id: 'd3', values: [{ id: 'v1' }] },
        { id: 'd4', values: [] },
      ]
    },
  }
}
const sdmxData2 = {
  structure: {
    dimensions: {
      observation: [
        { id: 'd0', values: [{ id: 'v1' }, { id: 'v2' }] },
        { id: 'd1', values: [{ id: 'v1' }, { id: 'v2' }, { id: 'v3' }] },
        { id: 'd2', values: [{ id: 'v1' }, { id: 'v2' }] },
        { id: 'd3', values: [{ id: 'v1' }] },
        { id: 'd4', values: [] },
      ]
    },
    annotations: [
      {
        title: "d6:d1,d3,d4",
        type: "COMBINED_CONCEPTS",
        text: "d6:Combined unit of measure",
        texts: {
          en: "d6:Combined unit of measure"
        }
      }
    ]
  }
}

describe('isSharedLayoutCompatible', () => {
  it('table compatible test', () => {
    const layoutIds = { rows: ['d0'], sections: ['d1'], header: ['d2'] };
    expect(isSharedLayoutCompatible(sdmxData, { type: 'table', config: { table: { layoutIds } } })).to.equal(true);
  });
  it('table compatible test combined', () => {
    const layoutIds = { rows: ['d0'], sections: ['d6'], header: ['d2'] };
    expect(isSharedLayoutCompatible(sdmxData2, { type: 'table', config: { table: { layoutIds, locale: 'en' } } })).to.equal(true);
  });
  it('table incompatible absent dimension in multi dims test', () => {
    const layoutIds = { rows: ['d0', 'd3'], sections: ['d1'], header: ['d2'] };
    expect(isSharedLayoutCompatible(sdmxData, { type: 'table', config: { table: { layoutIds } } })).to.equal(false);
  });
  it('table incompatible absent dimension in layout test', () => {
    const layoutIds = { rows: [], sections: ['d1'], header: ['d2'] };
    expect(isSharedLayoutCompatible(sdmxData, { type: 'table', config: { table: { layoutIds } } })).to.equal(false);
  });
  it('scatter chart compatible test', () => {
    const chartDimension = { id: 'd0', xId: 'v1', yId: 'v2' };
    expect(isSharedLayoutCompatible(sdmxData, { type: 'ScatterChart', config: { chart: { chartDimension } } })).to.equal(true);
  });
  it('scatter chart incompatible  absent axis value test', () => {
    const chartDimension = { id: 'd0', xId: 'v1', yId: 'v3' };
    expect(isSharedLayoutCompatible(sdmxData, { type: 'ScatterChart', config: { chart: { chartDimension } } })).to.equal(false);
  });
  it('scatter chart incompatible wrong dimension test', () => {
    const chartDimension = { id: 'd3', xId: 'v1', yId: 'v2' };
    expect(isSharedLayoutCompatible(sdmxData, { type: 'ScatterChart', config: { chart: { chartDimension } } })).to.equal(false);
  });
  it('h symbol chart compatible test', () => {
    const chartDimension = { id: 'd1', serie: [{ id: 'v1' }, { id: 'v2' }, { id: 'v3'}] };
    expect(isSharedLayoutCompatible(sdmxData, { type: 'HorizontalSymbolChart', config: { chart: { chartDimension } } })).to.equal(true);
  });
  it('h symbol chart incompatible absent symbol value test', () => {
    const chartDimension = { id: 'd0', serie: [{ id: 'v1' }, { id: 'v2' }, { id: 'v3'}] };
    expect(isSharedLayoutCompatible(sdmxData, { type: 'HorizontalSymbolChart', config: { chart: { chartDimension } } })).to.equal(false);
  });
  it('h symbol chart incompatible absent dimension value test', () => {
    const chartDimension = { id: 'd5', serie: [{ id: 'v1' }, { id: 'v2' }, { id: 'v3'}] };
    expect(isSharedLayoutCompatible(sdmxData, { type: 'HorizontalSymbolChart', config: { chart: { chartDimension } } })).to.equal(false);
  });
  it('v symbol chart compatible test', () => {
    const chartDimension = { id: 'd1', serie: [{ id: 'v1' }, { id: 'v2' }, { id: 'v3'}] };
    expect(isSharedLayoutCompatible(sdmxData, { type: 'VerticalSymbolChart', config: { chart: { chartDimension } } })).to.equal(true);
  });
  it('v symbol chart incompatible absent symbol value test', () => {
    const chartDimension = { id: 'd0', serie: [{ id: 'v1' }, { id: 'v2' }, { id: 'v3'}] };
    expect(isSharedLayoutCompatible(sdmxData, { type: 'VerticalSymbolChart', config: { chart: { chartDimension } } })).to.equal(false);
  });
  it('v symbol chart incompatible absent dimension value test', () => {
    const chartDimension = { id: 'd5', serie: [{ id: 'v1' }, { id: 'v2' }, { id: 'v3'}] };
    expect(isSharedLayoutCompatible(sdmxData, { type: 'VerticalSymbolChart', config: { chart: { chartDimension } } })).to.equal(false);
  });
  it('stacked chart compatible test', () => {
    const chartDimension = { id: 'd0' };
    expect(isSharedLayoutCompatible(sdmxData, { type: 'StackedBarChart', config: { chart: { chartDimension } } })).to.equal(true);
  });
  it('stacked chart incompatible test', () => {
    const chartDimension = { id: 'd4' };
    expect(isSharedLayoutCompatible(sdmxData, { type: 'StackedBarChart', config: { chart: { chartDimension } } })).to.equal(false);
  });
});
