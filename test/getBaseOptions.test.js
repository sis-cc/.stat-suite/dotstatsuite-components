import { expect } from 'chai';
import { getBaseOptions } from '../src/rules/src/chart/getBaseOptions';

describe('getBaseOptions tests', () => {
  it('blank test', () => {
    expect(getBaseOptions(null, null)).to.deep.equal({});
  });
  it('default bar test', () => {
    expect(getBaseOptions('BarChart', {})).to.deep.equal({
      minDisplayWidth: 300,
      minDisplayHeight: 100,
      padding: { top: 20 },
      innerPadding: { left: 20, right: 20, bottom: 10 }
    });
  });
  it('default row test', () => {
    expect(getBaseOptions('RowChart', {})).to.deep.equal({
      minDisplayWidth: 100,
      minDisplayHeight: 300,
      padding: { top: 10, left: 10, right: 20 }
    });
  });
  it('default scatter test', () => {
    expect(getBaseOptions('ScatterChart', {})).to.deep.equal({
      minDisplayWidth: 100,
      minDisplayHeight: 100,
      padding: { top: 40, right: 10 }
    });
  });
  it('default timeline test', () => {
    expect(getBaseOptions('TimelineChart', {})).to.deep.equal({
      minDisplayWidth: 100,
      minDisplayHeight: 300,
      isAnnotated:false,
      padding: { top: 20 },
      innerPadding: { left: 40, right: 40 }
    });
  });
  it('default horizontal symbol test', () => {
    expect(getBaseOptions('HorizontalSymbolChart', {})).to.deep.equal({
      minDisplayWidth: 100,
      minDisplayHeight: 300,
      padding: { top: 10, left: 10, right: 20 },
    });
  });
  it('default vertical symbol test', () => {
    expect(getBaseOptions('VerticalSymbolChart', {})).to.deep.equal({
      minDisplayWidth: 300,
      minDisplayHeight: 100,
      padding: { top: 20 },
      innerPadding: { left: 20, right: 20, bottom: 10 }
    });
  });
  it('default stacked test', () => {
    expect(getBaseOptions('StackedBarChart', {})).to.deep.equal({
      minDisplayWidth: 300,
      minDisplayHeight: 100,
      padding: { top: 20 },
      innerPadding: { left: 20, right: 20, bottom: 10 }
    });
  });
  it('customization test', () => {
    expect(getBaseOptions('StackedBarChart', {
      base: {
        minDisplayHeight: 300,
        customOptions: { deep: 'test' },
        innerPadding: { top: 15, left: 0 }
      }
    })).to.deep.equal({
      customOptions: { deep: 'test' },
      minDisplayWidth: 300,
      minDisplayHeight: 300,
      padding: { top: 20 },
      innerPadding: { left: 0, right: 20, bottom: 10, top: 15 }
    });
  });
});
