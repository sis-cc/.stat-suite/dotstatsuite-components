import { expect } from 'chai';
import { parseCombinations } from '../src/rules2/src/';

describe('parseCombinations tests', () => {
  it('complete case', () => {
    const combinations = [
      { id: 'COMB1', concepts: ['DIM1', 'DIM2'] },
      { id: 'COMB2', concepts: ['DIM3', 'DIM4'] },
      { id: 'COMB3', concepts: ['ATTR1', 'ATTR2'] },
      { id: 'COMB4', concepts: ['ATTR3', 'ATTR4'] },
      { id: 'COMB5', concepts: ['DIM1', 'ATTR4'] },
      { id: 'COMB6', concepts: ['DIM5', 'ATTR5'] },
      { id: 'COMB7', concepts: ['ATTR5', 'ATTR6'] },
      { id: 'COMB8', concepts: ['DIM5', 'DIM6'] },
      { id: 'COMB9', concepts: ['DIM1', 'ATTR5'] },
      { id: 'COMB10', concepts: ['DIM4', 'ATTR5'] },
      { id: 'COMB11', concepts: ['DIM4', 'DIM7'] },
      { id: 'COMB12', concepts: ['DIM4', 'ATTR7'] },
      { id: 'COMB13', concepts: ['DIM7', 'ATTR7'] },
      { id: 'COMB14', concepts: ['DIM3', 'ATTR5', 'ATTR8'] },
    ];

    const dimensions = [
      { id: 'DIM1', header: true, values: [{ id: 'v1' }] },
      { id: 'DIM2', header: true, values: [{ id: 'v1' }] },
      { id: 'DIM3', header: false, values: [{ id: 'v1' }, { id: 'v2' }] },
      { id: 'DIM4', header: false, values: [{ id: 'v1' }, { id: 'v2' }] },
      {
        id: 'DIM5',
        header: false,
        values: [{ id: '_Z' }, { id: 'v2', display: false }],
      },
      {
        id: 'DIM6',
        header: false,
        values: [
          { id: 'v1', display: false },
          { id: 'v2', display: false },
        ],
      },
      {
        id: 'DIM7',
        header: false,
        values: [],
      },
    ];

    const attributes = [
      { id: 'ATTR1', header: true, relationship: [], values: [{ id: 'v' }] },
      {
        id: 'ATTR2',
        header: true,
        relationship: ['DIM1'],
        values: [{ id: 'v' }],
      },
      { id: 'ATTR3', header: true, relationship: [], values: [{ id: 'v' }] },
      {
        id: 'ATTR4',
        series: true,
        relationship: ['DIM3', 'DIM4'],
        values: [{ id: 'v' }],
      },
      {
        id: 'ATTR5',
        series: true,
        display: false,
        relationship: ['DIM5'],
        values: [{ id: 'v' }],
      },
      {
        id: 'ATTR6',
        series: true,
        relationship: [],
        values: [{ id: '0', display: false }, { id: '_Z' }],
      },
      {
        id: 'ATTR7',
        series: true,
        relationship: [],
        values: [],
      },
      {
        id: 'ATTR8',
        series: true,
        relationship: ['DIM5'],
        values: [{ id: 'v' }],
      },
    ];

    expect(
      parseCombinations(combinations, attributes, dimensions),
    ).to.deep.equal([
      { id: 'COMB1', concepts: ['DIM1', 'DIM2'], header: true, display: true },
      {
        id: 'COMB2',
        concepts: ['DIM3', 'DIM4'],
        series: true,
        relationship: ['DIM3', 'DIM4'],
        display: true,
      },
      {
        id: 'COMB3',
        concepts: ['ATTR1', 'ATTR2'],
        header: true,
        display: true,
      },
      {
        id: 'COMB4',
        concepts: ['ATTR3', 'ATTR4'],
        series: true,
        relationship: ['DIM3', 'DIM4'],
        display: true,
      },
      {
        id: 'COMB5',
        concepts: ['DIM1', 'ATTR4'],
        series: true,
        relationship: ['DIM3', 'DIM4'],
        display: true,
      },
      {
        id: 'COMB6',
        concepts: ['DIM5', 'ATTR5'],
        series: true,
        relationship: ['DIM5'],
        display: true,
      },
      {
        id: 'COMB7',
        concepts: ['ATTR5', 'ATTR6'],
        header: true,
        display: false,
      },
      {
        id: 'COMB8',
        concepts: ['DIM5', 'DIM6'],
        series: true,
        relationship: ['DIM5', 'DIM6'],
        display: true,
      },
      { id: 'COMB9', concepts: ['DIM1', 'ATTR5'], header: true, display: true },
      {
        id: 'COMB10',
        concepts: ['DIM4', 'ATTR5'],
        series: true,
        relationship: ['DIM4'],
        display: true,
      },
      {
        id: 'COMB11',
        concepts: ['DIM4'],
        relationship: ['DIM4'],
        series: true,
        display: true
      },
      {
        id: 'COMB12',
        concepts: ['DIM4'],
        relationship: ['DIM4'],
        series: true,
        display: true
      },
      {
        id: 'COMB14',
        concepts: ['DIM3', 'ATTR5', 'ATTR8'],
        relationship: ['DIM3', 'DIM5'],
        display: true,
        series: true,
      },
    ]);
  });
});
