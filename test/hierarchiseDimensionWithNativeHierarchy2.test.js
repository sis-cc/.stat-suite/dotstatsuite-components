import { expect } from 'chai';
import { hierarchiseDimensionWithNativeHierarchy } from '../src/rules2/src/hierarchiseDimensionWithNativeHierarchy2';

describe('hierarchiseDimensionWithNativeHierarchy tests', () => {
  it('basic test', () => {
    const dimension = {
      id: 'test',
      values: [
        { id: 'FRA', parent: 'EA', __indexPosition: 101, isSelected: true },
        { id: 'GER', parent: 'EA', __indexPosition: 100, isSelected: true },
        { id: 'EA', parent: 'W', __indexPosition: 30, isSelected: true },
        { id: 'A', parent: 'W', __indexPosition: 20, isSelected: true },
        { id: 'W', parent: undefined, __indexPosition: 5, isSelected: true },
        { id: 'CHI', parent: undefined, __indexPosition: 0, isSelected: true }
      ]
    };
    
    const expected = {
      id: 'test',
      values: [
        { id: 'CHI', parent: undefined, parents: [], __indexPosition: 0, isSelected: true },
        { id: 'W', parent: undefined, parents: [], __indexPosition: 1, isSelected: true },
        { id: 'A', parent: 'W', parents: [1], __indexPosition: 2, isSelected: true },
        { id: 'EA', parent: 'W', parents: [1], __indexPosition: 3, isSelected: true },
        { id: 'GER', parent: 'EA', parents: [1, 3], __indexPosition: 4, isSelected: true },
        { id: 'FRA', parent: 'EA', parents: [1, 3], __indexPosition: 5, isSelected: true }
      ]
    };

    expect(hierarchiseDimensionWithNativeHierarchy(dimension)).to.deep.equal(expected);
  });
  it ('missing parent test', () => {
    const dimension = {
      id: 'test',
      values: [
        { id: 'FRA', parent: 'EA', __indexPosition: 101, isSelected: true },
        { id: 'GER', parent: 'EA', __indexPosition: 100, isSelected: true },
        { id: 'EA', parent: 'W', __indexPosition: 30, isSelected: true },
        { id: 'A', parent: 'W', __indexPosition: 20, isSelected: true },
        { id: 'CHI', parent: undefined, __indexPosition: 0, isSelected: true }
      ]
    };
    
    const expected = {
      id: 'test',
      values: [
        { id: 'CHI', parent: undefined, parents: [], __indexPosition: 0, isSelected: true },
        { id: 'A', parent: undefined, parents: [], __indexPosition: 1, isSelected: true },
        { id: 'EA', parent: undefined, parents: [], __indexPosition: 2, isSelected: true },
        { id: 'GER', parent: 'EA', parents: [2], __indexPosition: 3, isSelected: true },
        { id: 'FRA', parent: 'EA', parents: [2], __indexPosition: 4, isSelected: true }
      ]
    };

    expect(hierarchiseDimensionWithNativeHierarchy(dimension)).to.deep.equal(expected);
  });
  it ('empty id', () => {
    const dimension = {
      id: 'test',
      values: [
        { id: '', parent: '', __indexPosition: 101, isSelected: true },
        { id: 'GER', parent: 'EA', __indexPosition: 100, isSelected: true },
        { id: 'EA', parent: 'W', __indexPosition: 30, isSelected: true },
        { id: 'A', parent: 'W', __indexPosition: 20, isSelected: true },
        { id: 'CHI', parent: undefined, __indexPosition: 0, isSelected: true }
      ]
    };
    
    const expected = {
      id: 'test',
      values: [
        { id: 'CHI', parent: undefined, parents: [], __indexPosition: 0, isSelected: true },
        { id: 'A', parent: undefined, parents: [], __indexPosition: 1, isSelected: true },
        { id: 'EA', parent: undefined, parents: [], __indexPosition: 2, isSelected: true },
        { id: 'GER', parent: 'EA', parents: [2], __indexPosition: 3, isSelected: true },
        { id: '', parent: undefined, parents: [], __indexPosition: 4, isSelected: true }
      ]
    };

    expect(hierarchiseDimensionWithNativeHierarchy(dimension)).to.deep.equal(expected);
  });
  it('non selected parent', () => {
    const dimension = {
      id: 'test',
      values: [
        { id: 'FRA', parent: 'EA', __indexPosition: 101, isSelected: true },
        { id: 'GER', parent: 'EA', __indexPosition: 100, isSelected: true },
        { id: 'EA', parent: 'W', __indexPosition: 30, isSelected: false },
        { id: 'A', parent: 'W', __indexPosition: 20, isSelected: true },
        { id: 'W', parent: undefined, __indexPosition: 5, isSelected: true },
        { id: 'CHI', parent: undefined, __indexPosition: 0, isSelected: true }
      ]
    };
    
    const expected = {
      id: 'test',
      values: [
        { id: 'CHI', parent: undefined, parents: [], __indexPosition: 0, isSelected: true },
        { id: 'W', parent: undefined, parents: [], __indexPosition: 1, isSelected: true },
        { id: 'A', parent: 'W', parents: [1], __indexPosition: 2, isSelected: true },
        { id: 'EA', parent: 'W', parents: [1], __indexPosition: 3, isSelected: false },
        { id: 'GER', parent: 'EA', parents: [1], __indexPosition: 4, isSelected: true },
        { id: 'FRA', parent: 'EA', parents: [1], __indexPosition: 5, isSelected: true }
      ]
    };

    expect(hierarchiseDimensionWithNativeHierarchy(dimension)).to.deep.equal(expected);
  })
});
