import { expect } from 'chai';
import { getSerieDataWithMissingLines, getSerieDataWithoutMissingLines } from '../src/rules2/src/table/getLayoutData2';

describe('getLayoutData 2 tests', () => {
  it('getSerieDataWithMissingLines test 1', () => {
    const dimensions = [
			{
				id: 'D0',
				values: [
					{ id: 'v0' },
					{ id: 'v1' },
					{ id: 'v2' },
					{ id: 'v3' },
					{ id: 'v4' },
				]
			},
			{
				id: 'D1',
				values: [
					{ id: 'W', isSelected: true },
					{ id: 'NA', parentsIndexes: [0], isSelected: false },
					{ id: 'A', parentsIndexes: [0, 1], isSelected: true },
					{ id: 'USA', parentsIndexes: [0, 1, 2], isSelected: true },
					{ id: 'CAN', parentsIndexes: [0, 1, 2], isSelected: true },
					{ id: 'MEX', parentsIndexes: [0, 1, 2], isSelected: true }
				]
			}
		];

    const serie = { indexes: [0, 3], parentsIndexes: [[], []], missingIndexes: [[], [0, 2]] };
    expect(getSerieDataWithMissingLines(serie, dimensions, {}, {}, {}, {})).to.deep.equal([
      {
        data: [{ dimension: { id: 'D0' }, value: { id: 'v0', parents: [], missingParents: [] } }, { dimension: { id: 'D1' }, value: { id: 'W', parents: [], missingParents: [] } }],
        key: 'D0=v0:D1=W',
        flags: [],
        sideProps: null,
				isEmpty: true,
      },
      {
        data: [{ dimension: { id: 'D0' }, value: { id: 'v0', parents: [], missingParents: [] } }, { dimension: { id: 'D1' }, value: { id: 'A', parents: [0], missingParents: [] } }],
        key: 'D0=v0:D1=A',
        flags: [],
        sideProps: null,
				isEmpty: true
      },
      {
        data: [{ dimension: { id: 'D0' }, value: { id: 'v0', parents: [], missingParents: [] } }, { dimension: { id: 'D1' }, value: { id: 'USA', parents: [0, 2], missingParents: [] } }],
        coordinates: { D0: 'v0', D1: 'USA' },
        key: 'D0=v0:D1=USA',
        flags: [],
        sideProps: null,
      }
    ]);
  });
  it('getSerieDataWithMissingLines test 2', () => {
  	const dimensions = [
  		{
				id: 'D0',
				values: [
					{ id: 'W' },
					{ id: 'NA', parentsIndexes: [0] },
					{ id: 'A', parentsIndexes: [0, 1] },
					{ id: 'USA', parentsIndexes: [0, 1, 2] },
					{ id: 'CAN', parentsIndexes: [0, 1, 2] },
					{ id: 'MEX', parentsIndexes: [0, 1, 2] }
				]
			},
			{
				id: 'COMB0',
				concepts: ['D1', 'A0', 'D2'],
				dimensions: [
					{
						id: 'D1',
						values: [
							{ id: 'D1ROOT' },
							{ id: 'D1ROOT>CHILD' },
							{ id: 'D1ROOT>CHILD>CHILD' },
						]
					},
					{
						id: 'D2',
						values: [
							{ id: 'D2ROOT' },
							{ id: 'D2ROOT>CHILD' },
							{ id: 'D2ROOT>CHILD>CHILD' },
						]
					}
				]
			},
			{
				id: 'COMB1',
				concepts: ['D3', 'D4'],
				dimensions: [
					{
						id: 'D3',
						values: [
							{ id: 'D3ROOT' },
							{ id: 'D3ROOT>CHILD' },
							{ id: 'D3ROOT>CHILD>CHILD' },
						]
					},
					{
						id: 'D4',
						values: [
							{ id: 'D4ROOT' },
							{ id: 'D4ROOT>CHILD' },
							{ id: 'D4ROOT>CHILD>CHILD' },
						]
					}
				]
			},
  	];

  	const attributesSeries = {
  		'D1=D1ROOT>CHILD>CHILD:D2=D2ROOT>CHILD>CHILD': {
  			A0: { id: 'A0', value: { id: 'A0VAL' }, coordinates: { D1: 'D1ROOT>CHILD>CHILD', D2: 'D2ROOT>CHILD>CHILD' } },
  			A1: { id: 'A1', value: { id: 'A1VAL' }, coordinates: { D1: 'D1ROOT>CHILD>CHILD', D2: 'D2ROOT>CHILD>CHILD' } },
  		}
  	};

  	const topCoordinates = {};

  	const serie = {
  		indexes: [3, [2, 2], [2, 2]],
  		parentsIndexes: [[], [[], []], [[], []]],
  		missingIndexes: [[0, 2], [[0, 1], [0, 1]], [[0, 1], [0, 1]]],
  	};

  	expect(getSerieDataWithMissingLines(serie, dimensions, topCoordinates, attributesSeries, {}, {})).to.deep.equal([
  		{
  			data: [
  				{ dimension: { id: 'D0' }, value: { id: 'W', parents: [], missingParents: [] } }
  			],
  			key: 'D0=W',
  			flags: [],
  			sideProps: null,
				isEmpty: true
  		},
  		{
  			data: [
  				{ dimension: { id: 'D0' }, value: { id: 'A', parents: [0], missingParents: [] } }
  			],
  			key: 'D0=A',
  			flags: [],
  			sideProps: null,
				isEmpty: true
  		},
  		{
  			data: [
  				{ dimension: { id: 'D0' }, value: { id: 'USA', parents: [0, 2], missingParents: [] } },
  				{ dimension: { id: 'COMB0' }, values: [{ id: 'D1ROOT', parents: [], missingParents: [] }] }
  			],
  			key: 'D0=USA:D1=D1ROOT',
  			flags: [],
  			sideProps: null,
				isEmpty: true
  		},
  		{
  			data: [
  				{ dimension: { id: 'D0' }, value: { id: 'USA', parents: [0, 2], missingParents: [] } },
  				{ dimension: { id: 'COMB0' }, values: [{ id: 'D1ROOT>CHILD', parents: [0], missingParents: [] }] }
  			],
  			key: 'D0=USA:D1=D1ROOT>CHILD',
  			flags: [],
  			sideProps: null,
				isEmpty: true
  		},
  		{
  			data: [
  				{ dimension: { id: 'D0' }, value: { id: 'USA', parents: [0, 2], missingParents: [] } },
  				{ dimension: { id: 'COMB0' }, values: [{ id: 'D1ROOT>CHILD>CHILD', parents: [0, 1], missingParents: [] }, { id: 'D2ROOT', parents: [], missingParents: [] }] }
  			],
  			key: 'D0=USA:D1=D1ROOT>CHILD>CHILD:D2=D2ROOT',
  			flags: [],
  			sideProps: null,
				isEmpty: true
  		},
  		{
  			data: [
  				{ dimension: { id: 'D0' }, value: { id: 'USA', parents: [0, 2], missingParents: [] } },
  				{ dimension: { id: 'COMB0' }, values: [{ id: 'D1ROOT>CHILD>CHILD', parents: [0, 1], missingParents: [] }, { id: 'D2ROOT>CHILD', parents: [0], missingParents: [] }] }
  			],
  			key: 'D0=USA:D1=D1ROOT>CHILD>CHILD:D2=D2ROOT>CHILD',
  			flags: [],
  			sideProps: null,
				isEmpty: true
  		},
  		{
  			data: [
  				{ dimension: { id: 'D0' }, value: { id: 'USA', parents: [0, 2], missingParents: [] } },
  				{ dimension: { id: 'COMB0' }, values: [{ id: 'D1ROOT>CHILD>CHILD', parents: [0, 1], missingParents: [] }, { id: 'A0VAL' }, { id: 'D2ROOT>CHILD>CHILD', parents: [0, 1], missingParents: [] }] },
  				{ dimension: { id: 'COMB1' }, values: [{ id: 'D3ROOT', parents: [], missingParents: [] }] }
  			],
  			key: 'D0=USA:D1=D1ROOT>CHILD>CHILD:D2=D2ROOT>CHILD>CHILD:D3=D3ROOT',
  			flags: [],
  			sideProps: null,
				isEmpty: true
  		},
  		{
  			data: [
  				{ dimension: { id: 'D0' }, value: { id: 'USA', parents: [0, 2], missingParents: [] } },
  				{ dimension: { id: 'COMB0' }, values: [{ id: 'D1ROOT>CHILD>CHILD', parents: [0, 1], missingParents: [] }, { id: 'A0VAL' }, { id: 'D2ROOT>CHILD>CHILD', parents: [0, 1], missingParents: [] }] },
  				{ dimension: { id: 'COMB1' }, values: [{ id: 'D3ROOT>CHILD', parents: [0], missingParents: [] }] }
  			],
  			key: 'D0=USA:D1=D1ROOT>CHILD>CHILD:D2=D2ROOT>CHILD>CHILD:D3=D3ROOT>CHILD',
  			flags: [],
  			sideProps: null,
				isEmpty: true
  		},
  		{
  			data: [
  				{ dimension: { id: 'D0' }, value: { id: 'USA', parents: [0, 2], missingParents: [] } },
  				{ dimension: { id: 'COMB0' }, values: [{ id: 'D1ROOT>CHILD>CHILD', parents: [0, 1], missingParents: [] }, { id: 'A0VAL' }, { id: 'D2ROOT>CHILD>CHILD', parents: [0, 1], missingParents: [] }] },
  				{ dimension: { id: 'COMB1' }, values: [{ id: 'D3ROOT>CHILD>CHILD', parents: [0, 1], missingParents: [] }, { id: 'D4ROOT', parents: [], missingParents: [] }] }
  			],
  			key: 'D0=USA:D1=D1ROOT>CHILD>CHILD:D2=D2ROOT>CHILD>CHILD:D3=D3ROOT>CHILD>CHILD:D4=D4ROOT',
  			flags: [],
  			sideProps: null,
				isEmpty: true
  		},

  		{
  			data: [
  				{ dimension: { id: 'D0' }, value: { id: 'USA', parents: [0, 2], missingParents: [] } },
  				{ dimension: { id: 'COMB0' }, values: [{ id: 'D1ROOT>CHILD>CHILD', parents: [0, 1], missingParents: [] }, { id: 'A0VAL' }, { id: 'D2ROOT>CHILD>CHILD', parents: [0, 1], missingParents: [] }] },
  				{ dimension: { id: 'COMB1' }, values: [{ id: 'D3ROOT>CHILD>CHILD', parents: [0, 1], missingParents: [] }, { id: 'D4ROOT>CHILD', parents: [0], missingParents: [] }] }
  			],
  			key: 'D0=USA:D1=D1ROOT>CHILD>CHILD:D2=D2ROOT>CHILD>CHILD:D3=D3ROOT>CHILD>CHILD:D4=D4ROOT>CHILD',
  			flags: [],
  			sideProps: null,
				isEmpty: true
  		},
  		{
  			data: [
  				{ dimension: { id: 'D0' }, value: { id: 'USA', parents: [0, 2], missingParents: [] } },
  				{ dimension: { id: 'COMB0' }, values: [{ id: 'D1ROOT>CHILD>CHILD', parents: [0, 1], missingParents: [] }, { id: 'A0VAL' }, { id: 'D2ROOT>CHILD>CHILD', parents: [0, 1], missingParents: [] }] },
  				{ dimension: { id: 'COMB1' }, values: [{ id: 'D3ROOT>CHILD>CHILD', parents: [0, 1], missingParents: [] }, { id: 'D4ROOT>CHILD>CHILD', parents: [0, 1], missingParents: [] }] }
  			],
  			coordinates: { D0: 'USA', D1: 'D1ROOT>CHILD>CHILD', D2: 'D2ROOT>CHILD>CHILD', D3: 'D3ROOT>CHILD>CHILD', D4: 'D4ROOT>CHILD>CHILD' },
  			key: 'D0=USA:D1=D1ROOT>CHILD>CHILD:D2=D2ROOT>CHILD>CHILD:D3=D3ROOT>CHILD>CHILD:D4=D4ROOT>CHILD>CHILD',
  			flags: [],
  			sideProps: {
					coordinates: {
						D0: 'USA',
						D1: 'D1ROOT>CHILD>CHILD',
						D2: 'D2ROOT>CHILD>CHILD',
						D3: 'D3ROOT>CHILD>CHILD',
						D4: 'D4ROOT>CHILD>CHILD',
					},
					hasAdvancedAttributes: true,
					hasMetadata: false
				}
  		}
  	]);
  });
	it('getSerieDataWithoutMissingLines test 1', () => {
  	const dimensions = [
  		{
				id: 'D0',
				values: [
					{ id: 'W' },
					{ id: 'NA', parentsIndexes: [0] },
					{ id: 'A', parentsIndexes: [0, 1] },
					{ id: 'USA', parentsIndexes: [0, 1, 2] },
					{ id: 'CAN', parentsIndexes: [0, 1, 2] },
					{ id: 'MEX', parentsIndexes: [0, 1, 2] }
				]
			},
			{
				id: 'COMB0',
				concepts: ['D1', 'A0', 'D2'],
				dimensions: [
					{
						id: 'D1',
						values: [
							{ id: 'D1ROOT' },
							{ id: 'D1ROOT>CHILD' },
							{ id: 'D1ROOT>CHILD>CHILD' },
						]
					},
					{
						id: 'D2',
						values: [
							{ id: 'D2ROOT' },
							{ id: 'D2ROOT>CHILD' },
							{ id: 'D2ROOT>CHILD>CHILD' },
						]
					}
				]
			},
			{
				id: 'COMB1',
				concepts: ['D3', 'D4'],
				dimensions: [
					{
						id: 'D3',
						values: [
							{ id: 'D3ROOT' },
							{ id: 'D3ROOT>CHILD' },
							{ id: 'D3ROOT>CHILD>CHILD' },
						]
					},
					{
						id: 'D4',
						values: [
							{ id: 'D4ROOT' },
							{ id: 'D4ROOT>CHILD' },
							{ id: 'D4ROOT>CHILD>CHILD' },
						]
					}
				]
			},
  	];

  	const attributesSeries = {
  		'D1=D1ROOT>CHILD>CHILD:D2=D2ROOT>CHILD>CHILD': {
  			A0: { id: 'A0', value: { id: 'A0VAL' }, coordinates: { D1: 'D1ROOT>CHILD>CHILD', D2: 'D2ROOT>CHILD>CHILD' } },
  			A1: { id: 'A1', value: { id: 'A1VAL' }, coordinates: { D1: 'D1ROOT>CHILD>CHILD', D2: 'D2ROOT>CHILD>CHILD' } },
  		}
  	};

  	const topCoordinates = {};

  	const serie = {
  		indexes: [3, [2, 2], [2, 2]],
  		parentsIndexes: [[], [[], []], [[], []]],
  		missingIndexes: [[0, 2], [[0, 1], [0, 1]], [[0, 1], [0, 1]]],
  	};

  	expect(getSerieDataWithoutMissingLines(serie, dimensions, topCoordinates, attributesSeries, {}, {})).to.deep.equal({
  			data: [
  				{ dimension: { id: 'D0' }, value: { id: 'USA', parents: [0, 2], missingParents: [{ id: 'W', parents: [], missingParents: [] }, { id: 'A', parents: [0], missingParents: [] }] } },
  				{
						dimension: { id: 'COMB0' },
						values: [
							{ id: 'D1ROOT>CHILD>CHILD', parents: [0, 1], missingParents: [{ id: 'D1ROOT', parents: [], missingParents: [] }, { id: 'D1ROOT>CHILD', parents: [0], missingParents: [] }] },
							{ id: 'A0VAL' },
							{ id: 'D2ROOT>CHILD>CHILD', parents: [0, 1], missingParents: [{ id: 'D2ROOT', parents: [], missingParents: [] }, { id: 'D2ROOT>CHILD', parents: [0], missingParents: [] }] }
						]
					},
  				{
						dimension: { id: 'COMB1' },
						values: [
							{ id: 'D3ROOT>CHILD>CHILD', parents: [0, 1], missingParents: [{ id: 'D3ROOT', parents: [], missingParents: [] }, { id: 'D3ROOT>CHILD', parents: [0], missingParents: [] }] },
							{ id: 'D4ROOT>CHILD>CHILD', parents: [0, 1], missingParents: [{ id: 'D4ROOT', parents: [], missingParents: [] }, { id: 'D4ROOT>CHILD', parents: [0], missingParents: [] }] }
						]
					}
  			],
  			coordinates: { D0: 'USA', D1: 'D1ROOT>CHILD>CHILD', D2: 'D2ROOT>CHILD>CHILD', D3: 'D3ROOT>CHILD>CHILD', D4: 'D4ROOT>CHILD>CHILD' },
  			key: 'D0=USA:D1=D1ROOT>CHILD>CHILD:D2=D2ROOT>CHILD>CHILD:D3=D3ROOT>CHILD>CHILD:D4=D4ROOT>CHILD>CHILD',
  			flags: [],
  			sideProps: {
					coordinates: {
						D0: 'USA',
						D1: 'D1ROOT>CHILD>CHILD',
						D2: 'D2ROOT>CHILD>CHILD',
						D3: 'D3ROOT>CHILD>CHILD',
						D4: 'D4ROOT>CHILD>CHILD',
					},
					hasAdvancedAttributes: true,
					hasMetadata: false
				}
  		});
  });
	it('getSerieDataWithoutMissingLines, sideProps and flags non dupplication', () => {
		const dimensions = [
			{ 
				id: 'D0',
				values: [
					{ id: 'D0V0', flags: [{ id: 'A0', value: { id: 'A0V' }}] },
					{ id: 'D1V1' }
				]
			}, {
				id: 'COMB',
				concepts: ['D1', 'A1'],
				dimensions: [
					{
						id: 'D1',
						values: [
							{ id: 'D1V0', hasAdvancedAttributes: true },
							{ id: 'D1V1' },
						]
					}
				]
			}
		];

		const attributesSeries = {
  		'D0=D0V0': {
  			A0: { id: 'A0', value: { id: 'A0V' }, coordinates: { D0: 'D0V0' }, relationship: ['D0'] },
  		},
			'D1=D1V0': {
  			A1: { id: 'A1', value: { id: 'A1V' }, coordinates: { D1: 'D1V0' }, relationship: ['D1'], combined: true },
  		},
			'D0=D0V0:D1=D1V0': {
				A2: { id: 'A2', value: { id: 'A2V' }, coordinates: { D0: 'D0V0', D1: 'D1V0' }, relationship: ['D0', 'D1'] },
			},
  	};

		const topCoordinates = {};

		const serie = {
  		indexes: [0, [0]],
  		parentsIndexes: [[], [[]]],
  		missingIndexes: [[], [[]]],
  	};

		const customAttributes = {
			flags: [],
			notes: ['A0', 'A2']
		}

		expect(getSerieDataWithoutMissingLines(serie, dimensions, topCoordinates, attributesSeries, customAttributes, {})).to.deep.equal({
			data: [
				{
					dimension: { id: 'D0' },
					value: { id: 'D0V0', missingParents: [], parents: [], flags: [{ id: 'A0', value: { id: 'A0V' } }] }
				},
				{
					dimension: { id: 'COMB' },
					values: [
						{ id: 'D1V0', missingParents: [], parents: [], hasAdvancedAttributes: true },
						{ id: 'A1V' },
					]
				}
			],
			coordinates: { D0: 'D0V0', D1: 'D1V0' },
			key: 'D0=D0V0:D1=D1V0',
			flags: [
				{ id: 'A2', value: { id: 'A2V' } }
			],
			sideProps: {
				coordinates: {
					D0: 'D0V0',
					D1: 'D1V0',
				},
				hasAdvancedAttributes: true,
				hasMetadata: false
			}
		});
	});
	it('getSerieDataWithMissingLines, advanced attributes as dim level plus empty series', () => {
		const dimensions = [
			{ 
				id: 'D0',
				values: [
					{ id: 'D0V0', flags: [], hasAdvancedAttributes: true },
					{ id: 'D0V1' }
				]
			}
		];

		const attributesSeries = {
			'D1=D1V0': {},
			'D0=D0V0:D1=D1V0': {},
  	};

		const topCoordinates = {};

		const serie = {
  		indexes: [0],
  		parentsIndexes: [[]],
  		missingIndexes: [[]],
  	};

		const customAttributes = {
			flags: [],
			notes: ['A0', 'A2']
		}

		expect(getSerieDataWithMissingLines(serie, dimensions, topCoordinates, attributesSeries, customAttributes, {})).to.deep.equal([{
			data: [
				{
					dimension: { id: 'D0' },
					value: { id: 'D0V0', missingParents: [], parents: [], flags: [], hasAdvancedAttributes: true }
				},
			],
			coordinates: { D0: 'D0V0' },
			key: 'D0=D0V0',
			flags: [],
			sideProps: {
				coordinates: {
					D0: 'D0V0',
				},
				hasAdvancedAttributes: true,
				hasMetadata: false
			}
		}]);
	});
	it('getSerieDataWithMissingLines, advanced attributes and inverted dim', () => {
		const dimensions = [
			{ 
				id: 'D0',
				values: [
					{ id: 'D0V0' },
					{ id: 'D0V1', hasAdvancedAttributes: true }
				]
			}
		];

		const attributesSeries = {};

		const topCoordinates = {};

		const serie = {
  		indexes: [-1],
  		parentsIndexes: [[]],
  		missingIndexes: [[]],
  	};

		const customAttributes = {
			flags: [],
			notes: []
		}

		expect(getSerieDataWithMissingLines(serie, dimensions, topCoordinates, attributesSeries, customAttributes, {})).to.deep.equal([{
			data: [
				{
					dimension: { id: 'D0' },
					value: { id: 'D0V1', missingParents: [], parents: [], hasAdvancedAttributes: true }
				},
			],
			coordinates: { D0: 'D0V1' },
			key: 'D0=D0V1',
			flags: [],
			sideProps: {
				coordinates: {
					D0: 'D0V1',
				},
				hasAdvancedAttributes: true,
				hasMetadata: false
			}
		}]);
	});
});
