import { expect } from 'chai';
import { getCellValue } from '../src/rules/src/table/factories/getCellValue';

describe('getCellValue tests', () => {
  it('simple muber test', () => {
    expect(getCellValue({ value: 2 })).to.equal('2');
  });
  it('number -1 scale test', () => {
    expect(getCellValue({ value: 8.3, prefscale: '-1' })).to.equal('83');
  });
  it('number 3 scale test', () => {
    expect(getCellValue({ value: 25.62, prefscale: '3' })).to.equal('0.02562');
  });
  it('number 0 decimals test', () => {
    expect(getCellValue({ value: 155062.56, decimals: '0' })).to.equal('155,063');
  });
  it('random string test', () => {
    expect(getCellValue({ value: 'blablablablabalba' })).to.equal('blablablablabalba');
  });
  it('numerical string test', () => {
    expect(getCellValue({ value: '654789', prefscale: '3', decimals: '1' })).to.equal('654789');
  });
  it('boolean test', () => {
    expect(getCellValue({ value: true })).to.equal(true);
  });
  it('month test', () => {
    expect(getCellValue({ value: '--06' })).to.equal('06');
  });
  it('monthDay test', () => {
    expect(getCellValue({ value: '--12-31' })).to.equal('12-31');
  });
});
