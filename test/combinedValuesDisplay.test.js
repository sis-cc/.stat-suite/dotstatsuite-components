import { expect } from 'chai';
import * as R from 'ramda';
import { combinedValuesDisplay } from '../src/rules2/src/combinedValuesDisplay';

describe('combinedValuesDisplay tests', () => {
  const values = [
    {
      id: 'F3RES',
      order: 424,
      name: 'Debt securities issued by residents',
      annotations: [21],
      display: true,
      start: null,
      notes: [],
      __indexPosition: 0,
      __index: 0,
      parents: [],
    },
    {
      id: 'W',
      order: 440,
      name: 'World',
      annotations: [11, 12],
      display: false,
      start: null,
      notes: [],
      __indexPosition: 0,
      __index: 0,
    },
    {
      id: 'S11',
      order: 2,
      name: 'Non financial corporations',
      parent: 'S1',
      parents: ['S1'],
      annotations: [18, 19],
      display: true,
      start: null,
      notes: [],
      __indexPosition: 1,
      __index: 0,
    },
    null,
  ];

  const testdata = [
    {
      display: 'label',
      expected: 'Debt securities issued by residents, Non financial corporations',
    },
    {
      display: 'code',
      expected: 'F3RES, W, S11'
    },
    {
      display: 'both',
      expected: '(F3RES, W, S11) Debt securities issued by residents, Non financial corporations',
    }
  ];

  R.forEach(({display, expected}) => {
    it(`should pass with display ${display}`, () => {
      expect(combinedValuesDisplay(display, values)).to.deep.equal(expected);
    });
  }, testdata);
});
