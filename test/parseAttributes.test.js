import { expect } from 'chai';
import { parseAttributes } from '../src/rules2/src/';

const attributes = [
  { id: 'a6', relationship: { primaryMeasure: 'OBS_VALUE' }, values: [{ id: 'a6v1' }] }, // correct observations attribute
  { id: 'a8', relationship: { dimensions: ['d1'] }, values: [{ id: 'a8v1' }] }, // single 'one value' dimension relationship
  { id: 'a9', relationship: { dimensions: ['d4'] }, values: [{ id: 'a9v1' }] }, // single 'many values' dimension relationship
  { id: 'a10', relationship: { dimensions: ['d1', 'd2'] }, values: [{ id: 'a10v1' }] }, // many 'single value' dimensions realtionship
  { id: 'a11', relationship: { dimensions: ['d1', 'd2', 'd4'] }, values: [{ id: 'a11v1' }] }, // one 'many values' and many 'single value' dimensions relationship
  { id: 'a12', relationship: { dimensions: ['d4', 'd5', 'd7'] }, values: [{ id: 'a11v1' }] }, // exclude empty dimension from relationship
];

const customAttributes = {};

const dimensions = [
  { id: 'd1', header: true, values: [{ id: 'v0' }] },
  { id: 'd2', header: true, values: [{ id: 'v0' }] },
  { id: 'd3', header: true, values: [{ id: 'v0' }] },
  { id: 'd4', header: false, values: [{ id: 'v0' }, { id: 'v1' }] },
  { id: 'd5', header: false, values: [{ id: 'v0' }, { id: 'v1' }] },
  { id: 'd6', header: false, values: [{ id: 'v0' }, { id: 'v1' }] },
  { id: 'd7', header: false, values: [] }
];

describe('parseAttributes test', () => {
  it('complete case', () => {
    expect(parseAttributes(attributes, dimensions, customAttributes)).to.deep.equal([
      { id: 'a6', index: 0, relationship: [], series: true, values: [{ id: 'a6v1' }] },
      { id: 'a8', index: 1, relationship: ['d1'], header: true, values: [{ id: 'a8v1' }] },
      { id: 'a9', index: 2, relationship: ['d4'], series: true, values: [{ id: 'a9v1' }] },
      { id: 'a10', index: 3, relationship: ['d1', 'd2'], header: true, values: [{ id: 'a10v1' }] },
      { id: 'a11', index: 4, relationship: ['d4'], series: true, values: [{ id: 'a11v1' }] },
      { id: 'a12', index: 5, relationship: ['d4', 'd5'], series: true, values: [{ id: 'a11v1' }] },
    ]);
  });
});
