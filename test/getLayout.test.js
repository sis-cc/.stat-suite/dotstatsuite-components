import { expect } from 'chai';
import { getLayout } from '../src/rules2/src';

describe('getLayout tests', () => {
  it('complete test', () => {
    const isTimeInverted = true;
    const dimensions = [
      { id: 'D1', values: [{ id: 'v1' }, { id: 'v2' }] },
      { id: 'D2', values: [{ id: 'v1' }, { id: 'v2' }] },
      { id: 'D3', values: [{ id: 'v1' }, { id: 'v2' }] },
      { id: 'D4', values: [{ id: 'v1' }, { id: 'v2' }] },
      { id: 'D5', values: [{ id: 'v1' }, { id: 'v2' }] },
      { id: 'TIME_PERIOD', values: [{ id: 'v1' }, { id: 'v2' }] }
    ];

    const layoutIds = {
      header: ['TIME_PERIOD', 'COMB1', 'COMB2'],
      sections: ['D4'],
      rows: ['COMB3', 'COMB4']
    };

    const combinations = [
       { id: 'COMB1', concepts: ['D1', 'D2'], relationship: ['D1', 'D2'] },
       { id: 'COMB2', concepts: ['D3', 'A1'], relationship: ['D3', 'D4', 'TIME_PERIOD'] },
       { id: 'COMB3', concepts: ['D4', 'D5'], relationship: ['D4', 'D5'] },
       { id: 'COMB4', concepts: ['A2', 'A3'], relationship: ['D5', 'TIME_PERIOD'] }
    ];

    expect(getLayout(layoutIds, dimensions, combinations, isTimeInverted)).to.deep.equal({
      header: [
        { id: 'TIME_PERIOD', isInverted: true, values: [{ id: 'v1' }, { id: 'v2' }] },
        {
          id: 'COMB1',
          concepts: ['D1', 'D2'],
          relationship: ['D1', 'D2'],
          dimensions: [
            { id: 'D1', values: [{ id: 'v1' }, { id: 'v2' }] },
            { id: 'D2', values: [{ id: 'v1' }, { id: 'v2' }] },
          ]
        },
        { id: 'D3', values: [{ id: 'v1' }, { id: 'v2' }] },
      ],
      sections: [{ id: 'D4', values: [{ id: 'v1' }, { id: 'v2' }] }],
      rows: [
        {
          id: 'COMB3',
          concepts: ['D4', 'D5'],
          relationship: ['D4', 'D5'],
          dimensions: [{ id: 'D5', values: [{ id: 'v1' }, { id: 'v2' }] }]
        },
      ]
    })
  });
});
