# dotstatsuite-components

This library regroup a set of React components that were originally separated in their own git repositories.
Those components are:
  - [bridge-d3-react](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-components/blob/master/src/bridge-d3-react/README.md)
  - [chart](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-components/blob/master/src/chart/README.md)
  - [rules](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-components/blob/master/src/rules/README.md)
  - [rules-driver](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-components/blob/master/src/rules-driver/README.md)
  - [share](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-components/blob/master/src/share/README.md)

Unlike other libraries as [dotstatsuite-ui-components](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-ui-components), this library doesn't focus only on UI design, but also on data parsing and deduced behaviour, like the visualisation charts.

## setup

- install: `npm i @sis-cc/dotstatsuite-components`

## usage

see individuals readme files of the components
