import * as R from 'ramda';

export const isChartNoData = R.anyPass([
  R.pipe(R.path(['data', 'series']), R.isEmpty),
  R.allPass([
    R.pipe(R.prop('type'), R.equals('ChoroplethChart'), R.not),
    R.pipe(R.path(['data', 'series', 0, 'datapoints']), R.anyPass([R.isNil, R.isEmpty]))
  ])
]);

export const isChartDataNotReady = R.pipe(R.path(['data', 'series']), R.isNil);

export const getFontFromTheme = customPath => R.converge(
  R.mergeRight,
  [R.pathOr({}, ['mixins', 'chart', 'main']), R.pathOr({}, R.concat(['mixins', 'chart'], customPath))]
);

export const getOptionsFromFont = R.pipe(
  R.props(['color', 'fontFamily', 'fontSize', 'fontWeight']),
  ([color, family, size, weight]) => ({ color, size, family, weight })
);
