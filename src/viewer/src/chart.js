import React from 'react';
import * as R from 'ramda';
import { NoData, Loading } from '@sis-cc/dotstatsuite-visions';
import * as charts from '../../bridge-d3-react/src';
import filterSeriesRegardingDimensions from './chartUtils/series';
import AxisLegend from './legends/AxisLegend';
import { isChartNoData } from './utils';

const Chart = ({ options, series, type, width, getAxisOptions }) => {
  const ChartClass = charts[type];
  return (
    <div>
      {
        (R.is(Number, width) && width < 370)
        ? null
        : <AxisLegend axis="y" data={{ series }} type={type} />
      }
      <ChartClass
        data={series}
        options={options}
        getAxisOptions={getAxisOptions}
      />
    </div>
  );
};

const ChartWrapper = (props = {}) => {
  const options = R.over(
    R.lensPath(['base', 'height']),
    R.when(h => h < 0, R.always(300))
  )(props.options);

  if (isChartNoData(props)) {
    const height = R.path(['base', 'height'], options);
    const style = { height };
    return (
      <div style={style}>
        <NoData message={props.noData || 'No Data'} />
      </div>
    );
  }

  return (
    <Chart
      {...props}
      options={options}
      series={filterSeriesRegardingDimensions(props.data.series, props.type, options)} // responsive rule only display highlighted data
    />
  );
};

export default ChartWrapper;
