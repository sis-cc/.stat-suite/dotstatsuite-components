import React, { useState } from 'react';
import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { createTheme } from '@mui/material/styles';
import { sisccTheme } from '@sis-cc/dotstatsuite-visions';
import UseCase1 from './app/use-case-1';
import TableCase from './app/table';
import TableOneCell from './app/one-table-cell';
import UseCase2 from './app/use-case-2';
import UseCase3 from './app/use-case-3';
import ResNoDataCase from './app/nodata-res';
import SizedNoDataCase from './app/nodata-sized';
import Legacy from './app/leg';
import Viewer from '.';

const AppTab = ({ label, iconName }) => (
  <Tooltip content={label} position={Position.RIGHT}>
    <Icon iconName={iconName} />
  </Tooltip>
);

export default () => {
  const [isRtl, setRtl] = useState(false);

  const theme = createTheme(sisccTheme({ rtl: isRtl ? 'rtl' : 'ltr' }));

  const config = {
    logo: 'https://upload.wikimedia.org/wikipedia/en/thumb/0/0d/OECD_logo_new.svg/200px-OECD_logo_new.svg.png',
    owner: '©OECD',
    terms: { label: 'Terms & Conditions', link: 'https://www.oecd.org/termsandconditions/' },
  };

  const style = {
    height: 'calc(100vh - 70px)',
  };

  const options = {
    base: { height: 500 },
    axis: {
      x: {
        linear: {
          step: 5,
          max: 20,
        },
      },
      y: {
        linear: {
          step: 5,
          max: 20,
        },
      },
    },
  };


/*

<Tab2
            id="uc3"
            title={<AppTab label="fluid width" iconName="fullscreen" />}
            panel={<UseCase3 isRtl={isRtl} config={config} style={style} options={options} />}
          />
          <Tab2
            id="uc1"
            title={<AppTab label="full screen resizable chart" iconName="fullscreen" />}
            panel={<UseCase1 isRtl={isRtl} config={config} style={style} options={options} />}
          />
          <Tab2
            id="table0"
            title={<AppTab label="table" iconName="fullscreen" />}
            panel={<TableCase isRtl={isRtl} config={config} style={style} />}
          />
          <Tab2
            id="table1"
            title={<AppTab label="table one cell" iconName="fullscreen" />}
            panel={<TableOneCell isRtl={isRtl} config={config} style={style} />}
          />
          <Tab2
            id="uc2"
            title={<AppTab label="responsive fixed slot dashboard" iconName="grid-view" />}
            panel={<UseCase2 isRtl={isRtl} config={config} style={style} options={options} />}
          />
          <Tab2
            id="leg"
            title={<AppTab label="legacy should still work like a charm" iconName="comparison" />}
            panel={<Legacy isRtl={isRtl} config={config} />}
          />
          <Tab2
            id="unsized-nodata"
            title={<AppTab label="Unsized No Data Chart" iconName="fullscreen" />}
            panel={<ResNoDataCase isRtl={isRtl} config={config} />}
          />
          <Tab2
            id="sized-nodata"
            title={<AppTab label="Sized No Data Chart" iconName="fullscreen" />}
            panel={<SizedNoDataCase isRtl={isRtl} config={config} />}
          />

*/

  return (
    {/* <div style={{ padding: 10 }}>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
      <button onClick={() => setRtl(!isRtl)}>{isRtl ? 'left to right' : 'right to left'}</button>
      <Tabs2 renderActiveTabPanelOnly={false}>
        <Tab2
          id="uc1"
          title={<AppTab label="full screen resizable chart" iconName="fullscreen" />}
          panel={<UseCase1 isRtl={isRtl} config={config} style={style} options={options} />}
        />
      </Tabs2>
      </ThemeProvider>
      </StyledEngineProvider>
    </div> */}
  );
};
