import * as R from 'ramda';
import { getChartOptions } from '../../../rules/src/chart/getChartOptions';
import { getFontFromTheme, getOptionsFromFont } from '../utils';

export default (options, data, type, theme, timeFormats, locale) => {
  const axisFontOptions = R.pipe(getFontFromTheme(['axis']), getOptionsFromFont)(theme);
  const annotationFontOptions = R.pipe(getFontFromTheme(['annotation']), getOptionsFromFont)(theme);
  const tooltipFonts = {
    primary: getFontFromTheme(['tooltip', 'primary'])(theme),
    secondary: getFontFromTheme(['tooltip', 'secondary'])(theme)
  };
  const optionsWithFonts = R.evolve({
    axis: R.pipe(
      R.over(
        R.lensPath(['x', 'font']),
        R.pipe(R.when(R.isNil, R.always({})), R.mergeRight(axisFontOptions))
      ),
      R.over(
        R.lensPath(['y', 'font']),
        R.pipe(R.when(R.isNil, R.always({})), R.mergeRight(axisFontOptions))
      )
    ),
    serie: R.over(
      R.lensPath(['annotation', 'font']),
      R.pipe(R.when(R.isNil, R.always({})), R.mergeRight(annotationFontOptions))
    )
  })(options);

  return getChartOptions(data, type, optionsWithFonts, tooltipFonts, timeFormats, locale);
};
