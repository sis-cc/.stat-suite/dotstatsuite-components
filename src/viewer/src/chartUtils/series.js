import * as R from 'ramda';
import { H_SYMBOL, TIMELINE, V_SYMBOL } from '../../../rules/src/constants';

const isFocused = (obj) => {
  const highlightIndex = R.propOr(-1, 'highlightIndex', obj);
  const baselineIndex = R.propOr(-1, 'baselineIndex', obj);
  return ((highlightIndex > -1) || (baselineIndex > -1));
};

const sampleFilterSeries = (series) => (
  R.map(
    serie => {
      const _datapoints = R.filter(isFocused, serie.datapoints);
      return { ...serie, datapoints: _datapoints };
    },
    series
  )
);

const lineFilterSeries = R.filter(isFocused);

export default (series, type, options) => {
  const responsiveFocusFilter = R.pathOr(true, ['serie', 'responsiveFocusFilter'], options);
  if (!responsiveFocusFilter) {
    return series;
  }
  const height = R.path(['base', 'height'], options);
  const width = R.path(['base', 'width'], options);
  const minWidth = R.path(['base', 'minDisplayWidth'], options);
  const minHeight = R.path(['base', 'minDisplayHeight'], options);
  let filtered;
  if (type === H_SYMBOL) {
    if (height < minHeight) {
      filtered = sampleFilterSeries(series);
      return R.isEmpty(filtered[0].datapoints) ? series : filtered;
    }
  }
  else if (type === TIMELINE) {
    if (height < minHeight) {
      filtered = lineFilterSeries(series);
      return R.isEmpty(filtered) ? series : filtered;
    }
  }
  else if (type === V_SYMBOL) {
    if (width < minWidth) {
      filtered = sampleFilterSeries(series);
      return R.isEmpty(filtered[0].datapoints) ? series : filtered;
    }
  }
  return series;
};
