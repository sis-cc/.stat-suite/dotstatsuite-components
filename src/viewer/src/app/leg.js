import React from 'react';
import { map } from 'lodash';
import * as R from 'ramda';
import Viewer from '../';
import gppSymbolData from '../../mocks/gpp-symbol';

const injectConfig = config => R.over(R.lensProp('footerProps'), R.mergeRight(config));

export default ({ config }) => (
  <div className="app legacy">
    <div className="app-chart">
      <span className="app-note">Ordinal MinSize Vertical (Bar + VerticalSymbol)</span>
      <Viewer {...injectConfig(config)(gppSymbolData())} chartOptions={{ base: { width: 550, height: 430 } }} type="VerticalSymbolChart" />
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal MinSize Vertical (Bar + VerticalSymbol)</span>
      <Viewer {...injectConfig(config)(gppSymbolData())} chartOptions={{ base: { width: 540, height: 430 } }} type="VerticalSymbolChart" />
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal MinSize Horizontal (Row + HorizontalSymbol)</span>
      <Viewer {...injectConfig(config)(gppSymbolData())} chartOptions={{ base: { width: 400, height: 680 } }} type="HorizontalSymbolChart" />
    </div>
    <div className="app-chart">
      <span className="app-note">Ordinal MinSize Horizontal (Row + HorizontalSymbol)</span>
      <Viewer {...injectConfig(config)(gppSymbolData())} chartOptions={{ base: { width: 280, height: 680 } }} type="HorizontalSymbolChart" />
    </div>
    <div className="app-chart">
      <span className="app-note">Data Visibility Vertical (VerticalSymbol)</span>
      <Viewer {...injectConfig(config)(gppSymbolData())} chartOptions={{ base: { width: 550, height: 430 } }} type="VerticalSymbolChart" />
    </div>
    <div className="app-chart">
      <span className="app-note">Data Visibility Vertical (VerticalSymbol)</span>
      <Viewer {...injectConfig(config)(gppSymbolData())} chartOptions={{ base: { width: 100, height: 430 } }} type="VerticalSymbolChart" />
    </div>
  </div>
);
