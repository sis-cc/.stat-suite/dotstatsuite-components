import React, { Component } from 'react';
import { render } from 'react-dom';
import Viewer from '../';

const headerData = [
  {
    data: [
      { dimension: { label: 'Header 1' }, value: { label: 'H1 Value 1' } },
      { dimension: { label: 'Header 2' }, value: { label: 'H2 Value 1' } }
    ],
    key: 'c1',
    flags: [{ label: 'Column Flag' }]
  },
  {
    data: [
      { dimension: { label: 'Header 1' }, value: { label: 'H1 Value 2', flags: [{ label: 'Column Value Flag' }] } },
      { dimension: { label: 'Header 2' }, value: { label: 'H2 Value 2' } }
    ],
    key: 'c2',
    flags: []
  }
];

const sectionsData = [
  [
    {
      data: [{ dimension: { label: 'Section' }, value: { label: 'S Value 1' } }],
      key: 's1',
      flags: [{ label: 'Sectionwiuefhw iuefhwiuefhqwieurwiuergwieurg' }, { label: 'Flag' }]
    },
    [
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 1', parents: ['a', 'b'], flags: [{ label: 'Row Value Flag' }] } },
        ],
        key: 'r1',
        flags: []
      },
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 2' } },
        ],
        key: 'r2',
        flags: []
      },
    ]
  ],
  [
    {
      data: [{ dimension: { label: 'Section' }, value: { label: 'S Value 2', flags: [{ code: 'Y', label: 'Section Value Flag' }] } }],
      key: 's2',
      flags: []
    },
    [
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 1' } },
        ],
        key: 'r1',
        flags: []
      },
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 2', parents: ['a'] } },
        ],
        key: 'r2',
        flags: [{ label: 'Row' }, { label: 'Flags', code: 'R' }]
      },
    ]
  ],
  [
    {
      data: [{ dimension: { label: 'Section' }, value: { label: 'S Value 1' } }],
      key: 's3',
      flags: [{ label: 'Sectionwiuefhw iuefhwiuefhqwieurwiuergwieurg' }, { label: 'Flag' }]
    },
    [
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 1', parents: ['a', 'b'], flags: [{ label: 'Row Value Flag' }] } },
        ],
        key: 'r1',
        flags: []
      },
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 2' } },
        ],
        key: 'r2',
        flags: []
      },
    ]
  ],
  [
    {
      data: [{ dimension: { label: 'Section' }, value: { label: 'S Value 2', flags: [{ code: 'Y', label: 'Section Value Flag' }] } }],
      key: 's4',
      flags: []
    },
    [
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 1' } },
        ],
        key: 'r1',
        flags: []
      },
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 2', parents: ['a'] } },
        ],
        key: 'r2',
        flags: [{ label: 'Row' }, { label: 'Flags', code: 'R' }]
      },
    ]
  ],
  [
    {
      data: [{ dimension: { label: 'Section' }, value: { label: 'S Value 1' } }],
      key: 's5',
      flags: [{ label: 'Sectionwiuefhw iuefhwiuefhqwieurwiuergwieurg' }, { label: 'Flag' }]
    },
    [
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 1', parents: ['a', 'b'], flags: [{ label: 'Row Value Flag' }] } },
        ],
        key: 'r1',
        flags: []
      },
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 2' } },
        ],
        key: 'r2',
        flags: []
      },
    ]
  ],
  [
    {
      data: [{ dimension: { label: 'Section' }, value: { label: 'S Value 2', flags: [{ code: 'Y', label: 'Section Value Flag' }] } }],
      key: 's6',
      flags: []
    },
    [
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 1' } },
        ],
        key: 'r1',
        flags: []
      },
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 2', parents: ['a'] } },
        ],
        key: 'r2',
        flags: [{ label: 'Row' }, { label: 'Flags', code: 'R' }]
      },
    ]
  ],
  [
    {
      data: [{ dimension: { label: 'Section' }, value: { label: 'S Value 1' } }],
      key: 's7',
      flags: [{ label: 'Sectionwiuefhw iuefhwiuefhqwieurwiuergwieurg' }, { label: 'Flag' }]
    },
    [
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 1', parents: ['a', 'b'], flags: [{ label: 'Row Value Flag' }] } },
        ],
        key: 'r1',
        flags: []
      },
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 2' } },
        ],
        key: 'r2',
        flags: []
      },
    ]
  ],
  [
    {
      data: [{ dimension: { label: 'Section' }, value: { label: 'S Value 2', flags: [{ code: 'Y', label: 'Section Value Flag' }] } }],
      key: 's8',
      flags: []
    },
    [
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 1' } },
        ],
        key: 'r1',
        flags: []
      },
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 2', parents: ['a'] } },
        ],
        key: 'r2',
        flags: [{ label: 'Row' }, { label: 'Flags', code: 'R' }]
      },
    ]
  ],
  [
    {
      data: [{ dimension: { label: 'Section' }, value: { label: 'S Value 1' } }],
      key: 's9',
      flags: [{ label: 'Sectionwiuefhw iuefhwiuefhqwieurwiuergwieurg' }, { label: 'Flag' }]
    },
    [
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 1', parents: ['a', 'b'], flags: [{ label: 'Row Value Flag' }] } },
        ],
        key: 'r1',
        flags: []
      },
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 2' } },
        ],
        key: 'r2',
        flags: []
      },
    ]
  ],
  [
    {
      data: [{ dimension: { label: 'Section' }, value: { label: 'S Value 2', flags: [{ code: 'Y', label: 'Section Value Flag' }] } }],
      key: 's10',
      flags: []
    },
    [
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 1' } },
        ],
        key: 'r1',
        flags: []
      },
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 2', parents: ['a'] } },
        ],
        key: 'r2',
        flags: [{ label: 'Row' }, { label: 'Flags', code: 'R' }]
      },
    ]
  ],
  [
    {
      data: [{ dimension: { label: 'Section' }, value: { label: 'S Value 1' } }],
      key: 's11',
      flags: [{ label: 'Sectionwiuefhw iuefhwiuefhqwieurwiuergwieurg' }, { label: 'Flag' }]
    },
    [
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 1', parents: ['a', 'b'], flags: [{ label: 'Row Value Flag' }] } },
        ],
        key: 'r1',
        flags: []
      },
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 2' } },
        ],
        key: 'r2',
        flags: []
      },
    ]
  ],
  [
    {
      data: [{ dimension: { label: 'Section' }, value: { label: 'S Value 2', flags: [{ code: 'Y', label: 'Section Value Flag' }] } }],
      key: 's12',
      flags: []
    },
    [
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 1' } },
        ],
        key: 'r1',
        flags: []
      },
      {
        data: [
          { dimension: { label: 'Row' }, value: { label: 'R Value 2', parents: ['a'] } },
        ],
        key: 'r2',
        flags: [{ label: 'Row' }, { label: 'Flags', code: 'R' }]
      },
    ]
  ],
];

const cells = {
  c1: {
    s1: {
      r1: [{ value: 0, flags: [] }],
      r2: [{ value: 1, flags: [] }]
    },
    s2: {
      r1: [{ value: 2, flags: [] }],
      r2: [{ value: 3, flags: [{ label: 'some flag' }, { code: 'PP', label: 'other flag' }] }]
    },
    s3: {
      r1: [{ value: 0, flags: [] }],
      r2: [{ value: 1, flags: [] }]
    },
    s4: {
      r1: [{ value: 2, flags: [] }],
      r2: [{ value: 3, flags: [{ label: 'some flag' }, { code: 'PP', label: 'other flag' }] }]
    },
    s5: {
      r1: [{ value: 0, flags: [] }],
      r2: [{ value: 1, flags: [] }]
    },
    s6: {
      r1: [{ value: 2, flags: [] }],
      r2: [{ value: 3, flags: [{ label: 'some flag' }, { code: 'PP', label: 'other flag' }] }]
    },
    s7: {
      r1: [{ value: 0, flags: [] }],
      r2: [{ value: 1, flags: [] }]
    },
    s8: {
      r1: [{ value: 2, flags: [] }],
      r2: [{ value: 3, flags: [{ label: 'some flag' }, { code: 'PP', label: 'other flag' }] }]
    },
    s9: {
      r1: [{ value: 0, flags: [] }],
      r2: [{ value: 1, flags: [] }]
    },
    s10: {
      r1: [{ value: 2, flags: [] }],
      r2: [{ value: 3, flags: [{ label: 'some flag' }, { code: 'PP', label: 'other flag' }] }]
    },
    s11: {
      r1: [{ value: 0, flags: [] }],
      r2: [{ value: 1, flags: [] }]
    },
    s12: {
      r1: [{ value: 2, flags: [] }],
      r2: [{ value: 3, flags: [{ label: 'some flag' }, { code: 'PP', label: 'other flag' }] }]
    },
  },
  c2: {
    s1: {
      r1: [{ value: 4, flags: [] }],
      r2: [{ value: 5, flags: [] }]
    },
    s2: {
      r1: [{ value: 6, flags: [] }],
      r2: [{ value: 7, flags: [] }]
    },
    s3: {
      r1: [{ value: 4, flags: [] }],
      r2: [{ value: 5, flags: [] }]
    },
    s4: {
      r1: [{ value: 6, flags: [] }],
      r2: [{ value: 7, flags: [] }]
    },
    s5: {
      r1: [{ value: 4, flags: [] }],
      r2: [{ value: 5, flags: [] }]
    },
    s6: {
      r1: [{ value: 6, flags: [] }],
      r2: [{ value: 7, flags: [] }]
    },
    s7: {
      r1: [{ value: 4, flags: [] }],
      r2: [{ value: 5, flags: [] }]
    },
    s8: {
      r1: [{ value: 6, flags: [] }],
      r2: [{ value: 7, flags: [] }]
    },
    s9: {
      r1: [{ value: 4, flags: [] }],
      r2: [{ value: 5, flags: [] }]
    },
    s10: {
      r1: [{ value: 6, flags: [] }],
      r2: [{ value: 7, flags: [] }]
    },
    s11: {
      r1: [{ value: 4, flags: [] }],
      r2: [{ value: 5, flags: [] }]
    },
    s12: {
      r1: [{ value: 6, flags: [] }],
      r2: [{ value: 7, flags: [] }]
    },
  }
};

export default ({ config, style, options, isRtl }) => {
  const footerProps = config;
  const headerProps = {
    title: { label: 'Table Data' },
  };
  const tableProps = { headerData, sectionsData, cells }
  return (
    <div style={style}>
        <Viewer
          tableProps={tableProps}
          headerProps={headerProps}
          footerProps={footerProps}
          type="table"
          isRtl={isRtl}
        />
    </div>
  );
};