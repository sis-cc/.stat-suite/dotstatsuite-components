import React, { Component } from 'react';
import { render } from 'react-dom';
import Viewer from '../';

export default ({ config, style, options }) => {
  const footerProps = config;
  const headerProps = {
    title: { label: 'Science performance (PISA)' },
    subtitle: [{ label: 'Mean score 2015, Annual incl. OECD, all countries' }],
  };
  return (
    <div style={style}>
      <Viewer
        chartData={{ series: null, frequency: null }}
        chartOptions={{ base: { width: 500, height: 400 } }}
        headerProps={headerProps}
        footerProps={footerProps}
        type="BarChart"
        noData="Sized No Data Chart"
      />
    </div>
  );
};
