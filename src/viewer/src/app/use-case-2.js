import React, { Component } from 'react';
import { render } from 'react-dom';
import * as R from 'ramda';
import Viewer from '../';
import rowData from '../../mocks/row';
import barData from '../../mocks/bar';
import scatterData from '../../mocks/scatter';
import timeData from '../../mocks/gpp-time';

export default ({ config, style, options }) => {
  const style1 = {
    ...style,
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  };

  const style2 = {
    width: 500,
    height: 500,
    padding: 10,
  };

  return (
    <div style={style1}>
      <div style={style2}>
        <Viewer chartOptions={options} type="RowChart" {...rowData()} />
      </div>
      <div style={style2}>
        <Viewer chartOptions={options} type="BarChart" {...barData()} />
      </div>
      <div style={style2}>
        <Viewer chartOptions={options} type="ScatterChart" {...scatterData()} />
      </div>
    </div>
  );
};
