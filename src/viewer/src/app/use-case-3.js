import React from 'react';
import * as R from 'ramda';
import Viewer from '../';
import rowData from '../../mocks/row';
import stackData from '../../mocks/stack';

export default ({ options }) => {
  return (
    <div style={{ display: 'flex' }}>
      <div>
        <Viewer {...rowData()} chartOptions={options} type="RowChart" />
      </div>
      <div>
        <Viewer {...stackData()} chartOptions={options} type="StackedBarChart" />
      </div>
    </div>
  );
};
