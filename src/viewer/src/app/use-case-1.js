import React from 'react';
import * as R from 'ramda';
import Viewer from '../';
import rowData from '../../mocks/row';
//import stackData from '../../mocks/stack';
import { series } from '../../../rules/src';
import data from '../../../rules/test/oecd-HEALTH_PROT.json';

export default ({ config, style, options }) => {
  const type = 'StackedRowChart';
  const stackSeries = series(data, type, null, { mode: 'percent', limit: 5 }, null, null, null);
  return (
    <Viewer
      headerProps={{
        title: { label: 'Science performance (PISA)' },
        subtitle: [{ label: 'Mean score 2015, Annual incl. OECD, all countries, and to make it longer' }],
      }}
      footerProps={{
        logo: 'https://upload.wikimedia.org/wikipedia/en/thumb/0/0d/OECD_logo_new.svg/200px-OECD_logo_new.svg.png',
        copyright: { label: '© OECD', content: "Copyright: terms" },
        source: {
          label: "Labour force by educational attainment",
          link: "data.oecd.org",
        }
      }}
      chartData={{ series: stackSeries }}
      chartOptions={{
        base: { height: 700 }
      }}
      type={type}
    />
  );
};
