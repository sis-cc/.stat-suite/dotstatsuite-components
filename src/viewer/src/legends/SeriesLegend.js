import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import Legend from './Legend';
import { H_SYMBOL, STACKED_BAR, STACKED_ROW, V_SYMBOL } from '../../../rules/src/constants';

const getSymbolSeriesItems = ({ data, options }) => {
  const symbolMarkers = R.path(['serie', 'symbol', 'markers'], options);
  const nMarkers = R.length(symbolMarkers);
  const markerSize = Number(R.path(['serie', 'symbol', 'markerDefaultSize'], options));
  const strokeWidth = R.path(['serie', 'symbol', 'markerDefaultStrokeWidth'], options);
  const size = Math.round(Math.sqrt(markerSize)) + 8;
  return R.pipe(
    R.pathOr([], ['series', 0, 'symbolValues']),
    R.addIndex(R.map)(
      (label, serieIndex) => {
        const marker = R.nth(serieIndex % nMarkers, symbolMarkers);
        return ({
          d: marker.path(markerSize),
          transform: `translate(${size / 2}, ${size / 2}) rotate(${R.propOr(0, 'rotate', marker)})`,
          style: R.pipe(
            R.mergeRight({ strokeWidth }),
            R.when(
              s => s.fill !== s.stroke,
              R.assoc('fill', 'white')
            )
          )(marker.style),
          size,
          label
        })
      }
    )
  )(data);
};

const getStackedSeriesItems = R.pipe(
  R.pathOr([], ['options', 'serie', 'stacked', 'layerSeries']),
  R.when(s => R.length(s) === 1, R.always([]))
);

const stackedLayerRenderer = ({ baseColor }) => (
  <svg width="21" height="14">
    <rect x='0' y='0' width='21' height='14' fill={baseColor} />
  </svg>
);

const symbolRenderer = ({ d, size, style, transform }) => (
  <svg width={size} height={size}>
    <path
      d={d}
      transform={transform}
      {...style}
    />
  </svg>
);

// stacked entries = [{ label, color }];

const SeriesLegend = ({ data, options, type }) => {
  const itemRenderer = R.pipe(
    R.when(R.always(type === H_SYMBOL || type === V_SYMBOL), R.always(symbolRenderer)),
    R.when(R.always(type === STACKED_BAR || type === STACKED_ROW), R.always(stackedLayerRenderer))
  )(null);

  if (R.isNil(itemRenderer)) {
    return null;
  }

  const seriesItems = R.pipe(
    R.when(R.always(type === H_SYMBOL || type === V_SYMBOL), getSymbolSeriesItems),
    R.when(R.always(type === STACKED_BAR || type === STACKED_ROW), getStackedSeriesItems)
  )({ data, options });

  return (
    <Legend items={seriesItems} itemRenderer={itemRenderer} />
  );
};

SeriesLegend.propTypes = {
  data: PropTypes.object,
  options: PropTypes.object,
  type: PropTypes.string,
};

export default SeriesLegend;
