import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import makeStyles from '@mui/styles/makeStyles';
import { getFontFromTheme } from '../utils';

const useStyles = makeStyles(theme => ({
  legend: {
    ...getFontFromTheme(['chartLegend'])(theme),
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
  },
  entry: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginLeft: 10,
  },
  text: { marginLeft: 5 }
}));

const Legend = ({ items, itemRenderer }) => {
  if (!R.is(Function, (itemRenderer)) || R.isNil(items) || R.isEmpty(items)) {
    return null;
  }

  const classes = useStyles();

  const legendEntries = R.addIndex(R.map)(
    (entry, index) => {
      const style = R.isNil(entry.color) ? {} : { color: entry.color };
      return (
        <div className={classes.entry} style={style}  key={index}>
          {itemRenderer(entry)}
          <div className={classes.text} >{R.prop('label', entry)}</div>
        </div>
      );
    }
  )(items);

 return (<div className={classes.legend} >{legendEntries}</div>);
};

Legend.propTypes = {
  itemRenderer: PropTypes.func,
  items: PropTypes.array
};

export default Legend;
