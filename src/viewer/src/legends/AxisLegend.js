import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import makeStyles from '@mui/styles/makeStyles';
import { SCATTER } from '../../../rules/src/constants';
import { getFontFromTheme } from '../utils';

const useStyles = makeStyles(theme => {
  const legendFont = getFontFromTheme(['axisLegend'])(theme);
  return ({
    xLegend: legendFont,
    yLegend: {
      ...legendFont,
      left: 10,
      position: 'absolute',
      zIndex: 1,
    }
  })
});

const getAxisLabel = axis => R.pipe(
  R.propOr([], 'series'),
  R.head,
  R.when(R.isNil, R.always({})),
  R.path(['dimensionValues', axis, 'name'])
);

const AxisLegend = ({ axis, data, type }) => {
  const classes = useStyles();
  if (!R.equals(SCATTER, type))
    return null;
  switch (axis) {
    case 'x':
      return <span className={classes.xLegend}>→ {getAxisLabel('x')(data)}</span>;
    case 'y':
      return <span className={classes.yLegend}>↑ {getAxisLabel('y')(data)}</span>;
    default:
      return null;
  }
};

AxisLegend.propTypes = {
  axis: PropTypes.string.isRequired,
  data: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired,
};

export default AxisLegend;
