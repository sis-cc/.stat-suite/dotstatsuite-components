import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import Legend from './Legend';
import { SCATTER, TIMELINE } from '../../../rules/src/constants';

export const REDUCED_THRESHOLD = 160;

const scatterRenderer = ({ color }) => (
  <svg width='12' height='14'>
    <circle cx='7' cy='7' r='4' stroke={color} fill={color} />
  </svg>
);

const lineRenderer = ({ color }) => (
  <svg width='12' height='14'>
    <line x1='0' y1='7' x2='11' y2='7' stroke={color} strokeWidth='2' />
  </svg>
);

const FocusLegend = ({ data, options, type, width }) => {
  if (width < REDUCED_THRESHOLD) {
    return null;
  }
  
  const itemRenderer = R.pipe(
    R.when(R.always(type === SCATTER), R.always(scatterRenderer)),
    R.when(R.always(type === TIMELINE), R.always(lineRenderer)),
  )(null); 

  const getItemLabel = R.ifElse(
    R.equals(SCATTER),
    R.always(R.pipe(R.prop('label'), R.split(' - '), R.last)),
    R.always(R.prop('label'))
  )(type);

  const items = R.pipe(
    R.pathOr({}, ['share', 'focused']),
    R.mapObjIndexed((selection, key) => {
      if (R.isNil(selection)) {
        return null;
      }
      const colors = R.pathOr([], ['serie', `${key}Colors`], options);
      const nColors = R.length(colors);
      return R.pipe(
        R.addIndex(R.map)((entry, index) => {
          if (entry.value === 'uniq-dp') {
            return null;
          }
          const color = R.nth(index % nColors, colors);
          return ({ color, label: getItemLabel(entry) });
        }),
        R.filter(R.identity)
      )(selection);
    }),
    R.props(['baseline', 'highlight']),
    R.filter(R.identity),
    R.unnest
  )(data)

  return (<Legend items={items} itemRenderer={itemRenderer} />);
}

FocusLegend.propTypes = {
  data: PropTypes.object,
  options: PropTypes.object,
  type: PropTypes.string,
  width: PropTypes.number
};

export default FocusLegend;
