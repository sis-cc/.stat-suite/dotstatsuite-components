import React from 'react';
import * as R from 'ramda';
import numeral from 'numeral';
import { computeOptions } from '@sis-cc/dotstatsuite-d3-charts';
import { ChoroplethLegend } from '../../../bridge-d3-react/src';
import { useTheme } from '@mui/material/styles';
import AxisLegend from './AxisLegend';
import FocusLegend from './FocusLegend';
import SeriesLegend from './SeriesLegend';
import { CHORO } from '../../../rules/src/constants';
import { getFontFromTheme, getOptionsFromFont } from '../utils';


const ChoroLegend = ({ type, options, data }) => {
  if (type !== CHORO) {
    return null;
  }
  const theme = useTheme();
  const fontOptions = R.pipe(getFontFromTheme(['mapLegend']), getOptionsFromFont)(theme);
  const legendOptions = R.mergeDeepLeft(
    options,
    {
      legend: {
        choropleth: {
          width: 300,
          height: 30,
          margin: { left: 15, right: 15 },
          axis: {
            thickness: 0,
            orient: 'bottom',
            font: fontOptions,
            tick: {
              size: 5,
              thickness: 0
            },
            format: {
              proc: d => numeral(d).format('0,0.[00]')
            }
          }
        }
      }
    }
  );

  return (
    <ChoroplethLegend options={legendOptions} data={data.series} />
  );
}

const ChartLegends = ({ data, options, type, width }) => {
  const engineOptions = R.pipe(
    R.assocPath(['serie', 'stacked', 'layerSeries'], R.pathOr([], ['series', 0, 'layerSeries'], data)),
    computeOptions
  )(options);

  return (
    <div>
      <AxisLegend axis="x" data={data} type={type} />
      <FocusLegend data={data} options ={engineOptions} type={type} width={width} />
      <SeriesLegend data={data} options={engineOptions} type={type} />
      <ChoroLegend data={data} options={options} type={type} />
    </div>
  );
};

export default ChartLegends;
