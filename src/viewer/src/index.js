import React, { useEffect, useState } from 'react';
import * as R from 'ramda';
import cx from 'classnames';
import useSize from '@react-hook/size';
import { Loading, NoData, TableHtml5 } from '@sis-cc/dotstatsuite-visions';
import { makeStyles, useTheme } from '@mui/styles';
import getChartOptions from './chartUtils/options';
import Header from './header';
import Chart from './chart';
import Footer from './footer';
import { isChartDataNotReady } from './utils';

const useStyles = makeStyles(() => ({
  container: {
    borderColor: '#007bc7',
    borderBottomWidth: 1,
    borderLeftWidth: 0,
    borderTopWidth: 1,
    borderRightWidth: 0,
    borderStyle: 'solid',
    '& svg text::selection': {
      background: 'none'
    }
  },
  tableContainer: {
    minWidth: '100%',
    minHeight: '100%',
    willChange: 'transform, opacity'
  },
  chartContainer: {
    width: ({ fixedWidth }) => fixedWidth || '100%',
    height: ({ fixedHeight }) => fixedHeight || '100%',
    overflow: 'hidden',
    position: 'relative'
  }
}));

const ViewContent = ({ loading, loadingProps={}, type, width, errorMessage, ...rest }) => {
  if (loading)
    return <Loading message={loading} {...loadingProps} />;
  if (errorMessage)
    return <NoData message={errorMessage} />;

  if (type === 'table') {
    const tableProps = R.propOr({}, 'tableProps', rest);
    const hasNoObs = R.pipe(R.prop('cells'), R.anyPass([R.isNil, R.isEmpty]))(tableProps);
    if (hasNoObs) return <Loading />;

    return <TableHtml5 isRtl={R.prop('isRtl', rest)} {...tableProps} />;
  }
  if (isChartDataNotReady({ data: R.prop('chartData', rest) }) || !width) {
    return <Loading />;
  }
  return (
    <Chart
      data={R.prop('chartData', rest)}
      getAxisOptions={R.prop('getAxisOptions', rest)}
      heightOffsets={R.prop('heightOffsets', rest)}
      options={R.prop('chartOptions', rest)}
      type={type}
      width={width}
    />
  );
};

const Viewer = ({ type, targets={}, width, ...rest }) => {
  const classes = useStyles({
    fixedWidth: rest.fixedWidth,
    fixedHeight: rest.fixedHeight
  });

  return (
    <div
      {...(targets.viewer ? { ref: targets.viewer } : {})}
      className={cx(classes.container, {
        [classes.tableContainer]: type === 'table',
        [classes.chartContainer]: type !== 'table'
      })}
    >
      <Header
        target={targets.header}
        {...R.propOr({}, 'headerProps', rest)}
      />
      <ViewContent type={type} width={width} {...rest} />
      <Footer
        width={width}
        target={targets.footer}
        type={type}
        chartData={R.prop('chartData', rest)}
        chartOptions={R.prop('chartOptions', rest)}
        {...R.prop('footerProps', rest)}
      />
    </div>
  );
};

const defaultChartHeight = chartOptions => R.pipe(
  R.path(['base', 'height']),
  h => (R.isNil(h) || isNaN(h)) ? 0 : h
)(chartOptions);

const ViewerWrapper = ({ chartOptions, type, getResponsiveSize, ...props }) => {
  const theme = useTheme();
  const viewerTarget = React.useRef(null);
  const headerTarget = React.useRef(null);
  const footerTarget = React.useRef(null);
  const [viewerWidth, viewerHeight] = useSize(viewerTarget);
  const [_, headerHeight] = useSize(headerTarget, { initialHeight: -1 });
  const [___, footerHeight] = useSize(footerTarget, { initialHeight: -1 });
  const [chartSvgHeight, setChartSvgHeight] = useState(0);
  const defChartHeight = defaultChartHeight(chartOptions);

  useEffect(() => {
    if (R.is(Function, getResponsiveSize) && viewerWidth && headerHeight !== -1 && footerHeight !== -1) {
      getResponsiveSize({
        responsiveWidth: viewerWidth,
        responsiveHeight: viewerHeight
      });
    }
  }, [viewerWidth, viewerHeight]);

  useEffect(() => {
    if (headerHeight !== -1 && footerHeight !== -1 && !isNaN(headerHeight) && !isNaN(footerHeight)) {
      const nextHeight = defChartHeight - headerHeight - footerHeight - 2;
      setChartSvgHeight(nextHeight);
    }
  }, [headerHeight, footerHeight, defChartHeight]);


  if (type === 'table') {
    return <Viewer
      {...props}
      width={viewerWidth}
      type={type}
    />;
  }

  const preparedChartOptions = R.pipe(
    R.over(R.lensPath(['base', 'width']), R.when(R.anyPass([R.isNil, isNaN]), R.always(viewerWidth || 0))),
    R.set(R.lensPath(['base', 'height']), chartSvgHeight),
    chartOptions => getChartOptions(chartOptions, props.chartData, type, theme, props.timeFormats, props.locale),
  )(chartOptions);

  return <Viewer
    {...props}
    type={type}
    chartOptions={preparedChartOptions}
    fixedWidth={R.path(['base', 'width'], chartOptions)}
    fixedHeight={defChartHeight}
    targets={{
      viewer: viewerTarget,
      header: headerTarget,
      footer: footerTarget
    }}
    width={viewerWidth}
  />;
};

export default ViewerWrapper;
