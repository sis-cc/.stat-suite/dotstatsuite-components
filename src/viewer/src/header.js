import React from 'react';
import { DataHeader } from '@sis-cc/dotstatsuite-visions';

const Header = ({ target, ...props }) => (
  <div {...(target ? { ref: target } : {})}>
    <DataHeader {...props} />
  </div>
);

export default Header;
