import React from 'react';
import * as R from 'ramda';
import { DataFooter } from '@sis-cc/dotstatsuite-visions';
import ChartLegends from './legends/ChartLegends';
import { isChartNoData } from './utils';

const Footer = ({ type, chartData, source, logo, copyright, chartOptions, isSticky, target, width }) => {
  const hasNoLegend = type === 'table' || isChartNoData({ data: chartData, type });
  const legend = hasNoLegend ? null : <ChartLegends data={chartData} options={chartOptions} type={type} width={width} />;

  return (
    <div {...(target ? { ref: target } : {})}>
      <DataFooter
        source={source}
        logo={logo}
        copyright={copyright}
        legend={legend}
        isSticky={isSticky}
      />
    </div>
  );
};

export default Footer;
