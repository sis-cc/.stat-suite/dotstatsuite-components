export default () => ({
  chartData: {
    series: [
      { datapoints: [
        {y: 'Hungary',                       x: 5.3},
        {y: 'Sweden',                        x: 5.6},
        {y: 'Estonia',                       x: 5.7},
        {y: 'Finland',                       x: 5.8},
        {y: 'Belgium',                       x: 5.9},
        {y: 'Turkey',                        x: 6},
        {y: 'France',                        x: 6.4},
        {y: 'Poland',                        x: 6.6},
        {y: 'Slovenia',                      x: 6.8},
        {y: 'European Union (28 countries)', x: 7.5878787878787878, baselineIndex: 0},
        {y: 'Euro area (18 countries)',      x: 8.2},
        {y: 'Ireland',                       x: 8.0000000000000000, highlightIndex: 0},
        {y: 'Italy',                         x: 8.9},
        {y: 'Slovak Republic',               x: 9.9},
        {y: 'Portugal',                      x: 10.9},
        {y: 'South Africa',                  x: 16.8},
        {y: 'Spain',                         x: 17.546484545454543, highlightIndex: 1},
        {y: 'Greece',                        x: 19.1, highlightIndex: 2}
        ]
      }
    ]
  },
  headerProps: {
    title: { label: 'Science performance (PISA)' },
    subtitle: [{ label: 'Mean score 2015, Annual incl. OECD, all countries, and to make it longer' }],
  },
  footerProps: {
    logo: 'https://upload.wikimedia.org/wikipedia/en/thumb/0/0d/OECD_logo_new.svg/200px-OECD_logo_new.svg.png',
    copyright: { label: '© OECD', content: "Copyright: terms" },
    source: {
      label: "Labour force by educational attainment",
      link: "data.oecd.org",
    },
  },
});
