export default () => ({
  headerProps: {
    title: { label: 'Science performance (PISA)' },
    subtitle: [{ label: 'Mean score 2015, Annual incl. OECD, all countries, and to make it longer' }],
  },
  footerProps: {
    logo: 'https://upload.wikimedia.org/wikipedia/en/thumb/0/0d/OECD_logo_new.svg/200px-OECD_logo_new.svg.png',
    copyright: { label: '© OECD', content: "Copyright: terms" },
    source: {
      label: "Labour force by educational attainment",
      link: "data.oecd.org",
    },
  },
  chartData: {
    series: [
      {
        "layerSeries": [
          {
            "id": "PRINTO01",
            "label": "Industrial production, s.a."
          },
          {
            "id": "PRMNTO01",
            "label": "Total manufacturing, s.a."
          }
        ],
        "datapoints": [
          {
            "category": "Czech Republic",
            "formaters": [
              null,
              null
            ],
            "formatedValues": [
              "4.623184",
              "6.0190227"
            ],
            "index": 4,
            "x": "Czech Republic",
            "y": [
              4.62318401937048,
              6.01902272562258
            ],
            "key": "CZE"
          },
          {
            "formaters": [
              null,
              null
            ],
            "category": "Austria",
            "formatedValues": [
              "2.1199507",
              "2.3654597"
            ],
            "index": 1,
            "x": "Austria",
            "y": [
              2.11995066296641,
              2.36545969532268
            ],
            "key": "AUT"
          },
          {
            "formaters": [
              null,
              null
            ],
            "category": "France",
            "formatedValues": [
              "1.9636044",
              "1.9280648"
            ],
            "index": 7,
            "x": "France",
            "y": [
              1.96360436882936,
              1.92806480043649
            ],
            "key": "FRA",
            highlightIndex: 1,
          },
          {
            "formaters": [
              null,
              null
            ],
            "category": "Chile",
            "formatedValues": [
              "0.230173",
              "0.2863486"
            ],
            "index": 8,
            "x": "Chile",
            "y": [
              2.230172987500003,
              4.286348587500551
            ],
            "key": "CHL",
            highlightIndex: 0,
          },
          {
            "formaters": [
              null,
              null
            ],
            "category": "Belgium",
            "formatedValues": [
              "-0.0160655",
              "0.1776473"
            ],
            "index": 2,
            "x": "Belgium",
            "y": [
              -7.0160655474335108,
              9.177647331428157
            ],
            "key": "BEL",
            baselineIndex: 0,
          },
          {
            "formaters": [
              null,
              null
            ],
            "category": "Finland",
            "formatedValues": [
              "-0.9588318",
              "-0.9386407"
            ],
            "index": 6,
            "x": "Finland",
            "y": [
              -0.958831808585503,
              -0.938640709195226
            ],
            "key": "FIN"
          },
          {
            "formaters": [
              null,
              null
            ],
            "category": "China",
            "formatedValues": [
              "-0.0160655",
              "0.1776473"
            ],
            "index": 2,
            "x": "China",
            "y": [
              -7.0160655474335108,
              9.177647331428157
            ],
            "key": "CHN",
          },
        ],
      },
    ],
  }
});
