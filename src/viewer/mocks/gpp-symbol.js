export default () => ({
  headerProps: {
    "title": { label: "Growth previous period - Annual - 2015" },
  },
  footerProps: {
    soure: { label: 'source', link: 'http://dotstat.oecd.org/' }
  },
  chartData: {
  "series": [
    {
      "datapoints": [
        {
          "category": "Finland",
          "values": [
            -0.958831808585503,
            -0.938640709195226
          ],
          highlightIndex: 0
        },
        {
          "category": "Canada",
          "values": [
            -0.734931326089172,
            0.178223109790082
          ]
        },
        {
          "category": "Belgium",
          "values": [
            -0.0160655474335108,
            0.177647331428157
          ],
          highlightIndex: 1
        },
        {
          "category": "Chile",
          "values": [
            0.230172987500003,
            0.286348587500551
          ]
        },
        {
          "category": "Estonia",
          "values": [
            0.292360493199452,
            1.4055978394304
          ]
        },
        {
          "category": "Australia",
          "values": [
            1.18005263064255,
            -1.70548071959404
          ],
          baselineIndex: 0
        },
        {
          "category": "Denmark",
          "values": [
            1.18634492777014,
            2.47915590644139
          ]
        },
        {
          "category": "France",
          "values": [
            1.96360436882936,
            1.92806480043649
          ]
        },
        {
          "category": "Austria",
          "values": [
            2.11995066296641,
            2.36545969532268
          ]
        },
        {
          "category": "Czech Republic",
          "values": [
            4.62318401937048,
            6.01902272562258
          ]
        }
      ],
      "symbolValues": [
        "Industrial production, s.a.",
        "Total manufacturing, s.a."
      ]
    }
  ],
  }
})