import * as R from 'ramda';
import { getFlagsAndNotes } from './table/getFlagsAndNotes';

export const getManyValuesDimensions = (dimensions, attributesSeries, customAttributes, combinations) => {
  const combConceptsIds = R.pipe(R.pluck('concepts'), R.unnest)(combinations);
  return R.reduce(
    (acc, dim) => {
      if (dim.header || !R.length(dim.values)) {
        return acc;
      }
      const enhancedValues = R.map(val => {
        const attrSeries = R.propOr({}, `${dim.id}=${val.id}`, attributesSeries);
        if (R.isEmpty(attrSeries)) {
          return val;
        }
        const flags = getFlagsAndNotes(attrSeries, customAttributes);
        const hasAdvancedAttributes = R.pipe(
          R.omit(
            R.unnest([
              customAttributes.flags || [],
              customAttributes.notes || [],
              combConceptsIds,
            ]),
          ),
          R.complement(R.isEmpty),
        )(attrSeries);
        return { ...val, flags, hasAdvancedAttributes };
      }, dim.values);
      return R.append(R.assoc('values', enhancedValues, dim), acc);
    },
    [],
    dimensions,
  );
};
