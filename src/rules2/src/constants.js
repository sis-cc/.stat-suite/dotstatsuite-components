export const SDMX_3_0_JSON_DATA_FORMAT = 'application/vnd.sdmx.data+json;version=2.0';
export const SDMX_3_0_CSV_DATA_FORMAT = 'application/vnd.sdmx.data+csv;version=2.0';
export const EMPTY_ATTACHMENT_LEVEL_CHAR = '~';
export const REPORTING_YEAR_START_DAY = 'REPORTING_YEAR_START_DAY';
export const REPYEARSTART = 'REPYEARSTART';
export const REJECTED_VALUE_IDS = ['_L', '_T', '_Z'];
