import * as R from 'ramda';

export const getDataflowMetadataStructure = sdmxJson => {
  const annotations = R.pathOr([], ['data', 'stucture', 'annotations'], sdmxJson);
  const dataSetAnnotIndexes = R.pathOr([], ['data', 'dataSets', 0, 'annotations'], sdmxJson);
  const dataSetAnnotations = R.props(dataSetAnnotIndexes, annotations);
  const metadataAnnotation = R.find(R.propEq('type', 'METADATA'), dataSetAnnotations);
  if (!metadataAnnotation) {
    return null;
  }
  return R.pipe(
    R.propOr('', 'title'),
    R.split('='),
    R.last,
    
  )(metadataAnnotation);
};
