import * as R from 'ramda';
import { REPORTING_YEAR_START_DAY, REPYEARSTART } from './constants';

export const parseAttributes = (attributes, dimensions, customAttributes) => {
  const indexedDimensions = R.indexBy(R.prop('id'), dimensions);
  return R.addIndex(R.map)((attr, index) => {
    let res = R.assoc('index', index, attr);
    const displayableValuesCount = R.pipe(
      R.filter(R.propOr(true, 'display')),
      R.length,
    )(attr);
    if (R.isEmpty(attr.values || []) || !R.propOr(true, 'display', attr)
      || displayableValuesCount < 1 || attr.id === REPORTING_YEAR_START_DAY
      || attr.id === REPYEARSTART) {
      return R.assoc('display', false, res);
    }
    if (R.propEq('id', customAttributes.prefscale, attr)) {
      return R.assoc('prefscale', true, res);
    }
    if (R.propEq('id', customAttributes.decimals, attr)) {
      return R.assoc('decimals', true, res);
    }
    if (R.hasPath(['relationship', 'none'], attr) || R.hasPath(['relationship', 'dataflow'], attr)) {
      return { ...res, header: true, relationship: [] };
    }
    if (R.hasPath(['relationship', 'primaryMeasure'], attr) || R.hasPath(['relationship', 'observation'], attr)) {
      return { ...res, series: true, relationship: [] };
    }
    const seriesDimensions = R.pipe(
      R.pathOr([], ['relationship', 'dimensions']),
      R.filter(id => {
        const dimension = R.prop(id, indexedDimensions);
        return !dimension.header && R.length(dimension.values || []) > 1;
      }),
    )(attr);
    if (R.length(seriesDimensions) > 0) {
      return { ...res, series: true, relationship: seriesDimensions };
    }
    return { ...res, header: true, relationship: R.prop('dimensions', attr.relationship || []) };
  }, attributes);
};
