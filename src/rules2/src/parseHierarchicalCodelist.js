import * as R from 'ramda';

export const parseHierarchicalCodelist = (sdmxJson, hierarchyId) => {
  const hCodelist = R.pathOr({}, ['data', 'hierarchicalCodelists', 0], sdmxJson);
  const hierarchy = R.find(R.propEq('id', hierarchyId), hCodelist.hierarchies || []);

  const getParentedCodes = (codes, parent) => {
    const ids = R.pluck('codeID', codes);

    const siblings = R.reduce(
      (acc, code) => {
        if (R.isEmpty(code.hierarchicalCodes || [])) {
          return acc;
        }
        return R.mergeRight(acc, getParentedCodes(code.hierarchicalCodes, code.id));
      },
      {},
      codes
    );

    return ({ ...siblings, [parent]: ids });
  };

  const getChildren = (codes, parents) => {
    const ids = R.pluck('codeID', codes);

    const children = R.reduce(
      (acc, code) => {
        if (R.isEmpty(code.hierarchicalCodes || [])) {
          return acc;
        }
        return ({
          ...acc,
          ...getChildren(code.hierarchicalCodes, R.append(code.codeID, parents))
        });
      },
      {},
      codes
    );
    const key = R.isEmpty(parents) ? '#ROOT' : R.join('.', parents);
    return ({ ...children, [key]: ids });
  }

  return R.pipe(
    R.propOr([], 'hierarchicalCodes'),
    codes => getChildren(codes, []),
  )(hierarchy);
};
