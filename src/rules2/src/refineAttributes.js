import * as R from 'ramda';

export const refineAttributes = (parsedAttributes, seriesCombinations) => {
  const indexedRelationships = R.reduce(
    (acc, comb) =>
      R.reduce((_acc, concept) => R.assoc(concept, comb.relationship, _acc), acc, comb.concepts),
    {},
    seriesCombinations,
  );
  return R.map(attr => {
    if (R.has(attr.id, indexedRelationships)) {
      return { ...attr, series: true, relationship: R.prop(attr.id, indexedRelationships), combined: true };
    }
    return attr;
  }, parsedAttributes);
};
