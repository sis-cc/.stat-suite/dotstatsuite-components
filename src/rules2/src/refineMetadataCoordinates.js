import * as R from 'ramda';

export const refineMetadataCoordinates = (metadataCoordinates, layoutIds) => {
  const relevantIds = R.pipe(R.values, R.unnest)(layoutIds);
  return R.reduce(
    (acc, coordinates) => {
      const refinedCoord = R.pick(relevantIds, coordinates);
      if (R.isEmpty(refinedCoord)) {
        return acc;
      }
      const removedHeaderCodes = R.omit(layoutIds.header, refinedCoord);
      if (R.isEmpty(removedHeaderCodes)) {
        return R.over(R.lensProp('header'), R.append(refinedCoord))(acc);
      }
      const removedSectionCodes = R.omit(layoutIds.sections, refinedCoord);
      if (R.isEmpty(removedSectionCodes)) {
        return R.over(R.lensProp('sections'), R.append(refinedCoord))(acc);
      }
      const removedRowsCodes = R.omit(layoutIds.rows, removedSectionCodes);
      if (R.isEmpty(removedRowsCodes)) {
        return R.over(R.lensProp('rows'), R.append(refinedCoord))(acc);
      }
      return R.over(R.lensProp('cells'), R.append(refinedCoord))(acc);
    }, 
    { cells: [], header: [], sections: [], rows: [] },
    metadataCoordinates
  );
};
