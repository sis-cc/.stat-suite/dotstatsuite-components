import * as R from 'ramda';

export const getAttributesSeries = observations => R.pipe(
  R.values,
  R.reduce(
    (acc, { attributes }) => {
      let res = acc.res;
      let rejected = acc.rejected;
      R.forEachObjIndexed(
        (attribute) => {
          if (!R.isNil(attribute.serieKey) && !attribute.isObs) {
            if (R.isNil(attribute.value)) {
              res = R.dissocPath([attribute.serieKey, attribute.id], res);
              rejected = R.assocPath([attribute.serieKey, attribute.id], true, rejected);
            }
            if (R.path([attribute.serieKey, attribute.id], rejected)) {
              return ;
            }
            const valueInRes = R.path([attribute.serieKey, attribute.id, 'value', 'id'], res);
            if (!R.isNil(valueInRes) && valueInRes !== R.path(['value', 'id'], attribute)) {
              res = R.dissocPath([attribute.serieKey, attribute.id], res);
              rejected = R.assocPath([attribute.serieKey, attribute.id], true, rejected);
            }
            else {
              res = R.assocPath([attribute.serieKey, attribute.id], attribute, res);
            }
          }
        },
        attributes
      );
      return ({ res, rejected });
    },
    { res: {}, rejected: {}, }
  ),
  R.prop('res')
)(observations);
