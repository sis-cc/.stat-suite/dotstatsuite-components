import * as R from 'ramda';

const addParentId = (id, ids) => {
  const last = R.last(ids);
  return R.append(R.isNil(last) ? id : `${last}.${id}`, ids); 
}

const refineUnconfirmedPaths = (paths) => {
  let confirmedPaths = R.filter(p => p.length === 1, paths);
  if (!R.isEmpty(confirmedPaths)) {
    confirmedPaths = R.reduce((acc, k) => {
      if (R.has(k, acc)) {
        return acc;
      }
      const ps = R.prop(k, paths);
      const flatConfirmed = R.unnest(R.values(acc));
      const filtered = R.filter(p => !!R.find(cP => R.includes(p, cP) || R.includes(cP, p), flatConfirmed), ps);
      if (!R.isEmpty(filtered)) {
        return R.assoc(k, [R.head(filtered)], acc);
      }
      return acc;
    }, confirmedPaths, R.keys(paths));
  }
  const remainingIds = R.reject(id => R.has(id, confirmedPaths), R.keys(paths));
  const getCommonPaths = ids => {
    const { commonPaths, rejectedIds } = R.reduce(
      (acc, id) => {
        const ps = R.prop(id, paths);
        if (R.isEmpty(acc.commonPaths)) {
          return R.assoc('commonPaths', ps, acc);
        }
        const common = R.intersection(acc.commonPaths, ps);
        return R.isEmpty(common)
          ? R.assoc('rejectedChildrenIds', id, acc)
          : R.assoc('commonPaths', common, acc);
      },
      { commonPaths: [], rejectedIds: [] }
    )(ids);
    if (!R.isEmpty(rejectedIds)) {
      return R.append(R.head(commonPaths), getCommonPaths(rejectedIds));
    }
    return [R.head(commonPaths)];
  };
  const commons = getCommonPaths(remainingIds);
  return R.reduce(
    (acc, id) => R.assoc(id, R.intersection(commons, R.prop(id, paths)), acc),
    confirmedPaths,
    remainingIds
  );
};

export const applyHierarchicalCodesToDim = (hCodes, dim) => {
  const confirmedHierarchisedChilds = new Set([]);
  let unconfirmedPaths = {};

  const indexedValues = R.reduce((acc, val) => {
    if (!val.isSelected) {
      return acc;
    }
    return R.assoc(val.id, val, acc);
  }, {}, dim.values || []);

  const parseHCodes = (parentsDef, parentsInDim) => R.reduce((acc, hC) => {
    const { codeID, hierarchicalCodes = [] } = hC;
    const hasInDim = R.has(codeID, indexedValues);
    const isUnconfirmed = R.length(parentsInDim) !== R.length(parentsDef);
    const children = parseHCodes(
      R.append(codeID, parentsDef),
      hasInDim ? addParentId(codeID, parentsInDim) : parentsInDim
    )(hierarchicalCodes);
    if (!hasInDim) {
      return R.concat(acc, children);
    }
    const path = R.join('.', parentsDef);
    let val = {
      ...R.prop(codeID, indexedValues),
      path,
      parents: parentsInDim,
      parent: R.last(parentsInDim),
      children
    };

    if (isUnconfirmed) {
      unconfirmedPaths = R.over(R.lensProp(val.id), paths => R.append(path, paths || []), unconfirmedPaths);
      val = R.assoc('unconfirmed', true, val);
      return R.append(val, acc);
    }
    confirmedHierarchisedChilds.add(codeID);
    return R.append(val, acc);
  }, []);

  const parsed = parseHCodes([], [])(hCodes);

  const _unconfirmedPaths = R.pipe(
    R.reject(id => confirmedHierarchisedChilds.has(id)),
    ids => R.pick(ids, unconfirmedPaths)
  )(R.keys(unconfirmedPaths));

  const refinedUnconfirmedPaths = refineUnconfirmedPaths(_unconfirmedPaths);

  const flattenDescendants = R.reduce((acc, val ) => {
    const children = flattenDescendants(val.children || []);
    return R.concat(acc, R.prepend(val, children));
  }, []);

  const isValid = val => !val.unconfirmed || R.includes(val.path, R.propOr([], val.id, refinedUnconfirmedPaths));

  const refineCodes = R.reduce((acc, hC) => {
    const _flatDescendants = flattenDescendants(hC.children);
    const flatDescendants = R.prepend(hC, _flatDescendants);
    const validDescendant = R.find(isValid, flatDescendants);
    if (!validDescendant) {
      return acc;
    }
    const refined = R.pipe(
      R.map(v => {
        confirmedHierarchisedChilds.add(v.id);
        return R.omit(['unconfirmed', 'children', 'path'], v);
      })
    )(flatDescendants);
    return R.concat(acc, refined);
  }, []);

  const refined = refineCodes(parsed);

  const missingValues = R.pipe(
    R.keys,
    R.filter(id => !confirmedHierarchisedChilds.has(id)),
    ids => R.props(ids, indexedValues),
    R.map(v => ({ ...v, parents: [], parent: undefined })),
    R.sortBy(R.propOr(-1, '__indexPosition')),
  )(indexedValues);

  let hierarchicalIndexes = {};
  return R.pipe(
    R.concat,
    R.addIndex(R.map)((v, ind) => {
      const parents = R.props(v.parents, hierarchicalIndexes);
      const hierId = R.last(v.parents) ? `${R.last(v.parents)}.${v.id}` : v.id;
      hierarchicalIndexes[hierId] = ind;
      return {
        ...v,
        parents,
        __indexPosition: ind
      };
    }),
    values => ({ ...dim, values })
  )(missingValues, refined);
};
