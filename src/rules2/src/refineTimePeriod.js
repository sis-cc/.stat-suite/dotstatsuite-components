import * as R from 'ramda';
import * as dateFns from 'date-fns';
import { getLocale, dateWithoutTZ } from '../../rules/src/date';

const computeDisplayFreq = (start, duration) => {
  const startMinute = dateFns.startOfMinute(start);
  const startHour = dateFns.startOfHour(start);
  const startDay = dateFns.startOfDay(start);
  if (!dateFns.isEqual(start, startMinute) || duration === 'S') {
    return 'S';
  }
  else if (!dateFns.isEqual(start, startHour) || !dateFns.isEqual(start, startDay)
    || duration === 'H' || duration === 'm') {
    return 'm';
  }
  else if (!dateFns.isFirstDayOfMonth(start) || duration === 'D') {
    return 'D';
  }
  else if (dateFns.getMonth(start) !== 0 || duration === 'M') {
    return 'M';
  }
  return 'Y';
};

const getAdder = (duration) => {
  if (duration === 'S')
    return dateFns.addSeconds;
  if (duration === 'm')
    return dateFns.addMinutes;
  if (duration === 'H')
    return dateFns.addHours;
  if (duration === 'D')
    return dateFns.addDays;
  if (duration === 'M')
    return dateFns.addMonths;
  return dateFns.addYears;
};

const getFormat = (freqDisplay) => {
  if (freqDisplay === 'Y')
    return 'YYYY';
  if (freqDisplay === 'D')
    return 'YYYY-MM-DD';
  if (freqDisplay === 'H' || freqDisplay === 'm') {
    return 'HH:mm';
  }
  if (freqDisplay === 'S') {
    return 'HH:mm:ss';
  }
  return 'YYYY-MMM';
}

const getEndDate = (start, mult, duration) => {
  const adder = getAdder(duration);
  const endDate = adder(start, Number(mult));
  return dateFns.subSeconds(endDate, 1);
};

const computeName = (start, end, freqDisplay, locale, format) => {
  const opts = { locale: getLocale(locale) };
  const isSameDay = dateFns.isSameDay(start, end);
  const isSameYear = dateFns.isSameYear(start, end);
  if (freqDisplay === 'H' || freqDisplay === 'm' || freqDisplay === 'S') {
    const dayFormat = getFormat('D');
    if (isSameDay)
      return `${dateFns.format(start, dayFormat, opts)} ${dateFns.format(start, format, opts)} - ${dateFns.format(end, format, opts)}`;
    return `${dateFns.format(start, `${dayFormat} ${format}`, opts)} - ${dateFns.format(end, `${dayFormat} ${format}`, opts)}`;
  }
  if (freqDisplay !== 'M') {
    return `${dateFns.format(start, format, opts)} - ${dateFns.format(end, format, opts)}`;
  }

  if (isSameYear && !R.includes('-', format)) {
    const yearFormatMatch = R.match(/([Y]+)/, format);
    const monthFormatMatch = R.match(/([M]+|Mo)/, format);
    const yearFormat = R.length(yearFormatMatch) >= 2 ? R.nth(1, yearFormatMatch) : null;
    const monthFormat = R.length(monthFormatMatch) >= 2 ? R.nth(1, monthFormatMatch) : null;
    const formattedYear = yearFormat ? dateFns.format(start, yearFormat, opts) : null;
    const formattedMonths = monthFormat ? `${dateFns.format(start, monthFormat, opts)} - ${dateFns.format(end, monthFormat, opts)}` : null;
    if (monthFormat) {
      return R.pipe(
        format => yearFormat ? R.replace(yearFormat, formattedYear, format) : format,
        R.replace(monthFormat, formattedMonths)
      )(format);
    }
  }
  return `${dateFns.format(start, format, opts)} - ${dateFns.format(end, format, opts)}`;
};

export const getStartDate = (start) => {
  if (!R.includes('T', start) && !R.endsWith('Z', start)) {
    return dateWithoutTZ(new Date(start));
  }
  return new Date(start);
};

const dateWithTZ = date => dateFns.addMinutes(date, -date.getTimezoneOffset());

export const refineTimePeriod = (timePeriod, { locale, monthlyFormat='YYYY-MMM' }) => {
  const { id } = timePeriod;
  const split = R.split('/', id);
  if (R.length(split) !== 2) {
    const start = getStartDate(R.prop('start', timePeriod));
    const end = getStartDate(R.prop('end', timePeriod))
    return ({ ...timePeriod, start: dateWithTZ(start).toISOString(), end: dateWithTZ(end).toISOString() });
  }
  const [start, range] = split;
  const startDate = getStartDate(start);
  const match = R.match(/^P(T?)(\d+)([YMDHS])$/, range);
  if (R.isEmpty(match) || !dateFns.isValid(startDate)) {
    return timePeriod;
  }
  const [timeIndicator, mult, _duration] = R.tail(match);
  const duration = !R.isEmpty(timeIndicator) && _duration === 'M' ? 'm' : _duration;
  const endDate = getEndDate(startDate, mult, duration);
  const freqDisplay = computeDisplayFreq(start, duration);
  const format = freqDisplay === 'M' ? monthlyFormat : getFormat(freqDisplay);
  const name = computeName(startDate, endDate, freqDisplay, locale, format);

  return ({
    ...timePeriod,
    id,
    name,
    start: dateWithTZ(startDate).toISOString(),
    end: dateWithTZ(endDate).toISOString()
  })
};
