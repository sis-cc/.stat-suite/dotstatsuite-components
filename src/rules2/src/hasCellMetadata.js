import * as R from 'ramda';

export const hasCellMetadata = (metadataCoordinates, cellCoordinates) => {
  if (R.propOr('OBS_VALUE', 'OBS_ATTRIBUTES', cellCoordinates) !== 'OBS_VALUE') {
    return false;
  }
  return R.pipe(
    R.find(coordinates => {
      const mergedCoord = R.mergeLeft(coordinates, cellCoordinates);
      return !R.isEmpty(coordinates) && R.equals(mergedCoord, cellCoordinates);
    }),
    R.complement(R.isNil)
  )(metadataCoordinates);
};
