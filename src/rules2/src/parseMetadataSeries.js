import * as R from 'ramda';
import { EMPTY_ATTACHMENT_LEVEL_CHAR } from './constants';

const dimensionValueDisplay = (display) => data => {
  if (display === 'code') {
    return R.prop('id', data);
  }
  const name = R.prop('name')(data);
  if (display === 'both') {
    return `${R.prop('id', data)}: ${name}`;
  }
  return name;
};
// options = { locale, display, dimensions = [], attributes = { [id]: { format, parent } } };
export const parseMetadataSeries = (metadataJson, options) => {
  const metadataAttributes = R.pathOr([], ['data', 'structures', 0, 'attributes', 'dimensionGroup'], metadataJson);
  const metaAttrLength = R.length(metadataAttributes);

  if (!metaAttrLength) {
    return ({});
  }

  const dimensions = R.pipe(
    R.pathOr([], ['data', 'structures', 0, 'dimensions']),
    ({ series = [], observation = [] }) => R.concat(series, observation),
    dims => R.isEmpty(options.dimensions || [])
      ? dims
      : R.props(R.pluck('id', options.dimensions), R.indexBy(R.prop('id'), dims))
  )(metadataJson);

  const metadataSeries = R.pipe(
    R.pathOr({}, ['data', 'dataSets', 0, 'dimensionGroupAttributes']),
    series => R.reduce((acc, serieKey) => {
      const indexes = series[serieKey];
      const metaIndexes = R.take(metaAttrLength, indexes);

      const evolvedKey = R.pipe(
        R.split(':'),
        R.addIndex(R.map)((vInd, dInd) => {
          if (R.isEmpty(vInd) || R.equals(EMPTY_ATTACHMENT_LEVEL_CHAR, vInd)) {
            return '';
          }
          const dim = R.nth(dInd, dimensions);
          const val = R.nth(Number(vInd), dim.values || []);

          const originalVal = R.find(
            R.propEq('id', val.id),
          )(R.propOr([], 'values', R.nth(dInd, options.dimensions)));
          return R.propOr('', '__index', originalVal);
        }),
        R.join(':')
      )(serieKey);

      const attributes = R.pipe(
        R.addIndex(R.reduce)(
          (acc, valueIndex, attrIndex) => {
            const attribute = R.nth(attrIndex, metadataAttributes);
            if (R.isNil(valueIndex) && !(attribute.isPresentational)) {
              return acc;
            }
            const attrOptions = R.pathOr({}, ['attributes', attribute.id], options);
            const id = attrOptions.id;
            const label = dimensionValueDisplay(options.display)({ ...attribute, id });

            const _value = R.prop('value', R.nth(valueIndex, attribute.values || []));
            const value = R.is(Object, _value) ? R.prop(options.locale, _value) : _value;

            return R.append({
              ...(R.isNil(value) ? {} : { value }),
              id: attribute.id,
              label,
              handlerProps: R.omit(['id'], R.pathOr({}, ['attributes', attribute.id], options))
            }, acc);
          },
          [],
        ),
        attributes => { // missing parents in metadata response 
          const groupedAttrs = R.groupBy(R.pathOr('#ROOT', ['handlerProps', 'parent']), attributes);
          const indexedAttrs = R.indexBy(R.prop('id'), attributes);

          return R.reduce(
            (acc, id) => {
              if (R.has(id, indexedAttrs) || id === '#ROOT') {
                return acc;
              }
              return R.append({
                id,
                label: dimensionValueDisplay(options.display)({ id, name: R.path(['attributes', id, 'name'], options) }),
                handlerProps: R.omit(['id'], R.pathOr({}, ['attributes', id], options))
              }, acc);
            },
            attributes,
            R.keys(groupedAttrs)
          );
        },
        attributes => {
          const groupedAttrs = R.groupBy(R.pathOr('#ROOT', ['handlerProps', 'parent']), attributes);
          const makeTree = R.map(attr => {
            if (R.has(attr.id, groupedAttrs)) {
              const children = makeTree(R.prop(attr.id, groupedAttrs));
              return R.assoc('children', children, attr);
            }
            return attr;
          });

          return makeTree(R.propOr([], '#ROOT', groupedAttrs));
        },
      )(metaIndexes);

      return R.assoc(evolvedKey, attributes, acc);
    },
      {},
      R.keys(series)
    ))(metadataJson);

  return metadataSeries;
};