import * as R from 'ramda';

const attribueValueDisplay = (display) => data => {
  if (display === 'code') {
    return R.prop('id', data);
  }
  if (display ===  'both') {
    return `${R.prop('id', data)}: ${R.prop('name', data)}`;
  }
  return R.prop('name', data);
};

export const getAdvAttrSeriesAtCoordinates = (coordinates, advancedAttributes, display) => R.pipe(
  R.filter(serie => {
    const mergedCoord = R.mergeLeft(serie.coordinates, coordinates);
    return R.equals(mergedCoord, coordinates);
  }),
  R.map(serie => {
    return R.pipe(
      R.propOr({}, 'attributes'),
      R.values,
      R.map(attribute => ({
        id: attribute.id,
        label: attribueValueDisplay(display)(attribute),
        value: attribueValueDisplay(display)(attribute.value)
      }))
    )(serie);
  }),
)(advancedAttributes);
