import * as R from 'ramda';

export const getHeaderCoordinates = (oneValueDimensions) => R.reduce(
  (acc, dim) => R.assoc(dim.id, R.path(['values', 0, 'id'], dim), acc),
  {},
  oneValueDimensions
);
