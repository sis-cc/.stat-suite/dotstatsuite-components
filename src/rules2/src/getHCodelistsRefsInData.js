import * as R from 'ramda';

export const getHCodelistsRefs = annotations => R.reduce(
  (acc, annotation) => {
    if (annotation.type !== 'HIER_CONTEXT') {
      return acc;
    }
    const references = annotation.text || annotation.title || '';
    const refs = R.pipe(
      R.split(','),
      R.reduce(
        (acc2, ref) => {
          const match = ref.match(/([\w@_.]+):([\w@_.]+):([\w@_.]+)\(([\d.]+)\).([\w@_.]+)$/);
          if (R.isNil(match)) {
            return acc2;
          }
          const [codelistId, agencyId, code, version, hierarchy] = R.tail(match);
          return R.assoc(codelistId, { agencyId, code, version, hierarchy, codelistId }, acc2);
        },
        {}
      )
    )(references);
    return R.mergeRight(acc, refs);
  },
  {},
  annotations,
);

export const getHCodelistsRefsInData = sdmxJson => {
  const annotations = R.pathOr([], ['data', 'structure', 'annotations'], sdmxJson);
  return getHCodelistsRefs(annotations);
};
