import * as R from 'ramda';
import { getFlagsAndNotes } from './table/getFlagsAndNotes';
import { dimensionValueDisplay } from '../../rules/src/';

export const getHeaderSubtitle = (dimensions, combinations, customAttributes, display, microdataDimension) => {
  const combinationsConepts = R.pipe(R.pluck('concepts'), R.unnest)(combinations);
  const DEFAULT_REJECTED_SUBTITLE_IDS = ['_L', '_T', '_Z'];

  return R.reduce(
    (acc, dim) => {
      if (!R.propOr(true, 'display', dim) || R.propEq('id', microdataDimension, dim)
        || R.includes(dim.id, combinationsConepts)) {
        return acc;
      }
      const value = R.path(['values', 0], dim);
      if (!R.propOr(true, 'display', value) || R.includes(value.id, DEFAULT_REJECTED_SUBTITLE_IDS)) {
        return acc;
      }
      const header = `${dimensionValueDisplay(display)(dim)}:`;
      const label = dimensionValueDisplay(display)(value);
      const flags = R.pipe(
        getFlagsAndNotes,
        R.map(entry => ({
          code: entry.code,
          header: `${dimensionValueDisplay(display)(entry)}:`,
          label: dimensionValueDisplay(display)(entry.value),
        })),
      )(dim.attrValues, customAttributes);
      return R.append({ header, label, flags }, acc);
    },
    [],
    dimensions,
  );
};
