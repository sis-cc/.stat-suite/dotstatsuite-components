import * as R from 'ramda';
import { formatValue } from '../../rules/src/preparators/formatValue';
import { REJECTED_VALUE_IDS } from './constants';

/*
  options = {
    customAttributes: {
      decimals: id,
      prefscale: id,
    },
  }
*/

const parseAttributesValues = (obs, attributes, indexedDimensions) => {
  const { attrValuesIndexes, dimValuesIndexes } = obs;
  return R.addIndex(R.reduce)(
    (acc, valueIndex, attrIndex) => {
      const attribute = R.nth(attrIndex, attributes);
      if (R.isNil(attribute) || !R.prop('series', attribute)) {
        return acc;
      }
      const _value = R.nth(valueIndex, attribute.values || []);
      let value = R.isNil(_value)
        ? _value
        : R.over(R.lensProp('display'), (display=true) => display && !R.includes(_value.id, REJECTED_VALUE_IDS))(_value);
      if ((!R.propOr(true, 'display', attribute) || !R.propOr(true, 'display', value))) {
        value = null;
      }
      const relationship = R.propOr([], 'relationship', attribute);
      let coordinates = {};
      const serieKey = R.pipe(
        dims => R.isEmpty(relationship) ? R.values(dims) : R.props(relationship, dims),
        R.map(dim => {
          const valueIndex = R.nth(dim.__index, dimValuesIndexes);
          const valueId = R.path(['values', valueIndex, 'id'], dim);
          coordinates = R.assoc(dim.id, valueId, coordinates);
          return `${dim.id}=${valueId}`;
        }),
        R.ifElse(R.isEmpty, R.always(null), R.join(':'))
      )(indexedDimensions);

      return R.assoc(
        attribute.id,
        { ...R.pick(['id', 'name', 'relationship', 'display', 'combined'], attribute), value, serieKey, coordinates, isObs: R.isEmpty(relationship) },
        acc
      );
    },
    {},
    attrValuesIndexes
  );
};

const getFormatAttributesIndexes = (attributes, customAttributes) => R.addIndex(R.reduce)(
  (acc, attribute, index) => {
    if (R.equals(attribute.id, customAttributes.decimals)) {
      return ({ ...acc, decimals: index });
    }
    if (R.equals(attribute.id, customAttributes.prefscale)) {
      return ({ ...acc, prefscale: index });
    }
    return acc;
  },
  { prefscale: null, decimals: null },
  attributes
);

export const enhanceObservations = (dimensions = [], observations = {}, attributes = [], options = {}) => {
  const formatAttributesIndexes = getFormatAttributesIndexes(attributes, R.propOr({}, 'customAttributes', options));
  const indexedDimensions = R.indexBy(R.prop('id'), dimensions);

  return R.map(
    (observation) => {
      const { dimValuesIndexes } = observation;

      const indexedDimValIds = R.addIndex(R.reduce)(
        (acc, dimension, dimensionIndex) => {
          const id = dimension.id;
          const valueIndex = R.nth(dimensionIndex, dimValuesIndexes);
          const valueId = R.path(['values', Number(valueIndex), 'id'], dimension);
          return R.assoc(id, valueId, acc);
        },
        {},
        dimensions
      );

      return ({
        ...observation,
        attributes: parseAttributesValues(observation, attributes, indexedDimensions),
        formattedValue: formatValue(observation, formatAttributesIndexes, attributes),
        indexedDimValIds,
      });
    },
    observations
  );
};
