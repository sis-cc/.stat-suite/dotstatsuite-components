import * as R from 'ramda';

export const trimedProps = (properties, obj) => R.reduce(
  (acc, prop) => {
    if (R.isNil(R.prop(prop, obj))) {
      return acc;
    }
    return R.append(obj[prop], acc);
  },
  [],
  properties,
);

export const getLayoutCoordinatesValidator = (layoutCoordinates, topCoordinates={}) => (coordinates) => {
  if (R.isEmpty(coordinates)) {
    return false;
  }
  if (R.has('OBS_ATTRIBUTES', layoutCoordinates) || R.has('OBS_ATTRIBUTES', topCoordinates)) {
    const obsAttrCoord = R.prop('OBS_ATTRIBUTES', layoutCoordinates)
    || R.prop('OBS_ATTRIBUTES', topCoordinates);
    if (obsAttrCoord !== 'OBS_VALUE') {
      return false;
    }
  }
  const keys = R.keys(coordinates);
  let res = true;
  let notInTop = {};
  R.forEach(key => {
    if (R.prop(key, layoutCoordinates) !== R.prop(key, coordinates)) {
      res = false;
    }
    if (!R.has(key, topCoordinates)) {
      notInTop[key] = key;
    }
  }, keys);
  return res && !R.isEmpty(notInTop);
};
