import * as R from 'ramda';

export const getDimensionValuesIndexes = (values) => R.addIndex(R.reduce)(
  (acc, value, position) => {
    const sdmxIndex = value.__index;
    return R.over(
      R.lensProp(String(sdmxIndex)),
      R.ifElse(R.isNil, R.always([position]), R.append(position))
    )(acc);
  },
  {},
  values
);
