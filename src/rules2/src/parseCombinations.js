import * as R from 'ramda';
import { REJECTED_VALUE_IDS } from './constants';

const getDisplayableValues = (values) =>
  R.filter(
    (val) =>
      !R.includes(R.prop('id', val), REJECTED_VALUE_IDS) &&
      R.propOr(true, 'display', val),
    values,
  );

// invalid concepts:
// - empty/null/undefined/!array values (so far)
const hasInvalidValues = R.pipe(
  R.prop('values'),
  R.anyPass([R.isNil, R.complement(R.is(Array)), R.isEmpty]),
);
const isConceptInvalid = R.anyPass([hasInvalidValues /* add more... */]);
const filterValidConcepts = R.reject(isConceptInvalid);
// concepts is an array of ids... (misnamed!)
const rejectInvalidConceptIds =
  (concepts = []) =>
  (combinations = []) => {
    const validConceptIds = R.pluck('id', filterValidConcepts(concepts));
    return R.reduce((acc, comb) => {
      const refinedConcepts = R.intersection(validConceptIds, comb.concepts);
      if (R.length(refinedConcepts) < 1) {
        return acc;
      }
      return R.append({ ...comb, concepts: refinedConcepts }, acc);
    }, [], combinations);
  };

export const parseCombinations = (
  combinations,
  parsedAttributes,
  dimensions,
) => {
  const concepts = R.concat(dimensions, parsedAttributes);

  const indexedDimensions = R.indexBy(R.prop('id'), dimensions);
  const indexedAttributes = R.indexBy(R.prop('id'), parsedAttributes);

  return R.pipe(
    rejectInvalidConceptIds(concepts),
    R.reduce((acc, comb) => {
      let relationship = [];
      let ids = {};
      let displayable_values_count = 0;
      const seriesConcepts = R.filter((concept) => {
        if (R.has(concept, ids)) {
          return false;
        }
        if (R.has(concept, indexedDimensions)) {
          const dimension = R.prop(concept, indexedDimensions);
          relationship = !dimension.header
            ? R.append(concept, relationship)
            : relationship;
          ids = { ...ids, [concept]: concept };
          displayable_values_count += R.length(dimension.values || []);
          return !dimension.header;
        } else if (!R.has(concept, indexedAttributes)) {
          return false;
        }
        const attribute = R.prop(concept, indexedAttributes);
        const displayableValues = getDisplayableValues(attribute.values || []);
        const isDisplayable =
          R.length(displayableValues) !== 0 &&
          R.propOr(true, 'display', attribute);
        relationship =
          attribute.series && isDisplayable
            ? R.pipe(
                R.reject((id) => R.has(id, ids)),
                R.concat(relationship),
              )(attribute.relationship)
            : relationship;
        ids = attribute.series  && isDisplayable
          ? { ...ids, ...R.indexBy(R.identity, attribute.relationship) }
          : ids;
        displayable_values_count = isDisplayable
          ? displayable_values_count + R.length(displayableValues)
          : displayable_values_count;
        return attribute.series && isDisplayable;
      }, comb.concepts);
      if (R.isEmpty(seriesConcepts)) {
        return R.append(
          { ...comb, header: true, display: displayable_values_count > 0 },
          acc,
        );
      }
      return R.append(
        {
          ...comb,
          series: true,
          relationship,
          display: displayable_values_count > 0,
        },
        acc,
      );
    }, []),
  )(combinations);
};
