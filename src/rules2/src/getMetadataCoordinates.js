import * as R from 'ramda';

export const getMetadataCoordinates = (sdmxJson) => {
  const dimensions = R.pathOr([], ['data', 'structure', 'dimensions', 'observation'], sdmxJson);
  const annotations = R.pathOr([], ['data', 'structure', 'annotations'], sdmxJson);
  const metadataAvailKeys = R.pipe(
    R.pathOr([], ['data', 'dataSets', 0, 'dimensionGroupAttributes']),
    R.map(indexes => R.props(indexes, annotations)),
    R.filter(R.find(a => a && R.propEq('type', 'HAS_METADATA', a))),
    R.keys
  )(sdmxJson);

  return R.map(
    key => {
      const indexes = R.split(':', key);
      return R.addIndex(R.reduce)(
        (acc, vIndex, dimIndex) => {
          if (R.isNil(vIndex) || R.isEmpty(vIndex)) {
            return acc;
          }
          const dim = R.nth(dimIndex, dimensions);
          if (R.isNil(dim)) {
            return acc;
          }
          const val = R.path(['values', Number(vIndex)], dim);
          if (R.isNil(val)) {
            return acc;
          }
          return R.assoc(dim.id, val.id, acc);
        },
        {},
        indexes
      );
    },
    metadataAvailKeys
  );
};
