import * as R from 'ramda';
import { getObservations } from '../../rules/src';
import { refineDimensions } from './refineDimensions';
import { enhanceObservations } from './enhanceObservations';
import { parseAttributes } from './parseAttributes';
import { getCombinationDefinitions, parseCombinationDefinition } from './getCombinationDefinitions';
import { parseCombinations } from './parseCombinations';
import { refineAttributes } from './refineAttributes';
import { duplicateObs } from './duplicateObservations';
import { getMetadataCoordinates } from './getMetadataCoordinates';
import { getAttributesSeries } from './getAttributesSeries';
import { getManyValuesDimensions } from './getManyValuesDimensions';
import { getOneValueDimensions } from './getOneValueDimensions';
import { hierarchiseDimensionWithNativeHierarchy } from './hierarchiseDimensionWithNativeHierarchy2';
import { getDataflowAttributes } from './getDataflowAttributes';
import { getHeaderTitle } from './getHeaderTitle';
import { getHeaderSubtitle } from './getHeaderSubtitle';
import { getHeaderCombinations } from './getHeaderCombinations';
import { getHeaderCoordinates } from './getHeaderCoordinates';
import { applyHierarchicalCodesToDim } from './applyHierarchicalCodesToDim';

export const prepareData = (sdmxJson, { customAttributes, locale, hierarchies, dataflow, display, defaultCombinations, dataquery }) => {
  const dimensions = R.pathOr([], ['data', 'structure', 'dimensions', 'observation'], sdmxJson);
  const refinedDimensions = refineDimensions(dimensions, dataquery);
  const attributes = R.pathOr([], ['data', 'structure', 'attributes', 'observation'], sdmxJson);
  const annotations = R.pathOr([], ['data', 'structure', 'annotations'], sdmxJson);
  const observations = getObservations(sdmxJson);
  let combinations = getCombinationDefinitions(annotations, locale);
  if (R.isEmpty(combinations) && !R.isEmpty(defaultCombinations)) {
    const { concepts, names } = defaultCombinations;
    combinations = parseCombinationDefinition(locale)(concepts, names);
  }
  const metadataCoordinates = getMetadataCoordinates(sdmxJson);

  const parsedAttributes = parseAttributes(attributes, refinedDimensions, customAttributes);
  const parsedCombinations = parseCombinations(combinations, parsedAttributes, refinedDimensions);
  const seriesCombinations = R.filter(R.prop('series'), parsedCombinations);
  const refinedAttributes = refineAttributes(parsedAttributes, seriesCombinations);

  const enhancedObservations = enhanceObservations(refinedDimensions, observations, refinedAttributes, { customAttributes });
  const attributesSeries = getAttributesSeries(enhancedObservations);
  const manyValuesDimensions = getManyValuesDimensions(refinedDimensions, attributesSeries, customAttributes, seriesCombinations);
  const oneValueDimensions = getOneValueDimensions(refinedDimensions, parsedAttributes);
  const headerCoordinates = getHeaderCoordinates(oneValueDimensions);
  const hierarchisedDimensions = R.map(dim => {
    if (R.isEmpty(R.propOr({}, dim.id, hierarchies))) {
      return hierarchiseDimensionWithNativeHierarchy(dim);
    }
    return applyHierarchicalCodesToDim(R.prop(dim.id, hierarchies), dim);
  }, manyValuesDimensions)
  const duplicatedObservations = duplicateObs(R.values(hierarchisedDimensions), enhancedObservations);

  const dataflowAttributes = getDataflowAttributes(parsedAttributes, parsedCombinations);
  const headerTitle = getHeaderTitle(dataflow, dataflowAttributes, display, customAttributes);
  const headerSubtitle = getHeaderSubtitle(oneValueDimensions, parsedCombinations, customAttributes, display);
  const headerCombinations = getHeaderCombinations(parsedCombinations, oneValueDimensions, refinedAttributes, display);
  
  return ({
    observations: duplicatedObservations,
    dimensions: hierarchisedDimensions,
    combinations: parsedCombinations,
    oneValueDimensions,
    attributesSeries,
    metadataCoordinates,
    attributes: refinedAttributes,
    header: {
      title: headerTitle,
      subtitle: headerSubtitle,
      combinations: headerCombinations,
      coordinates: headerCoordinates
    }
  });
};
