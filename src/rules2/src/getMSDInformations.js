import * as R from 'ramda';

export const getMSDInformations = msdJson => {
  const attributesDefinitions = R.pathOr([], ['data', 'metadataStructures', 0, 'metadataStructureComponents', 'reportStructures', 0, 'metadataAttributes'], msdJson);
  const indexedConcepts = R.pipe(R.pathOr([], ['data', 'conceptSchemes', 0, 'concepts']), R.indexBy(R.prop('id')))(msdJson);

  const parseAttributes = (parent) => R.reduce(
    (acc, attribute) => {
      const id = attribute.id;
      const name = R.path([id, 'name'], indexedConcepts);
      const hierarchicalId = parent === '#ROOT' ? id : `${parent}.${id}`;
      const format = R.path(['localRepresentation', 'textFormat', 'textType'], attribute);
      if (R.has('metadataAttributes', attribute)) {
        const collection = parseAttributes(hierarchicalId)(attribute.metadataAttributes);
        return ({ ...acc, ...collection, [hierarchicalId]: { format, parent, id, name } });
      }
      return R.assoc(hierarchicalId, { format, parent, id, name }, acc);
    },
    {}
  )

  return ({ attributes: parseAttributes('#ROOT')(attributesDefinitions) });
};
