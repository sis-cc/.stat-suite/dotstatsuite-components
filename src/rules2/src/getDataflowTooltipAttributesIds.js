import * as R from 'ramda';

export const getDataflowTooltipAttributesIds = (sdmxJson, defaults = {}) => {
  const dataflowAnnotationsIndexes = R.pathOr([], ['data', 'dataSets', 0, 'annotations'], sdmxJson);
  const dataflowAnnotations = R.props(dataflowAnnotationsIndexes, R.pathOr([], ['data', 'structure', 'annotations'], sdmxJson));

  const flagsAttrAnnotation = R.find(R.propEq('type', 'LAYOUT_FLAG'), dataflowAnnotations);
  const footnotesAttrAnnotation = R.find(R.propEq('type', 'LAYOUT_NOTE'), dataflowAnnotations);

  const customFlags = R.pipe(
    R.propOr('', 'title'),
    title => R.isNil(title) || R.isEmpty(title) ? [] : R.split(',', title)
  )(flagsAttrAnnotation || {});

  const customNotes = R.pipe(
    R.propOr('', 'title'),
    title => R.isNil(title) || R.isEmpty(title) ? [] : R.split(',', title),
    notes => R.difference(notes, customFlags)
  )(footnotesAttrAnnotation || {});

  const flags = R.isNil(flagsAttrAnnotation)
    ? R.difference(defaults.flags || [], customNotes)
    : customFlags;

  const notes = R.isNil(footnotesAttrAnnotation)
    ? R.difference(defaults.notes || [], flags)
    : customNotes;

  return ({ flags, notes });
};

