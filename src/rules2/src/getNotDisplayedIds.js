import * as R from 'ramda';
import { getNotDisplayedCombinations } from '@sis-cc/dotstatsuite-sdmxjs';

const getNotDisplayedValues = annot => {
  if (R.isNil(annot)) {
    return {};
  }
  const ids = R.split(',', annot.title || '');
  return R.reduce(
    (acc, entry) => {
      if (R.isEmpty(entry)) {
        return acc;
      }
      const parsed = R.split('=', entry);
      const dimensionId = parsed[0];
      if (R.length(parsed) === 1) {
        return R.assoc(dimensionId, dimensionId, acc);
      }
      const values = R.split('+', parsed[1]);
      if (R.length(values) === 1 && R.isEmpty(values[0])) {
        return R.assoc(dimensionId, dimensionId, acc);
      }
      return R.reduce(
        (_acc, val) => {
          let _val = val
          if (R.isEmpty(val)) {
            return _acc;
          }
          if (R.test('/[()]/')) {
            _val = R.replace(/[()]/g, '')(val)
          }
          const key = R.isEmpty(_val) ? dimensionId : `${dimensionId}.${_val}`;
          return R.assoc(key, key, _acc);
        },
        acc,
        values
      );
    },
    {},
    ids
  );
};

export const getNotDisplayedIds = (annotations) => R.pipe(
  R.find(R.propEq('type', 'NOT_DISPLAYED')),
  R.converge((hiddenValues, hiddenCombinations) => ({
    hiddenValues,
    hiddenCombinations
  }), [
    getNotDisplayedValues,
    (annot={ title: '' }) => getNotDisplayedCombinations(annot) 
  ])
)(annotations);
