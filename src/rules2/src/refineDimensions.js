import * as R from 'ramda';

export const refineDimensions = (dimensions=[], dataquery='') => {
  const selectionIds = R.pipe(R.split('.'), R.map(R.split('+')))(dataquery);

  return R.addIndex(R.map)((dim, index) => {
    const selection = R.nth(index, selectionIds);
    const values = R.map(val => {
      if (R.includes(val.id, selection || []) || R.isNil(selection) || R.length(selection) === 0 || R.isEmpty(R.head(selection))) {
        return R.assoc('isSelected', true, val);
      }
      return R.assoc('empty', true, val);
    }, dim.values || []);

    const size = R.length(values);
    return { ...dim, values, header: size === 1 || (size > 1 && R.length(selection) === 1 && !R.isEmpty(R.head(selection))) };
  }, dimensions);
};
