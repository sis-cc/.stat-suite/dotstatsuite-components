import * as R from 'ramda';
import { REJECTED_VALUE_IDS } from './constants';

export const getDataflowAttributes = (attributes, combinations) => {
  const combinationsConceptsIds = R.pipe(R.pluck('concepts'), R.unnest)(combinations);
  return R.reduce(
    (acc, attribute) => {
      if (!R.prop('header', attribute) || !R.isEmpty(attribute.relationship)
        || R.includes(attribute.id, combinationsConceptsIds)) {
        return acc;
      }
      const value = R.head(attribute.values);
      if (!R.propOr(true, 'display', attribute) || R.isNil(value)
        || !R.propOr(true, 'display', value) || R.includes(R.prop('id', value), REJECTED_VALUE_IDS)) {
        return acc;
      }
      return R.assoc(attribute.id, { ...R.pick(['id', 'name'], attribute), value }, acc);
    },
    {},
    attributes,
  );
};

