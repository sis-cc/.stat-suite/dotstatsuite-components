import * as R from 'ramda';
import { combinedValuesDisplay, singleValueDisplay } from '../combinedValuesDisplay'


export const getTableLabelAccessor = display => content => {
  return R.is(Array, content)
    ? combinedValuesDisplay(display, content)
    : singleValueDisplay(display, content);
}; 
