import * as R from 'ramda';

export const getCellsAttributesIds = (layoutIds, attributes) => {
  return R.reduce(
    (acc, attr) => {
      if (!attr.series) {
        return acc;
      }
      if (R.isEmpty(attr.relationship || [])) {
        return R.assoc(attr.id, attr.id, acc);
      }
      const indexedHeaderIds = R.indexBy(R.identity, layoutIds.header);
      const indexedSectionsIds = R.indexBy(R.identity, layoutIds.sections);
      const indexedRowsIds = { ...indexedSectionsIds, ...R.indexBy(R.identity, layoutIds.rows) };
      const [idsInHeader, rest] = R.partition(
        id => R.has(id, indexedHeaderIds),
        attr.relationship,
      );
      if (R.isEmpty(rest)) {
        return acc;
      } else if (!R.isEmpty(idsInHeader)) {
        return R.assoc(attr.id, attr.id, acc);
      } else {
        const idsNotInSections = R.reject(id => R.has(id, indexedSectionsIds), attr.relationship);
        if (R.isEmpty(idsNotInSections)) {
          return acc;
        }
        const idsNotInRows = R.reject(id => R.has(id, indexedRowsIds), idsNotInSections);
        if (R.isEmpty(idsNotInRows)) {
          return acc;
        }
      }
      return R.assoc(attr.id, attr.id, acc);
    },
    {},
    attributes,
  );
};
