import * as R from 'ramda';

export const getIndexedCombinationsByDisplay = (layout, combinations) => {
  const layoutIds = R.pipe(
    R.map(R.pluck('id')),
    R.values,
    R.unnest,
    R.indexBy(R.identity),
  )(layout);

  const [combsInLayout, combsInCells] = R.partition(
    comb => R.has(comb.id, layoutIds),
    combinations,
  );
  return { cells: combsInCells, layout: combsInLayout };
};
