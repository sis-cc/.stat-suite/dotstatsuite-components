import * as R from 'ramda';
import { isTimePeriodDimension } from '@sis-cc/dotstatsuite-sdmxjs';

export const getConceptsSet = (ids, indexedCombinations, _upper) => {
  const upper = R.isNil(_upper) ? new Set([]) : _upper;
  const res = new Set([]);
  R.forEach(id => {
    if (!R.has(id, indexedCombinations)) {
      res.add(id);
    }
    else {
      const comb = R.prop(id, indexedCombinations);
      R.forEach(c => {
        if (!upper.has(c)) {
          res.add(c);
        }
      }, comb.concepts);
    }
  }, ids);
  return res;
};

export const parseCombination = (_upperIds, _excludedIds) => (comb, indexedDimensions) => {
  const upperIds = R.isNil(_upperIds) ? new Set([]) : _upperIds;
  const excludedIds = R.isNil(_excludedIds) ? new Set([]) : _excludedIds;

  const { concepts, relationship } = comb;
  const dimensions = R.reduce((acc, id) => {
    if (upperIds.has(id) || !R.has(id, indexedDimensions)) {
      return acc
    }
    const dim = R.prop(id, indexedDimensions);
    return R.append(dim, acc);
  }, [], concepts);
  const excludedDep = R.find(i => excludedIds.has(i), relationship);
  if (excludedDep) {
    return dimensions;
  }
  return R.assoc('dimensions', dimensions, comb);
};

export const getLayout = (_layoutIds, dimensions, combinations, isTimeInverted) => {
  const layoutIds = R.mergeRight(
    { header: [], sections: [], rows: [] },
    _layoutIds,
  );
  const applyInversionOnTimeDim = dim =>
    isTimePeriodDimension(dim) && isTimeInverted
      ? R.assoc('isInverted', true, dim)
      : dim;

  const indexedDimensions = R.reduce(
    (acc, dim) => R.assoc(dim.id, applyInversionOnTimeDim(dim), acc),
    {},
    dimensions,
  );
  
  const indexedCombinations = R.indexBy(R.prop('id'), combinations);
  const headerConceptsSet = getConceptsSet(layoutIds.header, indexedCombinations);
  const sectionsConceptsSet = getConceptsSet(layoutIds.sections, indexedCombinations);
  const rowsConceptsSet = getConceptsSet(R.concat(layoutIds.sections, layoutIds.rows), indexedCombinations);

  const getLayoutLevel = (_upperIds, _excludedIds) => ids => R.reduce((acc, id) => {
    if (R.has(id, indexedDimensions)) {
      const dim = R.prop(id, indexedDimensions);
      return R.append(dim, acc);
    }
    if (!R.has(id, indexedCombinations)) {
      return acc;
    }
    const comb = R.prop(id, indexedCombinations);
    const parsedCombination = parseCombination(_upperIds, _excludedIds)(comb, indexedDimensions);
    if (R.is(Array, parsedCombination)) {
      return R.concat(acc, parsedCombination);
    }
    return R.append(parsedCombination, acc);
  }, [], ids);
  
  const header = getLayoutLevel(null, rowsConceptsSet)(layoutIds.header);
  const sections = getLayoutLevel(null, headerConceptsSet)(layoutIds.sections);
  const rows = getLayoutLevel(sectionsConceptsSet, headerConceptsSet)(layoutIds.rows);

  return ({ header, sections, rows });
};
