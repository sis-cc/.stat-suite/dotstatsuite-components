import * as R from 'ramda';
import { REJECTED_VALUE_IDS } from '../constants';

export const getFlagsAndNotes = (attributesValues, customAttributes) => {
  const flagsIds = R.propOr([], 'flags', customAttributes);
  const notesIds = R.propOr([], 'notes', customAttributes);

  return R.addIndex(R.reduce)(
    (acc, id, index) => {
      if (!R.has(id, attributesValues)) {
        return acc;
      }
      const attr = R.pick(['id', 'name', 'value', 'display'], R.prop(id, attributesValues));
      if (R.isNil(attr.value) || !R.pathOr(true, ['value', 'display'], attr) || !R.propOr(true, 'display', attr)
        || R.includes(R.path(['value', 'id'], attr), REJECTED_VALUE_IDS)) {
        return acc;
      }
      return R.append(index < flagsIds.length ? R.assoc('code', R.path(['value', 'id'], attr), attr) : attr, acc);
    },
    [],
    R.concat(flagsIds, notesIds)
  );
};
