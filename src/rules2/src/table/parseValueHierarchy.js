import * as R from 'ramda';

export const isSameValueAsPrevious = (value, previousValue) => {
  const valueHierarchicalPath = R.append(value.id, R.propOr([], 'parents', value));
  const previousValueHierarchicalPath = R.append(previousValue.id, R.propOr([], 'parents', previousValue));

  return R.equals(valueHierarchicalPath, previousValueHierarchicalPath);
}

export const parseValueHierarchy = (value, previousValue, indexedValues) => {
  const parentsIds = R.propOr([], 'parents', value);
  if (R.isEmpty(parentsIds)) {
    return value;
  }
  const _previousParentsIds = R.propOr([], 'parentsIds', previousValue);
  const previousParentsIds = R.isNil(previousValue) ? [] : R.append(R.prop('id', previousValue), _previousParentsIds);
  const [presentIds, _missingIds] = R.addIndex(R.splitWhen)((val, ind) => R.nth(ind, previousParentsIds) !== val, parentsIds);
  const __previousParentsIds = R.pipe(R.propOr([], 'parents'), R.pluck('id'))(previousValue);
  const missingIds = R.pipe(
    R.takeLastWhile(id => !R.includes(id, __previousParentsIds) && id !== R.prop('id', previousValue)),
    ids => R.concat(ids, _missingIds)
  )(presentIds);
  const _previousParents = R.propOr([], 'parents', previousValue);
  const previousParents = R.isNil(previousValue) ? [] : R.append(R.pick(['id', 'name'], previousValue), _previousParents);
  const parents = R.takeWhile(p => R.includes(p.id, presentIds), previousParents);
  const missingParents = R.props(missingIds, indexedValues);
  return ({
    ...value,
    parents,
    parentsIds,
    missingParents
  });
};
