import * as R from 'ramda';

/*
  const layout = {
    header: [
      { id, __index },
      { id: COMB, dimensions: [{ id, __index }] }
    ],
    sections: [],
    rows: []
  }

*/

const getLayoutPivots = layoutEntry => {
  const valIndexGetter = d => R.pipe(
    R.nth(R.prop('__index', d)),
    ind => d.isInverted ? R.negate(ind) : ind
  );
  return indexes => R.map(
    R.ifElse(
      R.has('dimensions'),
      c => R.map(d => valIndexGetter(d)(indexes), R.prop('dimensions', c)),
      d => {
        if (d.id === 'OBS_ATTRIBUTES') {
          return R.last(indexes);
        }
        return valIndexGetter(d)(indexes);
      }
    ),
    layoutEntry
  );
};

const comparator = (a, b) => {
  const size = R.length(a);
  let i = 0;
  while (i < size) {
    if (R.is(Array, a[i])) {
      const _a = a[i];
      const _b = b[i];
      const _size = R.length(_a);
      let j = 0;
      while (_a[j] === _b[j] && j < _size) {
        j++;
      }
      if (_a[j] !== _b[j]) {
        return _a[j] - _b[j];
      }
    }
    else if (a[i] !== b[i]) {
      return a[i] - b[i];
    }
    i++;
  }
  return a[i] - b[i];
};

// simple call to ramda uniq method is very slow regarding performance
const uniqIndexes = (indexes) => R.pipe(
  R.reduce(
    (acc, i) => {
      const key = R.join(':', R.unnest(i));
      if (R.has(key, acc.keys)) {
        return acc;
      }
      return ({
        indexes: R.append(i, acc.indexes),
        keys: R.assoc(key, key, acc.keys)
      });
    },
    { indexes: [], keys: {} },
  ),
  R.prop('indexes')
)(indexes);

export const getSortedLayoutIndexes = (layout, observations) => {
  const dimIndexes = R.pipe(R.values, R.map(R.propOr([], 'orderedDimIndexes')))(observations);

  const headerPivots = getLayoutPivots(layout.header);
  const sectionsPivots = getLayoutPivots(R.concat(layout.sections, layout.rows));

  const sectionLength = R.length(layout.sections);
  const { header, sections } = R.pipe(
    R.reduce(
      (acc, indexes) => {
        const headerIndexes = headerPivots(indexes);
        const sectionsIndexes = sectionsPivots(indexes);

        return ({
          header: R.append(headerIndexes, acc.header),
          sections: R.append(sectionsIndexes, acc.sections)
        });
      },
      { header: [], sections: [] },
    ),
    R.mapObjIndexed(uniqIndexes),
    R.evolve({
      header: R.sort(comparator),
      sections: R.sort(comparator)
    }),
    indexes => ({
      ...indexes,
      sections: R.reduce(
        (acc, i) => {
          const [sectionIndexes, rowIndexes] = R.splitAt(sectionLength, i);
          const previousSecIndexes = R.pipe(R.nth(-1), i => R.isNil(i) ? null : R.head(i))(acc);
          if (R.equals(R.unnest(sectionIndexes), previousSecIndexes ? R.unnest(previousSecIndexes) : null)) { //perhaps a bit dirty ...
            return R.over(
              R.lensIndex(-1),
              R.over(R.lensIndex(1), R.append(rowIndexes))
            )(acc);
          }

          return R.append([sectionIndexes, [rowIndexes]], acc);
        },
        [],
        indexes.sections
      )
    })
  )(dimIndexes);

  return ({ header, sections });
};