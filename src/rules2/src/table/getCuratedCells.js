import * as R from 'ramda';

export const getCuratedCells = (cells, layout) => {
  const layoutIds = R.map(
    R.pipe(
      R.map(entry =>
        R.has('dimensions', entry) ? R.pluck('id', entry.dimensions || []) : entry.id,
      ),
      R.unnest,
    ),
  )(layout);
  return R.pipe(
    R.values,
    R.reduce((acc, cell) => {
      const keys = R.map(
        R.pipe(
          R.map(dim => {
            const val = R.prop(dim, cell.indexedDimValIds);
            return `${dim}=${val}`;
          }),
          R.join(':'),
        ),
        layoutIds,
      );

      return R.over(
        R.lensPath(R.props(['header', 'sections', 'rows'], keys)),
        cells => (R.isNil(cells) ? [cell] : R.append(cell, cells)),
        acc,
      );
    }, {}),
  )(cells);
};
