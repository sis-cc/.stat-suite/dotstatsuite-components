import * as R from 'ramda';

const registerParsedIndexes = ({ isSameSerie, ...parsed }, register) => {
  const merged = R.mergeWith((a, b) => R.append(a || [], b), parsed, register);
  return ({ ...merged, isSameSerie });
};

const parseParentsIndexesWithPrevious = (parentsIndexes, previousParentsIndexes) => {
  const previousParentsSet = new Set(previousParentsIndexes);
  const lastParentIndexInPrevious = R.findLastIndex(i => previousParentsSet.has(i), parentsIndexes);
  const [rawPresentParentsIndexes, missingParentsIndexes] = lastParentIndexInPrevious !== -1
    ? R.splitAt(lastParentIndexInPrevious + 1, parentsIndexes)
    : [[], parentsIndexes];
  const presentParentsIndexes = R.filter(i => previousParentsSet.has(i), rawPresentParentsIndexes);
  return ({ presentParentsIndexes, missingParentsIndexes });
};

const parseSerieIndexesHierarchies = (serieIndexes, previousSerie, dimensions, isSameSerie, replicate) => {
	return R.addIndex(R.reduce)((acc, _valueIndex, dimensionIndex) => {
		if (R.is(Array, _valueIndex)) {
      const previous = R.isEmpty(previousSerie) ? {} : R.map(R.nth(dimensionIndex), previousSerie);
			const parsed = parseSerieIndexesHierarchies(
        _valueIndex,
        previous,
        R.pathOr([], [dimensionIndex, 'dimensions'], dimensions),
        acc.isSameSerie,
        replicate
      );
      return registerParsedIndexes(parsed, acc);
		}
    const valueIndex = Math.abs(_valueIndex);
    const parents = R.pathOr([], [dimensionIndex, 'values', valueIndex, 'parents'], dimensions);
    const previousIndex = R.pipe(R.path(['indexes', dimensionIndex]), i => R.isNil(i) ? -1 : Math.abs(i))(previousSerie)
    const previousParentsIndexes = acc.isSameSerie
      ? R.pipe(R.pathOr([], ['parentsIndexes', dimensionIndex]), R.append(previousIndex))(previousSerie) : [];
    const previousMissingIndexes = acc.isSameSerie
      ? R.pathOr([], ['missingIndexes', dimensionIndex], previousSerie) : [];
    /*
      if replicate = true (sections), missing parents insdexes are replicated on each brother nodes not just the first (as empty lines)
      thats why passing previous missing as previous parents in case replicate = false
    */
    const { presentParentsIndexes, missingParentsIndexes } = parseParentsIndexesWithPrevious(
      parents,
      replicate ? previousParentsIndexes : R.concat(previousParentsIndexes, previousMissingIndexes),
    );
    return registerParsedIndexes({
      parentsIndexes: presentParentsIndexes,
      missingIndexes: missingParentsIndexes,
      isSameSerie: acc.isSameSerie && valueIndex === previousIndex
    }, acc);
  }, { parentsIndexes: [], missingIndexes: [], isSameSerie }, serieIndexes);
};

export const parseSeriesIndexesHierarchies = (seriesIndexes, dimensions, replicateMissing = false) => {
  return R.reduce((acc, serieIndexes) => {
    const previousSerie = R.last(acc) || {};
    const { isSameSerie, ...parsed } = parseSerieIndexesHierarchies(serieIndexes, previousSerie, dimensions, true, replicateMissing);
    return R.append(R.assoc('indexes', serieIndexes, parsed), acc);
  }, [], seriesIndexes);
};

export const parseLayoutIndexesHierarchies = (layoutIndexes, layout) => {
  const header = parseSeriesIndexesHierarchies(layoutIndexes.header, layout.header);
  const sections = R.pipe(
    R.transpose,
    ([_sections, _sectionsRows]) => {
      if (R.isNil(_sections)) {
        return [];
      }
      // replicate = true on sections
      const sections = parseSeriesIndexesHierarchies(_sections, layout.sections, true);
      const sectionsRows = R.map(rows => parseSeriesIndexesHierarchies(rows, layout.rows), _sectionsRows);
      return R.transpose([sections, sectionsRows]);
    }
  )(layoutIndexes.sections);


  return ({ header, sections });
};
