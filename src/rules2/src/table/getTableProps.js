import * as R from 'ramda';
import { declineObservationsOverAttributes } from './declineObservationsOverAttributes';
import { getLayout } from './getLayout';
import { getSortedLayoutIndexes } from './getSortedLayoutIndexes';
import { parseLayoutIndexesHierarchies } from './parseSeriesIndexesHierarchies';
import { refineLayoutSize } from './refineLayoutSize2';
import { getLayoutData } from './getLayoutData2';
import { getCellsAttributesIds } from './getCellsAttributesIds';
import { getIndexedCombinationsByDisplay } from './getIndexedCombinationsByDisplay';
import { getCells } from './getCells';
import { getCuratedCells } from './getCuratedCells';
import { getSeriesCombinations } from '../getSeriesCombinations';
import { getCellsMetadataCoordinates } from './getCellsMetadataCoordinates';

export const getTableProps = ({ data, layoutIds, customAttributes, limit, isTimeInverted }) => {
  const {
    observations,
    dimensions,
    combinations,
    oneValueDimensions,
    attributesSeries,
    metadataCoordinates,
    attributes,
    header
  } = data;

  const obsAttributes = R.filter(
    a => a.series && R.isEmpty(a.relationship) && a.display && !a.combined,
    attributes
  );
  let dupObs = observations;
  const hasDuplicatedCells = R.pipe(
    R.values,
    R.unnest,
    R.find(R.equals('OBS_ATTRIBUTES')),
  )(layoutIds);
  if (hasDuplicatedCells) {
    const attrsIds = R.pluck('id', obsAttributes);
    dupObs = declineObservationsOverAttributes(attrsIds, observations);
  }
  const seriesCombinations = getSeriesCombinations(combinations, oneValueDimensions);
  const _dims =  R.append(
    {
      id: 'OBS_ATTRIBUTES',
      values: R.prepend({ id: 'OBS_VALUE' }, obsAttributes),
    },
    dimensions,
  );
  const layout = getLayout(layoutIds, _dims, seriesCombinations, isTimeInverted);
  const layoutDimsIds = R.map(R.reduce((acc, entry) => {
    if (R.has('dimensions', entry)) {
      return R.concat(acc, R.pluck('id', entry.dimensions));
    }
    return R.append(entry.id, acc);
  }, []), layout);
  const layoutIndexes = getSortedLayoutIndexes(layout, dupObs);
  const enhancedLayoutIndexes = parseLayoutIndexesHierarchies(
    layoutIndexes,
    layout,
  );
  const refinedLayoutIndexes = refineLayoutSize({ layout, observations: dupObs, limit })(enhancedLayoutIndexes);
  const layoutData = getLayoutData(refinedLayoutIndexes, layout, { metadataCoordinates, attributesSeries, customAttributes, topCoordinates: header.coordinates });

  const cellsAttributesIds = getCellsAttributesIds(layoutDimsIds, attributes);
  const indexedCombinations = getIndexedCombinationsByDisplay(layout, seriesCombinations);
  const cellsMetadataCoordinates = getCellsMetadataCoordinates(metadataCoordinates, oneValueDimensions, layoutDimsIds);
  const cells = getCells(customAttributes, cellsAttributesIds, indexedCombinations, attributesSeries, cellsMetadataCoordinates)(dupObs);

  return ({
    ...layoutData,
    cells: getCuratedCells(cells, layout),
    layout,
    combinations: seriesCombinations
  });
};
