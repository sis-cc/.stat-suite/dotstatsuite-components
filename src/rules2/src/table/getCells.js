import * as R from 'ramda';
import { getFlagsAndNotes } from './getFlagsAndNotes';
import { hasCellMetadata } from '../hasCellMetadata';

export const getCellRelevantAttributes = (attributes, attributesSeries, cellAttributeIds) => R.filter(
  (attr) => {
    if (R.isNil(attr.value)) {
      return false;
    }
    if (R.has(attr.id, cellAttributeIds)) {
      return true;
    }
    const attrInSerie = R.path([attr.serieKey, attr.id], attributesSeries);
    return R.isNil(attrInSerie);
  },
  attributes
);

export const getCellCombinedSeries = (attributes, combinations) => R.reduce(
  (acc, comb) => {
    const values = R.reduce(
      (acc, concept) => {
        if (R.has(concept, comb.fixedDimValues || {})) {
          return R.append(R.prop(concept, comb.fixedDimValues), acc);
        }
        if (R.has(concept, attributes || {})) {
          return R.append(R.path([concept, 'value'], attributes), acc);
        }
        return acc;
      },
      [],
      comb.concepts
    );

    if (R.isEmpty(values)) {
      return acc;
    }
    return R.append({ ...R.pick(['id', 'name'], comb), values }, acc);
  },
  [],
  combinations
);

// combinations = { cells, layout };
export const getCells = (customAttributes, cellsAttributesId, combinations, attributesSeries, metadataCoordinates) => observations => {
  const attributesInLayoutCombination = R.pipe(
    R.propOr([], 'layout'),
    R.pluck('concepts'),
    R.unnest
  )(combinations);

  const attributesInCellsCombination = R.pipe(
    R.propOr([], 'cells'),
    R.pluck('concepts'),
    R.unnest
  )(combinations);

  const _customAttributes = R.over(
    R.lensProp('notes'),
    notes => R.pipe(R.concat, R.uniq)(notes || [], attributesInLayoutCombination)
  )(customAttributes);

  return R.mapObjIndexed(
    obs => {
      const isAttrCell = R.pathOr('OBS_VAL', ['indexedDimValIds', 'OBS_ATTR'], obs) !== 'OBS_VAL';
      const relevantAttributes = isAttrCell ? [] : getCellRelevantAttributes(obs.attributes, attributesSeries, cellsAttributesId);
      const flagsAndNotes = isAttrCell ? [] : getFlagsAndNotes(R.omit(attributesInCellsCombination, relevantAttributes), _customAttributes);
      const combinedSeries = isAttrCell ? [] : getCellCombinedSeries(relevantAttributes, combinations.cells || []);
      const hasAdvancedAttributes = isAttrCell
        ? false : R.pipe(
        R.omit(R.unnest([_customAttributes.flags || [], _customAttributes.notes || [], attributesInCellsCombination])),
        res => !R.isEmpty(res)
      )(relevantAttributes);

      const advancedAttributes = R.omit(
        R.unnest([_customAttributes.flags || [], _customAttributes.notes || [], attributesInCellsCombination]),
        R.filter(attr => !R.isNil(attr.value), obs.attributes), 
      );

      const hasMetadata = isAttrCell ? false : hasCellMetadata(metadataCoordinates, obs.indexedDimValIds);

      return ({
        ...R.pick(['indexedDimValIds', 'key'], obs),
        flags: R.concat(flagsAndNotes, combinedSeries),
        sideProps: hasAdvancedAttributes || hasMetadata
          ? { hasMetadata, coordinates: obs.indexedDimValIds, advancedAttributes } : null,
        intValue: R.is(Number, obs.value) ? obs.value : null,
        value: obs.formattedValue,
      });
    },
    observations
  );
};
