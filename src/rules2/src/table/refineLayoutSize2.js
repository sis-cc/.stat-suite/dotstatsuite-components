import * as R from 'ramda';

const toKey = indexes => R.reduce(
  (acc, _ind) => {
    const ind = R.is(Array, _ind) ? toKey(_ind) : String(Math.abs(_ind));
    if (R.isEmpty(ind)) {
      return acc;
    }
    return R.isEmpty(acc) ? ind : `${acc}:${ind}`
  },
  '',
  indexes
);

const getPivotKey = indexes => R.pipe(
  R.props(indexes),
  R.join(':')
);

const getPivots = (layout, shape) => R.pipe(
  R.props(shape),
  R.map(
    R.pipe(
      R.map(entry => R.has('dimensions', entry) ? R.pluck('__index', entry.dimensions || []) : entry.__index),
      R.unnest,
      getPivotKey
    )
  )
)(layout);

const indexWithPivots = (pivots, observations) => R.pipe(
  R.values,
  R.reduce(
    (acc, obs) => {
      const path = R.map(
        pivot => pivot(obs.orderedDimIndexes),
        pivots
      );
      return R.over(
        R.lensPath(path),
        R.ifElse(R.isNil, R.always([obs]), R.append(obs))
      )(acc);
    },
    {}
  )
)(observations);

export const getCuratedCells = ({ layout, observations, shape }) => {
  const pivots = getPivots(layout, shape);
  return indexWithPivots(pivots, observations);
};

const filterSeries = (series, getHasStillData) => {
  let removedMissingIndexes = [];
  return R.reduce((acc, serie) => {
    const hasStillData = getHasStillData(serie);
    const serieParentsSets = R.map(p => new Set(p), R.propOr([], 'parentsIndexes', serie));
    if (!hasStillData) {
      removedMissingIndexes = R.addIndex(R.map)((mIs, ind) => {
        const filtered = R.filter(i => serieParentsSets[ind].has(i), R.nth(ind, removedMissingIndexes) || []);
        return R.concat(filtered, mIs);
      }, serie.missingIndexes || []);
      return acc;
    }
    const _serie = R.pipe(
      R.over(
        R.lensProp('missingIndexes'),
        (missingIndexes = []) => R.addIndex(R.map)((mIs, ind) => {
          const removed = R.nth(ind, removedMissingIndexes) || [];
          if (R.isEmpty(removed)) {
            return mIs;
          }
          const filteredRemoved = R.filter(i => serieParentsSets[ind].has(i), removed);
          return R.concat(filteredRemoved, mIs || []);
        }, missingIndexes)
      ),
      R.over(
        R.lensProp('parentsIndexes'),
        (parentsIndexes = []) => R.addIndex(R.map)((p, ind) => {
          return R.reject(i => R.includes(i, R.nth(ind, removedMissingIndexes) || []), p || []);
        }, parentsIndexes)
      )
    )(serie);
    removedMissingIndexes = [];
    return R.append(_serie, acc);
  }, [], series);
};

export const refineSections = (sections, extractedKeys, curatedObs) => R.pipe(
  R.map(
    (section) => {
      const sectionKey = toKey(R.propOr([], 'indexes', R.head(section)));
      const getHasRowStillData = (row) => {
        const rowKey = toKey(R.prop('indexes', row));
        return R.pipe(
          R.path([sectionKey, rowKey]),
          R.omit(extractedKeys),
          R.isEmpty,
          R.not
        )(curatedObs);
      };
      return R.over(
        R.lensIndex(1),
        rows => filterSeries(rows, getHasRowStillData)
      )(section);
    }
  ),
  R.filter(
    R.pipe(R.nth(1), R.isEmpty, R.not)
  )
)(sections);

export const refineHeader = (headers, extractedKeys, curatedObs) => {
  const getHasHeaderStillData = (header) => {
    const headerKey = toKey(R.prop('indexes', header));
    return R.pipe(
      R.prop(headerKey),
      (sections) => {
        return R.pickBy(
          (rows, section) => R.pipe(
            R.omit(R.propOr([], section, extractedKeys)),
            R.isEmpty,
            R.not
          )(rows),
          sections
        );
      },
      R.isEmpty,
      R.not
    )(curatedObs);
  };
  return filterSeries(headers, getHasHeaderStillData);
};

const getSerieLinesCount = serie => {
  const missingParents = R.propOr([], 'missingParents', serie);
  const missingParentsRowsCount = R.pipe(R.flatten, R.length)(missingParents);
  return missingParentsRowsCount + 1;
}

export const truncateSectionRows = (n, sectionsData) => {
  let truncated = sectionsData;
  let extractedKeys = {};
  let _n = n;
  while (_n > 0) {
    const lastSection = R.last(truncated);
    const rows = lastSection[1];
    const rowsLength = R.length(rows);
    let truncatedRowsCount = 0;
    while (_n > 0 && truncatedRowsCount !== rowsLength) {
      const rowsCount = getSerieLinesCount(R.nth(-1 * truncatedRowsCount, rows));
      _n = _n - rowsCount;
      truncatedRowsCount++;
    }
    const sectionKey = R.pipe(R.head, R.prop('indexes'), toKey)(lastSection);
    extractedKeys = {
      ...extractedKeys,
      [sectionKey]: R.pipe(
        R.takeLast(truncatedRowsCount),
        R.map(r => toKey(R.prop('indexes', r)))
      )(rows)
    };
    if (_n > 0) {
      truncated = R.dropLast(1, truncated);
    }
    else {
      truncated = R.over(R.lensIndex(-1), R.over(R.lensIndex(1), R.dropLast(truncatedRowsCount)))(truncated);
    }
  }
  return ({ truncated, extractedKeys });
};

export const truncateHeader = (n, headerData) => {
  let { truncated, extractedKeys } = { truncated: headerData, extractedKeys: [] };
  let _n = n;
  while (_n > 0) {
    const lastHeader = R.last(truncated);
    const columnsCount = getSerieLinesCount(lastHeader);
    const extractedKey = toKey(R.prop('indexes', lastHeader));
    extractedKeys = R.append(extractedKey, extractedKeys);
    truncated = R.dropLast(1, truncated);
    _n = _n - columnsCount;
  }
  return ({ truncated, extractedKeys });
};

const truncateLayout = (isVertical) => R.ifElse(
  R.always(isVertical),
  truncateSectionRows,
  truncateHeader,
);

const refineLayout = (isVertical) => R.ifElse(
  R.always(isVertical),
  refineHeader,
  refineSections
)

const getShape = isVertical => isVertical ? ['header', 'sections', 'rows'] : ['sections', 'rows', 'header'];

const getRefinedLayout = (isVertical, truncated, refined) => R.ifElse(
  R.always(isVertical),
  R.pipe(R.assoc('sections', truncated), R.assoc('header', refined)),
  R.pipe(R.assoc('header', truncated), R.assoc('sections', refined)),
)({});

const segregateLayout = (isVertical) => R.ifElse(
  R.always(isVertical),
  R.props(['sections', 'header']),
  R.props(['header', 'sections'])
);

export const refineLayoutSize = ({ layout, observations, limit }) => layoutIndexes => {
  const { header, sections } = layoutIndexes;
  if (R.isNil(limit) || limit === 0 || R.all(R.isEmpty, [header, sections])) {
    return R.pipe(
      R.set(R.lensProp('truncated'), false), 
      R.set(R.lensProp('totalCells'), null), 
    )(layoutIndexes);
  }

  //number of dimensions in header
  const headerDimCount = R.pipe(
    R.head, // first column
    R.when(R.isNil, R.always({})),
    R.propOr([], 'indexes'),
    R.length // number of dims
  )(header);

  //number of columns for values
  const headerValuesCount = R.pipe(
    R.reduce((acc, header) => {
      const columnsCount = getSerieLinesCount(header);
      return acc + columnsCount;
    }, 0),
    R.when(R.equals(0), R.always(1))
  )(header);

  //total of cells in header
  const headerCellsCount = headerDimCount * (headerValuesCount + 1);

  // number of dimensions in a row
  const rowDimCount = R.pipe(
    R.head, // firstSection
    R.last, // rows,
    R.head, // first row
    R.propOr([], 'indexes'),
    R.length
  )(sections);

  // number of cells in a row
  const rowCellsCount = rowDimCount + headerValuesCount + 1;

  // number of rows
  const rowsCount = R.pipe(
    R.map(R.last),
    R.unnest,
    R.reduce((acc, row) => {
      const rowsCount = getSerieLinesCount(row);
      return acc + rowsCount;
    }, 1)
  )(sections);

  // total of cells in all rows
  const rowsCellsCount = rowCellsCount * rowsCount;

  // number of sections cells
  const sectionsCellsCount = R.ifElse(
    R.pipe(R.head, R.head, R.propOr([], 'indexes'), R.length, R.equals(0)),
    R.always(0),
    R.length
  )(sections);

  const total = rowsCellsCount + sectionsCellsCount + headerCellsCount;

  const excess = total - limit;
  if (excess <= 0) {
    return R.pipe(
      R.set(R.lensProp('truncated'), false), 
      R.set(R.lensProp('totalCells'), total), 
    )(layoutIndexes);
  }

  // total of cells in one column
  const columnCellsCount = headerDimCount + rowsCount;

  const isVertical = columnCellsCount > rowCellsCount;

  const [toTruncate, toRefine] = segregateLayout(isVertical)(layoutIndexes);

  let cutLength = R.pipe(
    R.ifElse(R.identity, R.always(rowCellsCount), R.always(columnCellsCount)),
    R.divide(excess),
    Math.ceil
  )(isVertical);

  const maxCutLength = isVertical ? rowsCount - 2 : headerValuesCount - 1;
  cutLength = cutLength > maxCutLength ? maxCutLength : cutLength;

  const { truncated, extractedKeys } = truncateLayout(isVertical)(cutLength, toTruncate);

  const dimsLength = R.pipe(R.values, R.head, R.propOr([], 'orderedDimIndexes'), R.length)(observations);
  const _layout = R.map( 
    R.map(entry => entry.id === 'OBS_ATTRIBUTES' ? R.assoc('__index', dimsLength - 1, entry) : entry),
    layout
  );
  const curatedObs = R.pipe(
    getShape,
    (shape) => getCuratedCells({ layout: _layout, observations, shape })
  )(isVertical);


  const refined = refineLayout(isVertical)(toRefine, extractedKeys, curatedObs);

  const result = getRefinedLayout(isVertical, truncated, refined);

  return R.pipe(
    R.set(R.lensProp('truncated'), true), 
    R.set(R.lensProp('totalCells'), total), 
  )(result);
};
