import * as R from 'ramda';
import { getFlagsAndNotes } from './getFlagsAndNotes';
import { getLayoutCoordinatesValidator } from '../utils';

const getValueData = (index, dimension, parentsIndexes, missingParents = []) => ({
	dimension: R.pick(['id', 'name', '__index'], dimension),
	value: {
		...R.pipe(
			R.pathOr({}, ['values', index]),
			R.pick(['id', 'name', 'isNonCoded', 'flags', 'hasAdvancedAttributes', 'display'])
		)(dimension),
		parents: parentsIndexes,
		missingParents
	}
});

const addCoordinatesToKey = (key, dimensionId, valueId) =>
	`${key}${R.isEmpty(key) ? '' : ':'}${dimensionId}=${valueId}`;

const simpleValueDataSetter = (valueData, datas = []) => R.append(valueData, datas);

const combinedValueDataSetter = (valueData, datas = []) =>
	R.over(R.lensIndex(-1), d => ({ ...d, values: R.append(R.prop('value', valueData), d.values) }), datas);

const addValueToSerieData = (dataSetter) => (index, parentsIndexes, dimension, serieData, missingParents) => {
	const valueData = getValueData(index, dimension, parentsIndexes, missingParents);
	const valueId = R.path(['value', 'id'], valueData);
	return ({
		...serieData,
		data: dataSetter(valueData, R.prop('data', serieData)),
		key: addCoordinatesToKey(R.prop('key', serieData), R.prop('id', dimension), valueId)
	});
};

const addSimpleValueToSerieData = addValueToSerieData(simpleValueDataSetter);

const addCombinedValueToSerieData = addValueToSerieData(combinedValueDataSetter);

const getHasAdvancedAttributes = (attrValues, customAttributes) => R.pipe(
    R.omit(R.concat(customAttributes.flags || [], customAttributes.notes || [])),
    R.isEmpty,
    R.not
  )(attrValues);

const getCoordinates = (indexes, topCoordinates, definition) => R.addIndex(R.reduce)((acc, entry, index) => {
	if (R.has('dimensions', entry)) {
		return R.addIndex(R.reduce)((_acc, dim, _index) => {
			const valInd = R.path([index, _index], indexes);
			return R.assoc(dim.id, R.path(['values', Math.abs(valInd), 'id'], dim), _acc)
		}, acc, entry.dimensions);
	}
	const valInd = R.nth(index, indexes);
	return R.assoc(entry.id, R.path(['values', Math.abs(valInd), 'id'], entry), acc);
}, topCoordinates, definition);

const getAttributesSeries = (validator, attributesSeries) => R.reduce((acc, attrs) => {
	const attr = R.head(R.values(attrs)) || {};
	if (R.length(attr.relationship || []) === 1 && !attr.combined) {
		return acc;
	}
	const coordinates = R.propOr({}, 'coordinates', attr);
	const isValid = validator(coordinates);
	if (!isValid && !R.prop('isObs', attr)) {
		return acc;
	}
	return ({ ...acc, ...attrs });
}, {}, R.values(attributesSeries));

const combineConcepts = (combDimValues, definition, attrValues) => R.over(
	R.lensProp('data'),
	R.over(R.lensIndex(-1), data => ({
		...data,
		values: R.reduce((acc, conceptId) => {
			if (R.has(conceptId, combDimValues)) {
				return R.append(R.prop(conceptId, combDimValues), acc);
			}
			if (R.has(conceptId, definition.fixedDimValues || {})) {
				return R.append(R.prop(conceptId, definition.fixedDimValues), acc);
			}
			if (R.has(conceptId, attrValues)) {
				const value = R.path([conceptId, 'value'], attrValues);
				return R.append(value, acc);
			}
			return acc;
		}, [], definition.concepts)
	})));

const getSerieFlagsAndSideProps = (coordinates, validator, attributesValues, customAttributes, metadataCoordinates, data) => {
	const layoutAttrValues = R.reject(attr => R.prop('isObs', attr) && !validator(attr.coordinates), attributesValues);
  const flags = getFlagsAndNotes(layoutAttrValues, customAttributes);
  const hasMetadata = !R.isNil(R.find(validator, metadataCoordinates));
	const hasDataAdvancedAttributes = R.any(
		d => R.has('value', d) ? R.prop('hasAdvancedAttributes', d.value) : R.any(R.prop('hasAdvancedAttributes'), d.values)
	, data);
  const hasAdvancedAttributes = hasDataAdvancedAttributes || getHasAdvancedAttributes(layoutAttrValues, customAttributes);
  const sideProps = hasMetadata || hasAdvancedAttributes
    ? { hasMetadata, hasAdvancedAttributes, coordinates } : null;
	return { flags, sideProps };
};

const getSerieDimensionsData = (serie, definition, attributesValues, missingParentsGetter) =>  R.addIndex(R.reduce)(
	(onGoingSerie, entry, index) => {
		if (R.has('dimensions', entry)) {
			const combValuesIndexes = R.pathOr([], ['indexes', index], serie);
			const combParentsIndexes = R.pathOr([], ['parentsIndexes', index], serie);
			const combMissingParentsIndexes = R.pathOr([], ['missingIndexes', index], serie);
			let combDimValues = {};
			const res = R.addIndex(R.reduce)((combSerie, dimension, dimIndex) => {
				const valueIndex = Math.abs(R.nth(dimIndex, combValuesIndexes));
				const _parentsIndexes = R.nth(dimIndex, combParentsIndexes) || [];
				const missingParentsIndexes = R.nth(dimIndex, combMissingParentsIndexes) || [];
				const { parentsIndexes, missingParents } = missingParentsGetter(combinedValueDataSetter)(missingParentsIndexes, _parentsIndexes, dimension, combSerie);
				const next = addCombinedValueToSerieData(valueIndex, parentsIndexes, dimension, combSerie, missingParents);
				const value = R.pipe(R.prop('data'), R.last, R.prop('values'), R.last)(next);
				combDimValues = R.assoc(dimension.id, value, combDimValues);
				return next;
			},
			R.over(
				R.lensProp('data'),
				R.append({ dimension: R.pick(['id', 'name'], entry), values: [] })
			)(onGoingSerie),
			R.propOr([], 'dimensions', entry));

			const combined = combineConcepts(combDimValues, entry, onGoingSerie.attributes)(res);
			return  R.over(R.lensProp('attributes'), R.omit(entry.concepts), combined);
		}

		const valueIndex = Math.abs(R.path(['indexes', index], serie));
		const _parentsIndexes = R.pathOr([], ['parentsIndexes', index], serie);
		const missingParentsIndexes = R.pathOr([], ['missingIndexes', index], serie);
		const { parentsIndexes, missingParents } =  missingParentsGetter(simpleValueDataSetter)(missingParentsIndexes, _parentsIndexes, entry, onGoingSerie);
		return addSimpleValueToSerieData(valueIndex, parentsIndexes, entry, onGoingSerie, missingParents);
	},
	{ data: [], key: '', attributes: attributesValues },
	definition,
);

export const getSerieDataWithMissingLines = (serie, definition, topCoordinates, attributesSeries, customAttributes, metadataCoordinates) => {
	const lines = [];

	const serieCoordinates = getCoordinates(serie.indexes, topCoordinates, definition);
	const coordinatesValidator = getLayoutCoordinatesValidator(serieCoordinates, topCoordinates);
	const attributesValues = getAttributesSeries(coordinatesValidator, attributesSeries);

	const getMissingParents = dataSetter => (missingParentsIndexes, _parentsIndexes, dim, serieData) => R.reduce(
		(acc, index) => {
				const missingParentData = addValueToSerieData(dataSetter)(index, acc.parentsIndexes, dim, serieData);
				lines.push({ ...R.dissoc('attributes', missingParentData), sideProps: null, flags: [], isEmpty: true });
			return R.over(R.lensProp('parentsIndexes'), R.append(index), acc);
		},
	{ parentsIndexes: _parentsIndexes, missingParents: [] }, missingParentsIndexes);
	const { attributes, ...serieData } = getSerieDimensionsData(serie, definition, attributesValues, getMissingParents);
	const topCoordinatesValidator = getLayoutCoordinatesValidator(topCoordinates);
	const flagsValidator = coordinates => coordinatesValidator(coordinates) && !topCoordinatesValidator(coordinates);
	const { flags, sideProps } = getSerieFlagsAndSideProps(serieCoordinates, flagsValidator, attributes, customAttributes, metadataCoordinates, serieData.data);
	return R.append({ ...serieData, flags, sideProps, coordinates: serieCoordinates }, lines);
};

export const getSerieDataWithoutMissingLines = (serie, definition, topCoordinates, attributesSeries, customAttributes, metadataCoordinates) => {
	const serieCoordinates = getCoordinates(serie.indexes, topCoordinates, definition);
	const coordinatesValidator = getLayoutCoordinatesValidator(serieCoordinates, topCoordinates);
	const attributesValues = getAttributesSeries(coordinatesValidator, attributesSeries);

	const getMissingParents = () => (missingParentsIndexes, _parentsIndexes, dim) => R.reduce(
		(acc, index) => ({
			parentsIndexes: R.append(index, acc.parentsIndexes),
			missingParents: R.append(R.prop('value', getValueData(index, dim, acc.parentsIndexes)), acc.missingParents)
		}),
	{ parentsIndexes: _parentsIndexes, missingParents: [] }, missingParentsIndexes);

	const { attributes, ...serieData } = getSerieDimensionsData(serie, definition, attributesValues, getMissingParents);
	const { flags, sideProps } = getSerieFlagsAndSideProps(serieCoordinates, coordinatesValidator, attributes, customAttributes, metadataCoordinates, serieData.data);
	return ({ ...serieData, flags, sideProps, coordinates: serieCoordinates });
};

export const getLayoutData = (layoutIndexes, layout, { metadataCoordinates, attributesSeries, customAttributes, topCoordinates={} }) => {
  const { header, sections, ...rest } = layoutIndexes;
  const headerData = R.reduce((acc, serie) => {
  	const datas = getSerieDataWithMissingLines(serie, layout.header, topCoordinates, attributesSeries, customAttributes, metadataCoordinates);
  	return R.concat(acc, datas);
  }, [], header);

  const sectionsData = R.map(
  	([sectionSerie, rowsSeries]) => {
  		const sectionData = getSerieDataWithoutMissingLines(sectionSerie, layout.sections, topCoordinates, attributesSeries, customAttributes, metadataCoordinates, false);
  		return [
  			sectionData,
  			 R.reduce((acc, serie) => {
  				const datas = getSerieDataWithMissingLines(serie, layout.rows, R.propOr(topCoordinates, 'coordinates', sectionData), attributesSeries, customAttributes, metadataCoordinates);
  				return R.concat(acc, datas);
  			}, [], rowsSeries)
  		];
  	},
  	sections
  );

  return ({ headerData, sectionsData, ...rest });
};
