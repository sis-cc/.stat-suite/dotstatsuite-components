import * as R from 'ramda';

export const getCellsMetadataCoordinates = (metadataCoordinates, oneValueDimensions, layoutIds) => {
  const oneValueDimsIds = R.pluck('id', oneValueDimensions);
  return R.reject(coordinates => {
    const columnIds = R.concat(layoutIds.header, oneValueDimsIds);
    if (R.isEmpty(R.omit(columnIds, coordinates))) {
      return true;
    }
    const rowsIds = [...layoutIds.sections, ...layoutIds.rows, ...oneValueDimsIds];
    return R.isEmpty(R.omit(rowsIds, coordinates));
  }, metadataCoordinates);
};
