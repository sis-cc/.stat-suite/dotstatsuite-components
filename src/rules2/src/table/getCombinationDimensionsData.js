import * as R from 'ramda';
import { parseValueHierarchy, isSameValueAsPrevious } from './parseValueHierarchy';

export const getCombinationDimensionsData = (indexes, combination, previous, sameSerie) => {
  let coordinates = {};
  let ids = [];
  let _sameSerie = sameSerie;
  let hasAdvancedAttributes = false;

  const { dimensions=[] } = combination;
  const dimValues = R.addIndex(R.reduce)(
    (acc, valIndex, dimIndex) => {
      const dimension = R.nth(dimIndex, dimensions);
      const value = R.nth(Math.abs(valIndex), R.propOr([], 'values', dimension));
      hasAdvancedAttributes = !hasAdvancedAttributes ? value.hasAdvancedAttributes : true;
      coordinates = R.assoc(dimension.id, value.id, coordinates);
      ids = R.append(`${dimension.id}=${value.id}`, ids);
      const previousValue = R.propOr({}, dimension.id, previous);
      if (isSameValueAsPrevious(value, previousValue) && _sameSerie) {
        return R.assoc(dimension.id, previousValue, acc);
      }
      else {
        const _parsedValue = parseValueHierarchy(value, _sameSerie ? previousValue || {} : {}, dimension.indexedValues);
        const parsedValue = R.over(
          R.lensProp('display'),
          (display=true) => display && R.propOr(true, 'display', dimension)
        )(_parsedValue);
        if (!R.isNil(previous)) {
          _sameSerie = false;
        }
        return R.assoc(dimension.id, parsedValue, acc);
      }
    },
    {},
    indexes
  );

  return ({ dimValues, coordinates, ids, sameSerie: _sameSerie, hasAdvancedAttributes });
};
