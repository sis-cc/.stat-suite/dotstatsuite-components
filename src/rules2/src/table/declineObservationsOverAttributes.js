import * as R from 'ramda';

export const declineObservationsOverAttributes = (attrsIds, observations) => {
  const obsKeys = R.keys(observations);
  return R.reduce((acc, obsKey) => {
    const obs = R.prop(obsKey, observations);
    const enhancedObs = R.pipe(
      R.over(R.lensProp('orderedDimIndexes'), R.append(0)),
      R.over(R.lensProp('attributes'), R.omit(attrsIds)),
      R.assocPath(['indexedDimValIds', 'OBS_ATTRIBUTES'], 'OBS_VALUE')
    )(obs);
    return R.addIndex(R.reduce)((_acc, attrId, attrIndex) => {
      const attr = R.pathOr({}, ['attributes', attrId], obs);
      if (R.isEmpty(attr) || R.isNil(R.prop('value', attr)) || !R.pathOr(true, ['value', 'display'], attr)) {
        return _acc;
      }
      const declinedKey = `${obsKey}:${attrIndex+1}`;
      const declined = {
        ...obs,
        attributes: {},
        value: R.prop('value', attr),
        formattedValue: R.prop('value', attr),
        orderedDimIndexes: R.append(attrIndex+1, obs.orderedDimIndexes),
        indexedDimValIds: {
          ...obs.indexedDimValIds,
          OBS_ATTRIBUTES: attr.id
        }
      };
      return R.assoc(declinedKey, declined, _acc);
    }, R.assoc(`${obsKey}:0`, enhancedObs, acc), attrsIds);
  }, {}, obsKeys);
};
