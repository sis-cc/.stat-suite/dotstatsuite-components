import * as R from 'ramda';
import { isTimePeriodDimension, isRefAreaDimension } from '@sis-cc/dotstatsuite-sdmxjs';

export const getCombinationsNotInLayout = (combinations, layoutIds) => {
  const { header=[], sections=[], rows=[] } = layoutIds;
  const idsInLayout = new Set([...header, ...sections, ...rows]);
  return R.reject(comb => idsInLayout.has(comb.id), combinations);
};

export const indexCombsRelationships = (combinations) => R.reduce((acc, comb) => {
  const { id, relationship=[] } = comb;
  return R.assoc(id, relationship, acc);
}, {}, combinations);

export const getLayoutLevelRelationship = indexedRelationships => ids => R.reduce((acc, id) => {
  if (R.has(id, indexedRelationships)) {
    return R.concat(acc, R.prop(id, indexedRelationships));
  }
  return R.append(id, acc);
}, [], ids);

export const getLayoutRelationships = (indexedRelationships, layoutIds) => R.map(
  getLayoutLevelRelationship(indexedRelationships),
  layoutIds
);

export const injectCombination = combination => ids => {
  const { id, concepts=[] } = combination;
  const conceptsSet = new Set(concepts);
  let inject = true;
  const refined = R.reduce((acc, i) => {
    if (!conceptsSet.has(i)) {
      return R.append(i, acc);
    }
    if (inject) {
      inject = false;
      return R.append(id, acc);
    }
    return acc;
  }, [], ids);
  return inject ? R.append(id, refined) : refined;
};

export const getCombinationLevelCompatibility = (layoutRelationships, combination) => {
  const { relationship=[] } = combination;
  const idsInHeaderSet = new Set(layoutRelationships.header);
  const idsInSectionsSet = new Set(layoutRelationships.sections);
  const idsInRowsSet = new Set(layoutRelationships.rows);
  const [idsInHeader, rest] = R.partition(
    i => idsInHeaderSet.has(i),
    relationship
  );
  const [idsInSections, idsInRows] = R.reduce((_acc, id) => {
    if (idsInSectionsSet.has(id))
      return R.over(R.lensIndex(0), R.append(id), _acc);
    if (idsInRowsSet.has(id))
      return R.over(R.lensIndex(1), R.append(id), _acc);
    return R.over(R.lensIndex(2), R.append(id), _acc);
  }, [[], [], []], rest);
  if (!R.isEmpty(idsInHeader) && R.isEmpty(idsInSections) && R.isEmpty(idsInRows)) {
    return 'header';
  }
  if (!R.isEmpty(idsInHeader)) {
    return null;
  }
  if (!R.isEmpty(idsInSections) && R.isEmpty(idsInRows)) {
    return 'sections';
  }
  if (!R.isEmpty(idsInRows)) {
    return 'rows';
  }
  return 'blank';
}

const filterLayoutIds = (indexedCombinations, layoutDimsIds, obsAttributes) => (layoutIds) => {
  const layoutConcepts = R.map(
    R.reduce((acc, id) => {
      if (R.has(id, indexedCombinations)) {
        return R.concat(acc, R.path([id, 'concepts'], indexedCombinations));
      }
      return R.append(id, acc);
    }, []),
    layoutIds
  );

  return R.mapObjIndexed(
    (ids, level) => R.filter(id => {
      if (id === 'OBS_ATTRIBUTES') {
        return !R.isEmpty(obsAttributes);
      }
      if (R.has(id, indexedCombinations)) {
        const comb = R.prop(id, indexedCombinations);
        const dims = R.filter(i => layoutDimsIds.has(i), comb.concepts);
        if (!R.isEmpty(dims)) {
          return true;
        }
        const oppositeIds = level === 'header'
          ? R.concat(R.prop('sections', layoutConcepts), R.prop('rows', layoutConcepts))
          : R.prop('header', layoutConcepts);
        return R.isEmpty(R.intersection(comb.relationship, oppositeIds));
      }
      return layoutDimsIds.has(id);
    }, ids),
    layoutIds
  );
};

export const injectCombinationsInLayout = (combinations, layoutIds, defaultLayoutIds = {}) => {
  const notInLayoutCombs = getCombinationsNotInLayout(combinations, layoutIds);
  if (R.isEmpty(notInLayoutCombs)) {
    return layoutIds;
  }
  const indexedCombsRelationships = indexCombsRelationships(combinations);
  let layoutRelationships = getLayoutRelationships(indexedCombsRelationships, layoutIds);
  const defaultLayoutRelationships = getLayoutRelationships(indexedCombsRelationships, defaultLayoutIds);
  return R.reduce((acc, comb) => {
    let levelCompatibility = getCombinationLevelCompatibility(layoutRelationships, comb);
    if (R.isNil(levelCompatibility)) {
      return acc;
    }
    if (R.equals('blank', levelCompatibility)) {
      levelCompatibility = getCombinationLevelCompatibility(defaultLayoutRelationships, comb);
      if (R.isNil(levelCompatibility)) {
        return acc;
      }
      levelCompatibility = R.equals('blank', levelCompatibility) ? 'rows' : levelCompatibility;
    }

    layoutRelationships[levelCompatibility] = R.concat(layoutRelationships[levelCompatibility], comb.relationship);
    const ids = injectCombination(comb)(acc[levelCompatibility]);
    return R.assoc(levelCompatibility, ids)(acc);
  }, layoutIds, notInLayoutCombs);
};

export const getLayoutDimsIds = dimensions => R.reduce(
  (acc, d) => (d.header ? acc : acc.add(d.id)),
  new Set([]),
  dimensions,
);

export const getLayoutCombinations = (combinations, layoutDimsIds, attributes) => {
  const indexedAttributes = R.indexBy(R.prop('id'), attributes);
  return R.reduce(
    (acc, comb) => {
      const { layoutConcepts, relationship } = R.reduce(
        (_acc, concept) => {
          if (layoutDimsIds.has(concept)) {
            return R.map(R.append(concept), _acc);
          }
          if (R.has(concept, indexedAttributes)) {
            const attr = R.prop(concept, indexedAttributes);
            return attr.series
              ? {
                  ..._acc,
                  relationship: R.concat(_acc.relationship, attr.relationship),
                }
              : _acc;
          }
          return _acc;
        },
        { layoutConcepts: [], relationship: [] },
        comb.concepts || [],
      );
      if (R.isEmpty(relationship)) {
        return acc;
      }
      return R.append({ ...comb, relationship, concepts: layoutConcepts }, acc);
    },
    [],
    combinations,
  );
};

const extractConcepts = (ids, indexedCombinations, layoutDimsIds) => {
  const entry = R.find(id => {
    if (R.has(id, indexedCombinations)) {
      const dimId = R.find(i => layoutDimsIds.has(i), R.pathOr([], [id, 'concepts'], indexedCombinations));
      return !R.isNil(dimId);
    }
    return true;
  }, ids);

  const getRelationship = id => {
    if (R.has(id, indexedCombinations)) {
      return R.append(id, R.path([id, 'relationship'], indexedCombinations));
    }
    return [id];
  };
  let indexedRels = R.reduce((acc, id) =>
    R.assoc(id, getRelationship(id), acc)
  , {}, ids);
  
  let relationship = [entry];
  while (!R.isEmpty(indexedRels)) {
    const related = R.pickBy(rels => !R.isEmpty(R.intersection(relationship, rels)), indexedRels);
    if (R.isEmpty(related)) {
      break;
    }
    indexedRels = R.omit(R.keys(related), indexedRels);
    relationship = R.concat(relationship, R.unnest(R.values(related)));
  }
  const set = new Set(relationship);
  return R.partition(id => set.has(id), ids);
}

export const injectRemainingDimensionsInLayout = (layoutIds, remainingDims, indexedCombinations, layoutDimsIds) => {
  let _layout = layoutIds;
  let _remaining = remainingDims;

  const dimInRows = R.find(id => {
    if (layoutDimsIds.has(id)) {
      return true;
    }
    const dimId = R.find(i => layoutDimsIds.has(i), R.pathOr([], [id, 'concepts'], indexedCombinations));
    return !R.isNil(dimId);
  }, _layout.rows);

  if (R.isNil(dimInRows)) {
    if (R.isEmpty(_remaining)) {
      const toTakeIn = R.isEmpty(_layout.sections) ? 'header' : 'sections';
      const [extracted, remains] = extractConcepts(R.prop(toTakeIn, _layout), indexedCombinations, layoutDimsIds);
      _layout = ({ ..._layout, rows: R.concat(_layout.rows, extracted), [toTakeIn]: remains });
    }
    else {
      _layout = R.over(R.lensProp('rows'), R.append(R.head(_remaining)))(_layout);
      _remaining = R.tail(_remaining)
    }
  }
  if (!R.isEmpty(_remaining)) {
    const hasNoHeader = R.isEmpty(_layout.header);
    const firstRemaining = R.head(_remaining);
    _remaining = hasNoHeader ? R.tail(_remaining) : _remaining;
    _layout = {
      ..._layout,
      sections: R.concat(_layout.sections, _remaining),
      header: hasNoHeader ? [firstRemaining] : _layout.header,
    };
  }
  return _layout;
};

export const getTableLayoutIds = (
  defaultLayoutIds,
  currentLayoutIds,
  dimensions,
  attributes,
  combinations
) => {
  const layoutDimsIds = getLayoutDimsIds(dimensions);
  const layoutCombinations = getLayoutCombinations(
    combinations,
    layoutDimsIds,
    attributes,
  );
  const isBlank = R.pipe(R.values, R.all(R.isEmpty))(currentLayoutIds);

  const combinationsConcepts = R.pipe(R.pluck('_concepts'), R.unnest, ids => new Set(ids))(combinations);
  const obsAttributes = R.filter(a => 
    a.series && R.isEmpty(a.relationship || []) && a.display && !combinationsConcepts.has(a)
  , attributes);
  const indexedCombinations = R.indexBy(R.prop('id'), layoutCombinations);
  const filteredLayoutIds = filterLayoutIds(indexedCombinations, layoutDimsIds, obsAttributes)
    (isBlank ? defaultLayoutIds : currentLayoutIds);

  const layoutWithCombs = injectCombinationsInLayout(layoutCombinations, filteredLayoutIds, defaultLayoutIds);
  const layoutRelationshipsSets = R.map(R.pipe(
    ids => R.pick(ids, indexedCombinations),
    R.values,
    R.pluck('relationship'),
    R.unnest,
    i => new Set(i)
  ), layoutWithCombs);
  const layoutConceptsSets = R.pipe(
    R.map(
      R.reduce((acc, id) => {
        if (R.has(id, indexedCombinations)) {
          return R.concat(acc, R.path([id, 'concepts'], indexedCombinations));
        }
        return R.append(id, acc);
      }, []),
    ),
    R.values,
    R.unnest,
    ids => new Set(ids)
  )(layoutWithCombs);
  const defaultLayoutConceptsSets = R.map(
    ids => new Set(ids),
    defaultLayoutIds
  );

  const { nextLayout, remaining } = R.reduce((acc, dim) => {
    if (layoutConceptsSets.has(dim.id) || dim.header) {
      return acc;
    }
    if (layoutRelationshipsSets.header.has(dim.id) || defaultLayoutConceptsSets.header.has(dim.id)) {
      return R.over(R.lensPath(['nextLayout', 'header']), R.append(dim.id))(acc);
    }
    if (layoutRelationshipsSets.sections.has(dim.id) || defaultLayoutConceptsSets.sections.has(dim.id)) {
      return R.over(R.lensPath(['nextLayout', 'sections']), R.append(dim.id))(acc);
    }
    if (layoutRelationshipsSets.rows.has(dim.id)  || defaultLayoutConceptsSets.rows.has(dim.id)) {
      return R.over(R.lensPath(['nextLayout','rows']), R.append(dim.id))(acc);
    }
    if (isTimePeriodDimension(dim)) {
      return R.over(R.lensPath(['nextLayout','header']), R.append(dim.id))(acc);
    }
    if (isRefAreaDimension(dim)) {
      return R.over(R.lensPath(['nextLayout','rows']), R.append(dim.id))(acc);
    }
    return R.over(R.lensProp('remaining'), R.append(dim.id))(acc);
  }, { nextLayout: layoutWithCombs, remaining: [] }, dimensions);

  return injectRemainingDimensionsInLayout(nextLayout, remaining, indexedCombinations, layoutDimsIds);
};
