import * as R from 'ramda';
import { dimensionValueDisplay } from '../../rules/src';
import { combinedValuesDisplay } from './combinedValuesDisplay';
import { REJECTED_VALUE_IDS } from './constants';

export const getHeaderCombinations = (combinations, dimensions, attributes, display) => {
  const indexedDimensions = R.indexBy(R.prop('id'), dimensions);
  const indexedAttributes = R.indexBy(R.prop('id'), attributes);
  return R.reduce(
    (acc, comb) => {
      if (!R.prop('header', comb) || !R.propOr(true, 'display', comb)) {
        return acc;
      }

      const header = `${dimensionValueDisplay(display)(comb)}:`;

      const label = R.pipe(
        R.reduce(
          (_acc, concept) => {
            const artefact = R.has(concept, indexedDimensions)
              ? R.prop(concept, indexedDimensions)
              : R.prop(concept, indexedAttributes);
            if (R.isNil(artefact)) {
              return _acc;
            }
            const value = R.path(['values', 0], artefact);
            if (R.isNil(value)) {
              return _acc;
            }
            if (R.has(concept, indexedAttributes) && (!R.propOr(true, 'display', value)
              || !R.propOr(true, 'artefact', value) || R.includes(value.id, REJECTED_VALUE_IDS))) {
              return _acc;
            }
            return R.append(value, _acc);
          },
          []
        ),
        values => combinedValuesDisplay(display, values)
      )(comb.concepts || []);
      if (R.isEmpty(label)) {
        return acc;
      }
      return R.append({ header, label }, acc);
    },
    [],
    combinations,
  );
};
