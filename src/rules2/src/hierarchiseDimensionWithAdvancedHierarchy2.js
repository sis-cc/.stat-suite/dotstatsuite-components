import * as R from 'ramda';

const uniq = R.pipe(
  R.reduce(
    ({ ids, list }, id) => {
      if (R.has(id, list)) {
        return ({ ids, list });
      }
      return ({ ids: R.append(id, ids), list: R.assoc(id, id, list) });
    },
    { ids: [], list: {} }
  ),
  R.prop('ids')
);

export const refinePartialHierarchy = (hier, indexed) => {
  const { intact, broken } = R.pipe(
    R.keys,
    R.reduce(
      ({ intact, broken }, key) => {
        const ids = R.filter(id => R.has(id, indexed), hier[key]);
        if (key === '#ROOT') {
          return ({ broken, intact: R.assoc(key, ids, intact) });
        }
        const splitKey = R.split('.', key);
        const filtered = R.takeLastWhile(id => R.has(id, indexed), splitKey);
        if (R.length(filtered) === R.length(splitKey)) {
          return ({ broken, intact: R.assoc(key, ids, intact) });
        }
        const refinedKey = R.isEmpty(filtered) ? '#ROOT' : R.join('.', filtered);
        return ({
          intact,
          broken: R.over(R.lensProp(refinedKey), R.ifElse(R.isNil, R.always([ids]), R.append(ids)))(broken)
        });
      },
      { intact: {}, broken: {} }
    )
  )(hier);
  const intactIds = R.pipe(R.values, R.unnest)(intact);
  const refinedBroken = R.pipe(
    R.omit(intactIds),
    R.over(
      R.lensProp('#ROOT'),
      R.pipe(
        i => R.isNil(i) ? [] : i,
        R.unnest,
        R.reject(R.flip(R.includes)(intactIds)))
      ),
    R.map(R.pipe(R.unnest, uniq))
  )(broken);

  return ({
    ...refinedBroken,
    ...intact,
    '#ROOT': R.concat(refinedBroken['#ROOT'], intact['#ROOT'])
  })
};
