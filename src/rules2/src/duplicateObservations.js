import * as R from 'ramda';

export const duplicateObs = (dims, observations) => {
  const obsValues = R.values(observations);
  if (R.length(dims) === 0) {
    return observations;
  }
  const duplicated = R.reduce(
    (obs, dim) => {
      const groupedObs = R.groupBy(o => R.prop(dim.id, o.indexedDimValIds), obs);
      const enhancedObs = R.addIndex(R.map)(
        (value, index) => {
          const matchingObs = R.propOr([], value.id, groupedObs);

          return R.map(
            obs => R.over(
              R.lensProp('orderedDimIndexes'),
              _indexes => {
                const indexes = R.isNil(_indexes) ? R.map(i => Number(i), obs.dimValuesIndexes) : _indexes;
                return R.set(R.lensIndex(dim.__index), index)(indexes);
              }
            )(obs),
            matchingObs
          );
        },
        dim.values
      );
      return R.unnest(enhancedObs);
    },
    obsValues,
    dims
  );
  const res =  R.indexBy(R.pipe(R.prop('orderedDimIndexes'), R.join(':')), duplicated);
  return res;
};
