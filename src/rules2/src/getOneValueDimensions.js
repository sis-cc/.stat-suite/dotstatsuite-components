import * as R from 'ramda';
import { REJECTED_VALUE_IDS } from './constants';

export const getOneValueDimensions = (dimensions, attributes) => {
  const indexedRelevantAttributes = R.reduce(
    (acc, attr) => {
      if (!attr.header || !R.propOr(true, 'display', attr) || R.length(attr.relationship || []) !== 1) {
        return acc;
      }
      const value = R.path(['values', 0], attr);
      if (R.isNil(value) || !R.propOr(true, 'display', value) || R.includes(R.prop('id', value), REJECTED_VALUE_IDS)) {
        return acc;
      }
      const dim = R.head(attr.relationship);
      return R.over(R.lensProp(dim), (values = {}) => ({
        ...values,
        [attr.id]: { ...attr, value },
      }))(acc);
    },
    {},
    attributes,
  );
  return R.reduce(
    (acc, dim) => {
      if (!dim.header) {
        return acc;
      }
      const attrValues = R.propOr({}, dim.id, indexedRelevantAttributes);
      return R.append({ ...dim, attrValues }, acc);
    },
    [],
    dimensions,
  );
};
