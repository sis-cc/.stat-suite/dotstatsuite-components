import * as R from 'ramda';
import { dimensionValueDisplay } from '../../rules/src';
import { REJECTED_VALUE_IDS } from './constants';

const getRefinedMissinParentsLabels = R.pipe(
  R.propOr([], 'missingParents'),
  R.reduce((acc, par) => {
    if (
      !R.propOr(true, 'display', par) ||
      R.includes(par.id, REJECTED_VALUE_IDS)
    ) {
      return acc;
    }
    return R.append(dimensionValueDisplay('label')(par), acc);
  }, []),
);

export const singleValueDisplay = (display, value) => {
  if (display === 'label') {
    return R.converge(
      (missingParents, label) =>
        R.isEmpty(missingParents)
          ? label
          : R.join(' > ', R.append(label, missingParents)),
      [getRefinedMissinParentsLabels, dimensionValueDisplay(display)],
    )(value);
  } else if (display === 'both') {
    return `(${singleValueDisplay('code', value)}) ${singleValueDisplay(
      'label',
      value,
    )}`;
  }
  return dimensionValueDisplay(display)(value);
};

const _combinedValuesDisplay = (_display) => (values) =>
  R.pipe(
    R.reduce((acc, val) => {
      if (R.isNil(val)) return acc;
      const isDisplayed = R.propOr(true, 'display', val);
      const isRejected = R.includes(val.id, REJECTED_VALUE_IDS);
      if ((!isDisplayed || isRejected) && _display === 'label') return acc;
      return R.append(singleValueDisplay(_display, val), acc);
    }, []),
    (labels) => {
      if (!R.isEmpty(labels) || _display !== 'label') {
        return R.join(', ', labels);
      }
      const totalValue = R.find(R.propEq('id', '_T'), values);
      if (!R.isNil(totalValue)) {
        return dimensionValueDisplay('label')(totalValue);
      }
      if (R.isEmpty(values)) {
        return '';
      }
      const firstValue = R.head(values);
      return dimensionValueDisplay('label')(firstValue);
    },
  )(values);

export const combinedValuesDisplay = (display, values) => {
  if (display === 'both') {
    return R.converge(
      (_ids, labels) => {
        return dimensionValueDisplay('both')({
          id: _ids,
          name: labels,
        });
      },
      [_combinedValuesDisplay('code'), _combinedValuesDisplay('label')],
    )(values);
  }
  return _combinedValuesDisplay(display)(values);
};
