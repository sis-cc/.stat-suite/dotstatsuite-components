import * as R from 'ramda';

const parseTexts = texts => R.reduce(
  (acc, text) => {
    const splitted = R.split(':', text);
    if (R.length(splitted) !== 2) {
      return acc;
    }
    const [id, name] = splitted;
    return R.assoc(id, name, acc);
  },
  {},
  texts
);

export const parseCombinationDefinition = (locale) => (title, texts) => R.useWith(
  (titles, texts) =>
    R.reduce((acc, title) => {
      const split = R.split(':', title);
      if (R.length(split) !== 2) {
        return acc;
      }
      const [id, codes] = split;
      if (R.isEmpty(codes)) {
        return acc;
      }
      return R.append({
        id,
        concepts: R.split(',', codes),
        name: R.hasPath([locale, id], texts)
          ? R.path([locale, id], texts)
          : `[${id}]`,
      }, acc);
    }, [], titles),
  [R.split(';'), R.map(R.pipe(R.split(';'), parseTexts))],
)(title, texts);

export const getCombinationDefinitions = (annotations, locale) => {
  const annotation = R.find(annot => annot.type === 'COMBINED_CONCEPTS', annotations);
  if (R.isNil(annotation)) {
    return [];
  }
  const title = R.propOr('', 'title', annotation);
  const texts = R.propOr({}, 'texts', annotation);

  return parseCombinationDefinition(locale)(title, texts);
};
