import * as R from 'ramda';

export const sdmx_3_0_DataFormatPatch = (sdmxJson) => {
  const dataSet = R.pipe(R.pathOr({}, ['data', 'dataSets']), R.head)(sdmxJson);
  const structureIndex = R.prop('structure', dataSet);
  const structure = R.pipe(R.pathOr([], ['data', 'structures']), R.nth(structureIndex))(sdmxJson);

  return R.set(R.lensPath(['data', 'structure']), structure)(sdmxJson);
};
