import * as R from 'ramda';
import { dimensionValueDisplay } from '../../rules/src';
import { getFlagsAndNotes } from './table/getFlagsAndNotes';

export const getHeaderTitle = (dataflow, attributes, display, customAttributes) => ({
  label: dimensionValueDisplay(display)(dataflow),
  flags: R.pipe(
    getFlagsAndNotes,
    R.map(entry => ({
      code: entry.code,
      header: `${dimensionValueDisplay(display)(entry)}:`,
      label: dimensionValueDisplay(display)(entry.value),
    })),
  )(attributes, customAttributes),
});
