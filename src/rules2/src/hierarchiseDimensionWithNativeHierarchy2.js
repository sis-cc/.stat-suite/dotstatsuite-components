import * as R from 'ramda';

export const hierarchiseDimensionWithNativeHierarchy = (dimension) => {
  const values = R.sortBy(R.prop('__indexPosition'), dimension.values || []);
  const indexed = R.indexBy(R.prop('id'), values);
  const grouped = R.groupBy(val => val.parent && R.has(val.parent, indexed) ? val.parent : '#ROOT', values);

  let index = -1;
  const getChildren = ({ parentsIndexes, parentId }) => {
    const childs = R.propOr([], parentId, grouped);
    return R.reduce(
      (acc, child) => {
        index = index + 1;
        const refined = R.pipe(
          R.assoc('__indexPosition', index),
          R.assoc('parents', parentsIndexes),
          R.assoc('parent', R.isEmpty(parentsIndexes) ? undefined : parentId),
        )(child);
        const children = getChildren({
          parentsIndexes: R.propOr(false, 'isSelected', child)
           ? R.append(index, parentsIndexes) : parentsIndexes,
          parentId: child.id
        });
        return R.concat(acc, R.prepend(refined, children));
      },
      [],
      childs
    );
  }

  return R.set(
    R.lensProp('values'),
    getChildren({ parentsIndexes: [], parentId: '#ROOT' })
  )(dimension);
};
