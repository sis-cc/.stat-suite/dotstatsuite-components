import * as R from 'ramda';

export const getSeriesCombinations = (combinations, oneValuesDimensions) => {
  const indexedValues =  R.reduce(
    (acc, dim) => {
      const value = R.head(dim.values);
      return R.assoc(dim.id, value, acc);
    },
    {},
    oneValuesDimensions
  );

  return R.reduce(
    (acc, comb) => {
      if (!R.prop('series', comb)) {
        return acc;
      }
      const fixedDimValues = R.pick(comb.concepts, indexedValues);
      return R.append({ ...comb, fixedDimValues }, acc);
    },
    [],
    combinations
  );
};
