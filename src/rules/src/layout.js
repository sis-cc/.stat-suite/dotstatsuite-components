import * as R from 'ramda';
import {
  H_SYMBOL,
  SCATTER,
  STACKED_BAR,
  STACKED_ROW,
  V_SYMBOL
} from './constants';
import {
  getCombinationDefinitions,
  refineDimensions,
  parseCombinationDefinition
} from '../../rules2/src';

const isTableLayoutCompatible = (data, layout, combinationsDefinitions = [], dataquery = '') => {
  const indexedCombinations = R.indexBy(R.prop('id'), combinationsDefinitions);
  const indexedManyValuesDimensions = R.pipe(
    R.pathOr([], ['structure', 'dimensions', 'observation']),
    dimensions => refineDimensions(dimensions, dataquery),
    R.reject(d => R.prop('header', d) || !R.length(d.values || [])),
    R.indexBy(R.prop('id'))
  )(data);
  let remainings = indexedManyValuesDimensions;
  const layoutIds = R.pipe(R.values, R.unnest)(layout);
  let hasFoundIrrelevant = false;
  R.forEach(id => {
    if (R.has(id, indexedManyValuesDimensions)) {
      remainings = R.dissoc(id, remainings);
    }
    else if (R.has(id, indexedCombinations)) {
      remainings = R.omit(R.pathOr([], [id, 'concepts'], indexedCombinations), remainings);
    }
    else if (id !== 'OBS_ATTRIBUTES') {
      hasFoundIrrelevant = true;
    }
  }, layoutIds);
  return R.isEmpty(remainings) && !hasFoundIrrelevant;
};


const isScatterLayoutCompatible = (data, chartDimension) => {
  const { id, xId, yId } = chartDimension;

  const dimensions = R.pathOr([], ['structure', 'dimensions', 'observation'], data);

  const dimension = R.find(R.propEq('id', id), dimensions);
  if (R.isNil(dimension)) {
    return false;
  }
  return R.pipe(
    R.propOr([], 'values'),
    R.indexBy(R.prop('id')),
    R.pick([xId, yId]),
    R.values,
    R.length,
    R.equals(2)
  )(dimension);
};

const isSymbolLayoutCompatible = (data, chartDimension) => {
  const { id, serie } = chartDimension;
  const serieIds = R.pluck('id',serie);

  return R.pipe(
    R.pathOr([], ['structure', 'dimensions', 'observation']),
    R.indexBy(R.prop('id')),
    R.pathOr([], [id, 'values']),
    R.indexBy(R.prop('id')),
    R.pick(serieIds),
    R.values,
    R.length,
    R.equals(R.length(serieIds))
  )(data);
};

const isStackedLayoutCompatible = (data, chartDimension) => {
  const { id } = chartDimension;

  return R.pipe(
    R.pathOr([], ['structure', 'dimensions', 'observation']),
    R.filter(R.pipe(R.propOr([], 'values'), R.length, R.lt(1))),
    R.indexBy(R.prop('id')),
    R.has(id)
  )(data);
};

const isChartLayoutCompatible = (data, type, chartDimension) => {
  return R.cond([
    [R.always(R.equals(SCATTER, type)), isScatterLayoutCompatible],
    [R.always(R.equals(H_SYMBOL, type) || R.equals(V_SYMBOL, type)), isSymbolLayoutCompatible],
    [R.always(R.anyPass([R.equals(STACKED_BAR), R.equals(STACKED_ROW)])(type)), isStackedLayoutCompatible],
    [R.T, R.T]
  ])(data, chartDimension);
};

export const isSharedLayoutCompatible = (sdmxData, sharedData) => {
  const type = R.prop('type', sharedData);
  if (type === 'table') {
    const layoutIds = R.pathOr({}, ['config', 'table', 'layoutIds'], sharedData);
    const dataquery = R.path(['config', 'sdmxSource', 'dataquery'], sharedData);
    const annotations = R.pathOr({}, ['structure', 'annotations'], sdmxData)
    const locale = R.pathOr({}, ['config', 'table', 'locale'], sharedData);
    const rawDefaultCombinationsDefinition = R.pathOr({}, ['config', 'defaultCombinations'], sharedData);
    const { concepts='', names='' } = rawDefaultCombinationsDefinition;
    const defaultCombinationsDefinition = parseCombinationDefinition(locale)(concepts, names);
    const customCombinationsDefinitions = getCombinationDefinitions(annotations, locale);
    const combinationsDefinitions = R.isNil(customCombinationsDefinitions) || R.isEmpty(customCombinationsDefinitions)
      ? defaultCombinationsDefinition : customCombinationsDefinitions;
    return isTableLayoutCompatible(sdmxData, layoutIds, combinationsDefinitions, dataquery);
  }
  const chartDimension = R.pathOr({}, ['config', 'chart', 'chartDimension'], sharedData);
  return isChartLayoutCompatible(sdmxData, type, chartDimension);
};
