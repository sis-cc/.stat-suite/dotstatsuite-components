import { get, has, head, isUndefined, map, split, transform } from 'lodash';
import { getRefAreaDimension } from '@sis-cc/dotstatsuite-sdmxjs';
import { categoryDisplay, dimensionValueDisplay } from '../dimension-utils';

const parseByAreas = (observations, refAreaDimension) => {
  if (isUndefined(refAreaDimension)) {
    return null;
  }
  const refAreaDimIndex = get(refAreaDimension, 'index');
  const refAreaValues = refAreaDimension.values;

  return transform(
    observations,
    (acc, value, key) => {
      const splitedKey = split(key, ':');
      const areaValueIndex = get(splitedKey, `[${refAreaDimIndex}]`, null);
      const areaValue = get(refAreaValues, `[${areaValueIndex}]`, null);
      const areaId = get(areaValue, 'id', null);
      if (!has(acc, areaId)) {
        acc[areaId] = [];
      }
      acc[areaId].push({ splitedKey, value, areaValueIndex });
    },
    {}
  );
};

export default ({ observations, dimensions }, dimensionsWithValuesIndexedById, topoMap, display) => {
  const refAreaDimension = getRefAreaDimension({ dimensions });
  const observationsByAreas = parseByAreas(observations, refAreaDimension);
  const topology = get(topoMap, 'topology', {});
  const areaSelection = get(topoMap, 'areaSelection', null);
  const topoAreas = get(topology, `objects.${areaSelection}`, null);
  if (!observationsByAreas || !topoAreas) {
    return null;
  }
  const topoAreasWithData = map(
    topoAreas.geometries,
    (areaGeometry) => {
      const areaObservation = head(get(observationsByAreas, get(areaGeometry, 'properties.id', null), []));
      const areaValueIndex = get(areaObservation, 'areaValueIndex', null);
      const areaValue = get(refAreaDimension, `values[${areaValueIndex}]`, null);
      return {
        ...areaGeometry,
        properties: {
          ...get(areaGeometry, 'properties', {}),
          label: dimensionValueDisplay(display)(areaValue),
          value: head(get(areaObservation, 'value', [])),
          category: categoryDisplay(get(areaObservation, 'splitedKey', null), dimensions, dimensionsWithValuesIndexedById, refAreaDimension.id, display),
        }
      };
    }
  );
  return ({
    objects: {
      areas: {
        type: topoAreas.type,
        geometries: topoAreasWithData
      }
    },
    arcs: topology.arcs,
    bbox: topology.bbox,
    transform: topology.transform
  });
}
