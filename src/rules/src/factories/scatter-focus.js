import { map, each, omit, get, split, reduce, isEmpty, keys } from 'lodash';

export default function focus(series, focusSelections) {
  // scatter has only 1 serie
  const serie = get(series, '[0]', {});

  const _focusSelections = reduce(
    focusSelections,
    (memo, focusSelection, focusKey) => {
      if (!isEmpty(focusSelection)) memo[focusKey] = focusSelection;
      return memo;
    },
    {}
  );

  if (isEmpty(_focusSelections)) return series;

  const datapoints = get(serie, 'datapoints', []);

  let _datapoints = map(datapoints, (datapoint) => (
    omit(datapoint, map(keys(focusSelections), focusKey => `${focusKey}Index`))
  ));

  const __datapoints = map(_datapoints, (datapoint) => {
    each(_focusSelections, (focusSelection, focusKey) => {
      const _focusKey = `${focusKey}Index`;

      each(focusSelection, (focus, i) => {
        const _focus = get(focus, 'options', {});
        const dimensionValueId = get(datapoint, `dimensionValues.${_focus.dimensionId}.id`);
        if (dimensionValueId === _focus.dimensionValueId) datapoint[_focusKey] = i;
      });
    });

    return datapoint;
  });

  return [{ ...serie, datapoints: __datapoints }];
}
