import { map, each, findIndex, get, omit, isArray, keys } from 'lodash';

export default function focus(series, focusSelections) {
  return map(
    series,
    (serie, index) => {
      each(focusSelections, (focusSelection, focusKey) => {
        const _focusKey = `${focusKey}Index`;
        const res = findIndex(
          isArray(focusSelection) ? focusSelection : [focusSelection],
          focus => focus.value === serie.key
        );
        serie[`${_focusKey}`] = res;
      });
      return serie;
    }
  );
}