import * as R from 'ramda';
import { getRefAreaDimension, getTimePeriodDimension } from '@sis-cc/dotstatsuite-sdmxjs';

export default (dimensions, singularity) => {
  const { value, values } = dimensions;
  const mode = R.prop('mode', singularity) === 'percent' ? 'percent' : 'values';
  if (R.length(values) === 1) {
    return ({
      limit: R.propOr(Infinity, 'limit', singularity),
      mode,
      id: R.pathOr(null, [0, 'id'], value)
    });
  }
  let serieDimension = null;
  const id = R.prop('id', singularity);
  if (!R.isNil(id)) {
    serieDimension = R.find(R.propEq('id', id), values);
  }
  const areaDimension = getRefAreaDimension({ dimensions: values });
  const timeDimension = getTimePeriodDimension({ dimensions: values });

  serieDimension = serieDimension || areaDimension || timeDimension || R.head(values);

  return ({
    limit: R.propOr(Infinity, 'limit', singularity),
    mode,
    id: R.propOr(null, 'id', serieDimension)
  });
}