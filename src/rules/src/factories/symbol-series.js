import { fill, findIndex, get, head, isEmpty, isNil, map, reduce, split, size, sortBy } from 'lodash';
import { categoryDisplay, dimensionValueDisplay } from '../dimension-utils';
import { getFormatedObservation } from '../observation-formater';

const series = (observations, dimensions, dimensionsWithValuesIndexedById, symbolDimension, sortDirection, display, formaterAttrs) => {
  const symbolDimensionId = get(symbolDimension, 'id', null);
  if (symbolDimensionId === null) return [];
  const symbolDimensionIndex = findIndex(dimensions, d => d.id === symbolDimensionId);
  const datapoints = sortBy(
    reduce(
      observations,
      (memo, observationValue, observationKey) => {
        if (!(head(observationValue))) {
          // because yes it is possible to have null observations, why not ...
          return memo;
        }
        const oDimensionValuesIndexes = split(observationKey, ':');
        const oSymbolDimensionValueIndex = Number(oDimensionValuesIndexes[symbolDimensionIndex]);
        const oSymbolDimensionValueId = dimensions[symbolDimensionIndex].values[oSymbolDimensionValueIndex].id;

        const symbolIndex = findIndex(symbolDimension.serie, v => v.id === oSymbolDimensionValueId);
        if (symbolIndex === -1) {
          return memo;
        }
        const category = categoryDisplay(oDimensionValuesIndexes, dimensions, dimensionsWithValuesIndexedById, symbolDimensionId, display);
        const ids = categoryDisplay(oDimensionValuesIndexes, dimensions, dimensionsWithValuesIndexedById, symbolDimensionId, 'code');
        let dpIndex = findIndex(memo, dp => dp.key === ids);
        let dpValues = (dpIndex === -1) ? fill(Array(size(symbolDimension.serie)), null) : memo[dpIndex].values;
        let dpFormatedValues = (dpIndex === -1) ? fill(Array(size(symbolDimension.serie)), null) : memo[dpIndex].formatedValues;
        dpValues[oSymbolDimensionValueIndex] = head(observationValue);
        dpFormatedValues[oSymbolDimensionValueIndex] = getFormatedObservation(observationValue, formaterAttrs);
        let key = ids;
        if (isNil(ids) || isEmpty(ids)) {
          key = 'uniq-dp';
        }
        if (dpIndex !== -1) {
          memo[dpIndex].values = dpValues;
          memo[dpIndex].formatedValues = dpFormatedValues;
        }
        else {
          memo.push({ category, formatedValues: dpFormatedValues, values: dpValues, key });
        }
        return memo;
      },
      []
    ),
    d => head(d.values) * sortDirection
  );
  const symbolValues = map(symbolDimension.serie, v => dimensionValueDisplay(display)(v));
  return [{ datapoints , symbolValues }];
}

export default ({ observations, dimensions }, chartType, dimensionsWithValuesIndexedById, symbolDimension, display, formaterAttrs) => {
  switch(chartType) {
    case 'HorizontalSymbolChart':
      return series(observations, dimensions, dimensionsWithValuesIndexedById, symbolDimension, -1, display, formaterAttrs);
    case 'VerticalSymbolChart':
      return series(observations, dimensions, dimensionsWithValuesIndexedById, symbolDimension, 1, display, formaterAttrs);
    default:
      return [];
  }
}