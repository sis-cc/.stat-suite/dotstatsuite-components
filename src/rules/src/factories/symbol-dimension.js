import { find, get, head, isEmpty, isUndefined, isNil, reject, take } from 'lodash';
import { isRefAreaDimension, isTimePeriodDimension } from '@sis-cc/dotstatsuite-sdmxjs';

export default (dimensions, ids) => {
  if (isEmpty(dimensions)) return;

  let symbolDimension = undefined;
  const id = get(ids, 'id');
  if (!isNil(id)) symbolDimension = find(dimensions, { id });

  if (isUndefined(symbolDimension)) {
    const isAreaOrTimeDimension = d => isRefAreaDimension(d) || isTimePeriodDimension(d);
    const _dimensions1 = reject(dimensions, isAreaOrTimeDimension);
    const _dimensions2 = reject(dimensions, isRefAreaDimension);
    if (isEmpty(_dimensions1)) {
      symbolDimension = head(isEmpty(_dimensions2) ? dimensions : _dimensions2);
    }
    else {
      symbolDimension = head(_dimensions1);
    }
  }

  const symbolDimensionId = get(symbolDimension, 'id');

  return {
    id: symbolDimensionId,
    serie: take(symbolDimension.values, 5)
  };
}