import { findIndex, get, has, head, isEmpty, isNil, map, reduce, sortBy, split } from 'lodash';
import { getTimePeriodDimension } from '@sis-cc/dotstatsuite-sdmxjs';
import { categoryDisplay, dimensionValueDisplayAt } from '../dimension-utils';
import { getFormatedObservation } from '../observation-formater';

const getDimensionValues = (splitObservationKey, dimensions, dimensionsWithValuesIndexedById, rejectedId, display) => (
  reduce(
    splitObservationKey,
    (memo, dimensionValueIndex, dimensionIndex) => {
      const dimension = get(dimensions, `[${dimensionIndex}]`, {});
      if (has(dimensionsWithValuesIndexedById, dimension.id) && dimension.id !== rejectedId) {
        const dimensionValue = get(dimension, `values[${dimensionValueIndex}]`, {});
        memo[dimensionValue.id] = {
          ...dimensionValue,
          name: dimensionValueDisplayAt(dimension, dimensionValueIndex, display)
        }
      }
      return memo;
    },
    {}
  )
);

const series = (
  observations, dimensions,
  dimensionsWithValuesIndexedById,
  timePeriodDimensionIndex,
  display, formaterAttrs
) => {
  const rawSeries = reduce(
    observations,
    (memo, o, k) => {
      const oDimensionValuesIndexes = split(k, ':');

      const timePeriodDimension = get(dimensions, `[${timePeriodDimensionIndex}]`);
      const timePeriodDimensionValueIndex = oDimensionValuesIndexes[timePeriodDimensionIndex];
      const timePeriod = get(timePeriodDimension, `values[${timePeriodDimensionValueIndex}]`);

      const datapoint = {
        x: timePeriod.start,
        y: head(o),
        timeLabel: timePeriod.name,
        formatedValue: getFormatedObservation(o, formaterAttrs),
        dimensionValues: getDimensionValues(
          oDimensionValuesIndexes, dimensions, dimensionsWithValuesIndexedById, timePeriodDimension.id, display
        ),
        //index: Number(timePeriodDimensionValueIndex)
      };

      const category = categoryDisplay(
        oDimensionValuesIndexes, dimensions, dimensionsWithValuesIndexedById, timePeriodDimension.id, display
      );
      let key = categoryDisplay(
        oDimensionValuesIndexes, dimensions, dimensionsWithValuesIndexedById, timePeriodDimension.id, 'code'
      );
      if (isNil(key) || isEmpty(key)) {
        key = 'uniq-dp';
      }
      const serieIndex = findIndex(memo, serie => serie.key === key);

      if (serieIndex !== -1) memo[serieIndex].datapoints.push(datapoint);
      else memo.push({ datapoints: [datapoint], category, key });

      return memo;
    },
    []
  );
  return map(
    rawSeries,
    serie => ({
      ...serie,
      datapoints: sortBy(
        serie.datapoints,
        datapoint => datapoint.x
      )
    })
  );
};

export default ({ observations, dimensions }, dimensionsWithValuesIndexedById, display, formaterAttrs) => {
  const timePeriodDimension = getTimePeriodDimension({ dimensions });
  const timePeriodDimensionIndex = timePeriodDimension ? timePeriodDimension.index : -1;
  const timePeriodDimensionHasManyPeriods = !has(dimensionsWithValuesIndexedById, get(timePeriodDimension, 'id'));
  if ((timePeriodDimensionIndex === -1) || timePeriodDimensionHasManyPeriods) return [];
  
  return series(
    observations, dimensions,
    dimensionsWithValuesIndexedById,
    timePeriodDimensionIndex, display, formaterAttrs
  );
};
