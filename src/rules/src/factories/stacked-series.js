import { isTimePeriodDimension } from '@sis-cc/dotstatsuite-sdmxjs';
import { dimensionValueDisplay } from '../dimension-utils';
import { getObservationFormater } from '../observation-formater';
import * as R from 'ramda';

const flatDatapoints = (
  observations,
  dimensions,
  dimensionsWithValuesIndexedById,
  serieDimensionId,
  display,
  formaterAttrs,
  [stackKey, categoryKey]
) => {
  const datapoints = R.mapObjIndexed(
    (observationValue, observationKey) => {
      let category = null;
      const value = Number(R.head(observationValue));
      const formater = getObservationFormater(observationValue, formaterAttrs);
      const splittedKey = R.split(':', observationKey);
      let layerKey = [];
      const layerDimensions = R.addIndex(R.reduce)(
        (memo, dimensionValueIndex, dimensionIndex) => {
          const dimension = R.nth(dimensionIndex, dimensions);
          const _dimensionValue = R.pathOr({}, ['values', Number(dimensionValueIndex)], dimension);
          const label = dimensionValueDisplay(display)(_dimensionValue);
          const dimensionValue = {
            ..._dimensionValue,
            index: Number(dimensionValueIndex),
            label
          };
          if (dimension.id === serieDimensionId) {
            category = dimensionValue;
          }
          else if (R.has(dimension.id, dimensionsWithValuesIndexedById)) {
            memo.push(dimensionValue);
            layerKey.push(dimensionValueIndex);
          }
          return memo;
        },
        [],
        splittedKey
      );
      return ({
        formater,
        [categoryKey]: category,
        [stackKey]: value,
        layerDimensions,
        layerKey: R.join(':', layerKey)
      });
    },
    observations
  );

  return R.values(datapoints);
};

const serie = (
  observations,
  dimensions,
  dimensionsWithValuesIndexedById,
  singularity,
  display,
  formaterAttrs,
  [stackKey, categoryKey]
) => {
  const serieDimensionId = R.propOr(null, 'id', singularity);
  const serieDimension = R.find(R.propEq('id', serieDimensionId), dimensions);
  if (R.isNil(serieDimension)) {
    return { datapoints: [], layerSeries: [] };
  }
  const _flatDatapoints = flatDatapoints(observations, dimensions, dimensionsWithValuesIndexedById, serieDimensionId, display, formaterAttrs, [stackKey, categoryKey]);

  const layeredDatapoints = R.groupBy(
    R.prop('layerKey'),
    _flatDatapoints
  ); // { [layeredKey]: [dps] }

  const layerFullSeries = R.map(
    datapoints => ({
      layerDimensions: R.prop('layerDimensions', R.head(datapoints)),
      layerKey: R.prop('layerKey', R.head(datapoints))
    }),
    R.values(layeredDatapoints)
  ); // [{ layerDimensions, layerKey }]

  const layerLimit = R.propOr(Infinity, 'limit', singularity);
  const layerSeries = R.take(layerLimit, R.sortBy(R.prop('layerKey'), layerFullSeries));

  const layeredDatapointsKeyedByCategory = R.mapObjIndexed(
    R.indexBy(R.path([categoryKey, 'id'])),
    layeredDatapoints
  ); // { [layeredKey]: { [x.id]: dp } }

  const datapoints = R.addIndex(R.map)(
    (serieValue, index) => {
      const serieValueId = serieValue.id;
      const category = dimensionValueDisplay(display)(serieValue);
      const values = R.map(
        ({ layerKey }) => R.pathOr(null, [layerKey, serieValueId, stackKey], layeredDatapointsKeyedByCategory),
        layerSeries,
      );
      const formaters = R.map(
        ({ layerKey }) => R.pathOr(null, [layerKey, serieValueId, 'formater'], layeredDatapointsKeyedByCategory),
        layerSeries,
      );
      return ({ category, formaters, index, [categoryKey]: category, [stackKey]: values, key: serieValueId });
    },
    serieDimension.values
  );

  const _datapoints = R.when(
    R.pipe(R.length, R.equals(1)),
    R.pipe(
      R.set(R.lensPath([0, 'category']), ''),
      R.set(R.lensPath([0, categoryKey]), ''),
    ),
    datapoints
  );

  return ({
    datapoints: _datapoints,
    layerSeries: R.map(
      layer => ({
        id: R.pipe(R.prop('layerDimensions'), R.pluck('id'), R.join(' - '))(layer),
        label: R.pipe(R.prop('layerDimensions'), R.pluck('label'), R.join(' - '))(layer),
      }),
      layerSeries
    )
  });
};

const percentSerie = (stackKey) => R.pipe(
  R.over(
    R.lensProp('datapoints'),
    R.map(dp => {
      const total = R.pipe(R.map(v => Math.abs(v)), R.sum)(dp[stackKey]);
      if (!total) {
        return dp;
      }
      return ({
        ...dp,
        [stackKey]: R.map(v => (Math.abs(v) / total) * 100, dp[stackKey]),
        formaters: null,
      });
    })
  ),
  R.of
);

const serieSortedByTotal = (stackKey) => R.pipe(
  R.over(
    R.lensProp('datapoints'),
    R.sortBy(R.pipe(R.prop(stackKey), R.map(v => v > 0 ? R.negate(v) : 0), R.sum))
  ),
  R.of
);

const serieSortedByDataOrder = R.pipe(
  R.over(
    R.lensProp('datapoints'),
    R.sortBy(R.prop('index'))
  ),
  R.of
);

export default (
  { observations, dimensions },
  dimensionsWithValuesIndexedById,
  singularity,
  display,
  formaterAttrs,
  [stackKey, categoryKey]
) => {
  const mode = R.propOr('values', 'mode', singularity);
  const serieDimensionId = R.propOr(null, 'id', singularity);
  const _serie = serie(observations, dimensions, dimensionsWithValuesIndexedById, singularity, display, formaterAttrs, [stackKey, categoryKey]);
  if (mode === 'percent') {
    return percentSerie(stackKey)(_serie);
  }
  else if (isTimePeriodDimension({ id: serieDimensionId })) {
    return serieSortedByDataOrder(_serie);
  }
  return serieSortedByTotal(stackKey)(_serie);
};
