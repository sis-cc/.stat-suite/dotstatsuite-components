import { get, map, reduce, sortBy, head, isEmpty, isFunction, isNil, split } from 'lodash';
import { categoryDisplay } from '../dimension-utils';
import { getFormatedObservation } from '../observation-formater';

export const getDatapoints = (
  keys, observations, dimensions, dimensionsWithValuesIndexedById, sortDirection, display, formaterAttrs
) => {
  const [categoryKey, valueKey] = keys;
  const sortFunc =  dp => dp[valueKey] * sortDirection;

  return sortBy(
    map(
      observations,
      (observationValue, observationKey) => {
        const value = head(observationValue);
        const category = categoryDisplay(split(observationKey, ':'), dimensions, dimensionsWithValuesIndexedById, null, display);
        const categoryIds = categoryDisplay(split(observationKey, ':'), dimensions, dimensionsWithValuesIndexedById, null, 'code');
        const formatedValue = getFormatedObservation(observationValue, formaterAttrs);

        let key = categoryIds;
        if (isNil(key) || isEmpty(key)) {
          key = 'uniq-dp';
        }

        return { value, category, [valueKey]: value, [categoryKey]: category, key, formatedValue };
      }
    ),
    sortFunc
  );
};

export const series = (
  keys, observations, dimensions, dimensionsWithValuesIndexedById, sortDirection, display, formaterAttrs
) => {
  return [{
    datapoints: getDatapoints(
      keys, observations, dimensions, dimensionsWithValuesIndexedById, sortDirection, display, formaterAttrs
    )
  }]
};

export default (chartType, { observations, dimensions }, dimensionsWithValuesIndexedById, display, formaterAttrs) => {
  switch(chartType) {
    case 'BarChart':
      return series(['x', 'y'], observations, dimensions, dimensionsWithValuesIndexedById, 1, display, formaterAttrs);
    case 'RowChart':
      return series(['y', 'x'], observations, dimensions, dimensionsWithValuesIndexedById, -1, display, formaterAttrs);
    default:
      return [];
  }
};
