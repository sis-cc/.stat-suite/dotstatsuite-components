import { reject, head, isEmpty, get, isNil, isUndefined, find } from 'lodash';
import { isRefAreaDimension } from '@sis-cc/dotstatsuite-sdmxjs';

export default (dimensions, ids) => {
  if (isEmpty(dimensions)) return;

  let chartDimension;
  const id = get(ids, 'id');
  if (!isNil(id)) chartDimension = find(dimensions, { id });

  if (isUndefined(chartDimension)) {
    const _dimensions = reject(dimensions, isRefAreaDimension);
    chartDimension = head(isEmpty(_dimensions) ? dimensions : _dimensions);
  }

  const chartDimensionId = get(chartDimension, 'id');

  const values = get(chartDimension, 'values', []);

  const xValue = find(values, value => value.id === get(ids, 'xId', null));
  const yValue = find(values, value => (value.id === get(ids, 'yId', null) && (value.id !== xId)));
  //if same xId & yId provided, xId is taken in account
  const xId = isUndefined(xValue) ? head(isUndefined(yValue) ? values : reject(values, yValue)).id : xValue.id;
  const yId = isUndefined(yValue) ? find(values, value => value.id !== xId).id : yValue.id;

  return {
    id: chartDimensionId,
    xId,
    yId,
  };
};
