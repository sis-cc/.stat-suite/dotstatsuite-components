import { map, each, get, omit, isArray, keys, head } from 'lodash';

export default function focus(series, focusSelections) {
  const serie = head(series);
  const datapoints = get(serie, 'datapoints', []);

  const _datapoints = map(datapoints, (datapoint) => {
    let _datapoint = omit(datapoint, map(keys(focusSelections), focusKey => `${focusKey}Index`));
    
    each(focusSelections, (focusSelection, focusKey) => {
      const _focusKey = `${focusKey}Index`;

      each(isArray(focusSelection) ? focusSelection : [focusSelection], (focus, index) => {
        if (get(focus, 'value') === _datapoint.key) _datapoint[_focusKey] = index;
      });
    });

    return _datapoint;
  });

  return [{ ...serie, datapoints: _datapoints }];
}
