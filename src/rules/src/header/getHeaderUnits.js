import * as R from 'ramda';
import { dimensionValueDisplay, parseDisplay } from '../';

export const getHeaderUnits = ({ units, display }) => {
  const _display = parseDisplay(display);
  return R.converge(
    (header, label) => R.isEmpty(label) ? null : { header, label },
    [
      (units) => `${dimensionValueDisplay(_display)(units)}:`,
      R.pipe(
        R.propOr([], 'values'),
        R.map(R.pipe(R.propOr({}, 'value'), dimensionValueDisplay(_display))),
        R.join(', ')
      )
    ]
  )(units);
};
