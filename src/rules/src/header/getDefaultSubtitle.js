import * as R from 'ramda';
import { dimensionValueDisplay, getFlags, parseDisplay } from '../';
import { DEFAULT_REJECTED_SUBTITLE_IDS } from '../constants';

export const getDefaultSubtitle = ({ customAttributes, dimensions, display, units }) =>  {
  const _display = parseDisplay(display);
  const formatter = dimensionValueDisplay(_display);
  const _getFlags =  getFlags(customAttributes, formatter, {});
  const unitsValuesIds = R.pipe(R.propOr([], 'values'), R.pluck('id'))(units);
  
  return R.pipe(
    R.propOr([], 'one'),
    R.values,
    R.sortBy(R.prop('__index')),
    R.reduce(
      (acc, dim) => {
        if (!R.propOr(true, 'display', dim)) {
          return acc;
        }
        if (R.includes(dim.id, unitsValuesIds)) {
          return acc;
        }
        const value = R.path(['values', 0], dim);

        if (R.includes(value.id, DEFAULT_REJECTED_SUBTITLE_IDS) || !R.propOr(true, 'display', value)) {
          return acc;
        }

        const flags = _getFlags({ attributes: R.propOr({}, 'attributes', value), notes: R.propOr([], 'notes', value) });

        return R.append({
          header: `${formatter(dim)}:`,
          label: formatter(value),
          flags
        }, acc);
      },
      []
    )
  )(dimensions);
};
