import * as R from 'ramda';
import { extractSdmxArtefacts, prepareData } from '../';

const initialState = {
  dataflow: null,
  dataflowAttributes: null,
  dimensions: null,
  units: null,
  id: null,
  sourceLabel: null,
  subtitle: null,
  title: null,
  withCopyright: true,
  withLogo: true
};

export const getInformationsStateFromNewProps = (props, state) => {
  if (R.isNil(props.data)) {
    return initialState
  }
  const id = R.pipe(
    R.prop('data'),
    extractSdmxArtefacts,
    R.prop('id')
  )(props);
  if (id === state.id && props.display === state.display) {
    return {};
  }
  const {
    dataflowAttributes,
    dataflowName,
    dimensions,
    units
  } = prepareData({ data: props.data }, props.customAttributes, props.units);
  return {
    ...initialState,
    dataflow: {
      id: props.dataflowId,
      name: dataflowName
    },
    dataflowAttributes,
    dimensions,
    display: props.display,
    id,
    units
  };
};
