import * as R from 'ramda';
import { TIMELINE } from '../constants';
import { getTimePeriodDimension } from '@sis-cc/dotstatsuite-sdmxjs';

export const getHasNoTimePeriodError = ({ data, type }) => {
  if (!R.equals(TIMELINE, type)) {
    return false;
  }
  const dimensions = R.pathOr([], ['structure', 'dimensions', 'observation'], data);
  const timeDimension = getTimePeriodDimension({ dimensions });
  if (R.isNil(timeDimension)) {
    return true;
  }
  return R.pipe(
    R.propOr([], 'values'),
    R.length,
    R.gte(1)
  )(timeDimension);
};
