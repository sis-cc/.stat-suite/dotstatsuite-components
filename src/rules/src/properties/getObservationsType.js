import * as R from 'ramda';
import {
  OBS_TYPE_NUMBER,
  OBS_TYPE_NUMERICAL_STRING,
  OBS_TYPE_STRING
} from '../constants';

export const getObservationsType = (data) => {
  if (R.isNil(data)) {
    return undefined;
  }
  const firstObs = R.pipe(
    R.pathOr({}, ['dataSets', 0, 'observations']),
    R.values,
    R.find(obs => R.not(R.either(R.isNil, R.isEmpty)(R.head(obs))))
  )(data);
  if (R.isNil(firstObs)) {
    return undefined;
  }
  const firstObsValue = R.head(firstObs);
  if (R.is(Number, firstObsValue)) {
    return OBS_TYPE_NUMBER;
  }
  return R.ifElse(
    R.equals(NaN),
    R.always(OBS_TYPE_STRING),
    R.always(OBS_TYPE_NUMERICAL_STRING)
  )(Number(firstObsValue));
};
