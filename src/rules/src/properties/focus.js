import * as R from 'ramda';
import { extractSdmxArtefacts, focus } from '../';
import { BAR, ROW, SCATTER, TIMELINE, TYPES } from '../constants';
import { toSingularity, hasFocus } from './utils';

const sampleFocusTypes = { [BAR]: BAR, [ROW]: ROW, [TIMELINE]: TIMELINE };

const focusComparator = (op1, op2) => op1.value === op2.value;

export const focusOptions = (props, state, otherFocus, singularity) => {
  if (!hasFocus(props.type)) {
    return [];
  }
  const options = focus(
    props.data,
    props.type,
    singularity,
    props.display
  );
  return R.differenceWith(
    focusComparator,
    options,
    R.prop(otherFocus, state)
  );
};

export const onChangeFocusSelection = (_value) => {
  const value = R.isNil(_value) ? [] : _value;
  return (R.is(Array, value) ? value : [value]);
};

const toKey = () => R.prop('value');

const filterFocus = (focus, keyedOptions, type) => R.reduce(
  (acc, entry) => {
    const entryKey = toKey(type)(entry);
    if (R.has(entryKey, keyedOptions)) {
      return R.append(R.prop(entryKey, keyedOptions), acc);
    }
    return acc;
  },
  [],
  focus
)

export const focusStateFromNewProps = ({ data, display, type }, singularity, state) => {
  const { id } = extractSdmxArtefacts(data);
  const highlightState = R.propOr([], 'highlight', state);
  const baselineState = R.propOr([], 'baseline', state);
  const options = focus(data, type, singularity, display);
  const keyedOptions = R.indexBy(toKey(type), options);
  return ({
    baseline: filterFocus(baselineState, keyedOptions, R.prop('type', state)),
    highlight: filterFocus(highlightState, keyedOptions, R.prop('type', state)),
  });
};

/*
  This props/state parser which will be exposed in highlight/baseline properties,
  only deals for Bar and Row Chart (no chart dimension singularity involved).
  For other chart types, their own chart dimension singularity parser will handle focus management.
*/

export const sampleFocusStateFromNewProps = (props, state) => {
  if (!R.has(props.type, sampleFocusTypes) || R.isNil(props.data)) {
    return ({}); //no changes 
  }
  return focusStateFromNewProps(props, null, state);
};