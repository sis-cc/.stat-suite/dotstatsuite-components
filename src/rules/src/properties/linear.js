import * as R from 'ramda';
import {
  BAR,
  H_SYMBOL,
  ROW,
  SCATTER,
  STACKED_BAR,
  STACKED_ROW,
  TIMELINE,
  V_SYMBOL,
} from '../constants';
import { isNumber, isPositiveNumber } from './utils';
import { isStacked } from './stacked';

const hasXLinear = (type) => R.includes(type, [H_SYMBOL, ROW, SCATTER, STACKED_ROW]);

const hasYLinear = (type) => R.includes(type, [BAR, SCATTER, STACKED_BAR, TIMELINE, V_SYMBOL]);

const hasXPivot = (type) => R.equals(ROW, type);
const hasYPivot = (type) => R.equals(BAR, type);

const xInitialState = {
  computedMaxX: undefined,
  computedMinX: undefined,
  computedStepX: undefined,
  computedPivotX: undefined,
  maxX: undefined,
  minX: undefined,
  pivotX: undefined,
  stepX: undefined,
};

const yInitialState = {
  computedMaxY: undefined,
  computedMinY: undefined,
  computedStepY: undefined,
  computedPivotY: undefined,
  maxY: undefined,
  minY: undefined,
  pivotY: undefined,
  stepY: undefined,
};

export const initialState = { ...xInitialState, ...yInitialState };

export const linearStateFromNewProps = (type) => {
  const xLinearState = hasXLinear(type) ? {} : xInitialState;
  const YLinearState = hasYLinear(type) ? {} : yInitialState;
  return ({
    ...xLinearState,
    ...R.when(
      R.always(isStacked(type)),
      R.assoc('pivotY', 0)
    )(YLinearState),
  });
};

export const toProperties = (props, state, onChange) => ({
  maxX: {
    id: 'maxX',
    isActive: hasXLinear(props.type),
    onChange: (value) => onChange({
      maxX: isNumber(value) ? value : undefined
    }),
    value: R.isNil(state.maxX) ? String(state.computedMaxX) : state.maxX
  },
  maxY: {
    id: 'maxY',
    isActive: hasYLinear(props.type),
    onChange: (value) => onChange({
      maxY: isNumber(value) ? value : undefined
    }),
    value: R.isNil(state.maxY) ? String(state.computedMaxY) : state.maxY
  },
  minX: {
    id: 'minX',
    isActive: hasXLinear(props.type),
    onChange: (value) => onChange({
      minX: isNumber(value) ? value : undefined
    }),
    value: R.isNil(state.minX) ? String(state.computedMinX) : state.minX
  },
  minY: {
    id: 'minY',
    isActive: hasYLinear(props.type),
    onChange: (value) => onChange({
      minY: isNumber(value) ? value : undefined
    }),
    value: R.isNil(state.minY) ? String(state.computedMinY) : state.minY
  },
  pivotX: {
    id: 'pivotX',
    isActive: hasXPivot(props.type),
    onChange: (value) => onChange({
      pivotX: isNumber(value) ? value : undefined
    }),
    value: R.when(
      v => R.isNil(v) && (R.propEq('type', ROW)(props) || R.propEq('type', STACKED_ROW)(props)),
      R.always(0)
    )(R.isNil(state.pivotX) ? String(state.computedPivotX) : state.pivotX)
  },
  pivotY: {
    id: 'pivotY',
    isActive: hasYPivot(props.type),
    onChange: (value) => onChange({
      pivotY: isNumber(value) ? value : undefined
    }),
    value: R.when(
      v => R.isNil(v) && (R.propEq('type', BAR)(props) || R.propEq('type', STACKED_BAR)(props)),
      R.always(0)
    )(R.isNil(state.pivotY) ? String(state.computedPivotY) : state.pivotY)
  },
  stepX: {
    id: 'stepX',
    isActive: hasXLinear(props.type),
    onChange: (value) => onChange({
      stepX: isPositiveNumber(value) ? value : undefined
    }),
    value: R.isNil(state.stepX) ? String(state.computedStepX) : state.stepX
  },
  stepY: {
    id: 'stepY',
    isActive: hasYLinear(props.type),
    onChange: (value) => onChange({
      stepY: isPositiveNumber(value) ? value : undefined
    }),
    value: R.isNil(state.stepY) ? String(state.computedStepY) : state.stepY
  },
});
