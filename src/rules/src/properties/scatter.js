import * as R from 'ramda';
import { isRefAreaDimension } from '@sis-cc/dotstatsuite-sdmxjs';
import { SCATTER } from '../constants';
import {
  artefactToOption,
  getDimensionsWithValues,
  getPropertyDimension,
  toState,
} from './utils';
import { focusStateFromNewProps } from './focus';

const getScatterDimension = getPropertyDimension('scatterDimension');

const toScatterState = (dim, xVal, yVal) => ({
  scatterDimension: toState(dim),
  scatterX: toState(xVal),
  scatterY: toState(yVal),
});

export const toScatterSingularity = (state) => ({
  id: R.prop('scatterDimension', state),
  xId: R.prop('scatterX', state),
  yId: R.prop('scatterY', state),
}); 

export const isScatter = (type) => type === SCATTER;

export const scatterValueOptions = ({ data, type, display }, state) => {
  if (!isScatter(type)) {
    return [];
  }
  const scatterDimension = getScatterDimension(data, state);
  if (R.isNil(scatterDimension)) {
    return [];
  }
  return R.map(
    artefactToOption(display),
    R.prop('values', scatterDimension)
  );
};

export const onChangeScatterDim = (props, state) => (value) => {
  const { data } = props;
  const dimension = getScatterDimension(data, { scatterDimension: value });
  if (R.isNil(dimension)) {
    return ({});
  }

  const [xVal, yVal] = R.take(2, R.prop('values', dimension));

  const newScatterState = toScatterState(dimension, xVal, yVal);

  return ({
    ...newScatterState,
    ...focusStateFromNewProps(
      props,
      toScatterSingularity(newScatterState),
      state
    )
  });
};

export const scatterStateValueToOption = (property, props, state) => {
  const dimension = getScatterDimension(props.data, state);
  if (R.isNil(dimension) || !isScatter(props.type)) {
    return undefined;
  }
  const value = R.find(
    R.propEq('id', R.prop(property, state)),
    R.prop('values', dimension)
  );
  return artefactToOption(props.display)(value);
};

export const onChangeScatterVal = ({ data, type }, state, valKey, otherValKey) => (value) => {
  const options = scatterValueOptions({ data, type }, state);
  const [matching, otherOptions] = R.partition(R.propEq('value', value), options);
  if (R.isEmpty(matching)) {
    return ({});
  }
  const stateOtherValue = R.prop(otherValKey, state);
  return ({
    [valKey]: value,
    [otherValKey]: stateOtherValue === value
      ? R.prop('value', R.head(otherOptions))
      : stateOtherValue 
  }); 
};

//----------------------------------------------------------------------------------------------------

export const scatterStateFromNewProps = (props, state) => {
  const { data, type } = props;
  const undefinedState = toScatterState(undefined, undefined, undefined);
  if (!isScatter(type)) {
    return undefinedState;
  }
  if (R.isNil(data)) {
    return {};
  }
  const dimensions = getDimensionsWithValues(data);
  if (R.isEmpty(dimensions)) {
    return { ...undefinedState, highlight: [], baseline: [] };
  }

  let scatterDimension = getScatterDimension(props.data, state);
  if (R.isNil(scatterDimension)) {
    const refinedDimensions = R.reject(isRefAreaDimension, dimensions);
    scatterDimension = R.head(R.isEmpty(refinedDimensions) ? dimensions : refinedDimensions);
  }

  const values = R.prop('values', scatterDimension);

  const stateScatterX = R.prop('scatterX', state);
  let stateScatterY = R.prop('scatterY', state);

  if (stateScatterX === stateScatterY) {
    stateScatterY = undefined;
  }

  let _scatterX = undefined;
  let _scatterY = undefined;

  R.forEach(
    (value) => {
      if (R.prop('id', value) === stateScatterX) {
        _scatterX = value;
      }
      else if (R.prop('id', value) === stateScatterY) {
        _scatterY = value;
      }
    },
    values
  );

  //dirty as f
  const scatterX = R.isNil(_scatterX)
    ? R.head(R.reject(R.propEq('id', R.prop('id', _scatterY)), values))
    : _scatterX;
  const scatterY = R.isNil(_scatterY)
    ? R.head(R.reject(R.propEq('id', R.prop('id', scatterX)), values))
    : _scatterY;

  const scatterState = toScatterState(
    scatterDimension,
    scatterX,
    scatterY,
  );

  return ({
    ...scatterState,
    ...focusStateFromNewProps(
      props,
      toScatterSingularity(scatterState),
      state
    )
  });
};
