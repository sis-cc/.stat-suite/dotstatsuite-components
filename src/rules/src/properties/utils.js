import {
  equals,
  find,
  groupBy,
  has,
  head,
  is,
  isNil,
  length,
  map,
  not,
  omit,
  pipe,
  prop,
  propEq,
} from 'ramda';
import { isRefAreaDimension, isTimePeriodDimension } from '@sis-cc/dotstatsuite-sdmxjs';
import { extractSdmxArtefacts, parseDisplay, splitDimensions } from '../';
import { CHORO, STACKED_BAR, STACKED_ROW, TYPES } from '../constants';
import { dimensionValueDisplay } from '../dimension-utils';

export const isNumber = (input) => {
  const value = Number(input);
  return not(isNil(value) || isNaN(value));
};

export const isPositiveNumber = (input) => {
  return (isNumber(input) && Number(input) >= 0);
};

export const isNaturalInt = value => isNil(value) || /^\d+$/.test(String(value));

export const isInt = value => isNil(value) || /^-?\d+$/.test(String(value));

export const isChart = (type) => has(type, TYPES);

export const hasFocus = (type) => has(type, omit([CHORO], TYPES));

export const getDimensionsWithValues = (data) => pipe(
  splitDimensions,
  prop('values'),
)(data);

export const toState = (artefact) => prop('id', artefact);

export const getPropertyDimension = (property) => (data, state) => {
  const statePropertyDimensionId = prop(property, state);
  const { dimensions } = extractSdmxArtefacts(data);
  return find(
    propEq('id', statePropertyDimensionId),
    dimensions
  );
};

export const getGroupedDimensions = pipe(
  getDimensionsWithValues,
  groupBy(
    dimension => {
      if (isRefAreaDimension(dimension)) {
        return 'area';
      }
      if (isTimePeriodDimension(dimension)) {
        return 'time';
      }
      return 'other';
    }
  )
);

export const optionParser = option => pipe(
  option => is(Array, option) ? head(option) : option,
  option => is(Object, option) ? prop('value', option) : option,
  option => is(String, option) ? option : undefined
)(option);

export const optionsParser = selection => pipe(
  selection => is(Array, selection) ? selection : [selection],
  selection => map(optionParser, selection)
)(selection);

export const artefactToOption = (display) => (artefact) => isNil(artefact)
  ? undefined
  : {
    label: dimensionValueDisplay(parseDisplay(display))(artefact),
    value: prop('id', artefact)
  };

export const dimensionWithValuesToOptions = ({ data, display }) => pipe(
  getDimensionsWithValues,
  map(artefactToOption(display))
)(data);

export const chartDimensionToOption = (property) => ({ data, display }, state) => pipe(
  getPropertyDimension(property),
  artefactToOption(display)
)(data, state);

export const chartDimensionOptions = ({ data, type, display }, typeValidator) => {
  if (!typeValidator(type)) {
    return [];
  }
  const dimensions = prop('values', splitDimensions(data));
  if ((equals(type, STACKED_BAR) || equals(type, STACKED_ROW)) && length(dimensions) === 1) {
    return [];
  }
  return map(
    artefactToOption(display),
    dimensions
  );
};
