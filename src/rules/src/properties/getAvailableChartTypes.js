import * as R from 'ramda';
import {
  CHORO,
  OBS_TYPE_NUMBER,
  OBS_TYPE_NUMERICAL_STRING,
  TIMELINE,
  TYPES
} from '../constants';
import { getRefAreaDimension, getTimePeriodDimension } from '@sis-cc/dotstatsuite-sdmxjs';
import { getObservationsType } from './getObservationsType';

export const getAvailableChartTypes = (data) => {
  const observationsType = getObservationsType(data);
  if (!R.includes(observationsType, [OBS_TYPE_NUMBER, OBS_TYPE_NUMERICAL_STRING])) {
    return {};
  }
  const dimensions = R.pathOr([], ['structure', 'dimensions', 'observation'], data);
  const timeDimension = getTimePeriodDimension({ dimensions });
  const refAreaDimension = getRefAreaDimension({ dimensions });
  const hasNoTime = R.anyPass([
    R.isNil,
    R.pipe(R.propOr([], 'values'), R.length, R.gte(1))
  ])(timeDimension);
  return R.pipe(
    R.when(
      R.always(hasNoTime),
      R.omit([TIMELINE])
    ),
    R.when(
      R.always(R.isNil(refAreaDimension)),
      R.omit([CHORO])
    )
  )(TYPES);
};
