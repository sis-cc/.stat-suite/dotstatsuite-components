import * as R from 'ramda';
import { getDataFrequency } from '@sis-cc/dotstatsuite-sdmxjs';
import { series } from '../';
import {
  H_SYMBOL,
  SCATTER,
  STACKED_BAR,
  STACKED_ROW,
  TIMELINE,
  V_SYMBOL,
} from '../constants';
import {
  onChangeFocusSelection,
  focusOptions,
  sampleFocusStateFromNewProps,
} from './focus';
import {
  isScatter,
  onChangeScatterDim,
  onChangeScatterVal,
  scatterStateFromNewProps,
  scatterStateValueToOption,
  scatterValueOptions,
  toScatterSingularity,
} from './scatter';
import {
  isSymbol,
  onChangeSymbolDim,
  symbolStateFromNewProps,
  toSymbolSingularity,
} from './symbol';
import {
  isStacked,
  isStackedDimActive,
  onChangeStackedDimension,
  onChangeStackedMode,
  stackedModeOptions,
  stackedStateFromNewProps,
  toStackedSingularity
} from './stacked';
import {
  linearStateFromNewProps,
  initialState as linearInitialState,
  toProperties as toLinearProperties
} from './linear';
import {
  chartDimensionOptions,
  chartDimensionToOption,
  hasFocus,
  isChart,
  isNaturalInt,
  isNumber,
  optionParser,
} from './utils';
import { getTitleLabel, getSubtitle, getSourceLabel } from './getHeaderProps';
import { stringifySubtitle } from './getStringifiedSubtitle';
import { getHasNoTimePeriodError } from './errors';
/*
 - width/height -> initial undefined, if undefined or blank, replaced by options.base
*/

export const toSingularity = ({ data, type }, state) => {
  switch(type) {
    case SCATTER:
      return toScatterSingularity(state);
    case H_SYMBOL: case V_SYMBOL:
      return toSymbolSingularity(data, state);
    case STACKED_BAR: case STACKED_ROW:
      return toStackedSingularity(state);
    default:
      return null;
  }
};

export const toChartData = (props, state) => {
  const dimensions = R.pathOr([], ['data', 'structure', 'dimensions', 'observation'], props);
  const attributes = R.pathOr([], ['data', 'structure', 'attributes', 'observation'], props);
  const frequency = props.type === TIMELINE ? getDataFrequency({ dimensions, attributes }) : null;
  return ({
    frequency,
    series: series(
      props.data,
      props.type,
      R.pick(['highlight', 'baseline'], state),
      toSingularity(props, state),
      props.map,
      props.display,
      props.formaterIds
    ),
    share: {
      focused: R.pick(['highlight', 'baseline'], state),
      chartDimension: toSingularity(props, state),
    }
  });
};

export const getErrors = (props, state) => ({
  noTime: getHasNoTimePeriodError(props),
});

const toFloat = (value) => {
  const float = parseFloat(value);
  return isNaN(float) ? undefined : float;
}

const toOption = (stateValue, optionValue) => R.isNil(stateValue)
  ? optionValue : toFloat(stateValue);

const getFreqStepOption = (props, state) => {
  if (props.type !== TIMELINE) {
    return null;
  }
  const inputState = toFloat(state.freqStep);
  return R.isNil(inputState) ? 1 : inputState;
};

export const toChartOptions = (props, state) => {
  const options = R.propOr({}, 'options', props);
  const base = R.propOr({}, 'base', options);
  const axis = R.propOr({}, 'axis', options);
  const axeX = R.propOr({}, 'x', axis);
  const axeY = R.propOr({}, 'y', axis);
  const linearX = R.propOr({}, 'linear', axeX);
  const linearY = R.propOr({}, 'linear', axeY);
  const tickX = R.propOr({}, 'tick', axeX);
  const pivotX = R.propOr({}, 'pivot', linearX);
  const pivotY = R.propOr({}, 'pivot', linearY);
  const map = R.propOr({}, 'map', props);
  return ({
    ...options,
    axis: {
      ...axis,
      x: {
        ...axeX,
        tick: {
          ...tickX,
          step: getFreqStepOption(props, state)
        },
        linear: {
          ...linearX,
          max: toOption(state.maxX, linearX.max),
          min: toOption(state.minX, linearX.min),
          pivot: {
            value: toOption(state.pivotX, pivotX.value)
          },
          step: toOption(
            props.type === TIMELINE ? 1 :  state.stepX,
            linearX.step
          ),
        }
      },
      y: {
        ...axeY,
        linear: {
          ...linearY,
          max: toOption(state.maxY, linearY.max),
          min: toOption(state.minY, linearY.min),
          pivot: {
            value: toOption(state.pivotY, pivotY.value)
          },
          step: toOption(state.stepY, linearY.step),
        }
      }
    },
    map: R.pick(['projection', 'scale'], map),
    base: {
      ...base,
      height: toOption(state.fixedHeight, base.height),
      width: toOption(state.fixedWidth, base.width),
    },
  });
};

export const initialState = {
  highlight: [],
  baseline: [],
  fixedWidth: undefined,
  fixedHeight: undefined,
  freqStep: undefined,
  responsiveWidth: undefined,
  responsiveHeight: undefined,
  scatterDimension: undefined,
  scatterX: undefined,
  scatterY: undefined,
  symbolDimension: undefined,
  stackedDimension: undefined,
  stackedMode: undefined,
  id: undefined,
  type: undefined,
  ...linearInitialState,
};

export const stateFromNewProps = (props, state) => {
  const focusState = sampleFocusStateFromNewProps(props, state);
  const scatterState = scatterStateFromNewProps(props, state);
  const symbolState = symbolStateFromNewProps(props, state);
  const stackedState = stackedStateFromNewProps(props, state);
  const linearState = linearStateFromNewProps(props.type);
  return ({
  ...focusState,
  ...scatterState,
  ...symbolState,
  ...stackedState,
  ...linearState,
  fixedWidth: R.prop('fixedWidth', state),
  fixedHeight: R.prop('fixedHeight', state),
  responsiveWidth: R.prop('responsiveWidth', state),
  responsiveHeight: R.prop('responsiveHeight', state),
  freqStep: props.type === TIMELINE
    ? R.prop('freqStep', state) || '1' : undefined,
  type: R.prop('type', props),
  });
};

const propertiesParsers = {
  highlight: onChangeFocusSelection,
  baseline: onChangeFocusSelection,
  fixedWidth: (value) => isNaturalInt(value) ? value : undefined,
  fixedHeight: (value) => isNaturalInt(value) ? value : undefined,
  responsiveWidth: (value) => isNumber(value) ? value : undefined,
  responsiveHeight: (value) => isNumber(value) ? value : undefined,
  freqStep: (value) => isNaturalInt(value) ? value : undefined,
  title: R.identity,
  subtitle: R.identity,
  sourceLabel: R.identity,
  scatterDimension: optionParser,
  scatterX: optionParser,
  scatterY: optionParser,
  symbolDimension: optionParser,
  stackedDimension: optionParser,
  stackedMode: optionParser,
  maxX: (value) => isNumber(value) ? value : undefined,
  maxY: (value) => isNumber(value) ? value : undefined,
  minX: (value) => isNumber(value) ? value : undefined,
  minY: (value) => isNumber(value) ? value : undefined,
  pivotX: (value) => isNumber(value) ? value : undefined,
  pivotY: (value) => isNumber(value) ? value : undefined,
  stepX: (value) => isNaturalInt(value) ? value : undefined,
  stepY: (value) => isNaturalInt(value) ? value : undefined,
};

export const onChangeProperties = (state, onChange) => (newProperties) => {
  const apply = R.is(Function, onChange) ? onChange : R.identity;
  const refined = R.pipe(
    R.mapObjIndexed((value, key) => {
      const _parser = R.prop(key, propertiesParsers);
      const parser = R.is(Function, _parser) ? _parser : R.identity;
      return parser(value); 
    }),
    R.pickBy((value, key) => value !== R.prop(key, state))
  )(newProperties);
  if (R.isEmpty(refined)) {
    return ;
  }
  apply(refined);
};

export const toProperties = (props, state, onChange) => ({
  ...toLinearProperties(props, state, onChange),
  highlight: {
    id: 'highlight',
    isActive: hasFocus(props.type),
    onChange: R.pipe(
      onChangeFocusSelection,
      selection => {
        onChange({ highlight: selection });
      }
    ),
    options: focusOptions(
      props,
      state,
      'baseline',
      toSingularity(props, state)
    ),
    value: R.prop('highlight', state),
  },
  baseline: {
    id: 'baseline',
    isActive: hasFocus(props.type),
    onChange: R.pipe(
      onChangeFocusSelection,
      selection => onChange({ baseline: selection })
    ),
    options: focusOptions(
      props,
      state,
      'highlight',
      toSingularity(props, state)
    ),
    value: R.prop('baseline', state),
  },
  width: {
    id: 'width',
    isActive: isChart(props.type),
    onChange: (value) => onChange({
      fixedWidth: isNaturalInt(value) ? value : undefined
    }),
    placeholder: R.when(R.complement(R.isNil), (value) => String(Math.floor(Number(value))))(state.responsiveWidth),
    value: R.prop('fixedWidth', state),
  },
  height: {
    id: 'height',
    isActive: isChart(props.type),
    onChange: (value) => onChange({
      fixedHeight: isNaturalInt(value) ? value : undefined
    }),
    placeholder: R.when(R.complement(R.isNil), (value) => String(Math.floor(Number(value))))(state.responsiveHeight),
  },
  freqStep: {
    id: 'freqStep',
    isActive: props.type === TIMELINE,
    onChange: (value) => onChange({
      freqStep: isNaturalInt(value) ? String(value) : undefined
    }),
    value: R.prop('freqStep', state),
  },
  title: {
    id: 'title',
    isActive: isChart(props.type),
    isDefault: R.isNil(state.title),
    onChange: (value) => onChange({ title: value }),
    value: R.isNil(state.title) || R.isEmpty(state.title)
      ? R.pathOr('', ['title', 'label'], props) : state.title,
    onReset: R.isNil(state.title)
      ? null : () => onChange({ title: null })
  },
  subtitle: {
    id: 'subtitle',
    isActive: isChart(props.type),
    isDefault: R.isNil(state.subtitle),
    onChange: (value) => onChange({ subtitle: value }),
    value: R.isNil(state.subtitle) || R.isEmpty(state.subtitle)
      ? stringifySubtitle(props.subtitle || []) : state.subtitle,
    onReset: R.isNil(state.subtitle)
      ? null : () => onChange({ subtitle: null })
  },
  source: {
    id: 'source',
    isActive: isChart(props.type),
    isDefault: R.isNil(state.sourceLabel),
    onChange: (value) => onChange({ sourceLabel: value }),
    value: getSourceLabel(props, state),
    onReset: R.isNil(state.sourceLabel)
      ? null : () => onChange({ sourceLabel: null })
  },
  scatterDimension: {
    id: 'scatterDimension',
    isActive: isScatter(props.type),
    onChange: R.pipe(
      optionParser,
      onChangeScatterDim(props, state),
      onChange
    ),
    options: chartDimensionOptions(props, isScatter),
    value: chartDimensionToOption('scatterDimension')(props, state),
  },
  scatterX: {
    id: 'scatterX',
    isActive: isScatter(props.type),
    onChange: R.pipe(
      optionParser,
      onChangeScatterVal(props, state, 'scatterX', 'scatterY'),
      onChange
    ),
    options: scatterValueOptions(props, state),
    value: scatterStateValueToOption('scatterX', props, state),
  },
  scatterY: {
    id: 'scatterY',
    isActive: isScatter(props.type),
    onChange: R.pipe(
      optionParser,
      onChangeScatterVal(props, state, 'scatterY', 'scatterX'),
      onChange
    ),
    options: scatterValueOptions(props, state),
    value: scatterStateValueToOption('scatterY', props, state),
  },
  symbolDimension: {
    id: 'symbolDimension',
    isActive: isSymbol(props.type),
    options: chartDimensionOptions(props, isSymbol),
    onChange: R.pipe(
      optionParser,
      onChangeSymbolDim(props),
      onChange
    ),
    value: chartDimensionToOption('symbolDimension')(props, state)
  },
  stackedDimension: {
    id: 'stackedDimension',
    isActive: isStackedDimActive(props),
    options: chartDimensionOptions(props, isStacked),
    onChange: R.pipe(
      optionParser,
      onChangeStackedDimension(props, state),
      onChange,
    ),
    value: chartDimensionToOption('stackedDimension')(props, state)
  },
  stackedMode: {
    id: 'stackedMode',
    isActive: isStacked(props.type),
    options: stackedModeOptions,
    onChange: R.pipe(
      optionParser,
      onChangeStackedMode,
      onChange
    ),
    value: R.prop('stackedMode', state)
  },
  logo: {
    id: 'logo',
    isActive: isChart(props.type),
    checked: !state.withLogo,
    onChange: () => onChange({ withLogo: !state.withLogo })
  },
  copyright: {
    id: 'copyright',
    isActive: isChart(props.type),
    checked: !state.withCopyright,
    onChange: () => onChange({ withCopyright: !state.withCopyright })
  }
});
