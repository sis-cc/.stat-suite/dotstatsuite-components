import * as R from 'ramda';

export const stringifySubtitle = R.pipe(
  R.map(({ header, label }) => R.isNil(header) ? label : `${header} ${label}`),
  R.join(' · ')
);
