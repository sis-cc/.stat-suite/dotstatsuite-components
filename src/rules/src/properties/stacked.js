import * as R from 'ramda';
import { PERCENT, STACKED_BAR, STACKED_ROW, VALUES } from '../constants';
import {
  getDimensionsWithValues,
  getGroupedDimensions,
  getPropertyDimension,
  toState
} from './utils';
import { splitDimensions } from '../';
import { focusStateFromNewProps } from './focus';

const getStackedDimension = getPropertyDimension('stackedDimension');

const stackedModes = { [PERCENT]: PERCENT, [VALUES]: VALUES };

export const isStacked = (type) => type === STACKED_BAR || type === STACKED_ROW;

const toStackedState = (dimension, mode) => ({
  stackedDimension: toState(dimension),
  stackedMode: mode,
});

export const toStackedSingularity = (state) => ({
  id: R.prop('stackedDimension', state),
  mode: R.prop('stackedMode', state),
  limit: 5
});

export const onChangeStackedDimension = ({ data }, state) => (value) => {
  const dimension = getStackedDimension(data, { stackedDimension: value });
  if (R.isNil(dimension)) {
    return ({});
  }
  return ({
    stackedDimension: toState(dimension),
    highlight: [],
    baseline: [],
  })
};

export const stackedModeOptions = [{ value: VALUES }, { value: PERCENT }];

export const isStackedDimActive = ({ data, type }) => {
  const { values } = splitDimensions(data);
  if (R.length(values) < 2) {
    return false;
  }
  return isStacked(type);
};

export const onChangeStackedMode = (value) => R.has(value, stackedModes)
  ? ({ stackedMode: value })
  : ({});

export const stackedStateFromNewProps = (props, state) => {
  const { data, type } = props;
  if (!isStacked(type)) {
    return toStackedState(undefined, undefined);
  }
  if (R.isNil(data)) {
    return {};
  }
  const dimensions = getDimensionsWithValues(data);
  if (R.isEmpty(dimensions)) {
    return { ...toStackedState(undefined, undefined), baseline: [], highlight: [] };
  }

  let dimension = getStackedDimension(data, state);
  if (R.isNil(dimension)) {
    const { value, values } = splitDimensions(data);
    if (R.length(values) === 1) {
      dimension = R.head(value);
    }
    else {
      const { area, time, other } = getGroupedDimensions(data);
      if (!R.isNil(area) && !R.isEmpty(area)) {
        dimension = R.head(area);
      }
      else if (!R.isNil(time) && !R.isEmpty(time)) {
        dimension = R.head(time)
      }
      else {
        dimension = R.head(other);
      }
    }
  }
  const _mode = R.prop('stackedMode', state);
  const mode = R.has(_mode, stackedModes) ? _mode : VALUES;
  const stackedState = toStackedState(dimension, mode);
  return ({
    ...stackedState,
    ...focusStateFromNewProps(
      props,
      toStackedSingularity(stackedState), 
      state
    )
  });
}
