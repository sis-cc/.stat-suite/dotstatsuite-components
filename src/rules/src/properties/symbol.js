import * as R from 'ramda';
import { extractSdmxArtefacts } from '../';
import { H_SYMBOL, V_SYMBOL } from '../constants';
import {
  getDimensionsWithValues,
  getGroupedDimensions,
  getPropertyDimension,
  toState
} from './utils';
import { focusStateFromNewProps } from './focus';

const getSymbolDimension = getPropertyDimension('symbolDimension');

export const isSymbol = (type) => (type === H_SYMBOL || type === V_SYMBOL);

const toSymbolState = (dimension) => ({
  symbolDimension: toState(dimension)
});

export const toSymbolSingularity = (data, state) => {
  const symbolDimension = getSymbolDimension(data, state);
  const res = ({
    id: R.isNil(symbolDimension) ? undefined : R.prop('id', symbolDimension),
    serie: R.pipe(
      R.prop('values'),
      values => R.isNil(values) ? [] : values,
      R.take(5)
    )(symbolDimension),
  });
  return res;
};

export const symbolStateFromNewProps = (props, state) => {
  const { data, type } = props;
  if (!isSymbol(type)) {
    return toSymbolState(undefined);
  }
  if (R.isNil(data)) {
    return {};
  }
  const dimensions = getDimensionsWithValues(data);
  if (R.isEmpty(dimensions)) {
    return { ...toSymbolState(undefined), baseline: [], highlight: [] };
  }
  let symbolDimension = getSymbolDimension(data, state);
  if (R.isNil(symbolDimension)) {
    const { area, other, time } = getGroupedDimensions(data);
    if (!R.isNil(other) && !R.isEmpty(other)) {
      symbolDimension = R.head(other);
    }
    else if (!R.isNil(time) && !R.isEmpty(time)) {
      symbolDimension = R.head(time);
    }
    else {
      symbolDimension = R.head(area);
    }
  }
  const symbolState = toSymbolState(symbolDimension);
  return ({
    ...symbolState,
    ...focusStateFromNewProps(
      props,
      toSymbolSingularity(data, symbolState),
      state
    )
  });
};

export const onChangeSymbolDim = ({ data }) => (option) => {
  const dimension = getSymbolDimension(data, { symbolDimension: option });
  if (R.isNil(dimension)) {
    return ({});
  }
  return ({
    ...toSymbolState(dimension),
    highlight: [],
    baseline: [],
  });
};
