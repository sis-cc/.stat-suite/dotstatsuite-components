import * as R from 'ramda';
import {
  isTimePeriodDimension,
} from '@sis-cc/dotstatsuite-sdmxjs';
import {
  getRelationnalAnnotations,
  setAnnotationsLayout,
  getIsHidden,
} from './sdmx-data';
import { getReportingYearStart } from './preparators/getReportingYearStart';
import { getNotDisplayedIds } from '../../rules2/src';
import { getValuesEnhanced } from './get-values-enhanced';

const TIME_PERIOD_ID = 'TIME_PERIOD';

export const dataTransformer = (dataNew, options = {}) => {
  const { locale } = options;
  const reportYearStart = getReportingYearStart(dataNew);
  const getName = R.prop('name')
  //---------------------------------------------------------------------------------------Annotations
  const getStructure = R.pathOr({}, ['data', 'structure']);
  const structure = getStructure(dataNew);
  const getAnnotations = R.propOr([], 'annotations');
  const annotations = getAnnotations(structure);
  const getDataSets = R.pathOr({}, ['data', 'dataSets']);
  const dataSets = getDataSets(dataNew);

  const dataSetsAnnotations = R.props(
    R.pathOr([], [0, 'annotations'], dataSets),
    annotations,
  );

  /*
    hiddenValues => codelistIds or codelistValuesIds for display computation
    hiddenCombinations => used only for dimensions, if dimension values match the provided set, all dimension is removed
  */
  const { hiddenValues, hiddenCombinations } = getNotDisplayedIds(dataSetsAnnotations);
  const getIsAllDimHidden = ({ id, values }) => {
    if (!R.has(id, hiddenCombinations)) {
      return false;
    }
    const valuesIds = R.pluck('id', values);
    return R.isEmpty(R.difference(valuesIds, hiddenCombinations[id]));
  }

  //--------------------------------------------------------------------------------------------Header
  const getMeta = R.propOr({}, 'meta');
  const meta = getMeta(dataNew);

  const getSender = R.propOr({}, 'sender');
  const sender = getSender(meta);

  const resHeader = {
    ...meta,
    sender: { ...sender, name: getName(sender) },
  };

  //----------------------------------------------------------------------------------------Attributes
  const getAttributes = R.propOr({}, 'attributes');
  const attributes = getAttributes(structure);

  const getAttrObservations = R.propOr([], 'observation');
  const attrObservations = getAttrObservations(attributes);

  const resAttrObservations = R.addIndex(R.map)((observation, index) => {
    const obsAnnotations = R.props(observation.annotations || [], annotations);
    return {
      ...observation,
      __index: index,
      display:
        !R.has(observation.id, hiddenValues) && !getIsHidden(obsAnnotations),
      name: getName(observation),
      values: getValuesEnhanced({
        locale,
        annotations,
        parent: observation.id,
        reportYearStart,
        hiddenIds: hiddenValues,
        options,
      })(R.propOr([], 'values', observation)),
      roles: !R.isNil(observation.role) ? R.head(observation.role) : null,
    };
  }, attrObservations);

  const datasetAttributes = R.map((attr) => {
    const attrAnnotations = R.props(attr.annotations || [], annotations);
    return {
      ...attr,
      display: !R.has(attr.id, hiddenValues) && !getIsHidden(attrAnnotations),
      name: getName(attr),
      values: getValuesEnhanced({
        locale,
        annotations,
        parent: attr.id,
        hiddenIds: hiddenValues,
        options,
        reportYearStart,
      })(R.propOr([], 'values', attr)),
    };
  }, R.propOr([], 'dataSet', attributes));

  const resAttributes = {
    ...attributes,
    observation: R.concat(resAttrObservations, datasetAttributes),
  };

  //----------------------------------------------------------------------------------------Dimensions
  const getDimensions = R.propOr({}, 'dimensions');
  const dimensions = getDimensions(structure);

  const getDimObservations = R.propOr([], 'observation');
  const dimObservations = getDimObservations(dimensions);

  const getObservations = (locale) =>
    R.addIndex(R.reduce)(
      (acc, observation, obsIndex) => {
        const id = R.prop('id')(observation);
        const dimAnnotations = getRelationnalAnnotations(
          R.propOr([], 'annotations')(observation),
        )(annotations);
        const isTimeDimension = isTimePeriodDimension(observation);
        const values = getValuesEnhanced({
          locale,
          annotations,
          isTimeDimension,
          reportYearStart,
          parent: id,
          hiddenIds: hiddenValues,
          options,
        })(R.propOr([], 'values', observation));
        const isAllDimHidden = getIsAllDimHidden({ id, values });
        const isHidden = isAllDimHidden || R.has(id, hiddenValues)
          || getIsHidden(dimAnnotations);
        return {
          observation: R.append({
            ...observation,
            display: !isHidden,
            __index: obsIndex,
            name: getName(observation),
            values: isAllDimHidden ? [] : values,
            role: isTimeDimension
              ? TIME_PERIOD_ID
              : R.head(R.propOr([], 'roles', observation)),
          })(acc.observation),
          dimensionsLayout: setAnnotationsLayout(
            id,
            acc.dimensionsLayout,
          )(dimAnnotations),
        };
      },
      { observation: [], dimensionsLayout: {} },
    );

  //-----------------------------------------------------------------------------------------Structure
  const { dimensionsLayout, observation } =
    getObservations(locale)(dimObservations);
  const resStructure = {
    ...structure,
    name: getName({ ...structure, id: options.dataflowId }),
    annotations,
    attributes: resAttributes,
    dimensions: { observation },
  };

  //------------------------------------------------------------------------------------------------------Layout
  const dataSetsLayout = R.reduce((acc, dataSetsAnnotation) => {
    const title = R.propOr('', 'title')(dataSetsAnnotation);
    if (R.isEmpty(title)) return acc;
    return setAnnotationsLayout(title, acc)([dataSetsAnnotation]);
  }, {})(dataSetsAnnotations);

  return {
    data: { header: resHeader, dataSets, structure: resStructure },
    layout: R.mergeRight(dimensionsLayout, dataSetsLayout),
  };
};
