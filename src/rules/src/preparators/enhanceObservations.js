import * as R from 'ramda';
import { formatValue } from './formatValue';

/*
  options = {
    attachmentSeriesIndexes, //units
    customAttributes: {
      decimals: id,
      prefscale: id,
    },
    unitsArtefacts,
    obsAttributes,
    rejectedValueIds,
  }
*/

const getSerieKey = (dimValuesIndexes, options) => {
  let indexes = R.propOr([], 'attachmentSeriesIndexes', options);
  return R.pipe(
    R.addIndex(R.map)((valueIndex, dimIndex) => {
      if (dimIndex === R.head(indexes)) {
        indexes = R.tail(indexes);
        return valueIndex;
      }
      return 'x';
    }),
    R.join(':')
  )(dimValuesIndexes);
};

const getUnitsValues = (indexes, options) => R.reduce(
  (acc, codelist) => {
    const { id } = codelist;
    if (!R.includes(id, R.propOr([], 'unitsIds', options))) {
      return acc;
    }
    const valueIndex = R.prop(codelist.__index, indexes);
    if (R.isNil(valueIndex) || !R.propOr(true, 'display', codelist)) {
      return acc;
    }
    const value = R.path(['values', Number(valueIndex)], codelist);
    if (R.isNil(value) || R.includes(value.id, R.propOr([], 'rejectedValueIds', options))
      || !R.propOr(true, 'display', value)) {
      return acc;
    }
    return R.assoc(
      id,
      {
        ...R.pick(['id', 'name', '__index'], codelist),
        value: R.when(R.complement(R.isNil), R.assoc('index', Number(valueIndex)))(value)
      },
      acc
    );
  },
  {}
);

const getObservationUnits = (observation, dimensions, attributes, options) => {
  const dimensionsUnits = getUnitsValues(
    R.propOr([], 'dimValuesIndexes', observation),
    options
  )(dimensions);

  const attributesUnits = getUnitsValues(
    R.propOr([], 'attrValuesIndexes', observation),
    options
  )(attributes);
  
  const serieKey = getSerieKey(R.propOr([], 'dimValuesIndexes', observation), options);

  return ({ ...dimensionsUnits, ...attributesUnits, serieKey });
};

const getFormatAttributesIndexes = (attributes, customAttributes) => R.addIndex(R.reduce)(
  (acc, attribute, index) => {
    if (R.equals(attribute.id, customAttributes.decimals)) {
      return ({ ...acc, decimals: index });
    }
    if (R.equals(attribute.id, customAttributes.prefscale)) {
      return ({ ...acc, prefscale: index });
    }
    return acc;
  },
  { prefscale: null, decimals: null },
  attributes
);

const getAttributeValue = (attribute, valueIndex, options) => {
  if (!R.propOr(true, 'display', attribute)) {
    return null;
  }
  const value = R.pipe(
    R.propOr([], 'values'),
    R.nth(valueIndex)
  )(attribute);

  if (R.isNil(value) || !R.propOr(true, 'display', value) || R.includes(value.id,R.propOr([],'rejectedValueIds')(options))) {
    return null;
  }

  return ({
    ...R.pick(['id', 'name'], attribute),
    value: R.pick(['id', 'name'], value)
  });
};


export const enhanceObservations = (dimensions = [], observations = {}, attributes = [], options = {}) => {
  const formatAttributesIndexes = getFormatAttributesIndexes(attributes, R.propOr({}, 'customAttributes', options));
  const _attributes = R.addIndex(R.map)((attr, index) => R.assoc('__index', index, attr), attributes);
  const _obsAttributes = R.pipe(
    R.indexBy(R.prop('id')),
    R.props(options.attributesIds || [])
  )(_attributes);

  return R.mapObjIndexed(
    (observation) => {
      const { attrValuesIndexes, dimValuesIndexes } = observation;
      const obsAttributes = R.reduce(
        (acc, attribute) => {
          const attrValue = getAttributeValue(attribute, R.nth(attribute.__index, attrValuesIndexes), options);
          if (R.isNil(attrValue)) {
            return acc;
          }
          return R.assoc(attribute.id, attrValue, acc);
        },
        {},
        _obsAttributes
      );

      const indexedDimValIds = R.addIndex(R.reduce)(
        (acc, dimension, dimensionIndex) => {
          const id = dimension.id;
          const valueIndex = R.nth(dimensionIndex, dimValuesIndexes);
          const valueId = R.path(['values', Number(valueIndex), 'id'], dimension);
          return R.assoc(id, valueId, acc);
        },
        {},
        dimensions
      );

      return ({
        ...observation,
        attributes: obsAttributes,
        formattedValue: formatValue(observation, formatAttributesIndexes, attributes),
        units: getObservationUnits(observation, dimensions, _attributes, options),
        indexedDimValIds,
      });
    },
    observations
  );
};
