import * as R from 'ramda';
import { REPORTING_YEAR_START_DAY, REPYEARSTART } from '../constants';

export const getReportingYearStart = (sdmxJson) => {
  const attributes = R.pipe(
    R.pathOr({}, ['data', 'structure', 'attributes']),
    ({ dataSet=[], observation=[], series=[] }) => [...dataSet, ...observation, ...series]
  )(sdmxJson);

  const reportYearStartAttr = R.find(({ id }) => {
    return id === REPORTING_YEAR_START_DAY || id === REPYEARSTART;
  }, attributes) || {};
  const reportYearStart = R.pathOr('--01-01', ['values', 0, 'value'], reportYearStartAttr);
  const [month = '01', day = '01'] = R.match(/[\d]{2}/g, reportYearStart);
  return ({ month, day });
};
