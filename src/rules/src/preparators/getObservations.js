import * as R from 'ramda';

export const getObservations = (sdmxJson) => R.pipe(
  R.pathOr({}, ['data', 'dataSets', 0, 'observations']),
  R.mapObjIndexed(
    (observation, observationKey) => {
      const value = R.head(observation);
      const attrValuesIndexes = R.tail(observation);
      const dimValuesIndexes = R.split(':', observationKey);

      return ({
        key: observationKey,
        value,
        attrValuesIndexes,
        dimValuesIndexes
      });
    },
  )
)(sdmxJson);
