import * as R from 'ramda';
import numeral from 'numeral';

const isValidNumber = R.both(R.is(Number), R.complement(R.equals(NaN)));
const parseValidNumber = R.pipe(
  v => R.isNil(v) ? null : Number(v),
  v => isNaN(v) ? null : v
);

const getFormatAttributesValues = (observation, formatAttributesIndexes, attributes) => R.mapObjIndexed(
  attributeIndex => {
    let valueIndex;
    const relationship = R.pipe(R.nth(attributeIndex), R.propOr({}, 'relationship'))(attributes);
    //if (!R.isNil(attributeIndex) && attributeIndex >= R.length(R.propOr([], 'attrValuesIndexes', observation))) {
    if (R.has('none', relationship) || R.has('dataflow', relationship)) {
      valueIndex = 0;
    }
    else {
      valueIndex = R.pipe(
        R.propOr([], 'attrValuesIndexes'),
        R.nth(attributeIndex),
        parseValidNumber
      )(observation);
    }

    return R.pipe(
      R.path([attributeIndex, 'values', valueIndex, 'id']),
      parseValidNumber
    )(attributes);
  },
  formatAttributesIndexes
);

export const formatValue = (observation, formatAttributesIndexes, attributes) => {
  const value = R.prop('value', observation);
  if (R.is(Boolean, value)) {
    return value;
  }
  if (R.is(String, value)) {    
    const monthMatch = value.match(/^\-\-([\d]{2})$/);
    if (R.is(Array, monthMatch)) {
      return R.nth(1, monthMatch);       
    }
    const monthDayMatch = value.match(/^\-\-([\d]{2}\-[\d]{2})$/);
    if (R.is(Array, monthDayMatch)) {
      return R.nth(1, monthDayMatch);       
    }
    const dayMatch = value.match(/^\-\-\-([\d]{2})$/);
    if (R.is(Array, dayMatch)) {
      return R.nth(1, dayMatch);
    }
    return value;
  }

  const { prefscale, decimals } = getFormatAttributesValues(
    observation,
    formatAttributesIndexes,
    attributes
  );

  const formatScale = R.when(
    R.always(isValidNumber(prefscale)),
    value => numeral(value).multiply(Math.pow(10, -1 * Number(prefscale))).value(),
  );

  const formatSeparator = value => numeral(value).format(`0,0.[0000000]`);
  const formatDecimals = R.ifElse(
    R.always(isValidNumber(decimals)),
    value => numeral(value).format(
      R.ifElse(R.equals(0), R.always('0,0'), d => `0,0.${R.join('', R.times(R.always('0'), d))}`)(decimals)
    ),
    formatSeparator
  );

  return R.ifElse(
    isValidNumber,
    R.pipe(
      formatScale,
      formatDecimals,
      res => res === 'NaN' ? String(value) : res
    ),
    R.always('..')
  )(observation.value);
};
