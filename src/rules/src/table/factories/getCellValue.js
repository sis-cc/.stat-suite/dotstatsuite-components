import * as R from 'ramda';
import numeral from 'numeral';

const isValidNumber = R.both(R.is(Number), R.complement(R.equals(NaN)));

export const getCellValue = (observation) => {
  const value = R.prop('value', observation);
  if (R.is(Boolean, value)) {
    return value;
  }
  if (R.is(String, value)) {    
    const monthMatch = value.match(/^\-\-([\d]{2})$/);
    if (R.is(Array, monthMatch)) {
      return R.nth(1, monthMatch);       
    }
    const monthDayMatch = value.match(/^\-\-([\d]{2}\-[\d]{2})$/);
    if (R.is(Array, monthDayMatch)) {
      return R.nth(1, monthDayMatch);       
    }
    const dayMatch = value.match(/^\-\-\-([\d]{2})$/);
    if (R.is(Array, dayMatch)) {
      return R.nth(1, dayMatch);
    }
    return value;
  }
  const scale = Number(observation.prefscale);
  const formatScale = R.ifElse(
    R.always(isValidNumber(scale)),
    value => numeral(value).multiply(Math.pow(10, -1 * Number(scale))).value(),
    R.identity
  );
  const formatSeparator = value => numeral(value).format(`0,0.[0000000]`);
  const decimals = Number(observation.decimals);
  const formatDecimals = R.ifElse(
    R.always(isValidNumber(decimals)),
    value => numeral(value).format(
      R.ifElse(R.equals(0), R.always('0,0'), d => `0,0.${R.join('', R.times(R.always('0'), d))}`)(decimals)
    ),
    formatSeparator
  );
  const res = R.ifElse(
    isValidNumber,
    R.pipe(
      formatScale,
      formatDecimals,
    ),
    R.always('..')
  )(observation.value);
  return res;
};
