import { isEmpty, isNil, get, has, reduce } from 'lodash';

export const dimensionValueDisplay =
  (display, accessors = {}) =>
  (dimensionValue) => {
    const id = get(dimensionValue, accessors.id || 'id', '');
    const name = get(dimensionValue, accessors.name || 'name', '');
    // a non-coded component (dim | attr) has no name, names nor label
    // the only valid thing to display is the id
    if (get(dimensionValue, 'isNonCoded', false)) return id;
    //auto-generated flags have no id nor name
    if (isEmpty(id) && isEmpty(name)) return ''
    switch (display) {
      case 'label':
        return isEmpty(name) ? `[${id}]` : name;
      case 'code':
        return id;
      case 'both':
        if (isNil(id) || isEmpty(id)) return name;
        return `(${id}) ${name}`;
      default:
        return null;
    }
  };

export const dimensionValueDisplayAt = (dimension, index, display) => {
  const dimensionValue = get(dimension, `values[${Number(index)}]`, {});
  return dimensionValueDisplay(display)(dimensionValue);
};

export const categoryDisplay = (
  splitObservationKey,
  dimensions,
  dimensionsWithValuesIndexedById,
  rejectedId,
  display,
) => {
  return reduce(
    splitObservationKey,
    (memo, dimensionValueIndex, dimensionIndex) => {
      const dimension = get(dimensions, `[${dimensionIndex}]`, {});
      if (
        has(dimensionsWithValuesIndexedById, dimension.id) &&
        dimension.id !== rejectedId
      ) {
        memo.push(
          dimensionValueDisplayAt(dimension, dimensionValueIndex, display),
        );
      }
      return memo;
    },
    [],
  ).join(' - ');
};
