import memoizee from 'memoizee';
import numeral from 'numeral';
import * as R from 'ramda';
import { isNan } from 'lodash';
import { extractSdmxArtefacts } from './';

export const DEFAULT_PREFSCALE_ATTR_IDS = ["PREF_SCALE"];

export const DEFAULT_DECIMALS_ATTR_IDS = ["DECIMALS"];

export const getFormaterAttribute = (ids, attributes) => R.find(
  (attr) => R.includes(attr.id, ids),
  attributes
);

export const parseAttributeFormaterIds = (ids = {}) => {
  let prefscale = R.prop('prefscale', ids);
  let decimals = R.prop('decimals', ids);
  decimals = R.isNil(decimals) ? DEFAULT_DECIMALS_ATTR_IDS : decimals;
  prefscale = R.isNil(prefscale) ? DEFAULT_PREFSCALE_ATTR_IDS : prefscale;
  return ({
    decimals: R.is(Array, decimals) ? decimals : [decimals],
    prefscale: R.is(Array, prefscale) ? prefscale : [prefscale],
  });
}

export const _getObservationsFormaterAttributes = (data, ids) => R.pipe(
  extractSdmxArtefacts,
  ({ attributes }) => {
    const { decimals, prefscale } = parseAttributeFormaterIds(ids);
    return ({
      decimals: getFormaterAttribute(decimals, attributes),
      prefscale: getFormaterAttribute(prefscale, attributes)
    });
  }
)(data);
export const getObservationsFormaterAttributes = memoizee(_getObservationsFormaterAttributes);

export const getObservationFormaterValue = (observation, attribute) => {
  const attributeIndex = R.prop('index', attribute);
  const attributeValueIndex = R.nth(attributeIndex, observation);
  return R.nth(attributeValueIndex, R.propOr([], 'values', attribute));
}

export const getObservationFormaterValues = (formaterAttributes) => (observation) => ({
  decimals: getObservationFormaterValue(observation, R.prop('decimals', formaterAttributes)),
  prefscale: getObservationFormaterValue(observation, R.prop('prefscale', formaterAttributes)),
});

export const makeObservationFormater = (formaterValues) => {
  let prefscale = 0;
  if (!R.isNil(R.prop('prefscale', formaterValues))) {
    prefscale = Number(R.propOr('0', 'id', formaterValues.prefscale));
  }
  let decimals = null;
  if (!R.isNil(R.prop('decimals', formaterValues))) {
    let _decimals = Number(R.prop('id', formaterValues.decimals));
    decimals = R.is(Number, _decimals) && !Number.isNaN(_decimals)
      ? _decimals : null;
  }
  let format = '0,0';
  if (R.isNil(decimals)) {
    format = R.concat(format, '.[0000000]');
  }
  else if (decimals > 0) {
    format = R.concat(format, `.[${'0'.repeat(decimals)}]`);
  }
  return value => numeral(Number(value))
    .multiply(Math.pow(10, R.negate(prefscale)))
    .format(format);
};

export const getObservationFormater = (observation, formaterAttributes) => {
  return R.pipe(
    R.tail,
    getObservationFormaterValues(formaterAttributes),
    makeObservationFormater
  )(observation);
}

export const getFormatedObservation = (observation, formaterAttributes) => {
  const formater = getObservationFormater(observation, formaterAttributes);
  return formater(R.head(observation));
};
