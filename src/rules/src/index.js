import {
  get, partition, reject, isEmpty, map, compact, size, keyBy, pick, omit, isNil, flatten, reduce,
  filter, find, includes, isArray, findIndex, isEqual
} from 'lodash';
import * as R from 'ramda';
import compose from 'lodash.compose';
import memoizee from 'memoizee';
import __sampleSeries from './factories/sample-series';
import __sampleFocus from './factories/sample-focus';
import __scatterSeries from './factories/scatter-series';
import __scatterFocus from './factories/scatter-focus';
import __scatterDimension from './factories/scatter-dimension';
import __symbolSeries from './factories/symbol-series';
import __symbolDimension from './factories/symbol-dimension';
import __timelineSeries from './factories/timeline-series';
import __timelineFocus from './factories/timeline-focus';
import __stackedSeries from './factories/stacked-series';
import __stackedDimension from './factories/stacked-dimension';
import __choroSeries from './factories/choro-series';
import {
  DEFAULT_UPR_IDS, DEFAULT_REJECTED_UPR_VALUES_IDS, DEFAULT_REJECTED_SUBTITLE_IDS,
  FREQ_A, FREQ_Q, FREQ_M
} from './constants';
import { dimensionValueDisplay } from './dimension-utils';
import { getObservationsFormaterAttributes } from './observation-formater';

export const freqA = FREQ_A;
export const freqQ = FREQ_Q;
export const freqM = FREQ_M;

// --------------------------------------------------------------------------------extractSdmxErrors
const _extractSdmxErrors = (data) => compose(
  (errors) => map(errors, (error) => ({ label: error.code, description: error.message })),
  (data) => get(data, 'errors', [])
);
export const extractSdmxErrors = memoizee(_extractSdmxErrors);

export const extractDataName = (data) => R.pathOr(null, ['structure', 'name'], data);

// -----------------------------------------------------------------------------extractSdmxArtefacts
const _extractSdmxArtefacts = (data) => {
  const parseStructure = (data) => get(data, 'structure', {});
  const parseItem = (item) => parseComponents(get(item, 'observation', []));
  const parseDimensions = (structure) => parseItem(get(structure, 'dimensions', {}));
  const parseAttributes = (structure) => parseItem(get(structure, 'attributes', {}));
  const parseComponents = (components) => map(components, parseComponent);
  const parseComponent = (component, index) => {
    let parsed = { ...pick(component, 'id', 'name', 'values', 'role', 'isHidden'), index };
    if (isEmpty(parsed))
      return parsed;
    if (isNil(parsed.role))
      parsed.role = parsed.id;
    parsed.values = R.sortBy(R.prop('__index'))(parsed.values);
    return parsed;
  };
  const parseDatasetItem = (item) => parseComponents(get(item, 'dataSet', []));
  const parseDatasetAttributes = (structure) => parseDatasetItem(get(structure, 'attributes', {}));

  return {
    attributes: parseAttributes(parseStructure(data)),
    datasetAttributes: parseDatasetAttributes(parseStructure(data)),
    dimensions: parseDimensions(parseStructure(data)),
    id: get(data, 'header.id', null),
    name: extractDataName(data),
    observations: R.pipe(
      R.pathOr({}, ['dataSets', 0, 'observations']),
      R.mapObjIndexed(obs => R.over(R.lensIndex(0), val => R.isNil(val) ? null : Number(val))(obs)),
      R.reject(R.pipe(R.head, R.anyPass([R.isNil, isNaN]))),
    )(data),
    source: get(data, 'header.links[0].href', null),
  };
};
export const extractSdmxArtefacts = memoizee(_extractSdmxArtefacts);

const _getRelevantsDatasetAttributes = (data) => {
  const { datasetAttributes } = extractSdmxArtefacts(data);
  return filter(datasetAttributes, attr => size(attr.values) === 1);
};
export const getRelevantsDatasetAttributes = memoizee(_getRelevantsDatasetAttributes);

// -------------------------------------------------------------------------------getDimensionValues
export const getDimensionValues = (dimension) => get(dimension, 'values', []);

// ----------------------------------------------------------------------------------splitDimensions
const _splitDimensions = (data) => {
  const { dimensions } = extractSdmxArtefacts(data);
  const [value, values] = partition(
    reject(dimensions, compose(isEmpty, getDimensionValues)),
    compose((values) => size(values) === 1, getDimensionValues)
  );
  return { value, values };
}
export const splitDimensions = memoizee(_splitDimensions);

// ------------------------------------------------------------------dimensionsWithValuesIndexedById
const _dimensionsWithValuesIndexedById = (data) => compose(
  (dimensionsWithValues) => keyBy(dimensionsWithValues, 'id'),
  (split) => split.values,
  splitDimensions
)(data);
export const dimensionsWithValuesIndexedById = memoizee(_dimensionsWithValuesIndexedById);

// -------------------------------------------------------------------------------------parseDisplay
const _parseDisplay = (display) =>
  (isNil(display) || !(includes(['both', 'code'], display)))
    ? 'label' : display;
export const parseDisplay = memoizee(_parseDisplay);

// -------------------------------------------------------------------------------------sampleSeries
const _sampleSeries = (data, type, display, formaterIds) => __sampleSeries(
  type,
  extractSdmxArtefacts(data),
  dimensionsWithValuesIndexedById(data),
  parseDisplay(display),
  getObservationsFormaterAttributes(data, formaterIds)
);
export const sampleSeries = memoizee(_sampleSeries);

// ---------------------------------------------------------------------------------scatterDimension
const _scatterDimension = (data, ids) => {
  const { values } = splitDimensions(data);
  return __scatterDimension(values, ids);
};
export const scatterDimension = memoizee(_scatterDimension);

// ----------------------------------------------------------------------------------symbolDimension
const _symbolDimension = (data, ids) => {
  const { values } = splitDimensions(data);
  return __symbolDimension(values, ids);
};
export const symbolDimension = memoizee(_symbolDimension);

const _stackedDimension = (data, singularity) => {
  return __stackedDimension(splitDimensions(data), singularity);
}
export const stackedDimension = memoizee(_stackedDimension);

// ------------------------------------------------------------------------------------scatterSeries
const _scatterSeries = (data, ids, display, formaterIds) => __scatterSeries(
  extractSdmxArtefacts(data),
  dimensionsWithValuesIndexedById(data),
  scatterDimension(data, ids),
  parseDisplay(display),
  getObservationsFormaterAttributes(data, formaterIds)
);
export const scatterSeries = memoizee(_scatterSeries);

// -------------------------------------------------------------------------------------symbolSeries
const _symbolSeries = (data, type, ids, display, formaterIds) => __symbolSeries(
  extractSdmxArtefacts(data),
  type,
  dimensionsWithValuesIndexedById(data),
  symbolDimension(data, ids),
  parseDisplay(display),
  getObservationsFormaterAttributes(data, formaterIds)
);
export const symbolSeries = memoizee(_symbolSeries);

// -----------------------------------------------------------------------------------timelineSeries
const _timelineSeries = (data, display, formaterIds) => __timelineSeries(
  extractSdmxArtefacts(data),
  dimensionsWithValuesIndexedById(data),
  parseDisplay(display),
  getObservationsFormaterAttributes(data, formaterIds)
);
export const timelineSeries = memoizee(_timelineSeries);

const _stackedSeries = (data, singularity, display, formaterIds, [stackKey, categoryKey]) => __stackedSeries(
  extractSdmxArtefacts(data),
  dimensionsWithValuesIndexedById(data),
  stackedDimension(data, singularity),
  parseDisplay(display),
  getObservationsFormaterAttributes(data, formaterIds),
  [stackKey, categoryKey]
);
export const stackedSeries = memoizee(_stackedSeries);

// --------------------------------------------------------------------------------------choroSeries
const _choroSeries = (data, map, display) => __choroSeries(
  extractSdmxArtefacts(data),
  dimensionsWithValuesIndexedById(data),
  map,
  parseDisplay(display)
);
export const choroSeries = memoizee(_choroSeries);

// -------------------------------------------------------------------------------------------series
const _series = (data, type, focus, singularity, map, display, formaterIds) => {
  switch(type) {
    case 'RowChart': case 'BarChart':
      return __sampleFocus(sampleSeries(data, type, display, formaterIds), focus);
    case 'ScatterChart':
      return __scatterFocus(scatterSeries(data, singularity, display, formaterIds), focus);
    case 'HorizontalSymbolChart': case 'VerticalSymbolChart':
      return __sampleFocus(symbolSeries(data, type, singularity, display, formaterIds), focus);
    case 'TimelineChart':
      return __timelineFocus(timelineSeries(data, display, formaterIds), focus);
    case 'StackedBarChart':
      return __sampleFocus(stackedSeries(data, singularity, display, formaterIds, ['y', 'x']), focus);
    case 'StackedRowChart':
      return __sampleFocus(stackedSeries(data, singularity, display, formaterIds, ['x', 'y']), focus);
    case 'ChoroplethChart':
      return choroSeries(data, map, display);
  }
};
export const series = memoizee(_series);

// -------------------------------------------------------------------------------------------source
const _source = (data, config, meta) => {
  const hasSource = get(meta, 'hasSource', true);
  if (!hasSource)
    return null;
  const source = get(config, 'source', null);
  if (source && !(isEmpty(source)))
    return source; 
  const artefacts = extractSdmxArtefacts(data);
  const uri = get(artefacts, 'source');
  if (!uri) 
    return get(config, 'defaultSource', null); 
  const match = uri.match(/(^http.*)\/SDMX-JSON\/data\/([^\/]+)\//);
  if (size(match) !== 3)
    return null;
  const [m, domain, dataset] = match;
  return `${domain}/Index.aspx?DataSetCode=${dataset}`; //tmp
};
export const source = memoizee(_source);

// --------------------------------------------------------------------------------------------focus
const _focus = (data, type, dimension, _display) => {
  const display = parseDisplay(_display);
  switch(type) {
    case 'RowChart': case 'BarChart':
    case 'HorizontalSymbolChart': case 'VerticalSymbolChart':
    case 'StackedBarChart': case 'StackedRowChart':
      return compose(
        (datapoints) => map(datapoints, (dp) => ({ label: dp.category, value: dp.key })),
        (series) => series[0].datapoints,
        series
      )(data, type, null, dimension, null, display);
    case 'TimelineChart':
      return compose(
        (series) => map(series, (serie) => ({ label: serie.category, value: serie.key })),
        series
      )(data, type, null, dimension, null, display)
    case 'ScatterChart':
      const id = get(dimension, 'id');
      if (isNil(id)) return [];
      const dimensions = compose(
        (dimensions) => omit(dimensions, id),
        dimensionsWithValuesIndexedById
      )(data);
      return flatten(map(
        dimensions,
        (dimension) => {
          return map(
            get(dimension, 'values'),
            (value) => ({
              label: `${dimensionValueDisplay(display)(dimension)} - ${dimensionValueDisplay(display)(value)}`,
              options: { dimensionId: dimension.id, dimensionValueId: value.id },
              value: `${dimension.id}-${value.id}`
            })
          )
        }
      ));
  }
};
export const focus = memoizee(_focus);

const _parseFocus = (data, type, dimension, display, { baseline, highlight }) => {
  const options = focus(data, type, dimension, display);

  const parseSelection = selection => compact(map(selection, ({ value }) => find(options, option => isEqual(option.value, value))));
  return ({ baseline: parseSelection(baseline), highlight: parseSelection(highlight) });
};
export const parseFocus = memoizee(_parseFocus);

const _defaultSubtitle = (data, display, REJECTED_SUBTITLE_IDS = DEFAULT_REJECTED_SUBTITLE_IDS) => {
  const { value } = splitDimensions(data);
  const values = reduce(
    R.reject(R.prop('isHidden'))(value),
    (memo, d) => {
      const dvId = get(d, 'values[0].id');
      if (!includes(REJECTED_SUBTITLE_IDS, dvId)) {
        memo.push(`${dimensionValueDisplay(display)(d)}: ${dimensionValueDisplay(display)(get(d, 'values[0]'))}`);
      }
      return memo;
    },
    []
  );
  return values;
}
export const defaultSubtitle = memoizee(_defaultSubtitle);

const _uprAttributes = (attributes, UPR_IDS = DEFAULT_UPR_IDS) => (
  reduce(
    UPR_IDS,
    (memo, ids, uprKey) => {
      const attr = find(attributes, attribute => includes(ids, get(attribute, 'id')));
      if (attr) {
        memo[uprKey] = attr;
      }
      return memo;
    },
    {}
  )
);
export const uprAttributes = memoizee(_uprAttributes);

const _uprAttributesWithOneRelevantValue = (uprAttributes, UPR_IDS = DEFAULT_UPR_IDS, REJECTED_UPR_VALUES_IDS = DEFAULT_REJECTED_UPR_VALUES_IDS) => reduce(
  uprAttributes,
  (memo, attr, key) => {
    if (size(get(attr, 'values', [])) === 1 && !(includes(REJECTED_UPR_VALUES_IDS[key], get(attr, 'values[0].id', ''))))
      memo[key] = attr;
    return memo;
  },
  {}
);
export const uprAttributesWithOneRelevantValue = memoizee(_uprAttributesWithOneRelevantValue);

export const headerUprsLabel = display => uprAttributes => {
  return map(
    uprAttributes,
    attr => dimensionValueDisplay(display)(get(attr, 'values[0]'))
  ).join(', ');
}

export const getUprs = (data, display) => compose(
  headerUprsLabel(parseDisplay(display)),
  uprAttributesWithOneRelevantValue,
  ({ attributes }) => uprAttributes(attributes),
  extractSdmxArtefacts
)(data);

const _header = (data, _title, _subtitle, _display) => {
  const display = parseDisplay(_display);
  const title = (isNil(_title) || isEmpty(_title)) ? get(data, 'structure.name') : _title;
  const subtitle = (isNil(_subtitle) || isEmpty(_subtitle)) ? defaultSubtitle(data, display) : _subtitle;
  return ({
    title,
    subtitle: isArray(subtitle) ? subtitle : [subtitle],
    uprs: compose(
      headerUprsLabel(display),
      uprAttributesWithOneRelevantValue,
      ({ attributes }) => uprAttributes(attributes),
      extractSdmxArtefacts
    )(data)
  });
};
export const header = memoizee(_header);

const _sourceLabel = (data, label) => (
  (isNil(label) || isEmpty(label)) ? extractSdmxArtefacts(data).name : label
);
export const sourceLabel = memoizee(_sourceLabel);

const _getTopologyAreasCollections = (topology) => map(topology.objects, (entry, key) => key);
export const getTopologyAreasCollections = memoizee(_getTopologyAreasCollections);

const _isNonIdealState = (data, type, map) => {
  const defaultSeries = series(data, type, null, null, map);
  if (type === 'ChoroplethChart')
  {
    return findIndex(get(defaultSeries, 'objects.areas.geometries', []), geometry => get(geometry, 'properties.value', null)) === -1;
  }
  return isEmpty(get(defaultSeries, '[0].datapoints', []));
}
export const isNonIdealState = memoizee(_isNonIdealState);

export { dimensionValueDisplay } from './dimension-utils';
export { DEFAULT_UPR_IDS, DIMENSION_DISPLAY_PARTS, DEFAULT_MAJOR_ATTRIBUTES, V8_SDMX_JSON,
  TYPES as chartTypes } from './constants';
export {
  initialState,
  stateFromNewProps,
  onChangeProperties,
  toChartData,
  toChartOptions,
  toProperties,
  getErrors,
  toSingularity
} from './properties';
export { dataTransformer as v8Transformer } from './v8-transformer';
export {
  getCellValue,
} from './table';
export {
  getFullName,
} from './sdmx-data';
export { getTimePeriodLabel } from './date';
export { getHeaderProps, getFooterProps,  } from './properties/getHeaderProps';
export { getInformationsStateFromNewProps } from './properties/getInformationsStateFromNewProps';
export { getObservationsType } from './properties/getObservationsType';
export { getAvailableChartTypes } from './properties/getAvailableChartTypes';
export { isSharedLayoutCompatible } from './layout';

export { getObservations } from './preparators/getObservations';

