import * as R from 'ramda';
import dateFns from 'date-fns';
import { getCodeOrder } from '@sis-cc/dotstatsuite-sdmxjs';
import { getIsHidden } from './sdmx-data';
import {
  getReportedTimePeriodLabel,
  getReportedTimePeriodNote,
  getReportedTimePeriod,
} from './date';
import { refineTimePeriod } from '../../rules2/src/refineTimePeriod';

const isNonCoded = R.both(R.has('value'), R.pipe(R.has('id'), R.not));

const getId = (value, fallbackId) => {
  const valueId = R.prop('id', value);
  if (valueId) return valueId;
  if (isNonCoded(value)) return R.prop('value', value);
  // Fallback (value has no id but is not a non-coded)
  return fallbackId;
};

const getName =
  ({ isTimeDimension, options, locale, start, reportYearStart }) =>
    (value) => {
      if (!isTimeDimension) return R.prop('name')(value);
      return getReportedTimePeriodLabel(
        options.frequency,
        options.timeFormat,
        locale,
      )(reportYearStart, {
        id: value.id,
        reportedStart: start,
        start: new Date(value.start),
      });
    };

export const getValuesEnhanced = ({
  locale = '',
  annotations = [],
  isTimeDimension = false,
  parent = '',
  reportYearStart,
  hiddenIds = {},
  options = {},
}) => {
  return R.pipe(
    R.addIndex(R.map)((value, __index) => {
      const id = getId(value, `${parent}-${__index}`);

      const valueAnnotations = R.pipe(
        R.propOr([], 'annotations'),
        R.flip(R.props)(annotations),
      )(value);

      const isHidden = R.has(`${parent}.${id}`, hiddenIds) ||
        getIsHidden(valueAnnotations);

      const { start, notes } = isTimeDimension
        ? getTimePeriodAttributes({ reportYearStart, options })(value)
        : { notes: [] };

      let __indexPosition = NaN;
      if (isNaN(__indexPosition)) {
        __indexPosition = getCodeOrder({ annotations: valueAnnotations });
      }
      let res = {
        // names is explicitely skipped because of metadata "standard"
        // not completely aligned with NSI "standard"
        // value is explicitely skipped (non-coded) into id
        ...R.omit(['names', 'value'], value),
        id,
        display: !isHidden,
        start: start ? start.toISOString() : null,
        flags: notes,
        __indexPosition,
        __index,
        isNonCoded: isNonCoded(value),
      };
      if (!isNonCoded(value)) {
        res = R.assoc(
          'name',
          getName({ isTimeDimension, options, locale, start, reportYearStart })(
            value,
          ),
          res,
        );
      }
      if (isTimeDimension) {
        res = refineTimePeriod(res, {
          locale,
          monthlyFormat: options.timeFormat,
        });
      }
      return res;
    }),
    // __indexPosition is used for pre-sorting
    R.sortWith(isTimeDimension ? timeDimensionSorts : defaultSorts),
    // __indexPosition is reset after pre-sorting
    R.addIndex(R.map)((val, index) => R.assoc('__indexPosition', index, val)),
    // values are re-sorted by __index after __indexPosition settlement
    R.sortBy(R.prop('__index')),
  );
};

const getDateForSort = (key) => (val) => dateFns.getTime(new Date(R.prop(key, val)));
const byStartDate = R.ascend(getDateForSort('start'));
const byEndDate = R.descend(getDateForSort('end'));
const timeDimensionSorts = [byStartDate, byEndDate];
const byIndexPosition = R.ascend(R.prop('__indexPosition'));
const byOrder = R.ascend(R.propOr(-1, 'order'));
const defaultSorts = [byIndexPosition, byOrder];

const getTimePeriodAttributes =
  ({ reportYearStart, options }) =>
    (value) => {
      const reported = getReportedTimePeriod(
        reportYearStart,
        value,
        options.frequency,
      );
      const start = reported.start;
      const end = reported.end;
      const reportTimeNote = getReportedTimePeriodNote(
        reportYearStart,
        start,
        end,
        options.frequency,
        options.isRtl,
      );
      const notes = R.isNil(reportTimeNote)
        ? []
        : R.append(reportTimeNote, []);
      return { start, notes };
    };
