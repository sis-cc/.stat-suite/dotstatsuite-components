import * as R from 'ramda';
import dateFns from 'date-fns';
import fr from 'date-fns/locale/fr';
import de from 'date-fns/locale/de';
import en from 'date-fns/locale/en';
import es from 'date-fns/locale/es';
import ar from 'date-fns/locale/ar';
import it from 'date-fns/locale/it';
import nl from 'date-fns/locale/nl';
import th from 'date-fns/locale/th';

const isValidDate = period =>
  R.and(dateFns.isDate, date => R.not(R.equals(date.getTime(date), NaN)))(new Date(period));

export const dateWithoutTZ = date => dateFns.addMinutes(date, date.getTimezoneOffset());

export const getLocale = R.cond([
  [R.equals('fr'), R.always(fr)],
  [R.equals('de'), R.always(de)],
  [R.equals('es'), R.always(es)],
  [R.equals('ar'), R.always(ar)],
  [R.equals('it'), R.always(it)],
  [R.equals('nl'), R.always(nl)],
  [R.equals('th'), R.always(th)],
  [R.T, R.always(en)]
]);

const formaterPeriod = (locale, format) => date => dateFns.format(date, format, { locale: getLocale(locale) });

export const getReportedTimePeriodLabel = (freq, _format, locale) => (reportYearStart, { id, start, reportedStart }) => {
  const year = dateFns.getYear(start);
  const reportStartDate = new Date(year, reportYearStart.month, reportYearStart.day);
  const isReportFirstDate = dateFns.isEqual(reportStartDate, new Date(year, 1, 1));
  if (isReportFirstDate || freq === 'D') {
    return R.isNil(_format) || freq !== 'M'
      ? id : dateFns.format(start, _format, { locale: getLocale(locale) });
  }
  const nextYear = year + 1;
  if (freq === 'A') {
    return `${year}/${nextYear}`;
  }
  if (freq === 'S') {
    const [semester] = R.match(/S\d/, id);
    return `${year}/${nextYear}-${semester}`;
  }
  if (freq === 'Q') {
    const [quarter] = R.match(/Q\d/, id);
    return `${year}/${nextYear}-${quarter}`;
  }
  if (freq === 'M') {
    const isFirstDayOfMonth = dateFns.isFirstDayOfMonth(reportedStart);
    if (isFirstDayOfMonth) {
      return R.isNil(_format) ? id : dateFns.format(reportedStart, _format, { locale: getLocale(locale) });
    }
    const [month] = R.match(/M[\d]{2}/, id);
    return `${year}/${nextYear}-${month}`;
  }
  const [week] = R.match(/W[\d]{2}/, id)
  return `${year}/${nextYear}-${week}`;
};

export const getReportedTimePeriod = (reportYearStart, value, freq) => {
  let start = new Date(R.prop('start', value));
  let end = dateWithoutTZ(new Date(R.prop('end', value)));
  if(reportYearStart.month === '01' && reportYearStart.day === '01') {
    return ({ start, end });
  }
  start = dateFns.addMonths(start, reportYearStart.month - 1);
  start = dateFns.addDays(start, reportYearStart.day - 1);
  if (freq === 'A') {
    end = dateFns.addYears(start, 1);
  }
  if (freq === 'S') {
    end = dateFns.addMonths(start, 6);
  }
  if (freq === 'Q') {
    end = dateFns.addMonths(start, 3);
  }
  if (freq === 'M') {
    end = dateFns.addMonths(start, 6);
  }
  if (freq === 'W' || freq === 'B') {
    end = dateFns.addDays(start, 7);
  }
  if (freq === 'D') {
    end = dateFns.addDays(start, 1);
  }
  if (freq === 'H') {
    end = dateFns.addHours(start, 1);
  }
  if (freq === 'N') {
    end = dateFns.addMinutes(start, 1);
  }
  end = dateFns.subSeconds(end, 1);
  return ({ start, end });
};

export const getReportedTimePeriodNote = (reportYearStart, start, end, freq) => {
  if(reportYearStart.month === '01' && reportYearStart.day === '01') {
    return null;
  } 
  if (R.includes(freq, ['D', 'H', 'N'])) {
    return null;
  }
  const isFirstDayOfMonth = dateFns.isFirstDayOfMonth(start);
  if (freq === 'M' && isFirstDayOfMonth) {
    return null;
  }
  const formatedStart = dateFns.format(start, 'YYYY-MM-DD');
  const formatedEnd = dateFns.format(end, 'YYYY-MM-DD');
  return ({ value: { name: `${formatedStart} - ${formatedEnd}` } });
};


export const getTimePeriodLabel = (locale, format = 'YYYY MMM') => R.pipe(
  R.when(R.isNil, R.always('')),
  R.when(isValidDate, formaterPeriod(locale, format)),
);
