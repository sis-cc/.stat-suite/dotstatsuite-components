export const BAR = 'BarChart';
export const ROW = 'RowChart';
export const SCATTER = 'ScatterChart';
export const TIMELINE = 'TimelineChart';
export const H_SYMBOL = 'HorizontalSymbolChart';
export const V_SYMBOL = 'VerticalSymbolChart';
export const STACKED_BAR = 'StackedBarChart';
export const STACKED_ROW = 'StackedRowChart';
export const CHORO = 'ChoroplethChart';
export const PERCENT = 'percent';
export const VALUES = 'values';
export const TYPES = {
  [BAR]: BAR,
  [ROW]: ROW,
  [SCATTER]: SCATTER,
  [TIMELINE]: TIMELINE,
  [H_SYMBOL]: H_SYMBOL,
  [V_SYMBOL]: V_SYMBOL,
  [STACKED_BAR]: STACKED_BAR,
  [STACKED_ROW]: STACKED_ROW,
  [CHORO]: CHORO,
};
export const FREQ_A = 'annual';
export const FREQ_S = 'semesterly';
export const FREQ_Q = 'quarterly';
export const FREQ_M = 'monthly';
export const DEFAULT_UPR_IDS = {
  U: ['UNIT', 'UNIT_MEASURE', 'UNIT_MEAS', 'UNITS'],
  P: ['UNIT_MULT', 'POWERCODE', 'POWER', 'MAGNITUDE', 'SCALE'],
  R: ['REF_PERIOD', 'REFERENCEPERIOD', 'BASE_PER']
};

export const DEFAULT_REJECTED_UPR_VALUES_IDS = {
  U: [],
  P: ['0'],
  R: []
};

export const DEFAULT_REJECTED_SUBTITLE_IDS = ['_L', '_T', '_Z'];

export const DIMENSION_DISPLAY_PARTS = {
  DESCRIPTION: 'description',
  CODE: 'code',
  BOTH: 'both',
};

export const DEFAULT_MAJOR_ATTRIBUTES = {
  DECIMALS: {
    ID: 'DECIMALS',
    ROLE: 'DECIMALS'
  },
  SCALE: {
    ID: 'SCALE'
  }
};

export const V8_SDMX_JSON = "application/vnd.sdmx.data+json;version=1.0.0-wd";

export const OBS_TYPE_NUMBER = 'OBS_TYPE_NUMBER';
export const OBS_TYPE_NUMERICAL_STRING = 'OBS_TYPE_NUMERICAL_STRING';
export const OBS_TYPE_STRING = 'OBS_TYPE_STRING';
export const REPORTING_YEAR_START_DAY = 'REPORTING_YEAR_START_DAY';
export const REPYEARSTART = 'REPYEARSTART';

