import * as R from 'ramda';
import {
  BAR,
  CHORO,
  H_SYMBOL,
  ROW,
  SCATTER,
  STACKED_BAR,
  STACKED_ROW,
  TIMELINE,
  V_SYMBOL
} from '../constants';

export const getBaseOptions = (type, options) => R.pipe(
  R.propOr({}, 'base'),
  R.cond([
    [
      R.always(R.equals(BAR, type)),
      R.mergeDeepRight({
        minDisplayHeight: 100,
        minDisplayWidth: 300,
        padding: { top: 20 },
        innerPadding: { left: 20, right: 20, bottom: 10 }
      })
    ],
    [
      R.always(R.equals(ROW, type)),
      R.mergeDeepRight({
        minDisplayHeight: 300,
        minDisplayWidth: 100,
        padding: { top: 10, left: 10, right: 20 }
      })
    ],
    [
      R.always(R.equals(SCATTER, type)),
      R.mergeDeepRight({
        minDisplayHeight: 100,
        minDisplayWidth: 100,
        padding: { top: 40, right: 10 }
      })
    ],
    [
      R.always(R.equals(TIMELINE, type)),
      R.mergeDeepRight({
        minDisplayHeight: 300,
        minDisplayWidth: 100,
        isAnnotated: false,
        padding: { top: 20 },
        innerPadding: { left: 40, right: 40 }
      })
    ],
    [
      R.always(R.equals(H_SYMBOL, type)),
      R.mergeDeepRight({
        minDisplayHeight: 300,
        minDisplayWidth: 100,
        padding: { top: 10, left: 10, right: 20 }
      })
    ],
    [
      R.always(R.equals(V_SYMBOL, type)),
      R.mergeDeepRight({
        minDisplayHeight: 100,
        minDisplayWidth: 300,
        padding: { top: 20 },
        innerPadding: { left: 20, right: 20, bottom: 10 }
      })
    ],
    [
      R.always(R.equals(STACKED_BAR, type)),
      R.mergeDeepRight({
        minDisplayHeight: 100,
        minDisplayWidth: 300,
        padding: { top: 20 },
        innerPadding: { left: 20, right: 20, bottom: 10 }
      })
    ],
    [
      R.always(R.equals(STACKED_ROW, type)),
      R.mergeDeepRight({ padding: { top: 10, left: 10, right: 20 } })
    ],
    [R.T, R.identity]
  ])
)(options);
