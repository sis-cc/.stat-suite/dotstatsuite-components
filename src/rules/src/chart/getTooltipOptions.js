import * as R from 'ramda';
import numeral from 'numeral';
import { format } from 'date-fns';
import {
  BAR,
  CHORO,
  H_SYMBOL,
  ROW,
  SCATTER,
  STACKED_BAR,
  STACKED_ROW,
  TIMELINE,
  V_SYMBOL
} from '../constants';
import { getTimeProc } from './getAxisOptions';

export const getTooltipOptions =  (data, type, options, fonts={}, timeFormats={}, locale) => {
  const primaryFonts = (color) => (`
    font-size: ${R.pathOr(12, ['primary', 'fontSize'], fonts)}px;
    font-family: ${R.pathOr('inherit', ['primary', 'fontFamily'], fonts)};
    font-weight: ${R.pathOr('normal', ['primary', 'fontWeight'], fonts)};
    color: ${color};
    padding: 5px;
    border: 1px solid ${color};
    background: white;
    opacity: .8;
  `);

  const secondaryFonts = `
    font-family: ${R.pathOr('inherit', ['secondary', 'fontFamily'], fonts)};
    font-size: ${R.pathOr(12, ['secondary', 'fontSize'], fonts)}px;
    font-weight: ${R.pathOr('normal', ['secondary', 'fontWeight'], fonts)};
  `;

  const valueFormatter = d => numeral(d).format('0,0.[00]');

  const timeFormatter = R.pipe(
    R.prop('frequency'),
    freq => getTimeProc(freq, timeFormats, locale),
    R.when(
      R.anyPass([R.isNil, R.complement(R.is)(Function)]),
      R.always(date => format(date, 'MMM YY'))
    )
  )(data);

  return R.pipe(
    R.pathOr({}, ['serie', 'tooltip']),
    R.cond([
      [
        R.always(R.equals(BAR, type)),
        R.mergeRight({
          layout: (serie, datum, color) => (`
            <div style="${primaryFonts(color)}">
              <div style="text-align: right;">
                ${datum.x}
              </div>
              <div style="text-align: right; ${secondaryFonts}">
                ${datum.formatedValue || valueFormatter(datum.y)}
              </div>
            </div>
          `)
        })
      ],
      [
        R.always(R.equals(ROW, type)),
        R.mergeRight({
          layout: (serie, datum, color) => (`
            <div style="${primaryFonts(color)}">
              <div style="text-align: right;">
                ${datum.y}
              </div>
              <div style="text-align: right; ${secondaryFonts}">
                ${datum.formatedValue || valueFormatter(datum.x)}
              </div>
            <div>
          `)
        })
      ],
      [
        R.always(R.equals(SCATTER, type)),
        R.mergeRight({
          layout: (serie, datum, color) => (`
            <div style="${primaryFonts(color)}">
              <div style="text-align: right;">
                ${R.pipe(R.propOr({}, 'dimensionValues'), R.values, R.pluck('name'), R.join(' '))(datum)}
              </div>
              <div style="text-align: right;">
                ${R.path(['dimensionValues', 'x', 'name'], serie)}
                <span style="${secondaryFonts}">
                  ${datum.xFormat || valueFormatter(datum.x)}
                </span>
              </div>
              <div style="text-align: right;">
                ${R.path(['dimensionValues', 'y', 'name'], serie)}
                <span style="${secondaryFonts}">
                  ${datum.yFormat || valueFormatter(datum.y)}
                </span>
              </div>
            </div>
          `)
        })
      ],
      [
        R.always(R.equals(TIMELINE, type)),
        R.mergeRight({
          layout: (serie, datum, color) => (`
            <div style="${primaryFonts(color)}">
              <div style="text-align: right;">
                ${R.pipe(R.propOr({}, 'dimensionValues'), R.values, R.pluck('name'), R.join('<br />'))(datum)}
              </div>
              <div style="text-align: right;">
                ${datum.timeLabel}
              </div>
              <div style="text-align: right; ${secondaryFonts}">
                ${datum.formatedValue || valueFormatter(datum.y)}
              </div>
            </div>
          `)
        })
      ],
      [
        R.always(R.equals(H_SYMBOL, type)),
        R.mergeRight({
            layout: (serie, datum, color) => (`
              <div style="${primaryFonts(color)}">
                <div style="text-align: right;">
                  ${datum.y}
                  <br />
                  ${datum.symbolValue}
                </div>
                <div style="text-align: right; ${secondaryFonts}">
                  ${datum.formatedValue || valueFormatter(datum.x)}
                </div>
              </div>
            `)
          })
      ],
      [
        R.always(R.equals(V_SYMBOL, type)),
        R.mergeRight({
          layout: (serie, datum, color) => (`
            <div style="${primaryFonts(color)}">
              <div style="text-align: right;">
                ${datum.x}
                <br />
                ${datum.symbolValue}
              </div>
              <div style="text-align: right; ${secondaryFonts}">
                ${datum.formatedValue || valueFormatter(datum.y)}
              </div>
            </div>
          `)
        })
      ],
      [
        R.always(R.equals(STACKED_BAR, type)),
        R.mergeRight({
          layout: (serie, datum, color) => (`
            <div style="${primaryFonts(color)}">
              <div style="text-align: right;">
                ${datum.x}
              </div>
              <div style="text-align: right;">
                ${datum.layerLabel}
              </div>
              <div style="text-align: right; font-size: ${R.pathOr(12, ['secondary', 'fontSize'], fonts)}px;">
                Value:
                <span style="${secondaryFonts}">
                  ${R.is(Function, datum.formater) ? datum.formater(datum.height) : valueFormatter(datum.height)}
                </span>
              </div>
              <div style="text-align: right; font-size: ${R.pathOr(12, ['secondary', 'fontSize'], fonts)}px;">
                Cumulated:
                <span style="${secondaryFonts}">
                  ${R.is(Function, datum.formater) ? datum.formater(datum.y) : valueFormatter(datum.y)}
                </span>
              </div>
            </div>
          `)
        })
      ],
      [
        R.always(R.equals(STACKED_ROW, type)),
        R.mergeRight({
          layout: (serie, datum, color) => (`
            <div style="${primaryFonts(color)}">
              <div style="text-align: right;">
                ${datum.y}
              </div>
              <div style="text-align: right;">
                ${datum.layerLabel}
              </div>
              <div style="text-align: right; font-size: ${R.pathOr(12, ['secondary', 'fontSize'], fonts)}px;">
                Value:
                <span style="${secondaryFonts}">
                  ${R.is(Function, datum.formater) ? datum.formater(datum.width) : valueFormatter(datum.width)}
                </span>
              </div>
              <div style="text-align: right; font-size: ${R.pathOr(12, ['secondary', 'fontSize'], fonts)}px;">
                Cumulated:
                <span style="${secondaryFonts}">
                  ${R.is(Function, datum.formater) ? datum.formater(datum.x) : valueFormatter(datum.x)}
                </span>
              </div>
            </div>
          `)
        })
      ],
      [
        R.always(R.equals(CHORO, type)),
        R.mergeRight({
          layout: (serie, datum, color) => (`
            <div style="${primaryFonts(color)}">
              <div style="text-align: right;">
                ${R.pathOr('', ['properties', 'label'], datum)}
              </div>
              <div style="text-align: right;">
                ${R.pathOr('', ['properties', 'category'], datum)}
              </div>
              <div style="text-align: right; ${secondaryFonts}">
                ${R.pipe(R.pathOr(null, ['properties', 'value']), valueFormatter)(datum)}
              </div>
            </div>
          `)
        })
      ],
      [R.T, R.identity]
    ])
  )(options);
};
