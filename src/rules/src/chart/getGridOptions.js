import * as R from 'ramda';
import {
  BAR,
  CHORO,
  H_SYMBOL,
  ROW,
  STACKED_BAR,
  STACKED_ROW,
  TIMELINE,
  V_SYMBOL
} from '../constants';

export const getGridOptions = (type, options) => {
  const grid = { baselines: [0], color: 'white' };
  const hideGrid = R.assoc('thickness', 0);

  return R.pipe(
    R.propOr({}, 'grid'),
    R.mergeDeepRight({ x: grid, y: grid }),
    R.cond([
      [
        R.always(R.equals(BAR, type)),
        R.mergeDeepRight({ x: { thickness: 0 } })
      ],
      [
        R.always(R.equals(ROW, type)),
        R.mergeDeepRight({ y: { thickness: 0 } })
      ],
      [
        R.always(R.equals(TIMELINE, type)),
        R.mergeDeepRight({ x: { thickness: 0 } })
      ],
      [
        R.always(R.equals(H_SYMBOL, type)),
        R.mergeDeepRight({ y: { thickness: 0 } })
      ],
      [
        R.always(R.equals(V_SYMBOL, type)),
        R.mergeDeepRight({ x: { thickness: 0 } })
      ],
      [
        R.always(R.equals(STACKED_BAR, type)),
        R.mergeDeepRight({ x: { thickness: 0 } })
      ],
      [
        R.always(R.equals(STACKED_ROW, type)),
        R.mergeDeepRight({ y: { thickness: 0 } })
      ],
      [R.T, R.identity]
    ])
  )(options);
};
