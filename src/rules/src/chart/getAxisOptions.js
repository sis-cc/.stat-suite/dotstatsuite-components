import * as R from 'ramda';
import numeral from 'numeral';
import dateFns from 'date-fns';
import isMonday from 'date-fns/is_monday';
import isTuesday from 'date-fns/is_tuesday';
import isWednesday from 'date-fns/is_wednesday';
import isThursday from 'date-fns/is_thursday';
import isFriday from 'date-fns/is_friday';
import isSaturday from 'date-fns/is_saturday';
import isSunday from 'date-fns/is_sunday';
import getISOWeek from 'date-fns/get_iso_week';
import { getLocale, dateWithoutTZ } from '../date';
import {
  BAR,
  CHORO,
  H_SYMBOL,
  ROW,
  SCATTER,
  STACKED_BAR,
  STACKED_ROW,
  TIMELINE,
  V_SYMBOL
} from '../constants';

const defaultTimeFormats = {
  A: 'YYYY',
  S: 'YYYY-[S][SEMESTER]',
  Q: 'YYYY-[Q]Q',
  M: 'YYYY-MMM',
  W: 'YYYY-[W]WW',
  B: 'YYYY-[W]WW',
  D: 'YYYY-MM-DD',
  H: 'YYYY-MM-DD[T]HH:mm:ss',
  N: 'YYYY-MM-DD[T]HH:mm:ss',
};

export const getTimeProc = (frequency, timeFormats = {}, locale) => {
  if (frequency === 'W' || frequency === 'B') {
    return datum => {
      const isoWeek = getISOWeek(datum);
      if (isoWeek !== 1) {
        return dateFns.format(datum, 'YYYY-[W]WW', { locale: getLocale(locale) })
      }
      const seventhDay = dateFns.endOfWeek(datum, { weekStartsOn: 1 });
      return dateFns.format(seventhDay, 'YYYY-[W]WW', { locale: getLocale(locale) })
    }
  }
  const format = R.pipe(
    R.mergeRight(defaultTimeFormats),
    R.ifElse(
      R.has(frequency),
      R.prop(frequency),
      R.prop('A')
    )
  )(timeFormats);
  return datum => dateFns.format(dateWithoutTZ(new Date(datum)), format, { locale: getLocale(locale) });
};

const getTimeFrequency = (data) => {
  const frequency = R.prop('frequency', data);
  if (frequency === 'W' || frequency === 'B') {
    const date = R.pathOr(['series', 0, 'datapoints', 'x']);
    return R.cond([
      [isMonday, R.always('monday')],
      [isTuesday, R.always('tueday')],
      [isWednesday, R.always('wednesday')],
      [isThursday, R.always('thursday')],
      [isFriday, R.always('friday')],
      [isSaturday, R.always('saturday')],
      [isSunday, R.always('sunday')],
      [R.T, R.always('monday')],
    ])(date);
  }
  return R.cond([
    [R.equals('A'), R.always('year')],
    [R.equals('S'), R.always('month')],
    [R.equals('Q'), R.always('month')],
    [R.equals('M'), R.always('month')],
    [R.equals('D'), R.always('day')],
    [R.equals('H'), R.always('hour')],
    [R.equals('N'), R.always('minute')],
    [R.T, R.always('year')],
  ])(frequency);
};

const getTimeStep = R.cond([
  [R.equals('A'), R.always(1)],
  [R.equals('S'), R.always(6)],
  [R.equals('Q'), R.always(3)],
  [R.equals('M'), R.always(1)],
  [R.equals('W'), R.always(1)],
  [R.equals('B'), R.always(1)],
  [R.equals('D'), R.always(1)],
  [R.equals('H'), R.always(1)],
  [R.equals('N'), R.always(1)],
  [R.T, R.always(1)],
]);

export const getAxisOptions = (data, type, options, timeFormats, locale) => {

  const axis = {
    linear: { pivot: { color: 'white' } },
    ordinal: { gap: .3, padding: .3 },
    thickness: 0,
  };

  const linearProc = d => numeral(d).format('0,0.[00]');

  return R.pipe(
    R.propOr({}, 'axis'),
    R.mergeDeepRight({ x: axis, y: axis }),
    R.cond([
      [
        R.always(R.equals(BAR, type)),
        R.pipe(
          R.mergeDeepRight({
            x: { format: { proc: R.identity }, tick: { thickness: 0, size: 0 } },
            y: {
              font: { baseline: 'ideographic' },
              format: { proc: linearProc },
              padding: 10,
              tick: { thickness: 0 },
            }
          }),
          R.over(R.lensPath(['y', 'linear', 'pivot', 'value']), R.when(R.isNil, R.always(0)))
        )
      ],
      [
        R.always(R.equals(ROW, type)),
        R.pipe(
          R.mergeDeepRight({
            x: {
              format: { proc: linearProc },
              orient: 'top',
              padding: 10,
              tick: { thickness: 0 },
            },
            y: {
              format: { proc: R.identity },
              ordinal: { minDisplaySize: 300 },
              tick: { thickness: 0, size: 0 },
            }
          }),
          R.over(R.lensPath(['x', 'linear', 'pivot', 'value']), R.when(R.isNil, R.always(0)))
        )
      ],
      [
        R.always(R.equals(SCATTER, type)),
        R.mergeDeepRight({
          x: {
            format: { proc: linearProc },
            tick: { thickness: 0, size: 0 },
          },
          y: {
            font: { baseline: 'ideographic' },
            format: { proc: linearProc },
            padding: 10,
            tick: { thickness: 0 },
          }
        })
      ],
      [
        R.always(R.equals(TIMELINE, type)),
        R.pipe(
          R.mergeDeepRight({
            x: {
              format: {
                isTime: true,
                pattern: null,
                proc: getTimeProc(R.prop('frequency', data), timeFormats, locale)
              },
              tick: { size: -6, minorSize: -3, thickness: 2, minorThickness: 2, color: 'white' },
            },
            y: {
              font: { baseline: 'ideographic' },
              format: { proc: d => numeral(d).format('0,0.[00]') },
              padding: 10,
              tick: { thickness: 0 },
            }
          }),
          R.evolve({
            x: R.pipe(
              R.set(R.lensPath(['linear', 'frequency']), getTimeFrequency(data)),
              R.set(R.lensPath(['linear', 'step']), getTimeStep(R.prop('frequency', data))),
              R.over(
                R.lensPath(['tick', 'step']),
                R.pipe(R.when(R.isNil, R.always(1)), R.multiply(getTimeStep(R.prop('frequency', data))))
              )
            )
          })
        )
      ],
      [
        R.always(R.equals(H_SYMBOL, type)),
        R.mergeDeepRight({
          x: {
            format: { proc: linearProc },
            orient: 'top',
            padding: 10,
            tick: { thickness: 0 },
          },
          y: {
            format: { proc: R.identity },
            ordinal: { minDisplaySize: 300 },
            tick: { thickness: 0, size: 5 },
          }
        })
      ],
      [
        R.always(R.equals(V_SYMBOL, type)),
        R.mergeDeepRight({
          x: { format: { proc: R.identity }, tick: { thickness: 0, size: 0 } },
          y: {
            font: { baseline: 'ideographic' },
            format: { proc: linearProc },
            padding: 10,
            tick: { thickness: 0 },
          }
        })
      ],
      [
        R.always(R.equals(STACKED_BAR, type)),
        R.pipe(
          R.mergeDeepRight({
            x: { format: { proc: R.identity }, tick: { thickness: 0, size: 0 } },
            y: {
              font: { baseline: 'ideographic' },
              format: { proc: linearProc },
              padding: 10,
              tick: { thickness: 0 },
            }
          }),
          R.over(R.lensPath(['y', 'linear', 'pivot', 'value']), R.when(R.isNil, R.always(0)))
        )
      ],
      [
        R.always(R.equals(STACKED_ROW, type)),
        R.pipe(
          R.mergeDeepRight({
            x: {
              format: { proc: linearProc },
              orient: 'top',
              padding: 10,
              tick: { thickness: 0 },
            },
            y: {
              format: { proc: R.identity },
              ordinal: { minDisplaySize: 300 },
              tick: { thickness: 0, size: 0 }
            }
          }),
          R.over(R.lensPath(['x', 'linear', 'pivot', 'value']), R.when(R.isNil, R.always(0)))
        )
      ],
      [R.T, R.identity]
    ])
  )(options);
};
