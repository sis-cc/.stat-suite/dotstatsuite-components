import * as R from 'ramda'; 
import numeral from 'numeral';
import { getAxisOptions } from './getAxisOptions';
import { getBaseOptions } from './getBaseOptions';
import { getGridOptions } from './getGridOptions';
import { getTooltipOptions } from './getTooltipOptions';
import { SCATTER, STACKED_BAR, STACKED_ROW, TIMELINE } from '../constants';

export const FOCUS_COLORS = {
  highlightColors: ['#E73741', '#0F8FD9', '#993484', '#DF521E', '#719E24', '#E1B400', '#32A674', '#0B68AF'],
  baselineColors: ['#0B1E2D']
};

export const symbolMarkers = () => {
  const paths = {
    circle: size => {
      const r = Math.sqrt(size) / 2;
      return `M0,${r} A${r},${r} 0 1,1 0,${-r} A${r},${r} 0 1,1 0,${r} Z`;
    },
    square: size => {
      const r = Math.sqrt(size) / 2;
      return `M${-r},${-r} L${r},${-r} ${r},${r} ${-r},${r} Z`;
    },
    cross: size => {
      const r = Math.sqrt(size) / 2;
      return `M${-r},${r} L${r},${-r} M${r},${r} L${-r},${-r}`;
    },
    'triangle': size => {
      const rx = Math.sqrt(size) / Math.sqrt(3);
      const ry = Math.sqrt(size) / 2;
      return `M0,${-ry} L${rx},${ry} ${-rx},${ry} Z`;
    }
  }
  return ([
    { style: { stroke: '#39617D', fill: '#39617D' }, path: paths.circle },
    { style: { stroke: '#39617D', fill: '#DBEBF2' }, rotate: 45, path: paths.square },
    { style: { stroke: '#39617D', fill: '#39617D', strokeWidth: 2 }, path: paths.cross },
    { style: { stroke: '#39617D', fill: '#DBEBF2' }, path: paths.square },
    { style: { stroke: '#39617D', fill: '#39617D' }, path: paths.triangle }
  ]);
};

const getAnnotationOptions = (type, options) => R.pipe(
  R.pathOr({}, ['serie', 'annotation']),
  R.mergeRight({
    format: {
      datapoint: { proc: d => numeral(d).format('0,0.[00]') }
    }
  }),
  R.cond([
    [
      R.always(R.equals(SCATTER, type)),
      R.mergeRight({ display: 'never' })
    ],
    [
      R.always(R.equals(TIMELINE, type)),
      R.mergeDeepRight({ format: { datapoint: { pattern: '.2f' } } })
    ],
    [R.T, R.identity]
  ])
)(options);

export const getChartOptions = (data, type, options, tooltipFonts, timeFormats, locale) => {
  const defaultSerieColors = R.ifElse(
    R.anyPass([R.equals(STACKED_BAR), R.equals(STACKED_ROW)]),
    R.always(['#607D8B']),
    R.always(['#8EA4B1'])
  )(type);

  return ({
    axis: getAxisOptions(data, type, options, timeFormats, locale),
    background: R.pipe(
      R.propOr({}, 'background'),
      R.mergeRight({ color: '#DBEBF2' })
    )(options),
    base: getBaseOptions(type, options),
    grid: getGridOptions(type, options),
    map: R.pipe(
      R.propOr({}, 'map'),
      R.mergeRight({ intBoundariesColor: '#8EA4B1' })
    )(options),
    serie: {
      ...R.propOr({}, 'serie', options),
      annotation: getAnnotationOptions(type, options),
      choropleth: R.pipe(
        R.pathOr({}, ['serie', 'choropleth']),
        R.mergeRight({
          divisions: 4,
          domain: 'YlorRd',
          invert: false,
          labelDisplay: 'none',
        })
      )(options),
      stacked: R.pipe(
        R.pathOr({}, ['serie', 'stacked']),
        R.mergeRight({ strokeThickness: 1, strokeColor: '#F8FAFC' })
      )(options),
      symbol: R.pipe(
        R.pathOr({}, ['serie', 'symbol']),
        R.mergeRight({
          markers: symbolMarkers(),
          markerDefaultSize: 60,
          markerDefaultStrokeWidth: 1,
          gapMinMaxColor: '#607D8B',
          gapMinMaxThickness: 1,
          gapAxisMinColor: 'white',
          gapAxisMinThickness: 1
        })
      )(options),
      colors: R.pathOr(defaultSerieColors, ['serie', 'colors'], options),
      overColors: R.pathOr(['#39617D'], ['serie', 'overColors'], options),
      highlightColors: R.pathOr(FOCUS_COLORS.highlightColors, ['serie', 'highlightColors'], options),
      baselineColors: R.pathOr(FOCUS_COLORS.baselineColors, ['serie', 'baselineColors'], options),
      tooltip: getTooltipOptions(data, type, options, tooltipFonts, timeFormats, locale)
    }
  });
};

