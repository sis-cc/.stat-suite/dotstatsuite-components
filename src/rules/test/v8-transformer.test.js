import * as V8 from '../sdmx-data';

describe('rules - sdmx-data', () => {
  describe('setAnnotationsLayout', () => {
    const layout1 = {};
    test('annotations is row', () => {
      expect(V8.setAnnotationsLayout('ID', layout1)([{ type: V8.LAYOUT_ROW }])).toEqual({ rows: ['ID'] });
    });
    test('annotations is header', () => {
      expect(V8.setAnnotationsLayout('ID', layout1)([{ type: V8.LAYOUT_COLUMN }, {type: 'truc'}, {type: 'truc'}])).toEqual({ header: ['ID'] });
    });
    test('annotations is sections', () => {
      expect(V8.setAnnotationsLayout('ID', layout1)([{type: 'truc'}, {type: 'truc'}, { type: V8.LAYOUT_ROW_SECTION }])).toEqual({ sections: ['ID'] });
    });
    test('annotations is undefined - should be return layout', () => {
      expect(V8.setAnnotationsLayout('ID', layout1)([undefined])).toEqual({});
    });
    test('ids should be ["ID1","ID2","ID3"]', () => {
      expect(V8.setAnnotationsLayout('ID1,ID2,ID3', layout1)([{ type: V8.LAYOUT_ROW_SECTION }])).toEqual({ sections: ['ID1','ID2','ID3'] });
    });
    test('id is undefined - should be return layout', () => {
      const layout2 = { header: ['id'] };
      expect(V8.setAnnotationsLayout(undefined, layout2)([{ type: V8.LAYOUT_ROW_SECTION }])).toEqual({ header: ['id'], sections: ['']});
    });
  });
  describe('getIsHidden', () => {
    test('return isHidden', () => {
      const annotations = [
        {"type": "NOT_DISPLAYED", "texts": { "en": "Dimension to be shown in row section - full description" }},
        {"type": "ORDER", "texts": { "en": "5" }},
        {"type": "FULL_NAME","texts": { "en": "Dimension to be shown in row section - full last"}},
      ];
      expect(V8.getIsHidden(annotations)).toEqual({ isHidden: true});
    });
    test('return empty string', () => {
      const annotations = [{"type": "ORDER", "texts": { "en": "5" }}];
      expect(V8.getIsHidden(annotations)).toEqual({});
    });
  });
  describe('setIndexPositions', () => {
    const annotations = [
      {type: 0},
      {type: 1},
      {type: 2},
      {type: 3 },
      {type: "ORDER", text: "5"},
      {type: "ORDER"},
      {type: 6},
      {type: 7},
      {type: 8}
    ];
    test('has no result', () => {
      const dimObservationAnnotations = [0,1];
      expect(V8.setIndexPositions('en')([])).toEqual({});
    }); 
    test('__indexPosition 5', () => {
      const dimObservationAnnotations = [4];
      expect(V8.setIndexPositions('en')([{type: "ORDER", texts: { "en": "5" }}] )).toEqual({ __indexPosition: 5});
    });
    test('__indexPosition null', () => {
      const dimObservationAnnotations = [5];
      expect(V8.setIndexPositions('en')([{type: "ORDER"}])).toEqual({ __indexPosition: null});
    });
  });
  describe('getRelationnalAnnotations', () => {
    const annotations = [{type: 0},{type: 1},{type: 2},{type: 3},{type: 4},{type: 5},{type: 6},{type: 7},{type: 8}];
    const dimAnnotations = [2,3,5,8];

    test('get the true indexes ', () => {
      expect(V8.getRelationnalAnnotations(dimAnnotations)(annotations)).toEqual([{type: 2},{type: 3},{type: 5},{type: 8}]);
    });
    test('annotations is empty', () => {
      expect(V8.getRelationnalAnnotations(dimAnnotations)([])).toEqual([{}, {}, {}, {}]);
    });
    test('dimAnnotations is empty', () => {
      expect(V8.getRelationnalAnnotations([])(annotations)).toEqual([]);
    });
  });
  describe('getPriorityIndexPosition', () => {
    test('__indexPosition isNil expected order value', () => {
      expect(V8.getPriorityIndexPosition(50)({ __indexPosition: undefined })).toEqual({ __indexPosition: 50 });
    });
    test('__indexPosition 5 expected __indexPosition', () => {
      expect(V8.getPriorityIndexPosition(50)({ __indexPosition: 5 })).toEqual({ __indexPosition: 5 });
    });
    test('__indexPosition isNil and order too expected null', () => {
      expect(V8.getPriorityIndexPosition(null)({ __indexPosition: undefined })).toEqual({ __indexPosition: null });
    });
  });
});
