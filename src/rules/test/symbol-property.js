import { expect } from 'chai';
import { BAR, H_SYMBOL } from '../src/constants';
import {
  onChangeSymbolDim,
  symbolStateFromNewProps
} from '../src/properties/symbol';
import oecd_KEI from './oecd-KEI.json';

const undefinedState = {
  symbolDimension: undefined,
};

const undefinedStateWithFocus = {
  baseline: [],
  highlight: [],
  symbolDimension: undefined,
};

describe('symbolStateFromNewProps tests', () => {
  it('undefined with focus from no data', () => {
    const props = { type: H_SYMBOL };
    const state = { symbolDimension: 'SUBJECT' };
    expect(symbolStateFromNewProps(props, state)).to.deep.equal(undefinedStateWithFocus);
  });
  it('undefined without focus from not symbol type', () => {
    const props = { data: oecd_KEI, type: BAR };
    const state = { symbolDimension: 'SUBJECT' };
    expect(symbolStateFromNewProps(props, state)).to.deep.equal(undefinedState);
  });
  it('default from no state', () => {
    const props = { data: oecd_KEI, type: H_SYMBOL};
    const expected = { baseline: [], highlight: [], symbolDimension: 'SUBJECT' };
    expect(symbolStateFromNewProps(props, undefined)).to.deep.equal(expected);
  });
  it('default from undefined state', () => {
    const props = { data: oecd_KEI, type: H_SYMBOL};
    const expected = { baseline: [], highlight: [], symbolDimension: 'SUBJECT' };
    expect(symbolStateFromNewProps(props, undefinedState)).to.deep.equal(expected);
  });
  it('default from default state', () => {
    const props = { data: oecd_KEI, type: H_SYMBOL};
    const state =  { symbolDimension: 'SUBJECT' };
    const expected = { baseline: [], highlight: [], symbolDimension: 'SUBJECT' };
    expect(symbolStateFromNewProps(props, state)).to.deep.equal(expected);
  });
  it('default from no existing dimension', () => {
    const props = { data: oecd_KEI, type: H_SYMBOL};
    const state =  { symbolDimension: 'RANDOM' };
    const expected = { baseline: [], highlight: [], symbolDimension: 'SUBJECT' };
    expect(symbolStateFromNewProps(props, state)).to.deep.equal(expected);
  });
  it('default from dimension with only one value', () => {
    const props = { data: oecd_KEI, type: H_SYMBOL};
    const state =  { symbolDimension: 'MEASURE' };
    const expected = { baseline: [], highlight: [], symbolDimension: 'SUBJECT' };
    expect(symbolStateFromNewProps(props, state)).to.deep.equal(expected);
  });
  it('correct dimension', () => {
    const props = { data: oecd_KEI, type: H_SYMBOL};
    const state =  { symbolDimension: 'LOCATION' };
    const expected = { baseline: [], highlight: [], symbolDimension: 'LOCATION' };
    expect(symbolStateFromNewProps(props, state)).to.deep.equal(expected);
  });
});

describe('focus tests through symbolStateFromNewProps', () => {
  it('empty focus', () => {
    const props = { data: oecd_KEI, type: H_SYMBOL };
    const state = { type: H_SYMBOL, baseline: [], highlight: [] };
    const expected = { baseline: [], highlight: [], symbolDimension: 'SUBJECT' };
    expect(symbolStateFromNewProps(props, state)).to.deep.equal(expected);
  });
  it('correct focus', () => {
    const props = { data: oecd_KEI, type: H_SYMBOL };
    const state = {
      baseline: [
        {
          label: 'France',
          value: 'FRA'
        }
      ],
      highlight: [
        {
          label: 'Australia',
          value: 'AUS'
        },
        {
          label: 'Austria',
          value: 'AUT'
        },
        {
          label: 'Czech Republic',
          value: 'CZE'
        }
      ]
    };
    const expected = { symbolDimension: 'SUBJECT', ...state }
    expect(symbolStateFromNewProps(props, { ...state, type: H_SYMBOL })).to.deep.equal(expected);
  });
  it('all wrong focus', () => {
    const props = { data: oecd_KEI, type: H_SYMBOL };
    const state = {
      type: H_SYMBOL,
      baseline: [
        {
          label: 'RANDOM 1',
          value: 'RANDOM_VAL1'
        }
      ],
      highlight: [
        {
          label: 'RANDOM 2',
          value: 'RANDOM_VAL2'
        },
        {
          label: 'RANDOM 3',
          value: 'RANDOM_VAL3'
        },
        {
          label: 'RANDOM 4',
          value: 'RANDOM_VAL4'
        }
      ]
    };
    const expected = { baseline: [], highlight: [], symbolDimension: 'SUBJECT' };
    expect(symbolStateFromNewProps(props, state)).to.deep.equal(expected);
  });
  it('mix of correct and wrong', () => {
    const props = { data: oecd_KEI, type: H_SYMBOL };
    const state = {
      type: H_SYMBOL,
      baseline: [
        {
          label: 'France',
          value: 'FRA'
        }
      ],
      highlight: [
        {
          label: 'RANDOM 2',
          value: 'RANDOM_VAL2'
        },
        {
          label: 'Austria',
          value: 'AUT'
        },
        {
          label: 'RANDOM 4',
          value: 'RANDOM_VAL4'
        }
      ]
    };
    const expected = {
      baseline: [
        {
          label: 'France',
          value: 'FRA'
        }
      ],
      highlight: [
        {
          label: 'Austria',
          value: 'AUT'
        }
      ],
      symbolDimension: 'SUBJECT'
    };
    expect(symbolStateFromNewProps(props, state)).to.deep.equal(expected);
  });
  it('code display', () => {
    const props = { data: oecd_KEI, type: H_SYMBOL, display: 'code' };
    const state = {
      type: H_SYMBOL,
      baseline: [
        {
          label: 'France',
          value: 'FRA'
        }
      ],
      highlight: [
        {
          label: 'Austria',
          value: 'AUT'
        }
      ]
    };
    const expected = {
      baseline: [
        {
          label: 'FRA',
          value: 'FRA'
        }
      ],
      highlight: [
        {
          label: 'AUT',
          value: 'AUT'
        }
      ],
      symbolDimension: 'SUBJECT'
    };
    expect(symbolStateFromNewProps(props, state)).to.deep.equal(expected);
  });
  it('both display', () => {
    const props = { data: oecd_KEI, type: H_SYMBOL, display: 'both' };
    const state = {
      type: H_SYMBOL,
      baseline: [
        {
          label: 'France',
          value: 'FRA'
        }
      ],
      highlight: [
        {
          label: 'Austria',
          value: 'AUT'
        }
      ]
    };
    const expected = {
      baseline: [
        {
          label: '(FRA) France',
          value: 'FRA'
        }
      ],
      highlight: [
        {
          label: '(AUT) Austria',
          value: 'AUT'
        }
      ],
      symbolDimension: 'SUBJECT'
    };
    expect(symbolStateFromNewProps(props, state)).to.deep.equal(expected);
  });
});

describe('onChangeSymbolDim tests', () => {
  it('no data', () => {
    const props = { type: H_SYMBOL };
    expect(onChangeSymbolDim(props)('LOCATION')).to.deep.equal({});
  });
  it('no selection', () => {
    const props = { data: oecd_KEI, type: H_SYMBOL };
    expect(onChangeSymbolDim(props)(undefined)).to.deep.equal({});
  });
  it('valid dimension within dataset', () => {
    const props = { data: oecd_KEI, type: H_SYMBOL };
    const expected = { baseline: [], highlight: [], symbolDimension: 'LOCATION' };
    expect(onChangeSymbolDim(props)('LOCATION')).to.deep.equal(expected);
  });
  it('non valid dimension', () => {
    const props = { data: oecd_KEI, type: H_SYMBOL };
    expect(onChangeSymbolDim(props)('RANDOM')).to.deep.equal({});
  });
  it('dimension with only one value', () => {
    const props = { data: oecd_KEI, type: H_SYMBOL };
    expect(onChangeSymbolDim(props)('MEASURE')).to.deep.equal({});
  })
});
