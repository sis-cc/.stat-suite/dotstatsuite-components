import * as R from 'ramda';
import { expect } from 'chai';
import { getValuesEnhanced } from '../src/get-values-enhanced';

const values = [
  {
    id: 'A',
    order: 0,
    name: 'Code A',
    names: {
      en: 'Code A',
    },
  },
  {
    id: 'B',
    order: 1,
    name: 'Code B',
    names: {
      en: 'Code B',
    },
  },
  {
    id: 'C',
    order: 2,
    name: 'Code C',
    names: {
      en: 'Code C',
    },
  },
];

const timeValues = [
  {
    start: '2020-01-01T00:00:00',
    end: '2020-12-31T23:59:59',
    id: '2020',
    name: '2020',
    names: {
      en: '2020',
    },
  },
  {
    start: '2021-01-01T00:00:00',
    end: '2021-12-31T23:59:59',
    id: '2021',
    name: '2021',
    names: {
      en: '2021',
    },
  },
  {
    start: '2022-01-01T00:00:00',
    end: '2022-12-31T23:59:59',
    id: '2022',
    name: '2022',
    names: {
      en: '2022',
    },
  },
];

const expectedTimeValues = [
  {
    start: '2020-01-01T00:00:00.000Z',
    end: '2020-12-31T23:59:59.000Z',
    id: '2020',
    name: '2020',
  },
  {
    start: '2021-01-01T00:00:00.000Z',
    end: '2021-12-31T23:59:59.000Z',
    id: '2021',
    name: '2021',
  },
  {
    start: '2022-01-01T00:00:00.000Z',
    end: '2022-12-31T23:59:59.000Z',
    id: '2022',
    name: '2022',
  },
];

const nonCodedValues = [{ value: 'A' }, { value: 'B' }, { value: 'C' }];

const expectedValues = (skippedValueEntries = [], isNonCoded = false) => R.addIndex(R.map)((value, index) => {
  return {
    ...R.omit(R.prepend('names', skippedValueEntries), value),
    __index: index,
    __indexPosition: index,
    display: true,
    notes: [],
    start: R.propOr(null, 'start', value),
    isNonCoded,
  };
});

describe('get values enhanced method', () => {
  it('should pass with coded values', () => {
    const enhancedValues = getValuesEnhanced({
      locale: 'en',
      parent: 'ID',
    })(values);
    expect(enhancedValues).to.deep.equal(expectedValues()(values));
  });

  it('should pass with non-coded values', () => {
    const enhancedValues = getValuesEnhanced({
      locale: 'en',
      parent: 'ID',
    })(nonCodedValues);
    expect(enhancedValues).to.deep.equal(
      expectedValues(['name', 'order', 'value'], true)(values),
    );
  });

  it('should pass with time values', () => {
    const enhancedValues = getValuesEnhanced({
      locale: 'en',
      parent: 'ID',
      isTimeDimension: true,
      reportYearStart: { month: '01', day: '01' },
    })(timeValues);
    expect(enhancedValues).to.deep.equal(
      expectedValues()(expectedTimeValues),
    );
  });
});
