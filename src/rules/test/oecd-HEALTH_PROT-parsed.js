export const dimensions = [
  {
    id: 'VAR',
    name: 'Variable',
    role: 'VAR',
    values: [
      {
        id: 'TPRIBASI',
        name: 'Total public and primary private health insurance'
      }
    ]
  },
  {
    id: 'UNIT',
    name: 'Measure',
    role: 'UNIT',
    values: [
      {
        id: 'COVPOPTX',
        name: '% of total population covered'
      }
    ]
  },
  {
    id: 'COU',
    name: 'Country',
    role: 'COU',
    values: [
      {
          id: 'AUS',
          name: 'Australia'
      },
      {
          id: 'AUT',
          name: 'Austria'
      },
      {
          id: 'BEL',
          name: 'Belgium'
      },
      {
          id: 'CAN',
          name: 'Canada'
      },
      {
          id: 'CZE',
          name: 'Czech Republic'
      },
      {
          id: 'DNK',
          name: 'Denmark'
      },
      {
          id: 'FIN',
          name: 'Finland'
      },
      {
          id: 'FRA',
          name: 'France'
      },
      {
          id: 'DEU',
          name: 'Germany'
      },
      {
          id: 'GRC',
          name: 'Greece'
      },
      {
          id: 'HUN',
          name: 'Hungary'
      },
      {
          id: 'ISL',
          name: 'Iceland'
      },
      {
          id: 'IRL',
          name: 'Ireland'
      },
      {
          id: 'ITA',
          name: 'Italy'
      },
      {
          id: 'JPN',
          name: 'Japan'
      },
      {
          id: 'KOR',
          name: 'Korea'
      },
      {
          id: 'LUX',
          name: 'Luxembourg'
      },
      {
          id: 'MEX',
          name: 'Mexico'
      },
      {
          id: 'NLD',
          name: 'Netherlands'
      },
      {
          id: 'NZL',
          name: 'New Zealand'
      },
      {
          id: 'NOR',
          name: 'Norway'
      },
      {
          id: 'POL',
          name: 'Poland'
      },
      {
          id: 'PRT',
          name: 'Portugal'
      },
      {
          id: 'SVK',
          name: 'Slovak Republic'
      },
      {
          id: 'ESP',
          name: 'Spain'
      },
      {
          id: 'SWE',
          name: 'Sweden'
      },
      {
          id: 'CHE',
          name: 'Switzerland'
      },
      {
          id: 'TUR',
          name: 'Turkey'
      },
      {
          id: 'GBR',
          name: 'United Kingdom'
      },
      {
          id: 'USA',
          name: 'United States'
      },
      {
          id: 'CHL',
          name: 'Chile'
      },
      {
          id: 'EST',
          name: 'Estonia'
      },
      {
          id: 'ISR',
          name: 'Israel'
      },
      {
          id: 'SVN',
          name: 'Slovenia'
      },
      {
          id: 'RUS',
          name: 'Russia'
      },
      {
        id: 'COL',
        name: 'Colombia'
      }
    ]
  },
  {
    id: 'TIME_PERIOD',
    name: 'Year',
    role: 'TIME_PERIOD',
    values: [
      {
        id: '2000',
        name: '2000'
      },
      {
        id: '2001',
        name: '2001'
      },
      {
        id: "2002",
        name: "2002"
      },
      {
        id: "2003",
        name: "2003"
      },
      {
        id: "2004",
        name: "2004"
      },
      {
        id: "2005",
        name: "2005"
      },
      {
        id: "2006",
        name: "2006"
      },
      {
        id: "2007",
        name: "2007"
      },
      {
        id: "2008",
        name: "2008"
      },
      {
        id: "2009",
        name: "2009"
      },
      {
        id: "2010",
        name: "2010"
      },
      {
        id: "2011",
        name: "2011"
      },
      {
        id: '2012',
        name: '2012'
      },
      {
        id: '2013',
        name: '2013'
      },
      {
        id: '2014',
        name: '2014'
      },
      {
        id: '2015',
        name: '2015'
      }
    ]
  }
]

export const scatterDimension = {
    id: 'COU',
    name: 'Country',
    role: 'COU',
    values: [
      {
        id: 'AUS',
        name: 'Australia'
      },
      {
        id: 'AUT',
        name: 'Austria'
      },
      {
        id: 'BEL',
        name: 'Belgium'
      },
      {
        id: 'CAN',
        name: 'Canada'
      },
      {
        id: 'CZE',
        name: 'Czech Republic'
      },
      {
        id: 'DNK',
        name: 'Denmark'
      },
      {
        id: 'FIN',
        name: 'Finland'
      },
      {
        id: 'FRA',
        name: 'France'
      },
      {
        id: 'DEU',
        name: 'Germany'
      },
      {
        id: 'GRC',
        name: 'Greece'
      },
      {
        id: 'HUN',
        name: 'Hungary'
      },
      {
        id: 'ISL',
        name: 'Iceland'
      },
      {
          id: 'IRL',
          name: 'Ireland'
      },
      {
        id: 'ITA',
        name: 'Italy'
      },
      {
        id: 'JPN',
        name: 'Japan'
      },
      {
        id: 'KOR',
        name: 'Korea'
      },
      {
        id: 'LUX',
        name: 'Luxembourg'
      },
      {
          id: 'MEX',
          name: 'Mexico'
      },
      {
        id: 'NLD',
        name: 'Netherlands'
      },
      {
        id: 'NZL',
        name: 'New Zealand'
      },
      {
        id: 'NOR',
        name: 'Norway'
      },
      {
        id: 'POL',
        name: 'Poland'
      },
      {
        id: 'PRT',
        name: 'Portugal'
      },
      {
        id: 'SVK',
        name: 'Slovak Republic'
      },
      {
        id: 'ESP',
        name: 'Spain'
      },
      {
        id: 'SWE',
        name: 'Sweden'
      },
      {
        id: 'CHE',
        name: 'Switzerland'
      },
      {
        id: 'TUR',
        name: 'Turkey'
      },
      {
        id: 'GBR',
        name: 'United Kingdom'
      },
      {
        id: 'USA',
        name: 'United States'
      },
      {
        id: 'CHL',
        name: 'Chile'
      },
      {
        id: 'EST',
        name: 'Estonia'
      },
      {
        id: 'ISR',
        name: 'Israel'
      },
      {
        id: 'SVN',
        name: 'Slovenia'
      },
      {
        id: 'RUS',
        name: 'Russia'
      },
      {
        id: 'COL',
        name: 'Colombia'
      }
    ]
  }