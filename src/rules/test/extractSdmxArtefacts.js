import eql from 'deep-eql';
import { extractSdmxArtefacts } from '../src';
import oecd_HEALTH_PROT from './oecd-HEALTH_PROT.json';
import { dimensions } from './oecd-HEALTH_PROT-parsed';

describe('global tests', () => {
  const expectedEmptyResult = {
    attributes: [],
    datasetAttributes: [],
    dimensions: [],
    observations: {},
    source: null
  }
  it('null', () => {
    eql(extractSdmxArtefacts(null), expectedEmptyResult);
  });
  it('undefined', () => {
    eql(extractSdmxArtefacts(null), expectedEmptyResult);
  })
});

describe('dimensions parsing', () => {
  it('basic test', () => {
    eql(extractSdmxArtefacts(oecd_HEALTH_PROT), dimensions);
  });
});
