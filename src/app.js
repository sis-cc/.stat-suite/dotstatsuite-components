import React, { StrictMode } from 'react';
import { createRoot } from 'react-dom/client';

const container = document.getElementById('root');
const root = createRoot(container);

const App = () => (
  <StrictMode>
    I'm not groot, I'm component
  </StrictMode>
);

root.render(<App />);
