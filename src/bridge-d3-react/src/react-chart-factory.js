import React, { Component } from 'react';
import { isFunction, cloneDeep } from 'lodash';

export default D3Chart => class ReactChart extends Component {
  constructor(props) {
    super(props);
    // shouldComponentUpdate is useful to determine isDirty
    // but can't be used alone since we still want to update meta
    this.needD3Update = true;
    this.chart = null;
  }

  componentDidMount() {
    super.componentDidMount && super.componentDidMount();

    if (!this.needD3Update) return;

    this.chart = new D3Chart(
      this.refs.chart,
      this.props.options,
      this.props.data,
      this.props.getInitialOptions,
      this.props.getAxisOptions
    );

    this.needD3Update = false;

    /*if (isFunction(this.props.getInitialOptions)) {
      this.props.getInitialOptions(cloneDeep(this.chart.options));
    }*/
  }

  componentDidUpdate() {
    super.componentDidUpdate && super.componentDidUpdate();

    if (!this.needD3Update) return;

    this.chart && this.chart.update && this.chart.update(
      this.refs.chart,
      this.props.options,
      this.props.data,
      this.props.getInitialOptions,
      this.props.getAxisOptions
    );

    this.needD3Update = false;
  }

  componentWillUnmount() {
    super.componentWillUnmount && super.componentWillUnmount();
    
    this.chart && this.chart.destroy && this.chart.destroy(this.refs.chart);
  }

  shouldComponentUpdate(nextProps) {
    super.shouldComponentUpdate && super.shouldComponentUpdate(nextProps);

    this.needD3Update = this.props.data !== nextProps.data || this.props.options !== nextProps.options;

    return true;
  }

  render() {
    return <div ref="chart"></div>;
  }
};
