import { createFactory } from 'react';

import factory from './react-chart-factory';
import * as rcwCharts from '@sis-cc/dotstatsuite-d3-charts';

export const BarChart = createFactory(factory(rcwCharts.BarChart));
export const RowChart = createFactory(factory(rcwCharts.RowChart));
export const ScatterChart = createFactory(factory(rcwCharts.ScatterChart));
export const LineChart = createFactory(factory(rcwCharts.LineChart));
export const SymbolChart = createFactory(factory(rcwCharts.SymbolChart));
export const TimelineChart = createFactory(factory(rcwCharts.TimelineChart));
export const VerticalSymbolChart = createFactory(factory(rcwCharts.VerticalSymbolChart));
export const HorizontalSymbolChart = createFactory(factory(rcwCharts.HorizontalSymbolChart));
export const StackedBarChart = createFactory(factory(rcwCharts.StackedBarChart));
export const StackedRowChart = createFactory(factory(rcwCharts.StackedRowChart));
export const ChoroplethChart = createFactory(factory(rcwCharts.ChoroplethChart));
export const ChoroplethLegend = createFactory(factory(rcwCharts.ChoroplethLegend));
