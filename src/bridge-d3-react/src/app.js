import React from 'react';
import { render } from 'react-dom';
import { map, random, range, size } from 'lodash';
import { BarChart, RowChart, ScatterChart, LineChart, TimelineChart, ChoroplethChart, ChoroplethLegend } from './';
import choroData from './mock-choro';

function _render(id, chart) {
  let root = document.createElement('div');
  root.id = id;
  document.getElementById('root').appendChild(root);
  render(chart, root);
}

const datapoints = [
  {label: 'label1', x: -90, y: -4 },
  {label: 'label2', x: 110, y: 10 },
  {label: 'label3', x: 120, y: 14 },
  {label: 'label4', x: 137, y: 22 },
  {label: 'label5', x: 155, y: 36 },
  {label: 'label6', x: 268, y: 37, baselineIndex: 0 },
  {label: 'label7', x: 320, y: 41, highlightIndex: 0},
  {label: 'label8', x: 569, y: 89, highlightIndex: 1}
];

const barData = [{ datapoints }];
const rowData = [{ datapoints: map(datapoints, dp => ({ ...dp, x: dp.y, y: dp.x })) }];
const scatterData = [{ datapoints: map(datapoints, dp => ({ ...dp, x: dp.y })) }];
const lineFuncs = [
  (x) => 10*x+7,
  (x) => x*2+4,
  (x) => x*.3+20,
  (x) => x*4+14,
  (x) => x*.8-4,
  (x) => x**.5+5,
  (x) => x*(x/40),
  (x) => x*(x/40)*(x/40)
]
const lineData = map(range(size(lineFuncs)), (r, i) => ({
  datapoints: map(range(50), (j) => ({ x: 2*j+1, y: lineFuncs[i%size(lineFuncs)](2*j+1) })),
  baselineIndex: i == 0 ? i : -1,
  highlightIndex: i == 2 || i == 7 ? i : -1
}));
const options = {
  base: {
    width: 800,
    height: 400,
    isAnnotated: true
  },
  legend: {
    choropleth: {
      width: 300,
      height: 50
    }
  }
};
const log = options => console.log(options);

_render('root1', <BarChart options={ options } data={ barData } getInitialOptions={ log } />);
_render('root2', <RowChart options={ options } data={ rowData } />);
_render('root3', <ScatterChart options={ options } data={ scatterData } />);
_render('root4', <LineChart options={ options } data={ lineData } />);
_render('root5', <TimelineChart options={ options } data={ lineData } />);
_render('root6', <ChoroplethChart options={ options } data={ choroData } />);
_render('root7', <ChoroplethLegend options={ options } data={ choroData } />);
