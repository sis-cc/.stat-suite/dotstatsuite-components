import meta from '../package.json';
console.info(`${meta.name}@${meta.version}`);

import * as rules from './rules/src';
import * as rules2 from './rules2/src';
import * as bridgeD3React from './bridge-d3-react/src';

export { rules };
export { rules2 };
export { bridgeD3React };

export { default as Viewer } from './viewer/src';